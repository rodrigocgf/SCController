#pragma once
#include "KList.h"
#include "KBaseException.h"

typedef void ( KListClearCallback )( sbyte *, void *obj );

class KNamedList : public KList
{
protected:
	bool Uniqueness;
public:
	KNamedList(void);

public:
	~KNamedList(void);

	void *GetByName( const sbyte *Name, bool *Found = NULL );
	stt_code Add( const sbyte *s, void *obj );
	stt_code Remove( const sbyte *s );
	void Clear( KListClearCallback *c );
	void *GetData( KListNode *Node );

	void SetKeyUniqueness( bool u ) {
		if( Count() > 0 )
			throw KBaseException( "Cannot change uniqueness on a non-empty named list" );
		Uniqueness = u;
	}
};


