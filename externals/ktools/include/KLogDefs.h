#if ( !defined(_KLOGDEFS_H_) || defined(GENERATE_STR_OPTIONS) )

#if (!defined(GENERATE_STR_OPTIONS))
    #define _KLOGDEFS_H_
#endif

#include "KLogDeclarator.h"

#ifdef __BCPLUSPLUS__
#pragma warn -8066
#endif

// ATENCAO: ao editar este enum, lembre-se de editar tambem KLogFilter::LoadConfig()
KLOG_OPTIONS( KLogSource )
    KLOG_SOURCE_ELEMENT( klogSystem,      "System" )
    KLOG_SOURCE_ELEMENT( klogK3L,         "K3L" )
    KLOG_SOURCE_ELEMENT( klogK3LRemote,   "K3LRemote" )
    KLOG_SOURCE_ELEMENT( klogIntfK3L,     "IntfK3L" )
    KLOG_SOURCE_ELEMENT( klogK3LClient,   "K3LClient" )
    KLOG_SOURCE_ELEMENT( klogFirmware,    "Firmware" )
    KLOG_SOURCE_ELEMENT( klogISDN,        "ISDN" )
    KLOG_SOURCE_ELEMENT( klogSIP,         "SIP" )
    KLOG_SOURCE_ELEMENT( klogSS7,         "SS7" )
    KLOG_SOURCE_ELEMENT( klogR2,          "R2" )
    KLOG_SOURCE_ELEMENT( klogTimer,       "Timer" )
    KLOG_SOURCE_ELEMENT( klogWatcher,     "Watcher" )
    KLOG_SOURCE_ELEMENT( klogAudio,       "Audio" )
    KLOG_SOURCE_ELEMENT( klogLogger,      "KLogger" )
    KLOG_SOURCE_ELEMENT( klogWD,          "KWD" )
    KLOG_SOURCE_ELEMENT( klogKD3,         "KD3" )
    KLOG_SOURCE_ELEMENT( klogTdmop,       "TDMoP" )
    KLOG_SOURCE_ELEMENT( klogMedia,       "Media" )
    KLOG_SOURCE_ELEMENT( klogKIBS,        "KIBS" )
    KLOG_SOURCE_ELEMENT( klogConfig,      "Config" )
    KLOG_SOURCE_ELEMENT( klogQuery,       "Query" )
    KLOG_SOURCE_ELEMENT( klogT38,         "T38" )
    KLOG_SOURCE_ELEMENT( klogGateway,     "Gateway" )
    KLOG_SOURCE_ELEMENT( klogMaxSources,  "MaxSources" )
    KLOG_OPTIONS_END( KLogSource )
// ATENCAO: ao editar este enum, lembre-se de editar tambem KLogFilter::LoadConfig()
// sen�o n�o recarrega!

// serve para K3L-C e para a K3L. substitui o wrapper
KLOG_OPTIONS( KLogAppIntfOptions )
    KLOG_OPTION_ELEMENT( klogAppAudioEventOption,   0,  "AudioEvent" )
KLOG_OPTIONS_END( KLogAppIntfOptions )

KLOG_OPTIONS( KLogK3L_Options )
    KLOG_OPTION_ELEMENT( klogCallProgressOption, 0, "CallProgress" )
    KLOG_OPTION_ELEMENT( klogCallAnalyzerOption, 1, "CallAnalyzer" )
    KLOG_OPTION_ELEMENT( klogCadenceRecogOption, 2, "CadenceRecog" )
    KLOG_OPTION_ELEMENT( klogCallControlOption,  3, "CallControl" )  // TODO - remover esta opcao
    KLOG_OPTION_ELEMENT( klogFaxOption,          4, "Fax" )
    KLOG_OPTION_ELEMENT( klogExceptions,         5, "Exceptions" )
KLOG_OPTIONS_END( KLogK3L_Options )

KLOG_OPTIONS( KLogK3LRemoteOptions )
	KLOG_OPTION_ELEMENT( klogK3LR,          0, "K3LR" )
	KLOG_OPTION_ELEMENT( klogK3LRServer,    1, "K3LRServer" )
	KLOG_OPTION_ELEMENT( klogK3LRClient,    2, "K3LRClient" )
KLOG_OPTIONS_END( KLogK3LRemoteOptions )

KLOG_OPTIONS( KLogK3LClientOptions )
    KLOG_OPTION_ELEMENT( klogK3LClientCmd,      0, "Commands" )
    KLOG_OPTION_ELEMENT( klogK3LClientEvt,      1, "Events" )
    KLOG_OPTION_ELEMENT( klogK3LClientComm,     2, "Comm" )
    KLOG_OPTION_ELEMENT( klogK3LClientMsg,      3, "Message" )
    KLOG_OPTION_ELEMENT( klogK3LClientFuncCall, 4, "FuncCall" )
    KLOG_OPTION_ELEMENT( klogK3LClientRTAudio,  5, "RTAudio" )
KLOG_OPTIONS_END( KLogK3LClientOptions )

KLOG_OPTIONS( KLogISDNOptions )
    KLOG_OPTION_ELEMENT( klogISDNLapdOption,      0, "Lapd" )
    KLOG_OPTION_ELEMENT( klogISDNQ931Option,      1, "Q931" )
KLOG_OPTIONS_END( KLogISDNOptions )

KLOG_OPTIONS( KLogR2Options )
    KLOG_OPTION_ELEMENT( klogR2SignalingOption,   0, "Signaling" )
    KLOG_OPTION_ELEMENT( klogR2StateOption,       1, "State" )
KLOG_OPTIONS_END( KLogR2Options )

KLOG_OPTIONS( KLogFirmwareOptions )
    KLOG_OPTION_ELEMENT( klogFwHdlcMsgOption,     0, "FwHdlcMsg" )
    KLOG_OPTION_ELEMENT( klogFwLinkErrors,        1, "FwLinkErrors" )
    KLOG_OPTION_ELEMENT( klogFwModemCharOption,   2, "FwModemChar" )
KLOG_OPTIONS_END( KLogFirmwareOptions )

KLOG_OPTIONS( KLogAudioOptions )
    KLOG_OPTION_ELEMENT( klogDspAudioOption , 0, "DSP" )
    KLOG_OPTION_ELEMENT( klogK3LAudioOption , 1, "K3L" )
KLOG_OPTIONS_END( KLogAudioOptions )

KLOG_OPTIONS( KLogSS7Options )
	KLOG_OPTION_ELEMENT( klogMTP2States,    0, "MTP2States" )
	KLOG_OPTION_ELEMENT( klogMTP2Debug,     1, "MTP2Debug" )
	KLOG_OPTION_ELEMENT( klogMTP3Management,2, "MTP3Management" )
    KLOG_OPTION_ELEMENT( klogMTP3Test,      3, "MTP3Test" )
	KLOG_OPTION_ELEMENT( klogMTP3Debug,     4, "MTP3Debug" )
	KLOG_OPTION_ELEMENT( klogISUPStates,    5, "ISUPStates" )
	KLOG_OPTION_ELEMENT( klogISUPDebug,     6, "ISUPDebug" )
    KLOG_OPTION_ELEMENT( klogISUPMessages,  7, "ISUPMessages" )
    KLOG_OPTION_ELEMENT( klogSS7CallControl,8, "CallControl" )
KLOG_OPTIONS_END( KLogSS7Options )

KLOG_OPTIONS( KLogKD3_Options )
    KLOG_OPTION_ELEMENT( klogKD3_BasicsOption,  0, "Basics" )
    KLOG_OPTION_ELEMENT( klogKD3_IPC_Option,    1, "IPC" )
KLOG_OPTIONS_END( KLogKD3_Options )

KLOG_OPTIONS( KLogTdmopOptions )
    KLOG_OPTION_ELEMENT( klogTdmopComm ,        0, "Communication" )
    KLOG_OPTION_ELEMENT( klogTdmopSession,      1, "AudioSession" )
    KLOG_OPTION_ELEMENT( klogTdmopCommClient,   2, "CommClient" )
    KLOG_OPTION_ELEMENT( klogTdmopDsp,          3, "Dsp" )
    KLOG_OPTION_ELEMENT( klogTdmopFpga,         4, "Fpga" )
    KLOG_OPTION_ELEMENT( klogTdmopMonitor,      5, "Monitor" )
    KLOG_OPTION_ELEMENT( klogTdmopTrunk,        6, "TrunkControl" )
    KLOG_OPTION_ELEMENT( klogTdmopMobile,       7, "MobileControl" )
    KLOG_OPTION_ELEMENT( klogTdmopLine,         8, "AnalogLineControl" )
    KLOG_OPTION_ELEMENT( klogTdmopDebug ,       9, "Debug" )
KLOG_OPTIONS_END( KLogTdmopOptions )

KLOG_OPTIONS( KLogMediaOptions )
    KLOG_OPTION_ELEMENT( klogMediaServerOption,       0, "Server" )
    KLOG_OPTION_ELEMENT( klogMediaClientOption,       1, "Client" )
    KLOG_OPTION_ELEMENT( klogMediaAudioOption,        2, "Audio" )
    KLOG_OPTION_ELEMENT( klogMediaVoipHandlerOption,  3, "VoipServer" )
    KLOG_OPTION_ELEMENT( klogMediaVoipClientOption,   4, "VoipClient" )
    KLOG_OPTION_ELEMENT( klogMediaConfigOption,       5, "Config" )
    KLOG_OPTION_ELEMENT( klogMediaClockOption,        6, "Clock" )
    KLOG_OPTION_ELEMENT( klogMediaThreadsOption,      7, "Threads" )
    KLOG_OPTION_ELEMENT( klogMediaHeapOption,         8, "Heap" )
    KLOG_OPTION_ELEMENT( klogMediaApiOption,          9, "API" )
    KLOG_OPTION_ELEMENT( klogMediaCommOption,         10, "Comm" )
KLOG_OPTIONS_END( KLogMediaOptions )

KLOG_OPTIONS( KLogKibsOptions )
    KLOG_OPTION_ELEMENT( klogKibsGeneral, 0, "General" )
    KLOG_OPTION_ELEMENT( klogKibsBOOTP  , 1, "BOOTP" )
    KLOG_OPTION_ELEMENT( klogKibsTFTP   , 2, "TFTP" )
KLOG_OPTIONS_END( KLogKibsOptions )

KLOG_OPTIONS( KLogGatewayOptions )
    KLOG_OPTION_ELEMENT( klogGatewayRouter, 0, "Router" )
    KLOG_OPTION_ELEMENT( klogGatewayMatch,  1, "Match" )
    KLOG_OPTION_ELEMENT( klogGatewayApi,    2, "API" )
    KLOG_OPTION_ELEMENT( klogGatewayDebug,  3, "Debug" )
KLOG_OPTIONS_END( KLogGatewayOptions )

KLOG_OPTIONS( KLogConfigOptions )
    KLOG_OPTION_ELEMENT( klogOptionalValues, 0, "OptionalValues" )
KLOG_OPTIONS_END( KLogConfigOptions )

#undef OPT_VAL

#endif


