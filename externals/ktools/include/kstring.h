#ifndef KTOOLS_KSTRING
#define KTOOLS_KSTRING

#ifndef __BORLANDC__

//Esta classe nao pode ser compilada no Borland!
#include "KD3/Basics/KString.h"
using ktools::kstring;
#else

#ifndef KMAX_STRING_SIZE
#define KMAX_STRING_SIZE 1024
#endif

#include "system.hpp"
#include "dstring.h"

// vsnprintf
#include <stdio.h>
#include <stdarg.h>
#include <string>

namespace ktools
{
    class kstring
    {
        AnsiString str;
    public:

        operator const AnsiString&() const {
            return str;
        }

        void sprintf( const char *fmt, ... )
        {
            va_list args;
            va_start( args, fmt );
            vFormat( fmt, args );
            va_end( args );
        }

        void Format( const char *fmt, ... )
        {
            va_list args;
            va_start( args, fmt );
            vFormat( fmt, args );
            va_end( args );
        }

        void vFormat( const char *fmt, va_list args )
        {
            if (!fmt)
            {
                str ="";
                return;
            }
            char buffer[ KMAX_STRING_SIZE ];
    #if defined(__GNUC__) || defined(__BORLANDC__)
            if( vsnprintf(buffer, KMAX_STRING_SIZE, fmt, args) >= 0 )
    #else
            if( _vsnprintf_s(buffer, KMAX_STRING_SIZE, fmt, args) >= 0 )
    #endif
                str = buffer;
            else
                str = "STRING > KMAX_STRING_SIZE";
        }

        void AppendFormat(const char *fmt, ... )
        {
            kstring tmp;
            va_list args;
            va_start( args, fmt );
            vAppendFormat( fmt, args );
            va_end( args );
        }

        void vAppendFormat(const char *fmt, va_list args )
        {
            kstring tmp;
            tmp.vFormat( fmt, args );
            str = str + tmp.str;
        }

        void assign(const char *s)
        {
            str = s;
        }

        void append(const char *s)
        {
            str += s;
        }

        kstring &operator=(const char *s)
        {
            str = s;
            return *this;
        }

        kstring::kstring(void){str="";};

        char * c_str() const
        {
            return str.c_str();
        }

        char * substr(int i, int j){return str.SubString(i,j).c_str();}

        void replace(uint32 startPos, uint32 numElements, const AnsiString& replacement)
        {
            return replace(startPos, startPos + numElements, replacement.c_str(), replacement.Length());
        }

        void replace(int startPos, int endPos, const char* replaceStr, int replaceLength)
        {
            int x,y;
            y=0;
           //resize(startPos + replaceLength + (mLength - endPos));
           if ((str.Length() < endPos)||((endPos-startPos) < replaceLength))
           {
                for(x=startPos;x<str.Length();x++)
                {
                    str[x]=replaceStr[y];
                }
                str+=substr(y,replaceLength);
           }
           else
           {
                for(x=startPos;x<endPos;x++)
                {   if(y<replaceLength-1)
                        str[x]=replaceStr[y];
                }
           }
        }

        uint32 find_first_of(const AnsiString& findStr)
        {
            return str.AnsiPos(findStr);
        }

        bool operator==(const char* right) const
        {
            return str == right;
        }

        //void replace(uint32 i, uint32 n, const AnsiString& str2){str.replace(i,n,str2)};

        //kstring( const char *p ) : std::string( p ) {};

        //TODO - colocar explicit neesse construtor, pq se o char* conter % e for chamado este construtor, vai dar pau!
        kstring( const char *fmt, ... )
        {
            va_list args;
            va_start( args, fmt );
            vFormat( fmt, args );
            va_end( args );
        };

        kstring( const std::string &s )
        {
            str = s.c_str();
        }
    };
}

using ktools::kstring;


#endif
/*
#include <string>
#include <stdio.h>
#include <stdarg.h>

#ifndef KMAX_STRING_SIZE
#define KMAX_STRING_SIZE 1024
#endif

class kstring : public std::string
{
public:
    void sprintf( const char *fmt, ... ) {
        va_list args;
        va_start( args, fmt );
        vFormat( fmt, args );
        va_end( args );
    }
    void Format( const char *fmt, ... ) {
        va_list args;
        va_start( args, fmt );
        vFormat( fmt, args );
        va_end( args );
    }
    void vFormat( const char *fmt, va_list args ) 
    {
        if (!fmt) 
        {
            this->assign( "" );
            return;
        }
        char buffer[ KMAX_STRING_SIZE ];
#if defined(__GNUC__) || defined(__BORLANDC__)
        if( vsnprintf(buffer, 512, fmt, args) >= 0 )
#else
        if( _vsnprintf_s(buffer, KMAX_STRING_SIZE, fmt, args) >= 0 )
#endif
            this->assign(buffer);
        else
            this->assign( "STRING > KMAX_STRING_SIZE" );
    }

    void AppendFormat(const char *fmt, ... ) {
        kstring tmp;
        va_list args;
        va_start( args, fmt );
        vAppendFormat( fmt, args );
        va_end( args );
    }

    void vAppendFormat(const char *fmt, va_list args ) {
        kstring tmp;
        tmp.vFormat( fmt, args );
        this->append(tmp) ;
    }

    kstring &operator=(const char *s)
    {
        assign(s);
        return *this;
    }

    kstring() : std::string() {};

    //kstring( const char *p ) : std::string( p ) {};

    //TODO - colocar explicit neesse construtor, pq se o char* conter % e for chamado este construtor, vai dar pau!
    kstring( const char *fmt, ... ) {
        va_list args;
        va_start( args, fmt );
        vFormat( fmt, args );
        va_end( args );
    }
};
           */
#endif

