#ifndef KNETWORKUTILS_H
#define KNETWORKUTILS_H

#include <KTypeDefs.h>
#include <kstring.h>

namespace ktools {

    const unsigned int KMAC_ADDR_LEN = 6;

    kstring MACToString( const byte* MAC, ksize MACSize, char Sep = ':' );
    
    // Timeout is undefined for Windows ARP reqest.
    stt_code WaitForARPResponse( const kstring &UnresolvedIP, 
            const kstring &SourceInterfaceIP, kstring &ResponseMAC );

    stt_code WaitForICMPEcho( const kstring IP, const ktime TimeOutInMs );

}

#endif
