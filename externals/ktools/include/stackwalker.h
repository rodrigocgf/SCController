#ifndef BORLAND
#ifndef _STACKWALKER_HPP
#define _STACKWALKER_HPP

#include "KLogger.h"
#ifndef KWIN32
#include <typeinfo>
#else /*KWIN32*/
//#include "lib_base.hpp"
//using namespace std;
#include <string>
#pragma once

#if (_MSC_VER < 1400)
  typedef unsigned __int64 ULONG_PTR, *PULONG_PTR;
  #include "dbginc\dbghelp.h"
#else
  #include "dbghelp.h"
#endif

#include "StdString.h"

enum BasicType                                              // Stolen from CVCONST.H in the DIA 2.0 SDK
{
    btNoType = 0,
    btVoid = 1,
    btChar = 2,
    btWChar = 3,
    btInt = 6,
    btUInt = 7,
    btFloat = 8,
    btBCD = 9,
    btBool = 10,
    btLong = 13,
    btULong = 14,
    btCurrency = 25,
    btDate = 26,
    btVariant = 27,
    btComplex = 28,
    btBit = 29,
    btBSTR = 30,
    btHresult = 31
};

const char* const rgBaseType[] =
{
    "<user defined>",                                     // btNoType = 0,
    "void",                                               // btVoid = 1,
    "char*",                                              // btChar = 2,
    "wchar_t*",                                           // btWChar = 3,
    "signed char",
    "unsigned char",
    "int",                                                // btInt = 6,
    "unsigned int",                                       // btUInt = 7,
    "float",                                              // btFloat = 8,
    "<BCD>",                                              // btBCD = 9,
    "bool",                                               // btBool = 10,
    "short",
    "unsigned short",
    "long",                                               // btLong = 13,
    "unsigned long",                                      // btULong = 14,
    "__int8",
    "__int16",
    "__int32",
    "__int64",
    "__int128",
    "unsigned __int8",
    "unsigned __int16",
    "unsigned __int32",
    "unsigned __int64",
    "unsigned __int128",
    "<currency>",                                         // btCurrency = 25,
    "<date>",                                             // btDate = 26,
    "VARIANT",                                            // btVariant = 27,
    "<complex>",                                          // btComplex = 28,
    "<bit>",                                              // btBit = 29,
    "BSTR",                                               // btBSTR = 30,
    "HRESULT"                                             // btHresult = 31
};

#ifndef INVALID_FILE_ATTRIBUTES
#define INVALID_FILE_ATTRIBUTES ((DWORD)-1)
#endif

class tStackWalkerInternal;
class tStackWalker
{
public:

    tStackWalker();
    virtual ~tStackWalker();

    typedef BOOL (__stdcall *PReadProcessMemoryRoutine)(
        HANDLE               hProcess,
        unsigned __int64     qwBaseAddress,
        PVOID                lpBuffer,
        DWORD                nSize,
        LPDWORD              lpNumberOfBytesRead,
        LPVOID               pUserData  // optional data, which was passed in "ShowCallstack"
        );

    BOOL LoadModules();

    bool ShowCallstack( KCrashLogger &logger, HANDLE hThread = GetCurrentThread(), CONTEXT *context = NULL );

    enum { STACKWALK_MAX_NAMELEN = 1024 };

protected:

    typedef struct CallstackEntry
    {
        unsigned __int64 offset;
        CHAR name[STACKWALK_MAX_NAMELEN];
        CHAR undName[STACKWALK_MAX_NAMELEN];
        CHAR undFullName[STACKWALK_MAX_NAMELEN];
        unsigned __int64 offsetFromSmybol;
        DWORD offsetFromLine;
        DWORD lineNumber;
        CHAR lineFileName[STACKWALK_MAX_NAMELEN];
        DWORD symType;
        LPCSTR symTypeString;
        CHAR moduleName[STACKWALK_MAX_NAMELEN];
        unsigned __int64 baseOfImage;
        CHAR loadedImageName[STACKWALK_MAX_NAMELEN];
    } CallstackEntry;

    typedef enum CallstackEntryType {firstEntry, nextEntry, lastEntry};

    virtual void OnSymInit(LPCSTR szSearchPath, LPCSTR szUserName);
    virtual void OnLoadModule(LPCSTR img, LPCSTR mod, unsigned __int64 baseAddr, DWORD size, DWORD result, LPCSTR symType, LPCSTR pdbName, ULONGLONG fileVersion);
    virtual void OnCallstackEntry(CallstackEntryType eType, CallstackEntry &entry);

    static tStackWalkerInternal *m_sw;
    HANDLE m_hProcess;
    DWORD m_dwProcessId;
    BOOL m_modulesLoaded;
    LPSTR m_szSymPath;

    static BOOL __stdcall myReadProcMem(HANDLE hProcess, unsigned __int64 qwBaseAddress, PVOID lpBuffer, DWORD nSize, LPDWORD lpNumberOfBytesRead);

    static BOOL CALLBACK EnumerateSymbolsCallback(PSYMBOL_INFO pSymInfo, ULONG SymbolSize, PVOID UserContext);
    static bool FormatSymbolValue(PSYMBOL_INFO pSym, STACKFRAME64* sf, CStdString &Buffer );
    static CStdString DumpTypeIndex( DWORD64 modBase, DWORD dwTypeIndex, uint32 nestingLevel, DWORD_PTR offset, bool & bHandled, char* Name );
    static BasicType tStackWalker::GetBasicType( DWORD typeIndex, DWORD64 modBase );
    static CStdString tStackWalker::FormatOutputValue( BasicType basicType, DWORD64 length, PVOID pAddress );

    friend tStackWalkerInternal;

private:
    std::string m_buffStackLine;
};
#endif /*KWIN32*/

class KRunningLog
{
public:
    //KRunningLog();

    static void Log( KLogLevel level, const sbyte *fmt, ... );
};

#endif  /*_STACKWALKER_HPP*/
#endif /*BORLAND*/
