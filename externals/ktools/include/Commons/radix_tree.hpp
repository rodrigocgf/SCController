/*
    KHOMP generic endpoint/channel library.
    Copyright (C) 2007-2009 Khomp Ind. & Com.

  The contents of this file are subject to the Mozilla Public License Version 1.1
  (the "License"); you may not use this file except in compliance with the
  License. You may obtain a copy of the License at http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  the specific language governing rights and limitations under the License.

  Alternatively, the contents of this file may be used under the terms of the
  "GNU Lesser General Public License 2.1" license (the “LGPL" License), in which
  case the provisions of "LGPL License" are applicable instead of those above.

  If you wish to allow use of your version of this file only under the terms of
  the LGPL License and not to allow others to use your version of this file under
  the MPL, indicate your decision by deleting the provisions above and replace them
  with the notice and other provisions required by the LGPL License. If you do not
  delete the provisions above, a recipient may use your version of this file under
  either the MPL or the LGPL License.

  The LGPL header follows below:

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this library; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef _RADIX_TREE_HPP_
#define _RADIX_TREE_HPP_

#ifndef NULL
# define NULL ((void*)0)
#endif

#include <string>
#include <list>

#ifndef RADIX_TREE_USE_HASH_MAP
#  include <map>
#else
#  ifdef KWIN32
#    include <hash_map>
#  else
#    include <ext/hash_map>
     namespace std { using namespace __gnu_cxx; }
#  endif
#endif

template < typename K, typename V >
struct RadixTree
{
    typedef V Value;
    typedef K   Key;

#ifndef RADIX_TREE_USE_HASH_MAP
    typedef std::map < Key, RadixTree * >          Map;
#else
    typedef std::hash_map < Key, RadixTree * >     Map;
#endif

    typedef std::list < Key >                   KeyList;
    typedef std::pair < KeyList, Value >           Path;
    typedef std::list < Path >                 PathList;

    typedef std::pair < const RadixTree *, KeyList > Choice;

    explicit RadixTree()
    : _value(static_cast< Value * >(NULL)) {};

    explicit RadixTree(const Value & value)
    : _value(new Value(value)) {};

    ~RadixTree()
    {
        for (typename Map::iterator i = _nodes.begin(); i != _nodes.end(); ++i)
        {
            delete i->second;
            i->second = NULL;
        }

        _nodes.clear();

        if (_value)
        {
            delete _value;
            _value = NULL;
        }
    };

    template < typename iterator >
    void insert(iterator cur, iterator end, const Value & value)
    {
        if (cur == end)
            return;

        /* cache current value and advance it */
        const Key & key = *cur++;

        /* get our index */
        typename Map::iterator idx = _nodes.find(key);

        RadixTree * elm = NULL;

        if (idx == _nodes.end())
        {
            elm = (cur == end ? new RadixTree(value) : new RadixTree());

            _nodes[key] = elm;
        }
        else
        {
            elm = idx->second;

            if (cur == end)
                elm->_value = new Value(value);
        }

        if (cur != end)
            elm->insert(cur, end, value);
    }

    template < typename Container >
    void insert(const Container & key, const Value & value)
    {
        insert(key.begin(), key.end(), value);
    }

    template < typename iterator >
    Value * find(iterator cur, iterator end, bool return_last = false) const
    {
        unsigned int level = 0u;
        return internal_find(cur, end, return_last, level, static_cast<Value *>(NULL));
    }

    template < typename iterator >
    Value * find(iterator cur, iterator end, unsigned int & level, bool return_last = false) const
    {
        return internal_find(cur, end, return_last, level, static_cast<Value *>(NULL));
    }

    template < typename Container >
    Value * find(const Container & key, bool return_last = false) const
    {
        return find(key.begin(), key.end(), return_last);
    }

    template < typename Container >
    Value * find(const Container & key, unsigned int & level, bool return_last = false) const
    {
        return find(key.begin(), key.end(), level, return_last);
    }

    template < typename iterator >
    const RadixTree * follow(iterator cur, iterator end) const
    {
        if (cur == end)
            return this;

        typename Map::const_iterator idx = _nodes.find(*cur);

        if (idx == _nodes.end())
            return static_cast<RadixTree *>(NULL);

        return idx->second->follow(++cur, end);
    }

    Choice next_choice() const
    {
        KeyList path;
        return Choice(internal_next_choice(path), path);
    }

    PathList paths() const
    {
        PathList list;
        KeyList  base;

        internal_paths(list, base);

        return list;
    }

    inline bool          empty() const { return _nodes.empty(); }

    inline Value * const value() const { return  _value;        }
    inline bool          final() const { return (_value != 0);  }

  protected:
    template <typename iterator>
    Value * internal_find(iterator iter, iterator end, const bool returnLastValid, unsigned int & level, Value * const lastValue) const
    {
        if (iter == end)
            return lastValue;

        typename Map::const_iterator idx = _nodes.find(*iter);

        if (idx == _nodes.end())
            return (returnLastValid ? lastValue : static_cast< Value * >(NULL));

        ++level;

        return idx->second->internal_find(++iter, end, returnLastValid, level, idx->second->value());
    }

    const RadixTree * internal_next_choice(KeyList & path) const
    {
        if (_nodes.size() == 1 && !_value)
        {
            typename Map::const_iterator iter = _nodes.begin();

            path.push_back(iter->first);

            return iter->second->internal_next_choice(path);
        }

        return this;
    }

    void internal_paths(PathList & pathlist, const KeyList prefix) const
    {
        if (final())
            pathlist.push_back(Path(prefix, *_value));

        for (typename Map::const_iterator i = _nodes.begin(); i != _nodes.end(); ++i)
        {
            KeyList prefixtmp(prefix);

            prefixtmp.push_back(i->first);
            i->second->internal_paths(pathlist, prefixtmp);
        }
    }

 protected:
    Map      _nodes;
    Value *  _value;
};

#endif /* _RADIX_TREE_HPP_ */
