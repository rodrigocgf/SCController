#ifndef CRINGBUFFER_H
#define CRINGBUFFER_H

#include <string.h>

#ifdef CRINGBUFFER_FD_OPS
#  include <limits.h>
#endif

#ifndef RB_POINTER_SIZE
# define RB_POINTER_SIZE 4
#endif

#define RB_PTR_MASK (0x80 << (((RB_POINTER_SIZE) - 1u) << 3u))

#define RB_PTR_POS(val) ((val) & (RB_PTR_MASK - 1u))
#define RB_PTR_LAP(val) ((val) &  RB_PTR_MASK)

#define RB_LAP_ODD  (RB_PTR_MASK)
#define RB_LAP_EVEN (0x0)

#if RB_POINTER_SIZE == 4
# define RB_POINTER_TYPE unsigned int
#else
# if RB_POINTER_SIZE == 2
#  define RB_POINTER_TYPE unsigned short
# else
#  if RB_POINTER_SIZE == 1
#   define RB_POINTER_TYPE unsigned char
#  else
#   error Unknown RB_POINTER_SIZE, valid values are: 4, 2 and 1.
#  endif
# endif
#endif

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef KWIN32
#pragma pack(1)
#endif
struct cringbuffer
{
    RB_POINTER_TYPE count;

    volatile RB_POINTER_TYPE reader;
    volatile RB_POINTER_TYPE writer;

    unsigned char * buffer;
}
#ifdef KWIN32
;
#pragma pack()
#else
__attribute__((packed));
#endif

void cringbuffer_init(struct cringbuffer * const rb, RB_POINTER_TYPE count, unsigned char * buffer);

RB_POINTER_TYPE cringbuffer_free(const struct cringbuffer * const rb);
RB_POINTER_TYPE cringbuffer_used(const struct cringbuffer * const rb);

int cringbuffer_put(struct cringbuffer * const rb, const unsigned char        );
int cringbuffer_get(struct cringbuffer * const rb,       unsigned char * const);

unsigned char cringbuffer_gets(struct cringbuffer * const rb, const unsigned char error_value);

int cringbuffer_provide(struct cringbuffer * const rb, const unsigned char * buffer, RB_POINTER_TYPE amount);
RB_POINTER_TYPE cringbuffer_consume(struct cringbuffer * const rb, unsigned char * const buffer, RB_POINTER_TYPE amount);

//unsigned char * cringbuffer_provider_init(struct cringbuffer * const rb, const unsigned int amount);

//unsigned int cringbuffer_provider_done(struct cringbuffer * const rb, const unsigned int amount);

#ifdef CRINGBUFFER_FD_OPS

#define RB_TRY_AGAIN INT_MIN

int cringbuffer_provide_fd_get(struct cringbuffer * const rb, int fd, RB_POINTER_TYPE amount);
int cringbuffer_consume_fd_put(struct cringbuffer * const rb, int fd, RB_POINTER_TYPE amount);

#endif

#ifdef __cplusplus
}
#endif

#endif /* CRINGBUFFER_H */
