/*
    KHOMP generic endpoint/channel library.
    Copyright (C) 2007-2009 Khomp Ind. & Com.

  The contents of this file are subject to the Mozilla Public License Version 1.1
  (the "License"); you may not use this file except in compliance with the
  License. You may obtain a copy of the License at http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  the specific language governing rights and limitations under the License.

  Alternatively, the contents of this file may be used under the terms of the
  "GNU Lesser General Public License 2.1" license (the “LGPL" License), in which
  case the provisions of "LGPL License" are applicable instead of those above.

  If you wish to allow use of your version of this file only under the terms of
  the LGPL License and not to allow others to use your version of this file under
  the MPL, indicate your decision by deleting the provisions above and replace them
  with the notice and other provisions required by the LGPL License. If you do not
  delete the provisions above, a recipient may use your version of this file under
  either the MPL or the LGPL License.

  The LGPL header follows below:

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this library; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

/* WARNING: This is a generic ringbuffer abstraction, which works for single-sized elements,
            partial elements, single/multi-elements read/writes.

            It works only for single-reader + single-writer, since it does not depends on
            external mutex functions.

      NOTE: For single element provide/consume, this abstraction has standard C++ semantics.
            For multiple element provide/consume, memcpy is used - thus complex C++ objects
            which need correct copy constructor semantics should not copied this way.
 */

#ifndef _RINGBUFFER_HPP_
#define _RINGBUFFER_HPP_

#define RINGBUFFER_EXTERNAL_OPERATIONS 1 // enabled by default

#include <string.h>

#include <cmath>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <algorithm>

#include "noncopyable.hpp"

#ifdef RINGBUFFER_ATOMIC_OPERATIONS
# include "atomic.hpp"            // for Overwritable*
#endif

#ifdef RINGBUFFER_DYNAMIC_BROADCAST
# include <KD3/Basics/KMutex.h>   // for OverwritableBroadcastReader
#endif

#ifdef KWIN32
#pragma warning( disable: 4355 )  //Tira o warning por usar this no construtor
#pragma warning( disable: 4521 )  //Warning de ter vários construtores de copia
#pragma warning( disable: 4522 )  //Warning de ter vários operator=
#endif

#ifndef KWIN32
# define ALWAYS_INLINE __attribute__((always_inline))
#else
# define ALWAYS_INLINE /* good luck */
#endif

struct RingbufferSide
{
    enum Type
    {
        READER,
        WRITER,
    };
};

/****************************** BASE POINTERS *******************************/

#ifdef KWIN32
#pragma pack(1)
#endif
struct BufferPointer
{
    BufferPointer(unsigned int _pos, unsigned short _lap)
    {
        ptr.info.pos = _pos;
        ptr.info.lap = _lap;
    }

    BufferPointer(const          BufferPointer & o) { ptr.data = o.ptr.data; }
    BufferPointer(const volatile BufferPointer & o) { ptr.data = o.ptr.data; }

    bool operator==(const BufferPointer & o) ALWAYS_INLINE { return (ptr.data == o.ptr.data); }

    inline void operator=(const volatile BufferPointer & o) volatile ALWAYS_INLINE
    {
        ptr.data = o.ptr.data;
    }

    inline void operator=(const volatile BufferPointer & o) ALWAYS_INLINE
    {
        ptr.data = o.ptr.data;
    }

    inline void operator=(const BufferPointer & o) volatile ALWAYS_INLINE
    {
        ptr.data = o.ptr.data;
    }

    inline void operator=(const BufferPointer & o) ALWAYS_INLINE
    {
        ptr.data = o.ptr.data;
    }

    union
    {
        struct
        {
            unsigned  pos:31;
            unsigned  lap:1;
        }
        info;

        unsigned long data;
    }
    ptr;
}
#ifdef KWIN32
;
#pragma pack()
#else
__attribute__((packed));
#endif

struct BufferPointerManager
{
    BufferPointerManager(const unsigned int _blockSize, const unsigned int _blockCount)
    : blockSize(_blockSize), blockCount(_blockCount) {};

    inline unsigned int free_blocks(const BufferPointer & r, const BufferPointer & w) const ALWAYS_INLINE
    {
        return (r.ptr.info.lap == w.ptr.info.lap) ? blockCount - (w.ptr.data - r.ptr.data) /* optimization */ : r.ptr.info.pos - w.ptr.info.pos;
    }

    inline unsigned int used_blocks(const BufferPointer & r, const BufferPointer & w) const ALWAYS_INLINE
    {
        return (r.ptr.info.lap == w.ptr.info.lap) ? w.ptr.data - r.ptr.data /* optimization */ : blockCount - (r.ptr.info.pos - w.ptr.info.pos);
    }

    inline bool writable(const BufferPointer & reader, const BufferPointer & writer) const ALWAYS_INLINE { return (free_blocks(reader, writer) > 0); }
    inline bool readable(const BufferPointer & reader, const BufferPointer & writer) const ALWAYS_INLINE { return (used_blocks(reader, writer) > 0); }

    inline const BufferPointer & jump(BufferPointer & p, const unsigned int amount) const ALWAYS_INLINE
    {
        const unsigned int res = p.ptr.info.pos + amount;

        if (res >= blockCount) { p.ptr.info.pos = (res - blockCount); p.ptr.info.lap = p.ptr.info.lap ? 0 : 1; }
        else                   { p.ptr.info.pos = res; }

        return p;
    }

    inline const BufferPointer & next(BufferPointer & ptr) const ALWAYS_INLINE { return jump(ptr, 1); }

    const unsigned int blockSize;
    const unsigned int blockCount;
};

#ifdef RINGBUFFER_ATOMIC_OPERATIONS
struct OverwritableBufferPointerManager
: public BufferPointerManager
{
    OverwritableBufferPointerManager(const unsigned int _blockSize, const unsigned int _blockCount)
    : BufferPointerManager(_blockSize, _blockCount) {};

    inline bool update(volatile BufferPointer & res, BufferPointer & oldvalue, BufferPointer & newvalue)
    {
        return Atomic::doCAS(&res, &oldvalue, newvalue);
    }
};
#endif /* RINGBUFFER_ATOMIC_OPERATIONS */

/* ** TODO: UNIMPLEMENTED! RETHINK INTERFACE FOR RB 2.0 **
struct BufferOffsetablePointer
{
    BufferOffsetablePointer(unsigned int _pos, unsigned short _off, unsigned short _lap): pos(_pos), off(_off), lap(_lap) {};

    BufferOffsetablePointer(const          BufferOffsetablePointer & o): pos(o.pos), off(o.off), lap(o.lap) {};
    BufferOffsetablePointer(const volatile BufferOffsetablePointer & o): pos(o.pos), off(o.off), lap(o.lap) {};

    bool operator==(const BufferOffsetablePointer & o) { return (pos == o.pos && off == o.off && lap == o.lap); }

    void operator=(const volatile BufferOffsetablePointer & o)
    {
        pos = o.pos;
        off = o.off;
        lap = o.lap;
    }

    void operator=(const BufferOffsetablePointer & o) volatile
    {
        pos = o.pos;
        off = o.off;
        lap = o.lap;
    }

    unsigned int   pos:19;
    unsigned short off:11;
    bool           lap:1;
}
__attribute__((packed));
*/

/**************************** BASE MANAGER STUFF *****************************/

struct MemoryOperations
{
    static inline void put(      char * const buffer, const unsigned int size, const unsigned int pos, const char * const src, const unsigned int bytes);
    static inline void get(const char * const buffer, const unsigned int size, const unsigned int pos,       char * const dst, const unsigned int bytes);
};

struct StreamOperations
{
    static inline bool         put(const char * const buffer, const unsigned int size, const unsigned int pos, std::ostream & src, const unsigned int bytes);
    static inline unsigned int get(      char * const buffer, const unsigned int size, const unsigned int pos, std::istream & dst, const unsigned int bytes);
};

/********************************************** GENERIC MANAGERS ***************************************************/

struct GenericReaderTraits
{
    GenericReaderTraits(BufferPointerManager & _manager): manager(_manager) {}

    struct BufferEmpty: public std::runtime_error
    {
        BufferEmpty(): std::runtime_error("BufferEmpty") {};
    };

  protected:
    unsigned int traits_consume       (volatile BufferPointer & reader, const volatile BufferPointer & writer, const char *, char *, unsigned int, bool);
    unsigned int traits_consume_begins(volatile BufferPointer & reader, const volatile BufferPointer & writer, const char *, char *, unsigned int, bool);
    bool         traits_consume_commit(volatile BufferPointer & reader, const volatile BufferPointer & writer, unsigned int);
    unsigned int traits_put           (volatile BufferPointer & reader, const volatile BufferPointer & writer, const char *, std::ostream &, unsigned int);

  protected:
    BufferPointerManager & manager;
};

struct GenericWriterTraits
{
    GenericWriterTraits(BufferPointerManager & _manager): manager(_manager) {}

    struct BufferFull: public std::runtime_error
    {
        BufferFull(): std::runtime_error("BufferFull") {};
    };

  protected:
    bool         traits_provide        (const volatile BufferPointer & reader, volatile BufferPointer & writer, char *,   const char *, unsigned int);
    unsigned int traits_get            (const volatile BufferPointer & reader, volatile BufferPointer & writer, char *, std::istream &, unsigned int);

    unsigned int traits_provider_append(const volatile BufferPointer & reader, volatile BufferPointer & writer, char *,   const char *, unsigned int, unsigned int);

  protected:
    BufferPointerManager & manager;
};

#ifdef RINGBUFFER_EXTERNAL_OPERATIONS

template < typename T >
struct ExtWriterData
{
    const T *    data;
    unsigned int size;
};

template < typename T >
struct ExternalImplWriterTable
{
    // put on buffer
    bool put(const ExtWriterData< T > * const, const unsigned int);
};

template < typename T >
struct ExternalImplWriterTraits
{
    typedef ExternalImplWriterTable< T > ObjType;

    typedef bool (ObjType::* PutType)(const ExtWriterData< T > * const, const unsigned int);
};

template < typename T >
struct ExternalImplWriter
{
    typedef typename ExternalImplWriterTraits< T >::ObjType ObjType;
    typedef typename ExternalImplWriterTraits< T >::PutType PutType;

    ExternalImplWriter()
    : _obj(reinterpret_cast< ObjType * >(0u))
    {};

    template < typename Functor >
    void setWriter(Functor & f)
    {
        _obj = reinterpret_cast< ObjType * >(&f);
        _put = reinterpret_cast< PutType   >(&Functor::put);
    }

    bool put(const T * const a0, unsigned int s0, const T * const a1, unsigned int s1)
    {
        if (reinterpret_cast<void *>(_obj) == 0)
            return false;

        const ExtWriterData< T > data[2] = { { a0, s0 }, { a1, s1 } };

        return ((_obj)->*(_put))(reinterpret_cast< const ExtWriterData< T > * const >(data), 2u);
    }

    bool put(const T * const a0, unsigned int s0)
    {
        if (reinterpret_cast<void *>(_obj) == 0)
            return false;

        const ExtWriterData< T > data[1] = { { a0, s0 } };

        return ((_obj)->*(_put))(reinterpret_cast< const ExtWriterData< T > * const >(data), 1u);
    }

  protected:
    ObjType * _obj;
    PutType   _put;
};

template < typename ManagerType >
struct ExternalWriter: public ExternalImplWriter< typename ManagerType::Type >, public GenericWriterTraits
{
    typedef typename ManagerType::Type   Type;
    typedef typename ManagerType::Reader Reader;

    const ExternalWriter & self() { return *this; }

    const volatile BufferPointer & pointer(void) const   { return _pointer;    };

    void pointer(const volatile BufferPointer & pointer) { _pointer = pointer; };

  protected:
    ExternalWriter(Type * const buffer, const Reader & reader, BufferPointerManager & _manager)
    : GenericWriterTraits(_manager), /* manager(_manager), */ _buffer(buffer), _pointer(0, false), _reader(reader)
    {};

    Type * const _buffer;

    volatile BufferPointer _pointer;

    const Reader & _reader;

  public:
    inline bool provide(unsigned int amount)
    {
        BufferPointer index(_pointer);
        BufferPointer other(_reader.pointer());

        if (amount > manager.free_blocks(other, index))
            return false;

        /* should we go around the buffer for writing? */
        if ((index.ptr.info.pos + amount) > manager.blockCount)
        {
            const unsigned int wr1 = manager.blockCount - index.ptr.info.pos;
            const unsigned int wr2 = amount - wr1;

            if (!put(&(_buffer[index.ptr.info.pos]), wr1, _buffer, wr2))
                return false;
        }
        else
        {
            /* we are talking about buffers here, man! */
            if (!put(&(_buffer[index.ptr.info.pos]), amount))
                return false;
        }

        _pointer = manager.jump(index, amount);
        return true;
    }

    inline bool provide(const Type * value, unsigned int amount)
    {
        return traits_provide(_reader.pointer(), _pointer, (char *)_buffer, (const char *) value, amount);
    }

    inline unsigned int provider_append(const Type * value, unsigned int amount, unsigned int offset = 0u)
    {
        return traits_provider_append(_reader.pointer(), _pointer, (char *)_buffer, (const char *) value, amount, offset);
    }

    inline bool provider_commit(unsigned int amount)
    {
        BufferPointer index(_pointer);
        BufferPointer other(_reader.pointer());

        if (manager.free_blocks(other, index) < amount)
            return false;

        _pointer = manager.jump(index, amount);
        return true;
    }
};

template < typename T >
struct ExtReaderData
{
    T *          data;
    unsigned int size;
};
    
template < typename T >
struct ExternalImplReaderTable
{
    // get from buffer
    unsigned int get(const ExtReaderData< T > * const, unsigned int);
};

template < typename T >
struct ExternalImplReaderTraits
{
    typedef ExternalImplReaderTable< T > ObjType;

    typedef unsigned int (ObjType::* GetType)(const ExtReaderData< T > * const, unsigned int);
};

template < typename T >
struct ExternalImplReader
{
    typedef typename ExternalImplReaderTraits< T >::ObjType ObjType;
    typedef typename ExternalImplReaderTraits< T >::GetType GetType;

    ExternalImplReader()
    : _obj(reinterpret_cast< ObjType * >(0u))
    {}

    template < typename Functor >
    void setReader(Functor & f)
    {
        _obj = reinterpret_cast< ObjType * >(&f);
        _get = reinterpret_cast< GetType   >(&Functor::get);
    }

    unsigned int get(T * const a0, unsigned int s0, T * const a1, unsigned int s1)
    {
        if (reinterpret_cast<void *>(_obj) == 0)
            return 0u;

        const ExtReaderData< T > data[2] = { { a0, s0 }, { a1, s1 } };

        return ((_obj)->*(_get))(reinterpret_cast< const ExtReaderData< T > * >(data), 2u);
    }

    unsigned int get(T * const a0, unsigned int s0)
    {
        if (reinterpret_cast<void *>(_obj) == 0)
            return 0u;

        const ExtReaderData< T > data[1] = { { a0, s0 } };

        return ((_obj)->*(_get))(reinterpret_cast< const ExtReaderData< T > * >(data), 1u);
    }

  protected:
    ObjType * _obj;
    GetType   _get;
};

template < typename ManagerType >
struct ExternalReader: public ExternalImplReader< typename ManagerType::Type >, public GenericReaderTraits
{
    typedef typename ManagerType::Type   Type;
    typedef typename ManagerType::Writer Writer;

    const ExternalReader & self() { return *this; }

    const volatile BufferPointer & pointer(void) const   { return _pointer;    };

    void pointer(const volatile BufferPointer & pointer) { _pointer = pointer; };

  protected:
    ExternalReader(Type * const buffer, const Writer & writer, BufferPointerManager & _manager)
    : GenericReaderTraits(_manager), /* manager(_manager), */ _buffer(buffer), _pointer(0, false), _writer(writer) {};

    Type * const _buffer;

    volatile BufferPointer _pointer;

    const Writer & _writer;

  public:
    unsigned int consume(const unsigned int amount, bool atomic_mode = false)
    {
        BufferPointer index(_pointer);
        BufferPointer other(_writer.pointer());

        const unsigned int available = manager.used_blocks(index, other);

        if ((atomic_mode && amount > available) || !available)
            return 0;

        unsigned int total = (std::min)(amount, available);

        /* should we go around the buffer for reading? */
        if ((index.ptr.info.pos + total) > manager.blockCount)
        {
            unsigned int rd1 = manager.blockCount - index.ptr.info.pos;
            unsigned int rd2 = total - rd1;

            /* two partial consumes (one at the end, another at the beginning) */
            total = get(&(_buffer[index.ptr.info.pos]), rd1, _buffer, rd2);
        }
        else
        {
            /* we are talking about buffers here, man! */
            total = get(&(_buffer[index.ptr.info.pos]), total);
        }

        _pointer = manager.jump(index, total);

        return total;
    };

    inline unsigned int consume(Type * value, unsigned int amount, bool atomic_mode = false)
    {
        return traits_consume(_pointer, _writer.pointer(), (const char *)_buffer, (char *) value, amount, atomic_mode);
    }

    /* returns the number of items that have been read (atomic_mode == true means 'all or nothing') */
    inline unsigned int consumer_start(Type * value, unsigned int amount, bool atomic_mode = false)
    {
        return traits_consume_begins(_pointer, _writer.pointer(), (const char *)_buffer, (char *) value, amount, atomic_mode);
    }

    /* returns true if we could commit that much of buffer (use only after consume_begins).    *
     * note: you may commit less bytes that have been read to keep some data inside the buffer */
    inline bool consumer_commit(unsigned int amount)
    {
        return traits_consume_commit(_pointer, _writer.pointer(), amount);
    }
};

#endif /* RINGBUFFER_EXTERNAL_OPERATIONS */


template < typename ManagerType >
struct GenericReader: protected GenericReaderTraits
{
    typedef typename ManagerType::Type   Type;
    typedef typename ManagerType::Writer Writer;

    const GenericReader & self() { return *this; }

    const volatile BufferPointer & pointer(void) const   { return _pointer;    };

    void pointer(const volatile BufferPointer & pointer) { _pointer = pointer; };

  protected:
    GenericReader(Type * const buffer, const Writer & writer, BufferPointerManager & _manager)
    : GenericReaderTraits(_manager),
      _buffer(buffer), _pointer(0, false), _writer(writer) {};

    Type * const _buffer;

    volatile BufferPointer _pointer;

    const Writer & _writer;

  public:
    /***** ONE-PHASE SINGLE/MULTI-ELEMENT BUFFER FUNCTIONS *****/

    bool consume(Type & value)
    {
        BufferPointer index(_pointer);
        BufferPointer other(_writer.pointer());

        if (!manager.readable(index, other))
            return false;

        value = _buffer[index.ptr.info.pos];
        _pointer = manager.next(index);

        return true;
    }

    /* returns the number of items that have been read (atomic_mode == true means 'all or nothing') */
    inline unsigned int consume(Type * value, unsigned int amount, bool atomic_mode = false)
    {
        return traits_consume(_pointer, _writer.pointer(), (const char *)_buffer, (char *) value, amount, atomic_mode);
    }

    /***** TWO-PHASE SINGLE/MULTI-ELEMENT BUFFER FUNCTIONS *****/

    const Type & consumer_start(void) const
    {
        BufferPointer index(_pointer);
        BufferPointer other(_writer.pointer());

        if (!manager.readable(index, other))
            throw BufferEmpty();

        return _buffer[index.ptr.info.pos];
    }

    void consumer_commit(void)
    {
        BufferPointer index(_pointer);
        _pointer = manager.next(index);
    }

    /* returns the number of items that have been read (atomic_mode == true means 'all or nothing') */
    inline unsigned int consumer_start(Type * value, unsigned int amount, bool atomic_mode = false)
    {
        return traits_consume_begins(_pointer, _writer.pointer(), (const char *)_buffer, (char *) value, amount, atomic_mode);
    }

    /* returns true if we could commit that much of buffer (use only after consume_begins).    *
     * note: you may commit less bytes that have been read to keep some data inside the buffer */
    inline bool consumer_commit(unsigned int amount)
    {
        return traits_consume_commit(_pointer, _writer.pointer(), amount);
    }

    /***** IO FUNCTIONS *****/

    /* returns the number of items written to from buffer to stream */
    inline unsigned int put(std::ostream &fd, unsigned int amount)
    {
        return traits_put(_pointer, _writer.pointer(), (const char *)_buffer, fd, amount);
    }
};

template < typename ManagerType >
struct GenericWriter: public GenericWriterTraits
{
    typedef typename ManagerType::Type   Type;
    typedef typename ManagerType::Reader Reader;

    const GenericWriter & self() { return *this; }

    const volatile BufferPointer & pointer(void) const   { return _pointer;    };

    void pointer(const volatile BufferPointer & pointer) { _pointer = pointer; };

  protected:
    GenericWriter(Type * const buffer, const Reader & reader, BufferPointerManager & _manager)
    : GenericWriterTraits(_manager),
      _buffer(buffer), _pointer(0, false), _reader(reader) {};

    Type * const _buffer;

    volatile BufferPointer _pointer;

    const Reader & _reader;

  public:
    /***** ONE-PHASE SINGLE/MULTI-ELEMENT BUFFER FUNCTIONS *****/

    bool provide(const Type & value)
    {
              BufferPointer index(_pointer);
        const BufferPointer other(_reader.pointer());

        if (!manager.writable(other, index))
            return false;

        _buffer[index.ptr.info.pos] = value;
        _pointer = manager.next(index);

        return true;
    }

    inline bool provide(const Type * value, unsigned int amount)
    {
        return traits_provide(_reader.pointer(), _pointer, (char *)_buffer, (const char *) value, amount);
    }

    inline unsigned int provider_append(const Type * value, unsigned int amount, unsigned int offset = 0u)
    {
        return traits_provider_append(_reader.pointer(), _pointer, (char *)_buffer, (const char *) value, amount, offset);
    }

    inline bool provider_commit(unsigned int amount)
    {
              BufferPointer  index(_pointer);
        const BufferPointer  other(_reader.pointer());

        if (manager.free_blocks(other, index) < amount)
            return false;

        _pointer = manager.jump(index, amount);
        return true;
    }

    /***** TWO-PHASE SINGLE-ELEMENT BUFFER FUNCTIONS *****/

    Type & provider_start(void)
    {
              BufferPointer index(_pointer);
        const BufferPointer other(_reader.pointer());

        if (!manager.writable(other, index))
            throw BufferFull();

        return _buffer[index.ptr.info.pos];
    }

    void provider_commit(void)
    {
        BufferPointer index(_pointer);
        _pointer = manager.next(index);
    }

    /***** IO FUNCTIONS *****/

    /* returns number of items read from stream to buffer */
    inline unsigned int get(std::istream &fd, unsigned int amount)
    {
        return traits_get(_reader.pointer(), _pointer, (char *)_buffer, fd, amount);
    }
};

template < template < typename M > class R, template < typename M > class W, typename T >
struct GenericManagerWrapper
{
    typedef T Type;

    typedef R< GenericManagerWrapper< R, W, T > > Reader;
    typedef W< GenericManagerWrapper< R, W, T > > Writer;
};

template < typename T, template < typename M > class R, template < typename M > class W >
struct GenericManager
: protected BufferPointerManager,
  public GenericManagerWrapper< R, W, T >,
  public R< GenericManagerWrapper< R, W, T > >,
  public W< GenericManagerWrapper< R, W, T > >
{
  protected:
    using BufferPointerManager::used_blocks;
    using BufferPointerManager::free_blocks;

  public:
    typedef typename GenericManagerWrapper< R, W, T >::Type Type;

    typedef typename GenericManagerWrapper< R, W, T >::Reader Reader;
    typedef typename GenericManagerWrapper< R, W, T >::Writer Writer;

    GenericManager(Type * const buffer, const unsigned int count)
    : BufferPointerManager(sizeof(T), count),
      Reader(buffer, Writer::self(), static_cast< BufferPointerManager &>(*this)),
      Writer(buffer, Reader::self(), static_cast< BufferPointerManager &>(*this))
    {}

    unsigned int used_blocks() const
    {
        const BufferPointer r(Reader::pointer());
        const BufferPointer w(Writer::pointer());

        return used_blocks(r, w);
    }

    unsigned int free_blocks() const
    {
        const BufferPointer r(Reader::pointer());
        const BufferPointer w(Writer::pointer());

        return free_blocks(r, w);
    }

    void clear()
    {
        Reader::pointer(Writer::pointer());
    };
};

/********************************************** OVERWRITABLE MANAGERS ***************************************************/

#ifdef RINGBUFFER_ATOMIC_OPERATIONS

struct OverwritableReaderTraits
{
    OverwritableReaderTraits(OverwritableBufferPointerManager & _manager): manager(_manager) {}; 

    struct BufferEmpty: public std::runtime_error
    {
        BufferEmpty(): std::runtime_error("BufferEmpty") {};
    };

    unsigned int traits_consume       (volatile BufferPointer & reader, const volatile BufferPointer & writer, const char *, char *, unsigned int, bool);
    unsigned int traits_consume_begins(volatile BufferPointer & reader, const volatile BufferPointer & writer, const char *, char *, unsigned int, bool);
    bool         traits_consume_commit(volatile BufferPointer & reader, const volatile BufferPointer & writer, unsigned int);
    unsigned int traits_put           (volatile BufferPointer & reader, const volatile BufferPointer & writer, const char *, std::ostream &, unsigned int);

  protected:
    OverwritableBufferPointerManager & manager;
};

struct OverwritableWriterTraits
{
    OverwritableWriterTraits(OverwritableBufferPointerManager & _manager): manager(_manager) {}; 

    struct BufferFull: public std::runtime_error
    {
        BufferFull(): std::runtime_error("BufferFull") {};
    };

    bool         traits_forward_reader(volatile BufferPointer & reader, BufferPointer & rdindex, BufferPointer & wrindex, unsigned int, bool, bool dry_run = false);

    bool         traits_provide(volatile BufferPointer & reader, volatile BufferPointer & writer, char *,   const char *, unsigned int, bool);
    unsigned int traits_get    (volatile BufferPointer & reader, volatile BufferPointer & writer, char *, std::istream &, unsigned int, bool);

  protected:
    OverwritableBufferPointerManager & manager;
};

struct OverwritableBroadcastWriterTraits: public OverwritableWriterTraits
{
    typedef std::vector< volatile BufferPointer * > PointerVector;

    OverwritableBroadcastWriterTraits(OverwritableBufferPointerManager & _manager): OverwritableWriterTraits(_manager) {};

    using OverwritableWriterTraits::traits_forward_reader;

    bool         traits_forward_reader(const PointerVector & reader_vec, BufferPointer & wrindex, unsigned int, bool, bool dry_run = false);

    bool         traits_provide(const PointerVector & reader_vec, volatile BufferPointer & writer, char *,   const char *, unsigned int, bool);
    unsigned int traits_get    (const PointerVector & reader_vec, volatile BufferPointer & writer, char *, std::istream &, unsigned int, bool);
};

template < typename BufferType >
struct OverwritableReader: protected OverwritableReaderTraits
{
    typedef typename BufferType::Type   Type;
    typedef typename BufferType::Writer Writer;

    OverwritableReader & self() { return *this; }

    volatile BufferPointer & pointer(void)               { return _pointer;    };

    void pointer(const volatile BufferPointer & pointer) { _pointer = pointer; };

    OverwritableReader(Type * const buffer, const Writer & writer, OverwritableBufferPointerManager & _manager)
    : OverwritableReaderTraits(_manager),
      _buffer(buffer), _pointer(0, false), _writer(writer) {};

  protected:
    Type * const _buffer;

    volatile BufferPointer _pointer;

    const Writer & _writer;

  public:
    /***** ONE-PHASE SINGLE/MULTI-ELEMENT BUFFER FUNCTIONS *****/

    bool consume(Type & value)
    {
              BufferPointer index(_pointer);
        const BufferPointer other(_writer.pointer());

        if (!manager.readable(index, other))
            return false;

        value = _buffer[index.ptr.info.pos];

        for (;;)
        {
            BufferPointer newindex(index);

            manager.next(newindex);

            if (manager.update(_pointer, index, newindex))
                break;
        }

        return true;
    }

    /* returns the number of items that have been read (atomic_mode == true means 'all or nothing') */
    inline unsigned int consume(Type * value, unsigned int amount, bool atomic_mode = false)
    {
        return traits_consume(_pointer, _writer.pointer(), (const char *)_buffer, (char *) value, amount, atomic_mode);
    }

    /***** TWO-PHASE SINGLE/MULTI-ELEMENT BUFFER FUNCTIONS *****/

    const Type & consumer_start(void)
    {
        BufferPointer index(_pointer); 
        BufferPointer other(_writer.pointer());

        if (!manager.readable(index, other))
            throw BufferEmpty();

        return _buffer[index.ptr.info.pos];
    }

    void consumer_commit(void)
    {
        BufferPointer index(_pointer);

        for (;;)
        {
            BufferPointer newindex(index);

            manager.next(newindex);

            if (manager.update(_pointer, index, newindex))
                break;

        }
    }

    /* returns the number of items that have been read (atomic_mode == true means 'all or nothing') */
    inline unsigned int consumer_start(Type * value, unsigned int amount, bool atomic_mode = false)
    {
        return traits_consume_begins(_pointer, _writer.pointer(), (const char *)_buffer, (char *) value, amount, atomic_mode);
    }

    /* returns true if we could commit that much of buffer (use only after consume_begins).    *
     * note: you may commit less bytes that have been read to keep some data inside the buffer */
    inline bool consumer_commit(unsigned int amount)
    {
        return traits_consume_commit(_pointer, _writer.pointer(), amount);
    }

    /***** IO FUNCTIONS *****/

    /* returns the number of items written to from buffer to stream */
    inline unsigned int put(std::ostream &fd, unsigned int amount)
    {
        return traits_put(_pointer, _writer.pointer(), (const char *)_buffer, fd, amount);
    }
};

template < typename BufferType >
struct OverwritableWriter: public OverwritableWriterTraits
{
    typedef typename BufferType::Type   Type;
    typedef typename BufferType::Reader Reader;

    OverwritableWriter & self() { return *this; }

    const volatile BufferPointer & pointer(void) const   { return _pointer;    };

    void pointer(const volatile BufferPointer & pointer) { _pointer = pointer; };

  protected:
    OverwritableWriter(Type * const buffer, Reader & reader, OverwritableBufferPointerManager & _manager)
    : OverwritableWriterTraits(_manager),
      _buffer(buffer), _pointer(0, false), _reader(reader) {};

    Type * const _buffer;

    volatile BufferPointer _pointer;

    Reader & _reader;

    bool forward_reader(volatile BufferPointer & reader_ptr, BufferPointer & rdindex, BufferPointer wrindex, bool overwrite)
    {
        for (;;)
        {
            if (manager.writable(rdindex, wrindex))
                break;

            if (!overwrite)
                return false;

            BufferPointer newrdindex(rdindex);

            manager.next(newrdindex);

            if (manager.update(_reader.pointer(), rdindex, newrdindex))
                break;
        }

        return true;
    }

  public:
    /***** ONE-PHASE SINGLE/MULTI-ELEMENT BUFFER FUNCTIONS *****/

    bool provide(const Type & value, bool overwrite = true)
    {
        BufferPointer wrindex(_pointer);
        BufferPointer rdindex(_reader.pointer());

        if (!forward_reader(_reader.pointer(), rdindex, wrindex, overwrite))
            return false;

        _buffer[wrindex.ptr.info.pos] = value;
        _pointer = manager.next(wrindex);

        return true;
    }

    /* writes everything or nothing */
    inline bool provide(const Type * value, unsigned int amount, bool overwrite = false)
    {
        return traits_provide(_reader.pointer(), _pointer, (char *)_buffer, (const char *) value, amount, overwrite);
    }

    /***** TWO-PHASE SINGLE-ELEMENT BUFFER FUNCTIONS *****/

    Type & provider_start(bool overwrite = true)
    {
        BufferPointer wrindex(_pointer);
        BufferPointer rdindex(_reader.pointer());

        // NOTE: advance here and not on commit because otherwise we will be messing with
        //       the next element to be readen (until we commit), which is not very kind.

        if (!forward_reader(_reader.pointer(), rdindex, wrindex, overwrite))
            throw BufferFull();

        return _buffer[wrindex.ptr.info.pos];
    }

    void provider_commit(void)
    {
        BufferPointer index(_pointer);
        _pointer = manager.next(index);
    }

    /***** IO FUNCTIONS *****/

    /* returns number of items read from stream to buffer */
    inline unsigned int get(std::istream &fd, unsigned int amount, bool overwrite = false)
    {
        return traits_get(_reader.pointer(), _pointer, (char *)_buffer, fd, amount, overwrite);
    }
};

template < typename BufferType >
struct OverwritableBroadcastReader
{
    typedef typename BufferType::Type   Type;
    typedef typename BufferType::Writer Writer;

    typedef OverwritableReader< BufferType >   ReaderType;
    typedef std::vector< ReaderType * >      ReaderVector;

    OverwritableBroadcastReader & self() { return *this; }

#ifdef RINGBUFFER_DYNAMIC_BROADCAST
    OverwritableBroadcastWriterTraits::PointerVector pointers(void)
#else
    const OverwritableBroadcastWriterTraits::PointerVector & pointers(void) const
#endif
    {
#ifdef RINGBUFFER_DYNAMIC_BROADCAST
        _mutex.Lock();
        OverwritableBroadcastWriterTraits::PointerVector tmp(_pointers);
        _mutex.Unlock();
        return tmp;
#else
        return _pointers;
#endif
    }

    void setReaderCount(const unsigned int count)
    {
        if (_readers.size()) // can set only once
            return;

        _readers.reserve(count);

        for (unsigned int i = 0; i < count; ++i)
        {
            ReaderType * const reader = new ReaderType(_buffer, _writer, _manager);

            _readers.push_back(reader);
            _pointers.push_back(&(reader->pointer()));
        }
    }

#ifdef RINGBUFFER_DYNAMIC_BROADCAST
    unsigned int createReader()
    {
        unsigned int i = 0u;

        ReaderType * const reader = new ReaderType(_buffer, _writer, _manager);

        _mutex.Lock();

        for (; i < _readers.size(); ++i)
        {
            if (!_readers[i]) break;
        }

        if (i == _readers.size())
            _readers.push_back(reader);
        else
            _readers[i] = reader;

        _pointers.push_back(&(reader->pointer()));

        _mutex.Unlock();

        return i;
    }

    void deleteReader(const unsigned int pos)
    {
        _mutex.Lock();

        ReaderType * ptr = _readers[pos];
        _readers[pos] = NULL;

        for (OverwritableBroadcastWriterTraits::PointerVector::iterator i = _pointers.begin(); i != _pointers.end(); ++i)
        {
            if ((*i) == &(ptr->pointer()))
            {
                _pointers.erase(i);
                break;
            }
        }

        _mutex.Unlock();

        delete ptr;
    }
#endif

#ifdef RINGBUFFER_DYNAMIC_BROADCAST
    ReaderType & reader(unsigned int i)
#else
    ReaderType & reader(unsigned int i) const
#endif
    {
#ifdef RINGBUFFER_DYNAMIC_BROADCAST
        _mutex.Lock();
        ReaderType * const reader = _readers.at(i);
        _mutex.Unlock();

        return *reader;
#else
        return *(_readers.at(i));
#endif
    }

  protected:
    OverwritableBroadcastReader(Type * const buffer, Writer & writer, OverwritableBufferPointerManager & manager)
    : _buffer(buffer), _writer(writer), _manager(manager) {}

    Type * const _buffer;

    Writer                           & _writer;
    OverwritableBufferPointerManager & _manager;

    OverwritableBroadcastWriterTraits::PointerVector _pointers;

#ifdef RINGBUFFER_DYNAMIC_BROADCAST
    ktools::KThreadMutex _mutex;
#endif

    ReaderVector _readers;
};

template < typename BufferType >
struct OverwritableBroadcastWriter: public OverwritableBroadcastWriterTraits
{
    typedef typename BufferType::Type   Type;
    typedef typename BufferType::Reader Reader;

    OverwritableBroadcastWriter & self() { return *this; }

    const volatile BufferPointer & pointer(void) const   { return _pointer;    };

    void pointer(const volatile BufferPointer & pointer) { _pointer = pointer; };

  protected:
    OverwritableBroadcastWriter(Type * const buffer, Reader & reader, OverwritableBufferPointerManager & _manager)
    : OverwritableBroadcastWriterTraits(_manager),
      _buffer(buffer), _pointer(0, false), _reader(reader) {};

    Type * const _buffer;

    volatile BufferPointer _pointer;

    Reader & _reader;

    bool forward_reader(const OverwritableBroadcastWriterTraits::PointerVector & reader_vec, BufferPointer wrindex, bool overwrite, bool dry_run)
    {
        for (OverwritableBroadcastWriterTraits::PointerVector::const_iterator i = reader_vec.begin(); i != reader_vec.end(); ++i)
        {
            volatile BufferPointer & reader_ptr = *(*i);

            BufferPointer rdindex(reader_ptr);

            for (;;)
            {
                if (manager.writable(rdindex, wrindex))
                    break;

                if (!overwrite)
                    return false;

                if (dry_run)
                    break;

                BufferPointer newrdindex(rdindex);

                manager.next(newrdindex);

                if (manager.update(reader_ptr, rdindex, newrdindex))
                    break;
            }
        }

        return true;
    }

  public:
    /***** ONE-PHASE SINGLE/MULTI-ELEMENT BUFFER FUNCTIONS *****/

    bool provide(const Type & value, bool overwrite = true)
    {
        BufferPointer wrindex(_pointer);

        if (!forward_reader(_reader.pointers(), wrindex, overwrite, true))
            return false;

        forward_reader(_reader.pointers(), wrindex, overwrite, false);

        _buffer[wrindex.ptr.info.pos] = value;
        _pointer = manager.next(wrindex);

        return true;
    }

    /* writes everything or nothing */
    inline bool provide(const Type * value, unsigned int amount, bool overwrite = false)
    {
        return traits_provide(_reader.pointers(), _pointer, (char *)_buffer, (const char *) value, amount, overwrite);
    }

    /***** TWO-PHASE SINGLE-ELEMENT BUFFER FUNCTIONS *****/

    Type & provider_start(bool overwrite = true)
    {
        BufferPointer wrindex(_pointer);

        // NOTE: advance here and not on commit because otherwise we will be messing with
        //       the next element to be readen (until we commit), which is not very kind.

        if (!forward_reader(_reader.pointers(), wrindex, overwrite, true))
            throw BufferFull();
            return false;

        forward_reader(_reader.pointer(), wrindex, overwrite, false);

        return _buffer[wrindex.ptr.info.pos];
    }

    void provider_commit(void)
    {
        BufferPointer index(_pointer);
        _pointer = manager.next(index);
    }

    /***** IO FUNCTIONS *****/

    /* returns number of items read from stream to buffer */
    inline unsigned int get(std::istream &fd, unsigned int amount, bool overwrite = false)
    {
        return traits_get(_reader.pointers(), _pointer, (char *)_buffer, fd, amount, overwrite);
    }
};

template < template < typename M > class R, template < typename M > class W, typename T >
struct OverwritableManagerWrapper
{
    typedef T Type;

    typedef R< OverwritableManagerWrapper< R, W, T > > Reader;
    typedef W< OverwritableManagerWrapper< R, W, T > > Writer;
};

template < typename T, template < typename M > class R, template < typename M > class W >
struct OverwritableManager
: protected OverwritableBufferPointerManager,
  public OverwritableManagerWrapper< R, W, T >,
  public R< OverwritableManagerWrapper< R, W, T > >,
  public W< OverwritableManagerWrapper< R, W, T > >
{
  protected:
    using OverwritableBufferPointerManager::used_blocks;
    using OverwritableBufferPointerManager::free_blocks;

  public:
    typedef typename OverwritableManagerWrapper< R, W, T >::Type Type;

    typedef typename OverwritableManagerWrapper< R, W, T >::Reader Reader;
    typedef typename OverwritableManagerWrapper< R, W, T >::Writer Writer;

    OverwritableManager(Type * const buffer, const unsigned int count)
    : OverwritableBufferPointerManager(sizeof(T), count),
      Reader(buffer, Writer::self(), static_cast< OverwritableBufferPointerManager &>(*this)),
      Writer(buffer, Reader::self(), static_cast< OverwritableBufferPointerManager &>(*this))
    {}

    unsigned int used_blocks() const
    {
        const BufferPointer r(Reader::pointer());
        const BufferPointer w(Writer::pointer());

        return used_blocks(r, w);
    }

    unsigned int free_blocks() const
    {
        const BufferPointer r(Reader::pointer());
        const BufferPointer w(Writer::pointer());

        return free_blocks(r, w);
    }

    void clear()
    {
        for (;;)
        {
            BufferPointer rd(Reader::pointer());
            BufferPointer wr(Writer::pointer());

            // set reader as being equal to writer, using CAS
            if (update(Reader::pointer(), rd, wr))
                break;
        }
    }
};

template < template < typename M > class R, template < typename M > class W, typename T >
struct OverwritableBroadcastManagerWrapper
{
    typedef T Type;

    typedef R< OverwritableBroadcastManagerWrapper< R, W, T > > Reader;
    typedef W< OverwritableBroadcastManagerWrapper< R, W, T > > Writer;
};

template < typename T, template < typename M > class R, template < typename M > class W >
struct OverwritableBroadcastManager
: protected OverwritableBufferPointerManager,
  public OverwritableBroadcastManagerWrapper< R, W, T >,
  public R< OverwritableBroadcastManagerWrapper< R, W, T > >,
  public W< OverwritableBroadcastManagerWrapper< R, W, T > >
{
  protected:
    using OverwritableBufferPointerManager::used_blocks;
    using OverwritableBufferPointerManager::free_blocks;

  public:
    typedef typename OverwritableBroadcastManagerWrapper< R, W, T >::Type Type;

    typedef typename OverwritableBroadcastManagerWrapper< R, W, T >::Reader Reader;
    typedef typename OverwritableBroadcastManagerWrapper< R, W, T >::Writer Writer;

    OverwritableBroadcastManager(Type * const buffer, const unsigned int count)
    : OverwritableBufferPointerManager(sizeof(T), count),
      Reader(buffer, Writer::self(), static_cast< OverwritableBufferPointerManager &>(*this)),
      Writer(buffer, Reader::self(), static_cast< OverwritableBufferPointerManager &>(*this))
    {}

    unsigned int used_blocks() const
    {
        OverwritableBroadcastWriterTraits::PointerVector rv(Reader::pointers());

        unsigned int res = 0u;

        for (OverwritableBroadcastWriterTraits::PointerVector::const_iterator i = rv.begin(); i != rv.end(); ++i)
        {
            const BufferPointer r(*i);
            const BufferPointer w(Writer::pointer());

            res = std::max< unsigned int >(used_blocks(r, w), res);
        }

        return res;
    }

    unsigned int free_blocks() const
    {
        OverwritableBroadcastWriterTraits::PointerVector rv(Reader::pointers());

        unsigned int res = blockCount;

        for (OverwritableBroadcastWriterTraits::PointerVector::const_iterator i = rv.begin(); i != rv.end(); ++i)
        {
            const BufferPointer r(*i);
            const BufferPointer w(Writer::pointer());

            res = std::min< unsigned int >(free_blocks(r, w), res);
        }

        return res;
    }

    void clear()
    {
        OverwritableBroadcastWriterTraits::PointerVector rv(Reader::pointers());

        for (OverwritableBroadcastWriterTraits::PointerVector::const_iterator i = rv.begin(); i != rv.end(); ++i)
        {
            for (;;)
            {
                volatile BufferPointer & reader_ptr = *i;

                BufferPointer rd(reader_ptr);
                BufferPointer wr(Writer::pointer());

                // set reader as being equal to writer, using CAS
                if (update(reader_ptr, rd, wr))
                    break;
            }
        }
    }
};

#endif

/********************************************** OFFSETABLE MANAGERS ***************************************************/

// ** TODO: UNIMPLEMENTED! RETHINK INTERFACE FOR RB 2.0 **
//    bool         traits_provide_partial(      char *, const char *, unsigned int);
//    unsigned int traits_consume_partial(const char *,       char *, unsigned int);

/*********************************************** RINGBUFFER COMMON STUFF *********************************************************/

template < typename T >
struct RingbufferResources
{
    RingbufferResources(unsigned int count)
    : _buffer(new T[count]), _malloc(true) {};

    RingbufferResources(T * const buffer)
    : _buffer(buffer), _malloc(false) {};

    ~RingbufferResources()
    {
        if (_malloc)
          delete[] _buffer;
    };

    T * const buffer() { return _buffer; }

  protected:
    T * const  _buffer;
    const bool _malloc;
};

template < typename T, template < typename T, template< class MR > class RR, template< class MW > class WW > class M, template < typename T > class R, template < typename T > class W >
struct RingbufferWrapper: public NonCopyable
{
    typedef T            Type;
    typedef M< T, R, W > Manager;
    typedef R< Manager > Reader;
    typedef W< Manager > Writer;
};

template < typename T, template < typename T, template< class MR > class RR, template< class MW > class WW > class M = GenericManager, template < typename M > class R = GenericReader, template < typename M > class W = GenericWriter >
struct Ringbuffer
: protected RingbufferResources < T >,
  public RingbufferWrapper< T, M, R, W >,
  public RingbufferWrapper< T, M, R, W >::Manager
{
    typedef typename RingbufferWrapper< T, M, R, W >::Type    Type;
    typedef typename RingbufferWrapper< T, M, R, W >::Manager Manager;
    typedef typename RingbufferWrapper< T, M, R, W >::Reader  Reader;
    typedef typename RingbufferWrapper< T, M, R, W >::Writer  Writer;

    typedef RingbufferResources< T > Resources;

    Ringbuffer(unsigned int _count)
    : RingbufferResources< T >(_count),
      Manager(Resources::buffer(), _count)
    {}

    Ringbuffer(unsigned int _count, T * const _buffer)
    : RingbufferResources< T >(_buffer),
      Manager(Resources::buffer(), _count)
    {}

    ~Ringbuffer()
    {}
};

typedef GenericReaderTraits::BufferEmpty KBufferEmptyException;
typedef GenericWriterTraits::BufferFull  KBufferFullException;

#endif /* _RINGBUFFER_HPP_ */
