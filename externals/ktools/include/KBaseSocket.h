#pragma once

#include "KSocketDefs.h"
#include <KD3/Basics/KSocketPoll.h>

class KBaseSocket
{
    KSocketsInitializer Initializer;
protected:
	int32 TimeOut;
    bool Msg_NO_Wait;

	KSocketError Error;

//	int autoresumeselect( int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds, struct timeval *timeout );

    void InitVars() 
	{
    	Handle = KINVALID_SOCKET;
	    TimeOut = 10000;
        Msg_NO_Wait = false;
    }

public:
	KBaseSocket();
	KBaseSocket( KSOCKET S ){
        InitVars();
        Handle = S;
    };
public:
	virtual ~KBaseSocket();

	KSOCKET Handle;
#ifdef KWIN32
    volatile SOCKET SignalFd[2];
#else
    volatile int SignalFd[2];
#endif

	void SetAsync();

	bool IsConnected(); 
	int32 Write( void *Buf, int32 Size );
	int32 Read( void *Buf, int32 MaxSize );
	bool WaitForData( int32 TimeOut );

    void OpenBase();
    void CloseBase();

    void Close()
    {
        CloseBase();
    }

    void Signal();

	void SetTimeOut( int32 to ) {
		TimeOut = to;
	}

protected:
#ifdef KWIN32
    int SocketPair(SOCKET socks[2]);
#endif
};


