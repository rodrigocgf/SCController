// KParser.h: interface for the KParser class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_KPARSER_H)
#define AFX_KPARSER_H

#include "KBaseException.h"

const size_t KMAX_PARAM_KEY = 100;

class KParser  
{
private:
    sbyte **KeyList;
    sbyte **ValueList;
    int32 ParamCount;
	size_t BUF_LIMIT;
    sbyte *Buffer;

public:
	KParser( const sbyte *str );
    virtual ~KParser();

    bool AddValue( sbyte *k, sbyte *v );

    void SetArrays( void *key, void *value, int32 count ) {
        KeyList = ( char ** ) key;
        ValueList = ( char ** ) value;
        ParamCount = count;
    }

    __inline const sbyte *GetValue( int32 i );
    bool GetValueBool( int32 i, bool def = false );
    bool IsValueTrue( int32 i );
    bool IsValueFalse( int32 i );
    int32 GetValueInt( int32 i, int32 def = 0 );

	bool Assigned( int32 i ) {
		if( i < 0 || i >= ParamCount )
			return false;
		return ( GetValue( i ) && *GetValue( i ) != 0 );
	}

    void Parse();
};
typedef KTemplateException<KParser> KParserUnexpectedInput;


class KSingleParam : public KParser
{
protected:
    sbyte Key[ KMAX_PARAM_KEY + 1 ];
    sbyte *pKey[ 1 ];
    sbyte *Value[ 1 ];
public:

    KSingleParam( const sbyte *n, const sbyte *s ) : KParser( s ) {
        memset( Key, 0, KMAX_PARAM_KEY + 1 );
        strncpy( Key, n, KMAX_PARAM_KEY );
        pKey[ 0 ] = Key;
        SetArrays( pKey, Value, 1 );

        Parse();
    }

    KSingleParam( const sbyte *n, const sbyte *s, sbyte* defaultval ) : KParser( s ) {
        memset( Key, 0, KMAX_PARAM_KEY + 1 );
        strncpy( Key, n, KMAX_PARAM_KEY );
        pKey[ 0 ] = Key;
        SetArrays( pKey, Value, 1 );

        try
        {
            Parse();
        }
        catch( KParserUnexpectedInput& )
        {
            Value[ 0 ] = defaultval;
        }
    }
};

__inline const sbyte *KParser::GetValue( int32 i ) 
{
#ifdef K_DEBUG
    if( i < 0 || i >= ParamCount )
    {
        throw KParserUnexpectedInput( "Indice fora de escopo i: %s max: %s (KParser::GetValue)", i, ParamCount );
    }
#endif
    if( ValueList[ i ] )
        return ValueList[ i ];
    else
        return NULL;
}

typedef KTemplateException<KSingleParam> KInvalidParamsException;

#endif


