// HardConfig.h: interface for the KHardConfig class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(KCONFIGREADER_H)
#define KCONFIGREADER_H

#define KMAX_CFG_LINE	200

#include "KList.h"
#include "KBaseException.h"
#include "kstring.h"

struct KCfgItem
{
	sbyte Name[ KMAX_CFG_LINE / 2 ];
	sbyte Value[ KMAX_CFG_LINE / 2 ];
};

class KConfigReader;
typedef KTemplateException<KConfigReader> KConfigException;

class KItemNotFoundException : public KConfigException
{
public:
    KItemNotFoundException( KConfigReader *cfg, const char *item ) 
        : KConfigException( cfg, "Config item [%s] not found", item )
    {}
};

class KValueUndefinedException : public KConfigException
{
public:
    KValueUndefinedException( KConfigReader *cfg, const char *item ) 
        : KConfigException( cfg, "Config item [%s] undefined (=@)", item )
    {}
};

class KConfigReader : public KList
{
private:
    void Warning( kstring s ) {
        Warning( s.c_str() );
    }
    void vWarning( const char *fmt, ... )
    {
        kstring s;
        va_list args;
        va_start( args, fmt );
        s.vFormat( fmt, args );
        va_end( args );
        Warning( s.c_str() );
    }


protected:
	KCfgItem *Search( const sbyte *Name );

    sbyte ConfigFileName[ KMAX_PATH ];
public:
	KConfigReader();
	virtual ~KConfigReader();

	KConfigReader( const sbyte *FileName ) {
		LoadFile( FileName );
	}
	KConfigReader( const kstring &FileName ) {
		LoadFile( FileName );
	}

    static kstring GetFileName( kstring file )
    {
        kstring str;
        str.sprintf( "%s" KDIR_SEP_STR "config" KDIR_SEP_STR "%s", 
            KHostSystem::GetWorkDirectory(), file.c_str() );
        return str;
    }

	void Clear();

    virtual void LoadFile( const kstring &p ) {
        LoadFile( p.c_str() );
    }
	virtual void LoadFile( const sbyte *FileName, bool CreateIfNotExist = false);
    virtual void LoadSection( const sbyte *FileName, const sbyte *Section );
	void SetConfigFileName( const sbyte *FileName );
	sbyte *GetConfigFileName() {return ConfigFileName;};
    virtual void SaveFile();

	sbyte *GetString( const sbyte *Name );
	int32 GetInteger( const sbyte *Name );

	uint32 GetHexadecimal( const sbyte *Name );
	bool GetBool( const sbyte *Name );
	double GetDouble( const sbyte *Name );
	void GetFieldList( const sbyte *Key, KConfigReader *List );
	sbyte *GetStringDef( const sbyte *Name, const sbyte *Default );
	int32 GetHexDef( const sbyte *Name, int32 Default );
	int32 GetIntDef( const sbyte *Name, int32 Default );
	bool GetBoolDef( const sbyte *Name, bool Default );
    double GetDoubleDef( const sbyte *Name, double Default );
    bool IsContained( const sbyte *Name, int32 Channel );
    bool GetStringAtPos( const sbyte *Name, sbyte *ReturnString, int32 Value );
    bool ValueExists( const sbyte *Name );
    void SetValue( const sbyte *Name, const sbyte *Value );

    template <typename value_type>
    value_type GetValueDef( const sbyte *Name, const value_type& errorVal ) 
    {
        try
        {
            KCfgItem *Item = Search( Name );
            if( Item )
            {
                if( strcmp( Item->Value, "@" ) == 0 )
                    return errorVal;
                return from_string<value_type>(Item->Value, errorVal);
            }
            else
                return errorVal;
        }
        catch( const KItemNotFoundException& )
        {
            return errorVal;
        }
    }
    
    template <typename value_type>
        void SetValue( const sbyte *Name, const value_type& Value ) { 
            SetValue(Name, to_string(Value).c_str() ); 
        }

    kstring ToString() {
        return kstring( "config[%s]", ConfigFileName );
    }
    // sobrescrever para tratar valores nao encontrados
    virtual void Warning( const sbyte *s ) {};
};


#endif // !defined(AFX_HardConfig_H)


