#ifndef KBUFFER_H
#define KBUFFER_H

#include "KSerialize.h"
#include "KException.h"
#include "Commons/ringbuffer.hpp"

namespace ktools
{
    const ksize MaxBufferSize = 8096;
    class KBuffer : public KSerializable
    {
        ksize _Size;
        byte *_Data;

    public:
        KBuffer( void * const data, const ksize size )
        : _Size(size), _Data( new byte[ size ] )
        {
            memcpy( _Data, data, size );
        }

        KBuffer( const ksize size )
        : _Size(size), _Data( new byte[ size ] )
        {}

        ~KBuffer()
        {
            delete[] _Data;
        }

        byte *GetBytes() {
            return _Data;
        }
        void *GetPointer() {
            return _Data;
        }
        byte* Data() { return _Data; };
        ksize Size() { return _Size; };

        void Serialize( KSerializer &S ) {
            ksize s = _Size;
            if( S.In() )
            {
                S & s;
                if( s > MaxBufferSize )
                    throw EXCEPTION( "Maximum buffer size of %d exceed (%d)", MaxBufferSize, s );
                if( _Data )
                    delete[] _Data;
                _Data = new byte[ s ];
                _Size = s;
                S.Read( _Data, _Size );
            }
            else
            { 
                S & _Size;
                S.Write( _Data, _Size );
            }
        }
        KBuffer &operator =(KBuffer &c) {
            if( _Data )
                delete[] _Data;
            _Data = new byte[ c._Size ];
            memcpy( _Data, c._Data, c._Size );
            _Size = c._Size;
            return *this;
        }
    };
    class KBufferHolder : public KSerializable
    {
        byte *Data;
        ksize Size;
        ksize MaxSize;
        bool MyData;

    public:
        KBufferHolder( void *data = 0, ksize size = 0 )
        : Data( (byte*)data ), Size(size), MaxSize(size), MyData(false)
        {}

        KBufferHolder( ksize s )
        : Data(0), Size(0), MaxSize(0), MyData(false)
        {
            MyData = false;
            Alloc( s );
        }

        KBufferHolder(const KBufferHolder &other)
        : Data(0), Size(0), MaxSize(0), MyData(false)
        {
            Copy(other);
        }

        ~KBufferHolder()
        {
            if( MyData && Data )
                delete[] Data;
        }

        void Assign( void *data, ksize size ) {
            if( MyData && Data )
                delete[] Data;
            Data = (byte*)data;
            Size = size;
            MaxSize = size;
            MyData = false;
        }
        void Alloc( ksize s ) {
            if( MyData && Data )
                delete[] Data;
            Data = new byte[ s ];
            Size = s;
            MaxSize = s;
            MyData = true;
        }
        void EnsureSize( ksize s ) {
            if( s <= MaxSize ) // all right
            {
                Size = s;
                return;
            }
            if( Data != NULL && !MyData )
                throw EXCEPTION( "%s", "Cannot free external data" );
            MyData = true;
            Alloc( s );
        }

        void Serialize( KSerializer &S ) {
            ksize s = Size;
            if( S.In() )
            {
                S & s;
                if( s > MaxBufferSize )
                    throw EXCEPTION( "Maximum buffer size of %d exceeded (%d)", MaxBufferSize, s );
                if( s != Size )
                {
                    if( s > MaxSize)
                    {
                        if( !MyData && Size > 0 )
                            throw EXCEPTION( "%s", "Impossible serialize on external alloced memory" );
                        delete[] Data;
                        Data = new byte[ s ];
                        MyData = true;
                        MaxSize = s;
                    }
                    Size = s;
                }
                S.Read( Data, Size );
            }
            else
            {
                S & Size;
                S.Write( Data, Size );
            }
        }
        void *GetPointer() const {
            return Data;
        }
        template<typename T> T* GetArrayOf() {
            return reinterpret_cast<T*>( Data );
        }
        byte *GetBytes( kindex i = 0 ) {
            return &Data[ i ];
        }
        ksize GetDataSize() const {
            return Size;
        }
        ksize GetMaxDataSize() const {
            return MaxSize;
        }
        KBufferHolder &operator =(KBufferHolder &c) {
            Assign( c.Data, c.Size );
            return *this;
        }
        KBufferHolder &Copy(const KBufferHolder &c) {
            if( Data && !MyData )
                throw EXCEPTION( "%s", "Cannot overwrite external Data" );
            if( Data && c.Size > MaxSize ) 
            {
                delete[] Data;
                Data = NULL;
            }
            if( !Data && c.Size )
            {
                MyData = true;
                Data = new byte[ c.Size ];
                MaxSize = c.Size;
            }
            memcpy( Data, c.Data, c.Size );
            Size = c.Size;
            return *this;
        }
    };

    class KBufferDeserializer : public KInSerializer 
    {
        friend class KBufferSerializer;
        byte *Offset;
        byte *Pointer;

    protected:
        KBufferDeserializer( void *p ) {
            Pointer = Offset = (byte *) p;
        }

    public:
        void Deserialize( KSerializable &data ) {
            data.Serialize( *this );
        }

        void Serialize( void *p, ksize s )
        {
            if ( !s ) return;

            if ( !Offset )
                throw EXCEPTION( "%s", "Buffer not initialized, invalid serialization" );
            memcpy( p, Offset, s );
            Offset += s;
        }
    };

    // TODO - Checksum
    class KBufferSerializer : public KOutSerializer
    {
    protected:
        bool MyBuffer;
        KBufferHolder *Buffer;
        byte *Offset;
    public:
        KBufferSerializer( const KSerializable &d ) {
            Buffer = new KBufferHolder;
            MyBuffer = true;
            ksize s = KSerialSizeCalculator::GetSize(d);
            Buffer->Alloc( s );
            Offset = Buffer->GetBytes();
            const_cast<KSerializable&>(d).Serialize( *this );
        };
        KBufferSerializer( KBufferHolder *buffer, KSerializable &d ) {
            Buffer = buffer;
            MyBuffer = false;
            ksize s = KSerialSizeCalculator::GetSize(d);
            Buffer->Alloc( s );
            Offset = Buffer->GetBytes();
            d.Serialize( *this );
        };
        ~KBufferSerializer() {
            if( MyBuffer )
                delete Buffer;
        }
        void Serialize( void *d, ksize s ) {
            memcpy( Offset, d, s );
            Offset += s;
        }
        void *GetData() {
            return Buffer->GetPointer();
        }

        KBufferHolder *GetBuffer() {
            return Buffer;
        }

        static void Deserialize( KSerializable &dest, void *source ) {
            KBufferDeserializer d( source );
            d.Deserialize( dest );
        }

        static void Serialize( KBufferHolder &buffer, KSerializable &data ) {
            KBufferSerializer b( &buffer, data );
        }
    };

    typedef Ringbuffer<byte> KRingBuffer;
}
#endif
