#pragma once

namespace ktools
{
#ifdef KWIN32
    HMODULE GetCurrentModule();
#endif
    class KProcess
    {
    public:
        static int GetCurrentPID() {
        #if KWIN32
	        return GetCurrentProcessId();
        #else
	        return getpid();
        #endif        
        };
        static void GetExeName( kstring &s ) {
        #if KWIN32
            sbyte fn[ MAX_PATH + 1 ];
            GetModuleFileName( NULL, fn, MAX_PATH );
            s.assign( fn );
        #endif        
        }
        static void GetModuleName( kstring &s ) {
        #if KWIN32
            sbyte fn[ MAX_PATH + 1 ];
            GetModuleFileName( GetCurrentModule(), fn, MAX_PATH );
            s.assign( fn );
        #endif        
        }
    };

    kstring Exec(const kstring& cmd);
};
