#pragma once

#include "KBaseSocket.h"
#include "KMutex.h"
#include "KThread.h"

namespace ktools
{
    class KClientSocket : public KTcpSocket
    {
        kstring Address;
        int32 Port;
        KThreadMutex WriteMutex, ReadMutex;
        bool Assigned;

    public:
        KClientSocket();
        virtual ~KClientSocket(void);

        void Write( void *Buf, ksize Size );
        void Write( const kstring& Str );

        ksize Read( void *Buf, ksize MaxSize );
        ksize Read( kstring &Result );

        void Open();
        void Open( const sbyte *RemoteAddress, uint16 RemotePort );
        void Assign( KSOCKET handle, const kstring &addr = "" );
        const kstring &GetAddress() const { return Address; }
        KThreadMutex &GetWriteMutex() { return WriteMutex; }
        KThreadMutex &GetReadMutex() { return ReadMutex; }
    };
}

/*
#define KREAD( a, b, c ) f_SocketRead( a, b, c, __FUNCTION__, __LINE__ )
void f_SocketRead( ktools::KClientSocket *socket, void *buf, int size, const char *f, int l );

#define KWRITE( a, b, c ) f_SocketWrite( a, b, c, __FUNCTION__, __LINE__ )
void f_SocketWrite( ktools::KClientSocket *socket, void *buf, int size, const char *f, int l );
*/
