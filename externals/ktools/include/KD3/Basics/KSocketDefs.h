#pragma once

#ifndef BASICSKSocketDefsH
#define BASICSKSocketDefsH

#ifdef WIN32
#include <winsock2.h>
#include <windows.h>
#endif

#define NO_DELAY_FLAG 0


#ifdef KWIN32

	typedef SOCKET KSOCKET; //TODO verificar isto em 64bits...

	#ifndef FD_SETSIZE
	#define FD_SETSIZE      2
	#endif /* FD_SETSIZE */

	#define KINVALID_SOCKET	INVALID_SOCKET
#else
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <netinet/tcp.h>
    #include <netdb.h>
    #include <errno.h>

    #include <arpa/inet.h>

    #include <sys/types.h>
    #include <sys/uio.h>

    typedef int KSOCKET;

    #define KINVALID_SOCKET	-1
    #define SOCKET_ERROR	-1
    #define closesocket		close
    #define WSAGetLastError()  errno


#endif

namespace ktools
{
    typedef unsigned short kport;
}

#endif
