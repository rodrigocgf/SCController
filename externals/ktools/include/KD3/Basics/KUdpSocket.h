#pragma once

#include "KBaseSocket.h"
#include "KString.h"

#include "KSocketPoll.h"

/**  Define os sockets UDP

    TODO:
        * I/O Ass�ncrono!
        * Verificar se a performance melhora se usar connect no socket sender e/ou receiver.
 */

namespace ktools
{

    class KUdpSenderSocket: private KSocketInitializer
    {
    protected:
        KSOCKET Handle;
        sockaddr_in _Address;
        KThreadMutex _Mutex;
        kstring Address;
        uint32 Port;
    public:
        KUdpSenderSocket( uint16 Port, kstring Address );
        KUdpSenderSocket( sockaddr_in Address );
        virtual ~KUdpSenderSocket();

        // Binds the sender to a local address
        void Bind( kstring LocalAddress );
        void Bind( kstring LocalAddress, uint16 Port );

        ksize Send( const byte *Buffer, ksize size );
        KThreadMutex& GetMutex() {
            return _Mutex;
        };

    };

    class KUdpReceiverSocket: private KSocketInitializer
    {
        KSOCKET Handle;
        uint16 _Port;
        sockaddr_in _MyAddr;
        sockaddr_in _From;
        ksize _FromLen;
        KThreadMutex _Mutex;
        bool _Blocking;
        KSocketPoll _Poller;
    public:
        KUdpReceiverSocket( uint16 Port, kstring Address = "" );
        virtual ~KUdpReceiverSocket();

        void Close() {
            if( Handle != KINVALID_SOCKET )
                closesocket( Handle );
            Handle = KINVALID_SOCKET;
        }

        //Seta o modo de I/O pra n�o bloqueante!
        void SetAsync();

        bool WaitForData( ktime timeout );
        //todo testar se foi passado o fromaddr e port e nao recever se for diferente
        //todo verificar se a performance melhora se usar um connect() no socket.
        ksize Recv( byte *Buffer, ksize Size, kstring *fromAddr = NULL, uint16 *fromPort = NULL);
        KThreadMutex& GetMutex() {
            return _Mutex;
        }
        sockaddr_in GetFrom() {
            return _From;
        }
        uint16 Port() { return _Port; }
    };


    class KUdpSocket: public KUdpSenderSocket, public KUdpReceiverSocket
    {
        //KUdpSenderSocket Sender;
        //KUdpReceiverSocket Receiver;
        //KThreadMutex _ReadMutex, _WriteMutex;
    public:
        KUdpSocket(uint16 RemotePort, kstring RemoteAddr, uint16 LocalPort, kstring LocalAddr = ""):
            KUdpSenderSocket( RemotePort, RemoteAddr ),
            KUdpReceiverSocket( LocalPort, LocalAddr )
        {
        }

        virtual ~KUdpSocket() {};

        ksize Write( void *Buffer, ksize size ){
            //todo - O que fazer quando todo o valor n�o for enviado? quebrar em v�rios envios mesmo sem garantia da ordem? 
            return Send( (const byte*)Buffer, size );
        }

        /**  Reads the next UDP datagram from the queue, if 'size' is smaller than the size read
           it will copy just 'size' bytes and discard the rest.
        */
        ksize Read( void* Buffer, ksize size ){
            //todo testar se  addr == RemoteAddr
            return Recv( (byte*)Buffer, size);
        }

        KMutex& WriteMutex() {
            return KUdpSenderSocket::GetMutex();
        }

        KMutex& ReadMutex() {
            return KUdpReceiverSocket::GetMutex();
        }

        static bool IsPortFree( uint16 LocalPort ) {
            try {
                KUdpReceiverSocket s(LocalPort);
            } catch( KSocketException & ) {
                return false;
            }
            return true;
        }
    };


    class KUdpSocket2: private KSocketInitializer
    {
    public:
        KUdpSocket2();
        virtual ~KUdpSocket2();

        void Close() {
            if( Handle != KINVALID_SOCKET )
                closesocket( Handle );
            Handle = KINVALID_SOCKET;
        }

        //Seta o modo de I/O pra n�o bloqueante!
        void SetAsync();
        void SetSOBuf();
        void SetSOReuseAddr();
        void SetTOS( kstring RemoteAddress, uint16 RemotePort );
        void Bind( kstring LocalAddress );
        void Bind( kstring LocalAddress, uint16 Port );
        void Bind( uint16 Port );
        void Connect( kstring RemoteAddress, uint16 Port );

        ksize SendTo( const byte *Buffer, ksize size, kstring addr, uint16 port );
        ksize Send( const byte* Buffer, ksize size );

        //todo testar se foi passado o fromaddr e port e nao recever se for diferente
        //todo verificar se a performance melhora se usar um connect() no socket.
        ksize RecvFrom(byte *Buffer, ksize Size, kstring *fromAddr = NULL, uint16 *fromPort = NULL);
        ksize Recv(byte* buffer, ksize Size);
        bool WaitForData( ktime timeout );
        void GetLocalAddress( kstring &Address, kport& Port);
        void GetRemoteAddress( kstring &Address, kport& Port);

        KThreadMutex& GetMutex() { return _Mutex; }

    private:
#ifdef KWIN32
        StackWinQoSClass WinQoSObject;
#endif

    private:
        KSOCKET Handle;
        KThreadMutex _Mutex;
        bool _Blocking;
        KSocketPoll _Poller;
    };

    kstring GetAddressToConnectTo(const kstring& RemoteAddress);

}
