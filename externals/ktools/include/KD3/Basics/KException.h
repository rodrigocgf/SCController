#ifndef KEXCEPTION_H
#define KEXCEPTION_H

#include "KTypes.h"
#include "KString.h"
#include "../../KBaseException.h"

#ifndef __BORLANDC__
#define        EXCEPTION( sa, ... )         KException( __FILE__, __LINE__, sa, ##__VA_ARGS__ )
#define KTOOLS_EXCEPTION( sa, ... ) ktools::KException( __FILE__, __LINE__, sa, ##__VA_ARGS__ )
#endif

namespace ktools
{
    class KException : public KBaseException
    {
    private:
        kstring Message;
        kstring File;
        int Line;
    public:
        KException( const char *s, ... );
        KException( char const *f, int l, const char *s, ... );

        KException( kstring s );

        virtual ~KException() throw();

        const char *GetString();

        kstring &GetFile();

        int GetLine();

        const char *what() const throw();
    };
}
#endif 
