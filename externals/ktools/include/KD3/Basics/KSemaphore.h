#ifndef KSEMAPHORE_H
#define KSEMAPHORE_H

#ifndef KWIN32
#include <semaphore.h>
#endif

#include <kstring.h>

#ifdef INFINITE
     #undef INFINITE
#endif
#define INFINITE 0xFFFFFFFF 

namespace ktools
{

#ifdef KWIN32
typedef KHandle KSemHandle;
#else
typedef sem_t *KSemHandle;
#endif

/*

    KSemaphore
        bool Wait()
        void Release()
*/
#define BIG_SEMAPHORE_NUMBER 1024*1024

    class KSemaphore
    {
#ifndef KWIN32
        bool named;
#endif
        kstring Name;
        ksize Max;
        KSemHandle Handle;
        bool Owner;
        KSemaphore( const KSemaphore &s );
    public:
        KSemaphore(std::string name, ksize max = BIG_SEMAPHORE_NUMBER, bool owner = true);
        KSemaphore( ksize max = BIG_SEMAPHORE_NUMBER );
        virtual ~KSemaphore();

        virtual bool Wait( ktime timeout = INFINITE );
        virtual void Release();
    };
};
#if 0
class KSemaphore
{
    KSemHandle Handle;
    void CreateSemaphore( const char *name, uint32 max ) {;
    #ifdef KWIN32
        SECURITY_DESCRIPTOR SecDesc;
        SECURITY_ATTRIBUTES SecAttr;
        memset( &SecAttr, 0, sizeof( SECURITY_ATTRIBUTES ) );
        memset( &SecDesc, 0, sizeof( SECURITY_DESCRIPTOR ) );
        InitializeSecurityDescriptor( &SecDesc, SECURITY_DESCRIPTOR_REVISION );
        SetSecurityDescriptorDacl( &SecDesc, true, ( PACL ) NULL, false );

        SecAttr.nLength              = sizeof( SecAttr );
        SecAttr.lpSecurityDescriptor = &SecDesc;
        SecAttr.bInheritHandle       = true;

        Handle = ::CreateSemaphore(
            &SecAttr,
            0,
            max,
            name ); 
        if( Handle == NULL )
            throw EXCEPTION( "CreateSemaphore error!" );
    #else
        Handle = sem_open( name, O_CREAT, 0664, 0 );
    #endif
    }
public:
    KSemaphore(std::string name, uint32 max = BIG_SEMAPHORE_NUMBER) {;
        this->CreateSemaphore( name.c_str(), max );
    };
    KSemaphore( ksize max = BIG_SEMAPHORE_NUMBER ) {;
        this->CreateSemaphore( NULL, max );
    };
    ~KSemaphore() {
#ifdef KWIN32
        CloseHandle( Handle );
#else
        sem_close( Handle );
#endif
    }

    virtual bool Wait() {
#ifdef KWIN32
        return WaitForSingleObject( Handle, INFINITE ) == WAIT_OBJECT_0;
#else
        return sem_wait( Handle ) == 0;
#endif
    };

    virtual void Release() {
#ifdef KWIN32
        int32 ret = ReleaseSemaphore( Handle, 1, 0 );
        if( ret == 0 )
            throw EXCEPTION( "Release semaphore error %d", GetLastError() );
#else
        int32 ret = sem_post( Handle );
        if( ret == -1 )
            throw EXCEPTION( "Release semaphore error %d", errno );
#endif
    };
};

}
#endif
#endif
