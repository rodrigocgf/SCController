#pragma once

#include "stdio.h"
#include "KBuffer.h"
#include "KBaseException.h"
#include <iostream>
#include <fstream>

namespace ktools
{
    class KDirectory
    {
    public:
        static void Create( kpchar Path )
        {
	        int32 InternalRet;
        #if KWIN32
	        InternalRet = CreateDirectory( Path, NULL );
	        if( !InternalRet )
	        {
		        int32 x = GetLastError(); // debug
		        if( x != ERROR_ALREADY_EXISTS )
                    throw EXCEPTION( "Directory creation error [%s][%d]", Path, x );
	        }
        #else
	        InternalRet = mkdir( Path, 0777 );
	        if( InternalRet )
	        {
		        if( errno != EEXIST )
                    throw EXCEPTION( "Directory creation error [%s][%d]", Path, errno );
	        }
        #endif
        }
    };

    class KBufferedFileReader;
    typedef KTemplateException<KBufferedFileReader> KFileException;

    class KReader
    {
    public:
        virtual ksize Read( void *dest, ksize size ) = 0;
        virtual void Scape( ksize count ) = 0;

        virtual ~KReader() {}
    };

    class KWriter
    {
    public:
        virtual bool Write( const void *buf, ksize size ) = 0;

        virtual ~KWriter() {}
    };

    class KFileWriter: public KWriter
    {
        static const ksize BUFFER_SIZE = 0x8000;
        std::ofstream File;
        char MyBuffer[ BUFFER_SIZE ];
    public:
        KFileWriter() {
        }

        void Init( const char *filename, bool buffered = true, bool append = false ) {
            File.open( filename, ( append ? std::ofstream::in : std::ofstream::out ) | std::ofstream::out | std::ofstream::binary );
            if( !File.good() )
                throw KFileException( "Could not open file %s for writing", filename );
            if( buffered )
                File.rdbuf()->pubsetbuf( MyBuffer, BUFFER_SIZE );
        }

        bool Write( const void *buf, ksize size ) {
            return File.write( (char*) buf, size ).good();
        }

        void Flush() {
            File.flush();
        }

        ksize Tell() {
            return (ksize)File.tellp();
        }

        void Close() {
            File.close();
        }

        void Reset() {
            File.seekp( 0, std::ofstream::beg );
        }

        void End() {
            File.seekp( 0, std::ofstream::end );
        }
    };

    class KBufferedFileReader : public KReader
    {
        std::ifstream File;
        KRingBuffer Buffer;
        ksize Size;
        ksize LogicalSize;
        ksize CurrOffset;
    public:
        /*
        enum SeekDir
        {
            Begin   = std::ifstream::beg,
            Current = std::ifstream::cur,
            End     = std::ifstream::end,
        };
        */

        KBufferedFileReader( kstring filename, ksize bufsize ) : Buffer(bufsize + 1), Size( bufsize ) {
            File.open( filename.c_str(), std::ifstream::in | std::ifstream::binary );
            if( !File.good() )
                throw KFileException( "Could not open file [%s] for reading", filename.c_str() );
            Buffer.get( File, Size );
            CurrOffset = LogicalSize = 0;
        }

        ksize Read( void *dest, ksize size ) {
            if( LogicalSize != 0 )
            {
                if( CurrOffset + size > LogicalSize )
                    size = LogicalSize - CurrOffset;
            }
            ksize read = Buffer.consume( (byte*) dest, size );
            if( size > Size )
                throw KFileException( "Invalid read size %d (max=%d)", size, Size );
            if( read < size )
            {
                ksize s = Buffer.get( File, Size );
                ksize next = s > size-read ? size-read : s;
                read += Buffer.consume( (byte*)dest + read, next );
            }
            CurrOffset += read;
            return read;
        }

        ksize Tell()
        {
            return CurrOffset;
            /*ksize ret = File.tellg(); 
            return ret - Buffer.unread();*/
        };

        void SetLogicalSize( ksize size ) 
        {
            LogicalSize = size;
            if( Tell() > LogicalSize )
                throw KFileException( "Invalid position" );
        }

        void Scape( ksize size ) 
        {
            if( size > Buffer.used_blocks() )
            {
                size -= Buffer.used_blocks();
                Buffer.clear();
                File.seekg( size, std::ifstream::cur );
            }
            else
                Buffer.consumer_commit( size );

            CurrOffset += size;
        }

        void Reset()
        {
            Buffer.clear();
            File.clear();
            File.seekg( 0, std::ifstream::beg );
            CurrOffset = 0;
        }

        ksize FileSize()
        {
            ksize pos = ( ksize ) File.tellg();

            File.seekg( 0, std::ifstream::end );
            ksize size = ( ksize ) File.tellg();
            File.seekg( pos, std::ifstream::beg );
            return size;
        }

        template<class T>
        KBufferedFileReader& operator>>( T& t)
        {
            ksize ReadSize = Read( &t, sizeof(T) );
            if( ReadSize < sizeof(T) )
                throw KFileException( "Error reading %d bytes from file", sizeof(T) );
            return *this;
        };
    };

};
