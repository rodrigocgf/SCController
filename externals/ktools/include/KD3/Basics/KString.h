#pragma once

//Esta classe nao pode ser compilada no Borland!
#ifndef __BORLANDC__

#include <string>
#include <stdio.h>
#include <stdarg.h>
#include <stdexcept>
#include <list>
#include "KSerialize.h"

#ifndef KMAX_STRING_SIZE
#define KMAX_STRING_SIZE 2048
#endif
namespace ktools
{
    class kstring : public std::string, public KSerializable
    {
    public:
        void sprintf( const char *fmt, ... ) {
            va_list args;
            va_start( args, fmt );
            vFormat( fmt, args );
            va_end( args );
        }
        void Format( const char *fmt, ... ) {
            va_list args;
            va_start( args, fmt );
            vFormat( fmt, args );
            va_end( args );
        }
        void vFormat( const char *fmt, va_list args ) 
        {
            if (!fmt) 
            {
                this->assign( "" );
                return;
            }
            char buffer1[ KMAX_STRING_SIZE ];
    #ifndef KWIN32
            va_list argscp;
            va_copy( argscp, args ); //required in linux when using 64 bits processors
            int ret = vsnprintf(buffer1, KMAX_STRING_SIZE, fmt, argscp);
            va_end( argscp );

            if( ret >= 0 )
    #else
            if( _vsnprintf_s(buffer1, KMAX_STRING_SIZE-1, fmt, args) >= 0 )
    #endif
                this->assign(buffer1);
            else
            {
                char buffer2[ KMAX_STRING_SIZE * 10 ]; // 2nd and last attempt, 10kb
    #ifndef KWIN32
                va_list argscp;
                va_copy( argscp, args ); //required in linux when using 64 bits processors
                int ret = vsnprintf(buffer2, KMAX_STRING_SIZE*10, fmt, argscp);
                va_end( argscp );

                if( ret >= 0 )
    #else
                if( _vsnprintf_s(buffer2, (KMAX_STRING_SIZE*10)-1, fmt, args) >= 0 )
    #endif
                    this->assign(buffer2);
                else
                    throw std::runtime_error( "cannot assign string bigger than 10KB" );
            }
        }

        void AppendFormat(const char *fmt, ... ) {
            kstring tmp;
            va_list args;
            va_start( args, fmt );
            vAppendFormat( fmt, args );
            va_end( args );
        }

        void vAppendFormat(const char *fmt, va_list args ) 
		{
            kstring tmp;
			#ifndef KWIN32
			va_list argscp;
			va_copy( argscp, args ); //required in linux when using 64 bits processors
			tmp.vFormat( fmt, argscp );
			va_end( argscp );
			#else
			tmp.vFormat( fmt, args );
            #endif
			this->append(tmp) ;
        }

        kstring &RemoveBad( const char *bad ) {
            const char *pbad = bad;
            while( *pbad )
            {
                kstring::size_type k = 0;
                while((k=find(*pbad,k))!=npos)
                    erase(k, 1);
                pbad++;
            }
            return *this;
        }

        kstring &operator=(const char *s)
        {
            assign(s);
            return *this;
        }
        kstring &operator=(const std::string &s)
        {
            assign(s);
            return *this;
        }

        operator const char *() {
            return c_str();
        }

        int32 icompare( const kstring &right ) const
        {
        #ifdef __TCPLUSPLUS__
        #define _stricmp stricmp
        #endif
        #if KWIN32
            return _stricmp( c_str(), right.c_str() );
        #else
	        return strcasecmp( c_str(), right.c_str() );
        #endif
        }

        kstring() : std::string() {};

        kstring(const std::string& p) : std::string( p ) {};

        //TODO - colocar explicit nesse construtor, pq se o char* conter % e for chamado este construtor, vai dar pau!
        kstring( const char *fmt, ... ) {
            va_list args;
            va_start( args, fmt );
            vFormat( fmt, args );
            va_end( args );
        }

        using std::string::replace;
        std::string& replace( const kstring& to_find, const kstring& to_replace )
        {
            size_type off = find(to_find);
            if( off == npos )
                return *this;
            return replace(off, to_find.length(), to_replace);
        }

        std::string& replace_all( const kstring& to_find, const kstring& to_replace )
        {
            size_type off = find(to_find);
            while( off != npos )
            {
                replace(off, to_find.length(), to_replace);
                off = find(to_find, off+1);
            }
            return *this;
        }

        std::list<kstring> split(char sep = ' ', bool trim = true) const
        {
            std::list<kstring> ret;
            size_t start = 0;
            while(true)
            {
                size_t pos = find_first_of( sep, start );
                kstring tmp = substr( start, pos-start );
                if( tmp.size() != 0 )
                {
                    if( trim ) tmp = tmp.trim();
                    ret.push_back( tmp );
                }
                if(pos == npos ) 
                    break;
                start = pos+1;
            }
            return ret;
        }

        std::string trim() const
        {
            size_t begin=0;
            while(begin<size() && isspace(at(begin)))
                ++begin;
            size_t end=size()-1;
            while(end>begin && isspace(at(end)))
                --end;
            return substr(begin, end-begin+1);
        }

        void Serialize( KSerializer &S ) { 
            ksize s;
            if( S.In() )
            {
                //s = (ksize) size() + 1;
                S & s;               //S.Serialize( &s, sizeof s );
                if( s > KMAX_STRING_SIZE )
                    throw std::runtime_error("cannot serialize string bigger than KMAX_STRING_SIZE(2048)");
                else
                {
                    this->resize( s-1, 'a' );
                    S.Read( (void*)c_str(), s );
                }
            }
            else
            {
                ksize s = (ksize) size() + 1;
                S & s;  //              S.Serialize( &s, sizeof s );
                S.Write( (void*)c_str(), (ksize) size() + 1 );
            }
        };
    };
}

#endif


