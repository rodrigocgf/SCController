#pragma once

#ifndef KMUTEX_H
#define KMUTEX_H

#include "KTypes.h"
#include "KAbstract.h"
#include "KSemaphore.h"
#include "ReadWriteLock.h" // ktools
#include "KException.h"
#include "../../Commons/noncopyable.hpp"

/*
#define mutex_context(mutex) for( ktools::KContextMutex m##__LINE__(mutex); \
        m##__LINE__.MakeContextMutexMacroAvailable(); )
*/
#ifndef mutex_context
#   define TOKENPASTE2(x, y) x ## y
#   define TOKENPASTE(x, y) TOKENPASTE2(x, y)
#   define mutex_context(mutex) for( KContextMutex TOKENPASTE(m, __LINE__)(mutex); \
            TOKENPASTE(m, __LINE__).MakeContextMutexMacroAvailable(); )
#endif

namespace ktools 
{
    class KMutex
    {
        friend class KContextMutex;
        friend class KReverseContextMutex;
    protected://� protected pra usar s� KContextMutex e coisas assim, n�o usar Lock e Unlock.
        virtual void Lock() = 0;
        virtual void Unlock() = 0;
    public:
        virtual ~KMutex() {};
    };

    class KThreadMutex : public KMutex, public NonCopyable
    {
    protected:
        KHandle Handle;    
    public:
        KThreadMutex() {
        #if KWIN32
	        CRITICAL_SECTION *KCS;
	        KCS = new CRITICAL_SECTION;
	        InitializeCriticalSection( KCS );
        #else 
	        pthread_mutex_t *KCS;
	        KCS = new pthread_mutex_t;
	        pthread_mutexattr_t MutexAttribute;
	        pthread_mutexattr_init( &MutexAttribute );
	        pthread_mutexattr_settype( &MutexAttribute, PTHREAD_MUTEX_RECURSIVE_NP);
	        pthread_mutex_init( KCS, &MutexAttribute );
        #endif
            Handle = KCS;
        }
        ~KThreadMutex() {
        #if KWIN32
	        DeleteCriticalSection( ( LPCRITICAL_SECTION ) Handle );
	        delete ( LPCRITICAL_SECTION ) Handle;
        #else
	        pthread_mutex_destroy( ( pthread_mutex_t *) Handle );
	        delete ( pthread_mutex_t * ) Handle;
        #endif
            Handle = NULL;
        }

        KHandle GetHandle() {
            return Handle;
        }

        void Lock() {
        #if KWIN32
	        EnterCriticalSection( ( LPCRITICAL_SECTION ) Handle );
        #else
	        pthread_mutex_lock( (pthread_mutex_t *) Handle );
        #endif
        }

        void Unlock() {
        #if KWIN32
            LeaveCriticalSection( ( LPCRITICAL_SECTION ) Handle );
        #else
            pthread_mutex_unlock( (pthread_mutex_t *) Handle );
        #endif
        }
    };

    // NON RECURSIVE!!!!
    class KProcessMutex : public KMutex
    {
    private:
        KSemaphore Sem;
        KHandle Handle;
    public:
        KProcessMutex( std::string n, bool owner = true ) : Sem( n.c_str(), 1, owner ) {
#ifdef WIN32            
            SECURITY_DESCRIPTOR SecDesc;
            SECURITY_ATTRIBUTES SecAttr;
            memset( &SecAttr, 0, sizeof( SECURITY_ATTRIBUTES ) );
            memset( &SecDesc, 0, sizeof( SECURITY_DESCRIPTOR ) );
            InitializeSecurityDescriptor( &SecDesc, SECURITY_DESCRIPTOR_REVISION );
            SetSecurityDescriptorDacl( &SecDesc, true, ( PACL ) NULL, false );

            SecAttr.nLength              = sizeof( SecAttr );
            SecAttr.lpSecurityDescriptor = &SecDesc;
            SecAttr.bInheritHandle       = true;
            //Handle = CreateMutex( &sa, FALSE, n.c_str() );
            kstring nn = n + "_MTX";
            Handle = CreateMutex( &SecAttr, FALSE, nn.c_str() );
            if( !Handle )
                throw KException( "Failed to create process mutex. name=[%s] error=%d", n.c_str(), GetLastError() ); // todo linux
#endif
        };
        
        ~KProcessMutex() {
#ifdef KWIN32
            CloseHandle(Handle);
#endif
        }

        void Lock() {
#ifndef KWIN32
            Sem.Wait();
#else
            int ret = WaitForSingleObject( Handle, INFINITE );
            if( ret != WAIT_OBJECT_0 )
                throw KException( "Process mutex failed to wait" );
#endif
        }
        void Unlock() {
#ifndef KWIN32
            Sem.Release();
#else
            ReleaseMutex( Handle );
#endif
        }
    };

    class KReaderMutex : public KMutex
    {
        KReadWriteLock &Locker;
    public:
        KReaderMutex( KReadWriteLock &locker ) : Locker(locker) {
        }
        void Lock() {
            Locker.LockReader();
        }
        void Unlock() {
            Locker.UnlockReader();
        }
    };

    class KWriterMutex : public KMutex
    {
        KReadWriteLock &Locker;
    public:
        KWriterMutex( KReadWriteLock &locker ) : Locker(locker) {
        }
        void Lock() {
            Locker.LockWriter();
        }
        void Unlock() {
            Locker.UnlockWriter();
        }
    };

    class KContextMutex: private NonCopyable
    {
    private:
        KMutex *Mutex;
        KHandle Handle;
        bool Locked;
        bool MacroCounter;
        void _Initialize(KMutex *p, KHandle h) {
            Mutex = p;
            Handle = h;
            MacroCounter = true;
            Lock();
        }

    public:
        KContextMutex( KMutex &p ) {
            _Initialize( &p, NULL );
        }
        KContextMutex( KHandle handle ) {
            _Initialize( NULL, handle );
        }

        ~KContextMutex() {
            if( !Locked )
                return;
            Unlock();
        }

        void Lock() {
            if( Mutex )
                Mutex->Lock();
            else if( Handle )
            {
            #if KWIN32
	            EnterCriticalSection( ( LPCRITICAL_SECTION )Handle );
            #else
	            pthread_mutex_lock( (pthread_mutex_t *)Handle );
            #endif
            }
            Locked = true;
        }

        void Unlock() {
            if( Mutex )
                Mutex->Unlock();
            else if( Handle )
            {
            #if KWIN32
	            LeaveCriticalSection( ( LPCRITICAL_SECTION )Handle );
            #else
	            pthread_mutex_unlock( (pthread_mutex_t *) Handle );
            #endif
            }
            Locked = false;
        }

        bool MakeContextMutexMacroAvailable()
        {
            bool oldMacro = MacroCounter;
            MacroCounter = false;
            return oldMacro;
        }
   };

    class KReverseContextMutex: private NonCopyable
    {
    private:
        KMutex *Mutex;
    public:
        KReverseContextMutex( KMutex &p ) {
            p.Unlock();
            Mutex = &p;
        }

        ~KReverseContextMutex() {
            if( Mutex )
                Mutex->Lock();
        }
    };

}

#endif
