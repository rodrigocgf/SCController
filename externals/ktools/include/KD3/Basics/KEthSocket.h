#ifndef KETHSOCKET_H
#define KETHSOCKET_H

#include "KSocketDefs.h"
#include "EthernetHeader.h"
#include "KMutex.h"

#ifndef KWIN32
# include "KSocketPoll.h"
#endif

#ifdef KWIN32   
    #include <KD3/Basics/pcap/pcap.h>
	#include <Winsock2.h>
	#include "pcap/packet32.h"
	#include "ntddndis.h"
#else
	#include <sys/ioctl.h>
	#include <net/ethernet.h>
	#include <linux/if_ether.h>
	#include <linux/if_arp.h>
	#include <linux/sockios.h>
#endif

#define SUBADDRESS_SIZE   sizeof(short)
#define DEVICE_NAME_SIZE 255

namespace ktools
{
    class KEthSocket
    {
    private:
		#ifdef KWIN32
			pcap_t *Adapter; //Descriptor of an open capture instance
		#else
			KSOCKET Handle;
			struct sockaddr_ll SocketAddr;
		#endif
        
        EthernetHeader EthHeader;
        sbyte DeviceText[DEVICE_NAME_SIZE];

        #ifndef KWIN32
        KSocketPoll Poller;
        #endif

        KThreadMutex Mutex;
        bool Assigned;
        int32 TimeOut;

        char **Buffer;
        int lastRead;

        ksize RawWrite( void *Buf, int32 Size );
        ksize RawRead( void *Buf, int32 MaxSize );

//        int autoresumeselect( int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds, struct timeval *timeout );

		#ifdef KWIN32
			bool GetSourceMAC(byte *sourceMAC);
		#endif
    public:
	    KEthSocket();
	    ~KEthSocket(void);

	    void Write( void *Buf, int32 Size );
	    void Read( void *Buf, int32 MaxSize );
        void Listen( );

        void Open( sbyte *, sbyte * );
        void Assign( KSOCKET handle );
        
		KThreadMutex &GetMutex() {
            return Mutex;
        }

        static KException &ThrowSocketException( kpchar file, int line, KSOCKET & );
    };
}

#define EthSocketException( s ) KEthSocket::ThrowSocketException( __FILE__, __LINE__, s )

#endif
