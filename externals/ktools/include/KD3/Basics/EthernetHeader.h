#ifndef ETHERNET_HEADER_H
#define ETHERNET_HEADER_H

#include "KTypes.h"

#define ETH_MAC_SIZE			6
#define ETH_ETHERTYPE_SIZE		2
#define ETH_HEADER_SIZE			ETH_MAC_SIZE + ETH_MAC_SIZE + ETH_ETHERTYPE_SIZE
#define ETH_MAX_PACKET_SIZE		1518 //1500 of payload and 18 of MAC dst, MAC src, ethertype and CRC. 1522 if VLAN Tag Header is included
#define ETH_ETHERTYPE			0xD00D //0xd00d = ETH_P_DAHDI_DETH. //0x88D8 = The protocol identifier, as allocated by the IEEE. Used in ethernet header

class EthernetHeader
{
private:
	char RemoteMAC[ETH_MAC_SIZE];
	char SourceMAC[ETH_MAC_SIZE];
	unsigned short EtherType;
    unsigned short DataSize;

public:
	EthernetHeader();
	~EthernetHeader();

	void SetEthernetHeader(byte *remoteMAC, byte *sourceMAC, unsigned short etherType);

	bool Pack(byte *packet, int len);
	bool Unpack(byte *packet, int len);

	char *GetRemoteMAC() { return RemoteMAC; };
	char *GetSourceMAC() { return SourceMAC; };
    unsigned short GetEtherType() { return EtherType; };
    unsigned short GetDataSize() { return DataSize; };

	void GetRemoteMACStr(char *str);
	void GetSourceMACStr(char *str);
};

#endif
