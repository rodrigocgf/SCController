#pragma once
#ifndef BASICS_KBASESOCKET_H
#define BASICS_KBASESOCKET_H

#include "KSocketDefs.h"
#include "KMutex.h"
#include "KException.h"

#include "KSocketPoll.h"


#ifdef KWIN32

#include <windows.h>
#include <ws2tcpip.h>
#include <Ntddndis.h>
#if _MSC_VER >= 1600
#include <QoS2.h>
#else
#include "win7_qos2.h"
#endif

extern "C" 
{
    typedef BOOL ( WINAPI *KfncQOSCreateHandle )( PQOS_VERSION Version, PHANDLE QOSHandle );
    typedef BOOL ( WINAPI *KfncQOSCloseHandle )( HANDLE QOSHandle );
    typedef BOOL ( WINAPI *KfncQOSAddSocketToFlow )( HANDLE QOSHandle, SOCKET Socket, PSOCKADDR DestAddr, QOS_TRAFFIC_TYPE TrafficType, DWORD Flags, PQOS_FLOWID FlowId );
    typedef BOOL ( WINAPI *KfncQOSRemoveSocketFromFlow )( HANDLE QOSHandle, SOCKET Socket, QOS_FLOWID FlowId, DWORD Flags );
};

class StackWinQoSClass
{
private:
    HANDLE QoSHandle;
    QOS_FLOWID QoSFlowId;
    static HMODULE QoSDllHandle;
    static int LastError;

    static KfncQOSCreateHandle winQOSCreateHandle;
    static KfncQOSCloseHandle winQOSCloseHandle;
    static KfncQOSAddSocketToFlow winQOSAddSocketToFlow;
    static KfncQOSRemoveSocketFromFlow winQOSRemoveSocketFromFlow;

public:
    StackWinQoSClass()
    {
        if( !QoSDllHandle )
            Start();

        QOS_VERSION	QoSVer = { 1, 0 };

        QoSHandle = NULL;
        QoSFlowId = 0;

        if( QoSDllHandle != NULL )
            winQOSCreateHandle( &QoSVer, &QoSHandle );
    }

    ~StackWinQoSClass()
    {
        if( QoSHandle != NULL )
        {
            if( QoSFlowId != 0 )
                winQOSRemoveSocketFromFlow( QoSHandle, NULL, QoSFlowId, 0 );

            winQOSCloseHandle( QoSHandle );
            QoSHandle = NULL;
        }
        /*if( QoSDllHandle != NULL )
        {
            FreeLibrary( QoSDllHandle );
            QoSDllHandle = NULL;
        }*/
    }

    int SetQoS( SOCKET Sock, struct sockaddr_storage *Dest )
    {
        if( !QoSHandle )
            return 0;

        if( QoSFlowId != 0 )
        {
            winQOSRemoveSocketFromFlow( QoSHandle, NULL, QoSFlowId, 0 );
            QoSFlowId = 0;
        }

        if( winQOSAddSocketToFlow( QoSHandle, Sock, ( PSOCKADDR )( Dest ), QOSTrafficTypeVoice, QOS_NON_ADAPTIVE_FLOW, &QoSFlowId ) == 0 )
        {
            LastError = ::GetLastError();
            return -1;
        }
        else
            return 1;
    }

    int GetLastError() { return LastError; }

private:
    void Start()
    {
        QoSDllHandle = LoadLibrary( "qwave.dll" ); //could use kdynamic lib

        LastError = 0;
        if( QoSDllHandle != NULL )
        {
            winQOSCreateHandle = ( KfncQOSCreateHandle ) GetProcAddress( QoSDllHandle, "QOSCreateHandle" );
            winQOSCloseHandle = ( KfncQOSCloseHandle ) GetProcAddress( QoSDllHandle, "QOSCloseHandle" );
            winQOSAddSocketToFlow = ( KfncQOSAddSocketToFlow ) GetProcAddress( QoSDllHandle, "QOSAddSocketToFlow" );
            winQOSRemoveSocketFromFlow = ( KfncQOSRemoveSocketFromFlow ) GetProcAddress( QoSDllHandle, "QOSRemoveSocketFromFlow" );

            if( winQOSCreateHandle == NULL    || winQOSCloseHandle == NULL          || 
                winQOSAddSocketToFlow == NULL || winQOSRemoveSocketFromFlow == NULL )
            {
                LastError = ::GetLastError();
                FreeLibrary( QoSDllHandle );
                QoSDllHandle = NULL;
            }
        }
    }
};
#endif


namespace ktools
{
    typedef uint16 kport;

    //Todas as classes de socket DEVEM herdar de alguma forma de KSocketInitializer,
    //pra que seja chamado WSAStartup() corretamente.
    class KSocketInitializer
    {
    public:
        KSocketInitializer();
    };

    class KSocketException: public KException
    {
        kstring err;
    public:
	    ~KSocketException() throw() {};
        static const kstring LastError();

        KSocketException( kstring s ): KException("") {
            err.sprintf( "Socket error: %s - %s", s.c_str(), LastError().c_str() );
        }

        KSocketException( KSOCKET s, const char* file, int line): KException( file, line, "" )
        {
            err.sprintf( "Socket error: Socket=%08X - %s (%s:%d)", s, LastError().c_str(), file, line );
        }

        KSocketException( kstring Msg, KSOCKET s, const char* file, int line): KException( file, line, "" )
        {
            err.sprintf( "Socket error: %s - Socket=%08X - %s (%s:%d)", Msg.c_str(), s, LastError().c_str(), file, line );
        }
               
        const char* what() const throw() {
            return err.c_str();
        }
    };
    #define SocketException( S ) ktools::KSocketException( (S) , __FILE__, __LINE__)
    #define SocketExceptionMsg( MSG, S) ktools::KSocketException( (MSG), (S), __FILE__, __LINE__ )

//    int autoresumeselect( int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds, struct timeval *timeout );

    class KTcpSocket: private KSocketInitializer
    {
    protected:
        int32 TimeOut;
        int32 NoDelay;
        bool Msg_NO_Wait, AllAsync;
        bool Eof_received;

        KSocketPoll * PollData[2];
        bool PollReady[2];

#ifndef KWIN32
        kindex PollSignal[2];
        int SignalFd[2];
#endif

        int NoDelayFlag() {
            return NoDelay;
        }

    public:
        int SetNoDelayFlag()
        {
            NoDelay = 1;

            if (Handle != KINVALID_SOCKET)
            {
                int flag = 1;

                if( setsockopt( Handle, IPPROTO_TCP, TCP_NODELAY, (char *)&flag, sizeof( flag ) ) )
                {
                    KSocketException e = SocketException( Handle );
                    closesocket( Handle );
                    throw e;
                }
            }
        }

    protected:
        void InitVars()
        {
            Handle = KINVALID_SOCKET;
            TimeOut = INFINITE;
            NoDelay = NO_DELAY_FLAG;
            Msg_NO_Wait = false;
            Eof_received = false;
            AllAsync = false;
            InitializeSignalFd();
            InitializeSockPoll();
        }

        void InitializeSockPoll();

        void InitializeSignalFd();
        void FinalizeSignalFd();

        void SignalFinalization();

    private:
#ifdef KWIN32
        StackWinQoSClass WinQoSObject;
#endif

    public:
        KTcpSocket()
        {
            InitVars();
        };

    public:
        virtual ~KTcpSocket(void);

        KSOCKET Handle;

        void SetSOBuf();
        void SetSOReuseAddr();
        void SetTOS( kstring RemoteAddress, uint16 RemotePort );
        void SetAsync();
#ifndef KWIN32              //windows also have this, but we are not using for windows at the moment
        void SetAllAsync();
#endif
        bool IsConnected();
        bool WaitForData( ktime TimeOut );

        virtual void Write( void *Buf, ksize Size ) = 0;
        virtual ksize Read( void *Buf, ksize MaxSize ) = 0;

        //advanced use
        ksize RawWrite( void *Buf, ksize Size );
        ksize RawRead( void *Buf, ksize MaxSize );

        virtual KThreadMutex& GetReadMutex() = 0;
        virtual KThreadMutex& GetWriteMutex() = 0;

        void Close()
        {
            SignalFinalization();
        }

        void SetTimeOut( int32 to ) {
            TimeOut = to;
        }

        bool SafeSelect( bool ForWriting, ktime timeout );

        void GetLocalAddress( kstring &Address, kport& Port);
        void GetRemoteAddress( kstring& Address, kport& Port);
    };

    void SetSocketBlockingMode( KSOCKET s, bool Blocking );
    sockaddr_in GetSocketAddress( kstring Address, uint16 Port );
    kstring GetSocketAddress( const sockaddr_in& addr );
    typedef KTcpSocket KBaseSocket;

#ifdef KWIN32
    int TrySetWinQoS( StackWinQoSClass &winQoSObject, KSOCKET handle, kstring RemoteAddress, uint16 RemotePort );
#endif

    namespace net {
        bool IsIPAddress( kstring Address );
    };
}

#endif
