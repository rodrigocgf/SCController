#ifndef KSERIALIZE_H
#define KSERIALIZE_H

#include "KTypes.h"
#include <stdio.h>
#include "KUtility.h"
#include <string.h>

#if _MSC_VER >= 1400
#pragma warning(disable:4996)
#define snprintf _snprintf
#pragma warning(disable:4996)
#endif

namespace ktools
{

    class kstring;
    class KBufferHolder;
    class KSerializer;
    class KInSerializer;
    class KOutSerializer;
    class KSerializable {
    public:
        KSerializable(): _Urgent(false) {};
        virtual ~KSerializable() {};
        virtual void Serialize( KSerializer &S ) = 0;
        bool _Urgent;
    };

    class KSerializer {

    protected:
        virtual void Serialize( void *p, ksize s ) = 0;

    public:
        virtual ~KSerializer() {};
        virtual void Read( void *p, ksize s ) = 0;
        virtual void Write( void *p, ksize s ) = 0;
        virtual bool In() = 0;
        bool Out() {
            return !In();
        }

    private:
        //fonte disto:
        // http://stackoverflow.com/questions/281725/template-specialization-based-on-inherit-class
        // http://stackoverflow.com/questions/734797/template-specialization-of-function-inside-of-a-templated-class
        // http://www.gotw.ca/publications/mxc++-item-4.htm
        // 
        template<typename _T>
        KSerializer &DoTheOperator( _T &data, Bool2Type<false> ) { //quando o tipo nao for derivado de KSerializable
            Serialize( &data, sizeof( _T ) );
            return *this;
        }

        template<typename _T>
        KSerializer &DoTheOperator( _T &data, Bool2Type<true> ) { //quando o tipo for derivado de KSerializable
            data.Serialize( *this );
            return *this;
        }

    public:
        template<typename _T> 
        KSerializer &operator &( _T &data ) {
            return DoTheOperator( data, Bool2Type<IsDerivedFrom<_T, KSerializable>::value>() );
        };

        KSerializer &operator &(KSerializable *s) {
            s->Serialize( *this );
            return *this;
        }

        KSerializer &operator &(char *&p) {
            ksize s;
            if( In() )
            {   
                Serialize( &s, sizeof s );
                if( p )
                    delete[] p;
                if( s > 1024 ) {
                    p = new char[ 100 ];
                    snprintf( (char*)p, 100, "trying to read a string bigger than 1024" );
                    return *this;
                }
                p = new char[ s ];
                Serialize( p, s );
            }
            else
            {
                s = (ksize) strlen( (char*) p ) + 1;
                Serialize( &s, sizeof s );
                Serialize( p, s );
            }
            return *this;
        }

        KSerializer &operator &(byte *&p) {
            char *t = (char *) p;
            operator&(t);
            p = (byte *) t;
            return *this;
        }

        // body is in the KPCH.cpp file
        KSerializer &operator &( KBufferHolder &b ); 
        KSerializer &operator &( kstring &s ); 
    };

    class KOutSerializer : public KSerializer {
    public:
        bool In() {
            return false;
        }
        void Write( void *p, ksize s ) {
            if( s == 0 )
                return;
            Serialize( p, s );
        };
        void Read( void *, ksize ) {}
    };
    class KInSerializer : public KSerializer {
    public:
        bool In() {
            return true;
        }
        void Read( void *p, ksize s ) {
            if( s == 0 )
                return;
            Serialize( p, s );
        };
        void Write( void *, ksize ) {}
    };

    class KSerialSizeCalculator : public KOutSerializer
    {
        ksize _Size;
        KSerialSizeCalculator() {
            _Size = 0;
        }
    public:
        void Serialize( void *, ksize s ) {
            _Size += s;
        }
        bool In() {
            return false;
        }
        static ksize GetSize( const KSerializable &s ) {
            KSerialSizeCalculator calc;
            const_cast<KSerializable&>(s).Serialize(calc);
            return calc._Size;
        }
    };

    template< class T > //Por favor s� utilizar em tipos simples.
    class KPlainData:  public ktools::KSerializable
    {
    public:
        T Value;
        operator T() { return Value; };
        KPlainData( T val): Value(val) {};
        KPlainData() {};
        T* operator->(){ return &Value; };
        T& operator*() { return Value; };
    private:
        void Serialize( ktools::KSerializer &S ) {
            S & Value;
        }
    };

    class KEmptyData:  public ktools::KSerializable
    {
        bool x;
    private:
        void Serialize( ktools::KSerializer & ) {
        }
    };

    extern KEmptyData null;
}

#endif
