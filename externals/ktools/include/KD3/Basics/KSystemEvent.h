#pragma once

#include "KString.h"
#include "KMutex.h"

#ifndef KWIN32

# undef INFINITE
# define INFINITE 0xFFFFFFFF 

# include <errno.h>
# include <string.h>

#endif

namespace ktools
{
    class KSystemEvent
    {
        KHandle Handle;
    public:
        KSystemEvent( const char *name = NULL );
        ~KSystemEvent();
        void Pulse();
        bool Wait( ktime timeout = INFINITE );
        // todo
        /*
        static KSystemEvent Open( const char *name ) {
        }
        */
    };

#ifdef WIN32
    class KBarrier
    {
        ksize Current, InitValue;
        KThreadMutex Mutex;
        HANDLE Event;
    public:
        KBarrier( ksize init = 1 ) : Current(init), InitValue(init)
        {
            Event = CreateEvent( NULL, true, 0, NULL );
        }

        ~KBarrier()
        {
            CloseHandle( Event );
        }

        const char * Reset( ksize init = 0 )
        {
            KContextMutex m(Mutex);
            if( init != 0 )
                InitValue = init;
            Current = InitValue;
            ResetEvent( Event );
            return NULL;
        }

        const char * Wait()
        {
            mutex_context(Mutex) 
            {
                if( Current > 0 )
                {
                    if( --Current == 0 )
                    {
                        SetEvent( Event );
                        return NULL;
                    }
                }
                else
                    return NULL;
            }
            WaitForSingleObject( Event, INFINITE ); 
            return NULL;
        }
    };
#else
# ifdef K_UCLIBC
    class KBarrier
    {
        ksize Current, InitValue;
        pthread_mutex_t PMutex;
        pthread_cond_t  PEvent;
    public:
        KBarrier( ksize init = 1 ) : Current(init), InitValue(init)
        {
            pthread_mutex_init(&PMutex, NULL);
            pthread_cond_init(&PEvent, NULL);
        }

        ~KBarrier()
        {
            pthread_mutex_destroy(&PMutex);
            pthread_cond_destroy(&PEvent);
        }

        const char * Reset( ksize init = 0 )
        {
            pthread_mutex_lock(&PMutex);
            if( init != 0 )
                InitValue = init;
            Current = InitValue;
            pthread_mutex_unlock(&PMutex);
            return NULL;
        }

        const char * Wait()
        {
            pthread_mutex_lock(&PMutex);

            if( Current > 0 )
            {
                if( --Current == 0 )
                {
                    pthread_cond_broadcast(&PEvent);
                    pthread_mutex_unlock(&PMutex);
                    return NULL;
                }
            }
            else
            {
                pthread_mutex_unlock(&PMutex);
                return NULL;
            }

            pthread_cond_wait(&PEvent, &PMutex);
            pthread_mutex_unlock(&PMutex);
            return NULL;
        }
    };
# else
    class KBarrier
    {
        pthread_barrier_t Barrier;
        ksize InitValue;
    public:
        KBarrier( ksize init = 1 )
        : InitValue(init)
        {
            pthread_barrier_init( &Barrier, NULL, init );
        }

        ~KBarrier()
        {
            pthread_barrier_destroy( &Barrier );
        }

        const char * Reset(ksize init = 0)
        {
            if( init != 0 )
                InitValue = init;

            if (pthread_barrier_init( &Barrier, NULL, InitValue ) == 0)
                return NULL;

            return strerror(errno);
        }

        const char * Wait()
        {
            const int ret = pthread_barrier_wait( &Barrier );

            if ((ret != 0) && (ret != PTHREAD_BARRIER_SERIAL_THREAD))
                return strerror(errno);

            return NULL;
        }
    };
# endif
#endif
};
