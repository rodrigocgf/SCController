#pragma once

#include "KString.h"
#include <vector>
#ifndef KWIN32
#   include <signal.h>
#else
#   include <WinSock2.h>
#   include <Windows.h>
#endif

namespace ktools {
namespace service {

    class KServiceBase
    {

        //M�todos que podem ser sobrescritos pelas classes!

        virtual bool OnStart(const std::vector<kstring> &Args);
        virtual bool OnStop();
        virtual void OnPause(); //s� no windows
        virtual void OnContinue(); //so no windows
        virtual void OnShutDown();//s� no windows
    public:
        kstring ErrorStr;

        KServiceBase(const kstring &ServiceName);
        KServiceBase(const kstring &ServiceName, const kstring &DisplayName);
        virtual ~KServiceBase();

        /** Este m�todo deve ser chamado de dentro da fun��o main da 
         * aplica��o.
         *  Quando chamado, este m�todo s� retorna para a aplica��o
         * ap�s o t�rmino do servi�o.
         */
        int Main(int argc, char **argv);

        const kstring& ServiceName() { return _ServiceName; };
        void DisplayName( kstring Name ) { _DisplayName = Name; };
        const kstring& DisplayName() { return _DisplayName; };

        bool InstallService();
        bool UninstallService();
    private:
        kstring _ServiceName;
        kstring _DisplayName;

        bool Debugging;

#ifdef KWIN32
        HANDLE                  SvcStopEvent;
        SERVICE_STATUS          SvcStatus; 
        SERVICE_STATUS_HANDLE   SvcStatusHandle;
        std::vector<kstring>    _Dependencies;

        void ReportStatus( DWORD dwCurrentState, DWORD dwWin32ExitCode, DWORD dwWaitHint);
        void LogWithLastError(const char* szFunction);
        void Log( int level, char * fmt, ... );

    public:
        void _SvcCtrlHandler( DWORD dwCtrl );
        void _SvcMain(int argc, char **argv);


        void AddDependency(kstring dependencyName);
#else

    private:
        int StartDaemon(int argc, char**argv);
        int StopDaemon();


        void SignalHandler();

        bool TestPidProc();
        bool TestPidFile(); // bool print=true, bool quit=true);
        void CreatePidFile(pid_t pid = 0);
        void RemovePidFile();

        void ActiveCoredump();

        void Log(int, const kstring& str );

        std::map<int, sighandler_t> _HandlerMap;
        kstring _PidFile;

    public:
        void _StopRequest();
#endif

    public:
        static KServiceBase* _Instance( KServiceBase* inst = NULL ) {
            static KServiceBase* Instance = NULL;
            if( Instance && inst )
                throw std::logic_error( "Only once instance of KServiceBase class is permitted!" );
            else if( !Instance )
                Instance = inst;

            return Instance;
        }
    };


}; //namespace Service
}; //namespace ktools
