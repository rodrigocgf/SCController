#ifndef BASICS_KTYPES_H
#define BASICS_KTYPES_H
#if defined( _WINDOWS ) || defined( _Windows ) || defined( _WIN32 )
	//#ifndef KWIN32
	#define KWIN32 1
	//#endif
#endif

// Khomp defined types
#ifdef KWIN32
	typedef __int64				    int64;
	typedef unsigned __int64	    uint64;
	#define Kstdcall __stdcall
#else
    #include <unistd.h>
    //
	typedef long long				int64;
	typedef unsigned long long		uint64;
	#define Kstdcall 
#endif

typedef void* ( *VoidConverter )( void * );
typedef void * KHandle;

typedef int					int32;
typedef unsigned int		uint32;

typedef uint64			    intptr;
typedef intptr              stackint;

typedef short int			int16;
typedef unsigned short int	uint16;

typedef signed char		    int8;
typedef unsigned char		uint8;

typedef unsigned char		byte;
typedef char				sbyte;

typedef double				float64;
typedef float				float32;

typedef uint32 ksize;
typedef uint32 kinterval;
typedef int32  koffset;
typedef const char * kpchar;
typedef uint32 ktime;
typedef uint32 kindex;
typedef uint32 kbitset;
#endif

