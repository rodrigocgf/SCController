#pragma once

#include "KSocketDefs.h"
#include "KClientSocket.h"
#include "KThread.h"

#include "KSocketPoll.h"

namespace ktools
{
    //typedef void ( Kstdcall KClientCallback )( KServerSocket *Server, KSOCKET Socket );
    class KServerSocket: public KSocketInitializer
    {
    private:
        void Prepare();
        bool DONTCreateThread;

    protected:
        static int32 Kstdcall StartListenerThread( void *p );
        static int32 Kstdcall ClientThread( void * );

        int32 InternalListen();

        volatile bool Listening;
        KSOCKET Handle;
        kport Port;
        KSemaphore JoinSem;

        KSocketPoll PollData;
        bool        PollReady;

#ifndef KWIN32
        kindex      PollSignal;
        volatile int SignalFd[2];
#endif

        int NoDelayFlag() {
            return NO_DELAY_FLAG;
        }

        virtual void OnClientConnect( KClientSocket *Socket ) = 0;
        virtual void OnFail() {}

    public:
        KServerSocket( bool NoThread = false );
	    virtual ~KServerSocket(void);

        void Listen( kport p, bool ExecInSameThread = false ) {
            Port = p;
            Prepare();
            if( ExecInSameThread )
                InternalListen();
            else
                KThread::StartThread( ( void * )StartListenerThread, this, 0, false, true );
        }

	    bool IsListening() {
		    return ( Listening );
	    }

        void Close() {
            if(Handle == KINVALID_SOCKET) return;
            
            if( Listening )
            {
                Listening = false;
#ifndef KWIN32
                // No Linux precisamos escrever em um pipe para acordar e 
                //  sinalizar que fecharemos o socket
                if (SignalFd[0] != KINVALID_SOCKET)
                {
                    const char buf[1] = { 0 };
                    while (write(SignalFd[1], buf, sizeof(buf)) == -1 && errno == EINTR) {};
                }
#else
                // No Windows, a thread acorda quando fechamos o socket
                KSOCKET tmp = Handle;
                Handle = KINVALID_SOCKET;
                closesocket( tmp );
#endif
                JoinSem.Wait(5000);
            }
            
            if( Handle != KINVALID_SOCKET)
            {
                KSOCKET tmp = Handle;
                Handle = KINVALID_SOCKET;
                closesocket( tmp );
            }

#ifndef KWIN32
            int tmp0 = SignalFd[0];
            int tmp1 = SignalFd[1];

            SignalFd[0] = KINVALID_SOCKET;
            SignalFd[1] = KINVALID_SOCKET;

            if (tmp0 != KINVALID_SOCKET) closesocket(tmp0);
            if (tmp1 != KINVALID_SOCKET) closesocket(tmp1);
#endif
        }

        kport GetPort() {
            return Port;
        }
    };
}
