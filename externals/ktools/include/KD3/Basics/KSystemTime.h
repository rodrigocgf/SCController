#pragma once
#ifndef _KD3_KSYSTEMTIME_H_
#define _KD3_KSYSTEMTIME_H_

#include "KTypes.h"
#include "KString.h"

namespace ktools {
namespace time {
    struct KSystemTime
    {
        uint16 Year;
        byte Month;
        byte Day;
        byte WeekDay;
        byte Hour;
        byte Minute;
        byte Second;
        uint16 Milli;
    };

    void Delay( uint32 Millis );
    KSystemTime Now();

    /* Returns the number of seconds since epoch (00:00, Jan 1, 1970 UTC) */
    ktime UnixTime();

    /* Returns a monotonic counter in milliseconds (it wraps every 49 days) */
    ktime GetTick();

    KSystemTime FromUnixTime( ktime unixtime );
    ktime FromSystemTime( const KSystemTime& time );

    int32 TimeZoneBias();

    kstring Format( const char* fmt );
    kstring Format( const KSystemTime& time, const char* fmt );
    kstring Format( ktime time, const char* fmt );

} //namespace time
//backward compatibility
using time::KSystemTime;
using time::Delay;
using time::Now;
} //namespace ktools

#endif

