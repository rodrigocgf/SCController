#pragma once

#ifdef WIN32
#include <winsock2.h>
#include <windows.h>
#endif


#include "KTypes.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
//#include <io.h>
#include <fcntl.h>


#include "KSerialize.h"
#include "KException.h"
#include "KMutex.h"
#include "KSemaphore.h"
#include "KSharedMemory.h"
#include "KString.h"
#include "KSystemEvent.h"
#include "KProcess.h"
#include "KFile.h"
#include "KBuffer.h"
#include "KSystemTime.h"
#include "KSocketDefs.h"
#include "KServerSocket.h"
#include "KConfiguration.h"
#include "KTimer.h"
#include "KPortManager.h"


#include "KServiceBase.h"

