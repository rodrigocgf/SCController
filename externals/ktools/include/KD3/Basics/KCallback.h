#pragma once

#include "KGeneralFunctionPointer.h"

namespace ktools {

template<typename Result = void, typename Arg = void *>
class KCallbackList
{
public:
    typedef ktools::KUnaryFunction<Arg, Result> CallbackFunc_t;

    virtual ~KCallbackList() {
        KContextMutex m(Mutex);
        typename Callbacks_t::iterator it = Callbacks.begin();
        for(; it != Callbacks.end(); ++it )
            delete *it;
    }

    /**  Registra a callback, mas somente uma vez, duplicatas ser�o descartadas.
         Utilize assim:
             cbl.RegisterCallback( MakeUnaryFunction( &obj, Class::Method ) );

         Obs:Toma posse do ponteiro e deleta ele depois
    */ 
    void RegisterCallback( CallbackFunc_t* func ) {
        KContextMutex m(Mutex);
        if( FindCallback( func ) == Callbacks.end() )
            Callbacks.push_back( func );
        else
            delete func;
    }

    /**  Remove a callback da lista.
         Utilize assim:
             cbl.UnregisterCallback( MakeUnaryFunction( &obj, Class::Method ) );
         Obs: Toma posse do ponteiro e deleta ele depois 
    */ 
    void UnregisterCallback( CallbackFunc_t* func ) {
        KContextMutex m(Mutex);
        UnregisterCallback( func );
        delete func;
    }

    /** Executa as callbacks registradas, na ordem de registro. */
    void Call( Arg arg ) {
        KContextMutex m(Mutex);
        typename Callbacks_t::iterator it = Callbacks.begin();
        for(; it != Callbacks.end(); ++it )
        {
            (**it)(arg);
        }
    }

private:
    //typedef ktools::KFunction<void> CallbackFunc_t;
    typedef std::list< CallbackFunc_t* > Callbacks_t;

    Callbacks_t Callbacks;
    ktools::KThreadMutex Mutex;

    /**  chamar j� com lock no mutex */
    void UnregisterCallback( CallbackFunc_t func ) {
        typename Callbacks_t::iterator found = FindCallback( func );
        if( found != Callbacks.end() )
        {
            delete *found;
            Callbacks.erase( found );
        }
    }


    /**  chamar j� com lock no mutex */
    typename Callbacks_t::iterator FindCallback( CallbackFunc_t* func )
    {
        typename Callbacks_t::iterator it = Callbacks.begin();
        for(; it != Callbacks.end(); ++it )
        {
            if( **it == *func )
                break;
        }
        return it;
    }
};




}; //namespace ktools