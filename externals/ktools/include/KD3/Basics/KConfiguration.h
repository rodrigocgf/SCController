#pragma once

//#include "../kd3.h"
#include <list>
#include <algorithm>
#include <memory>
#include "yaml.h"
#include <KLogger.h>
#include "KBaseException.h"
#include "../../KHostSystem.h"
#include "KUtility.h"
#include <KD3/Basics/KMutex.h>

namespace config
{
    struct KConfLog
    {
        static KLogger ConfigLog;
    };


    typedef void (*Callback)( void* );

    void RequestConfigurationReload();

    class KReloadable;
    //class KIniConfigReader;
    class KConfigReloader
    {
        static KConfigReloader *_Instance;
        friend class KReloadable;
        KConfigReloader()
            : ReloadCallback(NULL)
            , ReloadCallbackParam(NULL)
        {};
        typedef std::list<KReloadable*> KReloadableList;
        KReloadableList Items;
        inline void Register(KReloadable &r) { Instance()->Items.push_back(&r); }
        inline void Unregister(KReloadable &r) { Instance()->Items.remove(&r); }
    public:
        Callback ReloadCallback;
        void* ReloadCallbackParam;
        static Callback SetReloadCallback( Callback c, void * parm );
        static void Reload(const ktools::kstring &File = "");
        static void Reload(KReloadable &r, bool JustValidate = false);
        static void Validate();
        static void AsyncReloadAll(); //Faz a recarga de verdade

        static KConfigReloader * Instance() {
            if( !_Instance)
                _Instance = new KConfigReloader();
            return _Instance;
        }

        static void FreeInstance() {
            KConfigReloader* x = _Instance;
            _Instance = NULL;
            delete x;
        }
    };

    typedef YAML::Node KLoader;
    typedef YAML::Iterator KLoadIterator;
    typedef YAML::Exception KLoadException;

    typedef void (*KReloadCallback)(KReloadable *r);

    /** Todas as estruturas de configura��o devem herdar de KReloadable, esta classe
      * ser� a interface para o recarregamento das configura��es.
      *
      * Ap�s implentada a classe deve ser utilizada com o temlate KConfig<T> pra
      * assegurar que as configura��es ser�o chamadas na primeira vez e tamb�m
      * que s� h� um objeto desta classe. */
    class KReloadable
    {
    private:
        /** Este m�todo ser� chamado sempre que for necess�rio carregar/recarregar
          * as configura��es, a classe final dever� se encarregar de verificar a
          * validade dos dados carregados e carregar os novos dados ou n�o. */
        virtual void LoadConfig( const KLoader &) = 0;

        virtual std::auto_ptr<KReloadable> CloneForValidation() = 0;

        /** M�todo chamado somente na c�pia do CloneForValidation()
          * � usado para efetuar valida��es que n�o ocorrem no
          * LoadConfig, por exemplo. */
        virtual void AdditionalValidations() {};

    public:
        KReloadable(ktools::kstring file, ktools::kstring document ="")
            : File(file)
            , Document(document)
        {
            KConfigReloader::Instance()->Register(*this);
        };

        KReloadable& operator=(const KReloadable& r)
        {
            const_cast<ktools::kstring&>(File).assign(r.File);
            const_cast<ktools::kstring&>(Document).assign(r.Document);
            return *this;
        }

        virtual ~KReloadable()
        {
            KConfigReloader::Instance()->Unregister(*this);
        }

    private:
        friend class KConfigReloader;
        const ktools::kstring File;
        const ktools::kstring Document;
        //typedef std::list<KReloadCallback> KCallbacks;
        //KCallbacks  Callbacks;
    };

    //Esse EnableIf pra permimir que este template s� seja usado com tipos derivados de KReloadable
    template<class T, typename ktools::EnableIf<ktools::IsDerivedFrom<T, KReloadable>, int >::type dummy = 0>
    class KConfig // Pimpl idiom (pointer to implementation), ou Cheshire cat idiom (fonte:wikipedia e stackoverflow)
    {
        static T * object;
        static ktools::KThreadMutex mutex;

    public:
        typedef T Type;
        KConfig()
        {
            ktools::KContextMutex m( mutex );
            if( !object )
            {
                object = new Type();
                Reload();
            }
        }
        void Reload()
        {
            ktools::KContextMutex m( mutex );
            KConfigReloader::Reload( static_cast<KReloadable&>(*object) );
        }
        T* operator->()  { return object; }
        T& operator*() { return *object; }
    };
    template<class T, typename ktools::EnableIf<ktools::IsDerivedFrom<T, KReloadable>, int>::type dummy>
    T *KConfig<T, dummy>::object = NULL;
    template<class T, typename ktools::EnableIf<ktools::IsDerivedFrom<T, KReloadable>, int>::type dummy>
    ktools::KThreadMutex KConfig<T, dummy>::mutex;

    typedef KTemplateException<KReloadable> KConfigException;

    inline kstring FormatMark( const YAML::Mark &m )
    {
        return kstring( "line=%d,col=%d", m.line+1, m.column+1 );
    }

    //-------------------------------------------
    //Load helpers!
    template<class T>
    inline void Load( const KLoader& loader, T &value)
    {
        loader >> value;
    }

    template <>
    inline void Load<>( const KLoader& loader, kstring& value)
    {
        loader >> value;
        if( value == "~" )
            value.clear();
    }

    template <>
    inline void Load<>( const KLoader& loader, std::string& value)
    {
        loader >> value;
        if( value == "~" )
            value.clear();
    }

    template<class C>
    bool LoadList( const KLoader &loader, C& container, bool append = false )
    {
        if( !append )
            container.clear();
        for( KLoadIterator it = loader.begin(); it != loader.end(); ++it )
        {
            typename C::value_type t;
            Load( *it, t );
            container.push_back( t );
        }
        return true;
    }

    template<class C>
    bool LoadList( const KLoader &loader, const char *Name, C& container, bool append = false )
    {
        const KLoader* n = loader.FindValue(Name);
        if( !n )
        {
            KConfLog::ConfigLog.Trace("Could not load list '%s'(%s).", Name, FormatMark(loader.GetMark()).c_str() );
            return false;
        }
        return LoadList( *n, container, append );
    }

    template<class K, class V>
    bool LoadMap( const KLoader &loader, std::map<K,V> &map )
    {
        map.clear();
        for( KLoadIterator item = loader.begin(); item != loader.end(); ++item )
        {
            for( KLoadIterator pair = item->begin(); pair!= item->end(); ++pair )
            {
                K k;
                Load( pair.first(), k );
                V v;
                Load( pair.second(), v );
                map[k] = v;
            }
        }
        return true;
    }

    template<class K, class V>
    bool LoadMap( const KLoader &loader, const char *Name, std::map<K,V> &map )
    {
        const KLoader *n = loader.FindValue(Name);
        if( !n )
        {
            KConfLog::ConfigLog.Trace("Could not load map '%s'(%s).", Name, FormatMark(loader.GetMark()).c_str() );
            return false;
        }
        return LoadMap( *n, map );
    }

    template<class T, class Def>
    inline bool Load( const KLoader& loader, const char *Name, T &value, const Def& def, bool logIfDefault = true )
    {
        try
        {
            const KLoader* l = loader.FindValue(Name);
            if( l )
            {
                Load( *l, value );
                return true;
            }
        }
        catch( const YAML::InvalidScalar & ) {}

        value = def;
        if( logIfDefault )
            KConfLog::ConfigLog.Trace("Could not load '%s'(%s) using default value (%s).", Name, FormatMark(loader.GetMark()).c_str(), to_string(def).c_str() );
        else
		{
            KLogger( klogConfig, klogOptionalValues, "CFG-OPT", "ktools", klogPrefixOnHeader | klogAppendProcName ).Trace(
                "Could not load optional config '%s'(%s), using default value (%s)", Name, FormatMark(loader.GetMark()).c_str(), to_string(def).c_str() );
        }
        return false;

    }

    template<class T>
    inline bool Load( const KLoader& loader, const char *Name, T &value)
    {
        const KLoader* l = loader.FindValue(Name);
        if( !l )
        {
            KConfLog::ConfigLog.Trace("Could not load '%s'(%s), no default.", Name, FormatMark(loader.GetMark()).c_str() );
            return false;
        }
        Load( *l,  value );
        return true;
    }

    //---------------------------------
    // Validation helpers
    void ValidatePort(kindex value, const kstring& name );
}


