#pragma once

#include "KString.h"
#include <KBaseException.h>

namespace ktools {
namespace zip {

    void ZipFile(const kstring& in, const kstring& out);
    void ZipFile(const kstring& in);

    struct input_error;
    typedef KTemplateException<input_error> InputException;
    struct output_error;
    typedef KTemplateException<output_error> OutputException;

};
};
