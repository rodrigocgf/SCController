#pragma once

#include "KString.h"

namespace ktools {
namespace file {

kstring FullPath(const kstring& RelativePath);
kstring DirectoryPart( const kstring& file );
kstring FilenamePart( const kstring& file );
kstring UniquifyFilename( const kstring& path );
bool Exists(const kstring& file);

template<typename BinaryFunction>
ksize EnumerateFiles( const kstring& Path, const kstring& extension, BinaryFunction& Action, bool recursively = true )
{
    sbyte filename[KMAX_PATH];
    int32 success;
    kstring mypath = Path;// + KDIR_SEP_STR "*";
    KHandle findhandle = KHostSystem::FindFirst( mypath.c_str(), NULL, filename, &success );
    ksize counter = 0;

    if( success != ksSuccess && !findhandle )
    {
        //fprintf( stderr, "FindFirst for '%s' returned %d\n", mypath.c_str(), success );
        return counter;
    }

    do
    {
        if( kstring(".") == filename || kstring("..") == filename )
            continue;

        mypath.sprintf( "%s%c%s", Path.c_str(), KDIR_SEP_CHR, filename );
        mypath = FullPath( mypath );
        struct stat fileinfo;
        if( stat( mypath.c_str(), &fileinfo) )
        {
            //fprintf( stderr, "stat file '%s' returned error: %s\n", mypath.c_str(), strerror(errno) );
            continue;
        }

        try
        {
            if( fileinfo.st_mode & S_IFDIR )
            {
                ++counter;
                Action( mypath, fileinfo );
                if (recursively)
                    counter += EnumerateFiles( mypath, extension, Action, recursively );
            }
            else if( !extension.size() || KHostSystem::ExtensionCompare( filename, extension.c_str() ) )
            {
                ++counter;
                Action( mypath, fileinfo );
            }
        }
        catch( const std::exception &e )
        {
            KHostSystem::FindClose( findhandle );
            throw KBaseException("Error opening file %s (%s)", Path.c_str(), e.what() );
        }

    }
    while( KHostSystem::FindNext( findhandle, NULL, filename ) == ksSuccess );

    KHostSystem::FindClose( findhandle );

    return counter;
}

}
}
