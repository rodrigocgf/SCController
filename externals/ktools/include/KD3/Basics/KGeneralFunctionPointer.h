#pragma once


/*
    Usar quando quiser guardar ponteiros de fun��es que podem ser func�es 
  normais ou membros de objetos.

  tipo, um mapa de kstring pra fun��o que recebe string e retorna int32:

      std::map<kstring, KUnaryFunction<kstring,int32>* > Mapa

  Fun��o normal:
      Declara��o:
          int32 Func(kstring s);
      uso:
          Mapa[ "Func" ] = MakeUnaryFunction( Func );

  Fun��o membro:
      Declara��o:
          class A {
          public:
              int32 Func(kstring s);
          }
      Uso:
          A a;
          Mapa[ "Mem_Func" ] = MakeUnaryFunction( &a, &A::a );

*/




//http://stackoverflow.com/questions/2147722/how-to-define-a-general-member-function-pointer
#include <typeinfo>

namespace ktools {

/* Para m�todos/funcoes sem parametros */
template<class Res>
class KFunction
{
    virtual bool IsEqual( const KFunction & ) = 0;
public:
    Res operator()(void) { return Call(); };
    virtual Res Call() = 0;

    bool operator==( const KFunction&o ) {
        return (typeid(*this) == typeid(o) ) && IsEqual( o );
    }
};

template<class Res>
class KBasicFunction: public KFunction<Res>
{
    Res (*function)();
    bool IsEqual( const KFunction<Res>& o ) { 
        const KBasicFunction<Res> &other = dynamic_cast<const KBasicFunction<Res>& >(o);
        return function == other.function;
    }
public:
    KBasicFunction( Res(*_func)() )
        : function(_func) {}
    virtual Res Call() { return (*function)(); };
};

template<class AnyClass, class Res>
class KClassFunction: public KFunction<Res>
{
    Res (AnyClass::*function)();
    AnyClass* object;
    bool IsEqual( const KFunction<Res>& o ) { 
        const KClassFunction<AnyClass,Res> &other = dynamic_cast<const KClassFunction<AnyClass,Res>& >(o);
        return  object == other.object && function == other.function;
    }

public:
    KClassFunction(AnyClass* _obj, Res(AnyClass::*_func)())
        : function(_func), object(_obj) {}
    virtual Res Call() {return (*object.*function)(); };
};

template<class Res>
KBasicFunction<Res>* MakeFunction( Res(*func)() )
{
    return new KBasicFunction<Res>(func);
}

template<class AnyClass, class Res>
KClassFunction<AnyClass, Res>* MakeFunction( AnyClass* obj, Res(AnyClass::*func)() )
{
    return new KClassFunction<AnyClass, Res>( obj, func );
}




/* Para m�todos/fun��es com um par�metro */
template <class Arg, class Res>
class KUnaryFunction
{
    virtual bool IsEqual( const KUnaryFunction & ) = 0;
public:
    Res operator()(Arg a){ return Call(a); };
    virtual Res Call(Arg) = 0;

    bool operator==( const KUnaryFunction&o ) {
        return (typeid(*this) == typeid(o) ) && IsEqual( o );
    }
};

template<class Arg, class Res>
class KBasicUnaryFunction: public KUnaryFunction<Arg,Res>
{
    Res (*function)(Arg);
    bool IsEqual( const KUnaryFunction<Arg,Res>& o ) { 
        const KBasicUnaryFunction<Arg,Res> &other = dynamic_cast<const KBasicUnaryFunction<Arg,Res>& >(o);
        return function == other.function;
    }
public:
    KBasicUnaryFunction( Res(*_func)(Arg) )
        : function(_func) {}
    virtual Res Call(Arg a) { return (*function)(a); }
};

template<class AnyClass, class Arg, class Res>
class KClassUnaryFunction: public KUnaryFunction<Arg,Res> {
    Res (AnyClass::*function)(Arg);
    AnyClass* object;
    bool IsEqual( const KUnaryFunction<Arg,Res>& o ) { 
        const KClassUnaryFunction<AnyClass,Arg,Res> &other = dynamic_cast<const KClassUnaryFunction<AnyClass,Arg,Res>& >(o);
        return object == other.object && function == other.function;
    }
public:
    KClassUnaryFunction(AnyClass* _obj, Res(AnyClass::*_func)(Arg))
        : function(_func), object(_obj) {};
    virtual Res Call(Arg a) { return (*object.*function)(a); };
};

template< class Arg, class Res >
KBasicUnaryFunction<Arg,Res>* MakeUnaryFunction( Res(*func)(Arg) )
{
    return new KBasicUnaryFunction<Arg,Res>(func);
}

template<class AnyClass, class Arg, class Res>
KClassUnaryFunction<AnyClass, Arg, Res>* MakeUnaryFunction( AnyClass* obj, Res(AnyClass::*func)(Arg) )
{
    return new KClassUnaryFunction<AnyClass, Arg, Res>( obj, func );
}

}; // namespace ktools