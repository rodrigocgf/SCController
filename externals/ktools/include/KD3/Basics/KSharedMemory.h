#pragma once

#include "KSerialize.h"
#include "KMutex.h"

#ifdef KWIN32
#define SYS_PAGE_SIZE 0x1000
typedef KHandle KMemoryFileHandle;
#else
#include <sys/mman.h>
#include <sys/stat.h>
#define SYS_PAGE_SIZE 0x1000
//#define SYS_PAGE_SIZE sysconf(_SC_PAGE_SIZE)
typedef int32 KMemoryFileHandle;
#endif

namespace ktools
{
    class KSharedMemory 
    {
    protected:
        byte *Pointer;
        uint32 Capacity; // bytes
        uint32 RealCapacity; // bytes
        KProcessMutex *Mutex;
        KMemoryFileHandle SharedDescriptor;
        kstring Name;
        bool Owner;
#ifndef KWIN32
        uint32 *UseCount; //Verificar se no Windows ocorre o mesmo problema...
#endif
    public:
        KSharedMemory(std::string name, uint32 size, bool owner = true);
        ~KSharedMemory(void);
        void Read( koffset Offset, void *Data, ksize Size );
        void Write( koffset Offset, const void *Data, ksize Size );

        uint32 GetCapacity() {
            return Capacity;
        }
        
        KProcessMutex &GetMutex() {
            return *Mutex; 
        }

        const char *GetName() {
            return Name.c_str();
        }

        void *GetPointer() {
            return Pointer;
        }
        
        static const ksize DefaultMemSize = SYS_PAGE_SIZE * 4;
    };

}
