#pragma once
#include "KSocketDefs.h"
#include "KException.h"

namespace ktools {


class KPortManager
{
public:
    enum Flags {
        None            = 0x00,
        Descending      = 0x01,
        RoundRobin      = 0x02, //se nao for round-robin, d� exce��o quando acaba o range
    };

    KPortManager(){
        Initialize();
    }

    KPortManager( kbitset flags ){
        Initialize( 0,0, flags);
    }

    static const int MIN_PORT_RANGE = 100;

    void Configure( ktools::kport first, ktools::kport last ) 
    {
        if( last - first < MIN_PORT_RANGE )
            throw KBaseException( "Invalid port range %d-%d (<%d)", first, last, MIN_PORT_RANGE );

        if( NextPort != 0 )
        {
            if( NextPort > last )
                throw KBaseException( "It is not possible to change port range" );
            if( NextPort < first )
                NextPort = first;
        }
        FirstPort = first;
        LastPort = last;
        if( IsFlagSet(Descending) )
            NextPort = last;
        else
            NextPort = first;
    }

    ktools::kport GetFreePort() {
        return InternalGetNextPort();
    }

protected:
    bool IsFlagSet( Flags f ) { return (MyFlags & (kbitset)f) == (kbitset)f; }

    void Initialize( kport first = 0, kport last = 0, kbitset flags = None )
    {
        FirstPort = first;
        LastPort = last;
        MyFlags = flags;
        if( IsFlagSet(Descending) )
            NextPort = last;
        else
            NextPort = first;
    };

    ktools::kport InternalGetNextPort()
    {
        ktools::kport ret = NextPort;
        if( IsFlagSet(Descending) )
        {
            if( NextPort-- < FirstPort )
            {
                if( IsFlagSet(RoundRobin) )
                    ret = NextPort = LastPort;
                else
                    throw KBaseException( "KPortManager overflow (%d-%d[0x%X])", FirstPort, LastPort, MyFlags );
            }
        }
        else
        {
            if( NextPort++ > LastPort )
            {
                if( IsFlagSet(RoundRobin) )
                    ret = NextPort = FirstPort;
                else
                    throw KBaseException( "KPortManager overflow (%d-%d[0x%X])", FirstPort, LastPort, MyFlags );
            }
        }
        return ret;
    }
private:
    ktools::kport FirstPort;
    ktools::kport LastPort;
    ktools::kport NextPort;
    kbitset MyFlags;

};

}; //namespace ktools
