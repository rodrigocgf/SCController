#pragma once

#include "KString.h"
#include <map>

#include <sys/stat.h>

namespace ktools {
namespace tar {
    class KTarFile
    {
        kstring _Filename;
        FILE *_fp;
        bool _Terminated;
        //typedef std::map<kstring, off_t> FileList_t;
        //FileList_t _FileList;
    public:
        KTarFile(const kstring& file, bool output = true);
        ~KTarFile();

        //template<typename ForwardIterator>
        //KTarFile( ForwardIterator first_file, ForwardIterator last_file);
        void Append( const kstring& path );
        void AppendTree( const kstring& path );
        void Terminate();



        static const ksize KTAR_BLOCK_SIZE = 512;
        static const ksize KTAR_BUFFER_SIZE = 20 * KTAR_BLOCK_SIZE;

        static void int2oct(uint32 num, sbyte* oct, size_t oct_len);
        static void int2oct_nonull( uint64 num, sbyte* oct, size_t oct_len );
        static uint32 oct2int(sbyte *oct);
        static void add_file( const kstring& file_name, const kstring& name_to_save, FILE *fp );
        static void add_tree( const kstring& path, const kstring& save_path, FILE*fp );
        static void add_eof( FILE* fp );
        static std::map<kstring, off_t> list_files( FILE* fp );
    };


    namespace detail {

        //header mais simples, mais antigo
        struct header_old_tar {
            char name[100];
            char mode[8];
            char uid[8];
            char gid[8];
            char size[12];
            char mtime[12];
            char checksum[8];
            char linkflag[1];
            char linkname[100];
            char pad[255];
        };

        //header padrao unix, permite nomes de arquivo maiores.
        //struct header_posix_ustar {
        //    char name[100];
        //    char mode[8];
        //    char uid[8];
        //    char gid[8];
        //    char size[12];
        //    char mtime[12];
        //    char checksum[8];
        //    char typeflag[1];
        //    char linkname[100];
        //    char magic[6];
        //    char version[2];
        //    char uname[32];
        //    char gname[32];
        //    char devmajor[8];
        //    char devminor[8];
        //    char prefix[155];
        //    char pad[12];
        //};

        typedef header_old_tar TarHeader;


        struct AppendTreeAction {
            AppendTreeAction( const kstring& path_name, FILE* fp );
            void operator()(const kstring& file, struct stat info);
        private:
            const kstring& path_name;
            FILE* fp;
        };

    };

    //template<typename ForwardIterator>
    //int create( const kstring& out_file, ForwardIterator first_file, ForwardIterator last_file)
    //{

    //}
};
};