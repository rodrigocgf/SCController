#pragma once

#include "KString.h"
#include <list>


namespace ktools 
{
    typedef std::list<kstring> KStringList;

    kstring DataToHex( const void* data, size_t len, const kstring& sep = " " );

}
