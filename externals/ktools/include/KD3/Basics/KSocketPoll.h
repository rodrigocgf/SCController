#ifndef _KSOCKETPOLL_H_
#define _KSOCKETPOLL_H_

#include <limits.h>

#ifndef WIN32
# include <poll.h>
#endif

#include <KSocketDefs.h>

namespace ktools
{
struct KSocketPoll
{
    static const kindex INVALID_INDEX = UINT_MAX;

    enum PollType
    {
        PREADS = 0x1,
        PWRITE = 0x2,
        PERROR = 0x4,
        PALL   = 0x7,
    };

    KSocketPoll(ksize sizeHint = 0u);
    ~KSocketPoll();

    kindex Add(KSOCKET, PollType = PREADS);
    void   Del(KSOCKET, PollType = PALL);

    int Poll(ksize timeout);

    PollType Events(kindex);

  protected:
#ifdef KWIN32
    FD_SET ReadsSet;
    FD_SET WriteSet;
    FD_SET ErrorSet;

    FD_SET ReadsRes;
    FD_SET WriteRes;
    FD_SET ErrorRes;

    KSOCKET MaxValue;
    KSOCKET MinValue;
#else
    void EnsureSize(ksize newSize);

    struct pollfd * Data;
    ksize           Size;

    ksize Count;
#endif
};
};

#endif /* _KSOCKETPOLL_H_ */
