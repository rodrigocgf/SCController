#pragma once

#ifdef __GNUC__
# ifndef likely
#  define likely(x)       __builtin_expect((x),1)
# endif
# ifndef unlikely
#  define unlikely(x)     __builtin_expect((x),0)
# endif
#else
# ifndef likely
#  define likely(x)       (x)
# endif
# ifndef unlikely
#  define unlikely(x)     (x)
# endif
#endif

namespace ktools
{

    
    template<typename D, typename B>
    class IsDerivedFrom
    {
        class No { };
        class Yes { No no[3]; }; 

        static Yes Test( B* ); // not defined
        static No Test( ... ); // not defined 

        static void Constraints(D* p) { B* pb = p; pb = p; } 

    public:
        static bool const value = sizeof(Test(static_cast<D*>(0))) == sizeof( Yes );

        IsDerivedFrom() { void(*p)(D*) = Constraints; }
    };

    template<bool C, typename T = void>
    struct EnableIfC {
        typedef T type;
    };

    template<typename T>
    struct EnableIfC<false, T> { };

    template<class Cond, class T = void>
    struct EnableIf: public EnableIfC<Cond::value, T> {};

    template<bool C, typename T = void>
    struct DisableIfC {};

    template<typename T>
    struct DisableIfC<false, T> {
        typedef T type;
    };

    template<typename Cond, class T = void>
    struct DisableIf: public DisableIfC<Cond::value, T> {};

    template<typename, typename>
    struct IsSame {
        static bool const value = false;
    };

    template<typename A>
    struct IsSame<A, A> {
        static bool const value = true;
    };


    template<typename B, typename D>                                 
    struct IsBaseOf {                                                       
        static D * create_d();                     
        static char (& chk(B *))[1]; 
        static char (& chk(...))[2];           
        static bool const value = sizeof chk(create_d()) == 1 &&  
                                  !IsSame<B    volatile const, 
                                           void volatile const>::value;
    };


    template<bool v> struct Bool2Type{ static const bool value = v; };

    template <typename>
    struct IsNumeric { enum { value = false }; };

    template <> struct IsNumeric <char> { enum { value = true }; };
    template <> struct IsNumeric <unsigned char> { enum { value = true }; };
    template <> struct IsNumeric <int> { enum { value = true }; };
    template <> struct IsNumeric <unsigned int> { enum { value = true }; };
    template <> struct IsNumeric <long int> { enum { value = true }; };
    template <> struct IsNumeric <unsigned long int> { enum { value = true }; };
    template <> struct IsNumeric <short int> { enum { value = true }; };
    template <> struct IsNumeric <unsigned short int> { enum { value = true }; };
    template <> struct IsNumeric <long long> { enum { value = true }; };
    template <> struct IsNumeric <unsigned long long> { enum { value = true }; };
    template <> struct IsNumeric <float> { enum { value = true }; };
    template <> struct IsNumeric <double> { enum { value = true }; };
    template <> struct IsNumeric <long double> { enum { value = true }; };

}

