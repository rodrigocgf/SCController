#pragma once

#include "KMutex.h"

#ifdef KWIN32

#define KTHREAD_TIME_REALLY_CRITICAL    THREAD_PRIORITY_TIME_CRITICAL
#define KTHREAD_TIME_CRITICAL           THREAD_PRIORITY_TIME_CRITICAL
#define KTHREAD_HIGHEST                 THREAD_PRIORITY_HIGHEST
#define KTHREAD_ABOVE_NORMAL            THREAD_PRIORITY_ABOVE_NORMAL
#define KTHREAD_NORMAL                  THREAD_PRIORITY_NORMAL
#define KTHREAD_BELOW_NORMAL            THREAD_PRIORITY_BELOW_NORMAL

#define KDIR_SEP_STR    "\\"
#define KDIR_SEP_CHR    '\\'

#ifdef __BCPLUSPLUS__
#define BORLAND
#endif

#else

#define KTHREAD_TIME_REALLY_CRITICAL    5
#define KTHREAD_TIME_CRITICAL           4
#define KTHREAD_HIGHEST                 3
#define KTHREAD_ABOVE_NORMAL            2
#define KTHREAD_NORMAL                  1
#define KTHREAD_BELOW_NORMAL            0

#endif

#ifndef NotifyThreadInit
#define NotifyThreadInit() _NotifyThreadInit( __FILE__, __FUNCTION__, __LINE__ )
void _NotifyThreadInit( const char *file, const char *func, int l );
#endif

namespace ktools
{
class KThread
{
private:
    static int32 Starter( void * );
    KHandle Handle;
    bool Started;
    bool Starting;

protected:
    uint32 Scheduler;
    void KickOff()
    {
        Starting = true;
        Handle = KThread::StartThread( (void *) Starter, this, 0, false, false, this->Scheduler );
    }

public:
    // Realtime Schedulers
    // from bits/sched.h
    enum Sched
    {
        OTHER = 0,
        FIFO = 1,
        RR = 2,
        BATCH = 3,
        IDLE = 4
    };
    KThread( void );
    KThread( bool StartSuspended );
    virtual ~KThread( void );

    bool Terminated;

    virtual void Execute() = 0;

    void Terminate()
    {
        Terminated = true;
    }

    bool Joinable()
    {
        return ( Starting || Started ) && !Terminated;
    }

    /* Waits for this thread to terminate for @millis, returns true if thread terminates, false if timeouts.*/
    bool Join(ktime millis = INFINITE);

    static KHandle StartThread( KHandle StartAddress, KHandle Parameter, int32 Flags = 0, bool Log = false, bool CloseHandle = false, uint32 Scheduler = 0 /*OTHER*/ );
    static void CloseThreadHandle( KHandle h );
    static int32 SetPriority( int32 p );
    static int32 SetPriority( KHandle h, int32 p, int32 id = 1 );
    static int32 GetPriority( KHandle h = NULL );
};
}

