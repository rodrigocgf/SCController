#pragma once 
#include "Basics/KBasics.h"
#include "Basics/KUtility.h"
#include "KIPC/KSocketIPC.h"
#include <KD3/KIPC/KBufferedSocketIPC.h>
#include <KD3/KIPC/KSharedMemIPC.h>
#include <KD3/KIPC/KEthSocketIPC.h>
#include <KD3/KIPC/KPCH.h>

using ktools::KContextMutex;
