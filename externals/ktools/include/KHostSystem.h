#ifndef HOSTSYSTEM_H
#define HOSTSYSTEM_H

#include <KTypeDefs.h>
/*
#ifndef NUSE_DELMNG
inline void operator delete(void* p);
inline void *operator new(size_t size);
#endif
*/

#ifdef GCC3_VISIBILITY_ENABLED
#ifndef GCC3_VISIBILITY_NAMESPACE
#error GCC3_VISIBILITY_ENABLED defined but GCC3_VISIBILITY_NAMESPACE undefined! Check command line!
#endif
// the two macros are needed to force name expansion.
#define NAMESPACE_JOIN(name) name ## _Namespace
#define NAMESPACE_MAKE(name) NAMESPACE_JOIN(name)
#endif

typedef void* ( *VoidConverter )( void * );
typedef void (Kstdcall *KTimerFunction)( uint32 uID, uint32 uMsg, unsigned long dwUser, unsigned long dw1, unsigned long dw2 );
typedef void* KHandle;

#include <string>
#include <sstream>
#include "KD3/Basics/KSystemTime.h"
#include <sys/stat.h>

#ifndef KWIN32
	#include <sys/ipc.h>
	#include <sys/time.h>
	#include <sys/types.h>
	#include <sys/sem.h>
	#include <sys/ptrace.h>
	#include <sys/syscall.h>
	#include <dlfcn.h>
	#include <errno.h>
 	#include <fcntl.h>
	#include <pthread.h>
	#include <stdio.h>
	#include <time.h>
	#include <unistd.h>
	#include <dirent.h>
	#include <ctype.h>
	#include <stdarg.h>
	#include <getopt.h>
	#include <syslog.h>
	#include <limits.h>


	#define KMAX_PATH	255
	#define INFINITE	0xFFFFFFFF

	#define FALSE					0

    #define KTHREAD_BELOW_NORMAL            0
	#define KTHREAD_NORMAL					1
	#define KTHREAD_ABOVE_NORMAL			2
	#define KTHREAD_HIGHEST					3
	#define KTHREAD_TIME_CRITICAL			4
	#define KTHREAD_TIME_REALLY_CRITICAL	5

	#define KT_SUSPENDED			1

	#if defined(__i386__)
	# define __NR_ioprio_set	289
	# define __NR_ioprio_get	290
	#elif defined(__ppc__)
	# define __NR_ioprio_set	273
	# define __NR_ioprio_get	274
	#elif defined(__x86_64__)
	# define __NR_ioprio_set	251
	# define __NR_ioprio_get	252
	#elif defined(__ia64__)
	# define __NR_ioprio_set	1274
	# define __NR_ioprio_get	1275
	//#else
	//# error "Unsupported archiecture!"
	#endif

	#define IOPRIO_CLASS_SHIFT	13
	#define IOPRIO_PRIO_MASK	0xff

	enum
	{
		IOPRIO_CLASS_NONE,
		IOPRIO_CLASS_RT,
		IOPRIO_CLASS_BE,
		IOPRIO_CLASS_IDLE,
	};

	enum
	{
		IOPRIO_WHO_PROCESS = 1,
		IOPRIO_WHO_PGRP,
		IOPRIO_WHO_USER,
	};

    struct KSystemEvent
    {
        bool Set;
        KHandle Mutex;
        pthread_cond_t *Event;
    };

	struct LinSemaphore
	{
		KHandle Mutex;
		pthread_cond_t  Condition;
		int32 Count;
		int32 MaxCount;
	};

    #define KDIR_SEP_STR    "/"
    #define KDIR_SEP_CHR    '/'

#ifndef NotifyThreadInit
#define NotifyThreadInit() _NotifyThreadInit( __FILE__, __FUNCTION__, __LINE__ )
void _NotifyThreadInit( const char *file, const char *func, int l );
#endif

#else
	#include <stdio.h>
	#include <io.h>
	#include <fcntl.h>
    #include <WinSock2.h>
	#include <windows.h>
	#define KMAX_PATH	MAX_PATH

	#define KTHREAD_TIME_REALLY_CRITICAL	THREAD_PRIORITY_TIME_CRITICAL
	#define KTHREAD_TIME_CRITICAL			THREAD_PRIORITY_TIME_CRITICAL
	#define KTHREAD_HIGHEST					THREAD_PRIORITY_HIGHEST
	#define KTHREAD_ABOVE_NORMAL			THREAD_PRIORITY_ABOVE_NORMAL
	#define KTHREAD_NORMAL					THREAD_PRIORITY_NORMAL
    #define KTHREAD_BELOW_NORMAL            THREAD_PRIORITY_BELOW_NORMAL

	#define KT_SUSPENDED			CREATE_SUSPENDED

    #define KDIR_SEP_STR    "\\"
    #define KDIR_SEP_CHR    '\\'

#define NotifyThreadInit() _NotifyThreadInit( __FILE__, __FUNCTION__, __LINE__ )
void _NotifyThreadInit( const char *func, const char *f, int l );

    #ifdef __BCPLUSPLUS__
        #define BORLAND
        #undef NotifyThreadInit()
        #define NotifyThreadInit() /##/
    #endif

    HMODULE GetCurrentModule();

#endif

#ifdef KWIN32
# define GLOBAL_ALLOC(len) GlobalAlloc(0, len)
# define GLOBAL_FREE(ptr)  GlobalFree(ptr)
#else
# define GLOBAL_ALLOC(len) malloc(len)
# define GLOBAL_FREE(ptr)  free(ptr)
#endif

#if _MSC_VER >= 1400
#define snprintf _snprintf
#endif


using ktools::KSystemTime;

//#ifdef GCC3_VISIBILITY_ENABLED
//namespace NAMESPACE_MAKE(GCC3_VISIBILITY_NAMESPACE)
//{
//#endif

//#ifdef K3L_HOSTSYSTEM
//namespace K3L_HostSystem
//{
//#endif

#ifdef KWIN32
# define HOSTSYSTEM_HIDDEN_ATTRIBUTE
#else
# ifdef GCC3_VISIBILITY_ENABLED
#  define HOSTSYSTEM_HIDDEN_ATTRIBUTE __attribute__((visibility("hidden")))
# else
#  define HOSTSYSTEM_HIDDEN_ATTRIBUTE
# endif
#endif

class HOSTSYSTEM_HIDDEN_ATTRIBUTE KHostSystem
{
private:
    static sbyte LogDir[ 500 ];
	#ifdef KWIN32
		static KHandle RunOnceMutex;
	#else
		static int32 RunOnceSemaphore;
	#endif

public:
	KHostSystem();
	~KHostSystem();

    static KHandle hModule; // windows only

	static KHandle LoadDllLibrary( const sbyte *LibName, sbyte **Reason = NULL );
	static KHandle GetDllProcAddress( KHandle LibDll, const sbyte *FuncName );

	static KHandle CreateSystemEvent( const sbyte *EventName );
	static int32 CloseSystemEvent( KHandle ObjectHandle );
	static void PulseSystemEvent( KHandle OsEvent );
	static int32 WaitEvent( KHandle ObjectHandle, ktime TimeinMili );

	static KHandle CreateLocalMutex();
	static void DeleteLocalMutex( KHandle CriticalSection );
	static void EnterLocalMutex( KHandle CriticalSection );
	static void LeaveLocalMutex( KHandle CriticalSection );

#ifndef KWIN32
    static int32 AddMilisecToTime( struct timespec * BaseTime, ktime TimeInMili );
#endif

    static KHandle CreateCountSemaphore (int32 initialCount, int32 maxCount, const sbyte *name );
    static bool ReleaseSemaphore( KHandle sem, int32 releaseCount );
    static int32 WaitOnSemaphore( KHandle sem, ktime TimeInMili );
    static void DeleteSemaphore( KHandle sem );

	static byte CreateRunOnce();
	static byte TestRunOnce();
	static void ReleaseRunOnce();

	static KHandle StartThread( KHandle StartAddress, KHandle Parameter, int32 Flags = 0 );
    static void CloseThreadHandle(KHandle handle);
	static void ThreadSetPriority( KHandle ThreadHandle, int32 Priority, int32 ThreadId = 1 );
	static void ThreadSetPriority( int32 Priority );
	static uint32 ThreadGetPriority( KHandle ThreadHandle );
	static uint32 ThreadGetPriority();
	static KHandle ThreadSelf();
    static uint32 ThreadId();
	static void TerminateThread( KHandle Thread );

	static void Delay( ktime Time );
	static uint32 GetTick();
	static void GetTime( KSystemTime *DateTime );
    static void PrecisionDelay( ktime milli );

	static int32 GetCurrentProcId();
	static int32 ReadExternalMemory( KHandle PID, KHandle Address, KHandle Buffer, int32 Size, unsigned long *BytesRead );
	static int32 WriteExternalMemory( KHandle PID, KHandle Address, KHandle Buffer, int32 Size, unsigned long *BytesWritten );
	static int32 CloseExternalProcess( KHandle ProcHandle );
	static KHandle OpenExternalProcess( uint64 PID );

	static int32 MakeDirectory( const sbyte *Path );
	static int32 RemoveFile( const sbyte *Path );
    static int32 RemoveDirectory( const sbyte *Path );
	static KHandle FindFirst( const sbyte *Dir, const sbyte *Extension, sbyte *ReturnName, int32 *RetNext );
	static int32 FindNext( KHandle Handle, const sbyte *Extension, sbyte *ReturnName );
	static int32 FindClose( KHandle Handle );
	static sbyte *GetWorkDirectory();
    static KHandle OpenSharedFile( sbyte *file );
    static bool ExtensionCompare( const sbyte *String, const sbyte *Extension ); //Extension must be lowercase


	static uint32 StriCmp( const sbyte *Str1, const sbyte *Str2 );
	static sbyte *ItoA( int32 Number, sbyte* Buffer, int32 Base );
	static int32 AtoI( const sbyte* Buffer );
    static sbyte *StrTrim( sbyte *s );

	static sbyte *ReadLine( sbyte *Buffer, int32 MaxSize, FILE *fp );

	static void FileTruncate( sbyte *FileName, int32 Size );

    static bool GetExeName( sbyte *ExeName, int32 Size, bool full = false );

    static uint32 Hash( const sbyte* str, uint32 len, uint32 start = 0xAAAAAAAA );

    static uint32 GetSecondsSinceEpoch( int32 &TimeZoneBias );
};

bool operator<=( KSystemTime& left, KSystemTime& right );
bool operator>=( KSystemTime& left, KSystemTime& right );


//TODO: Colocar estas defini��es num header separado e talvez inclu�-lo aqui.
template <typename value_type>
value_type from_string( const std::string& str, const value_type& errorVal )
{
    std::istringstream is( str );
    if ( is.fail() ) return errorVal;
    value_type foo = value_type();
    if ( is >> foo ) return foo;
    return errorVal;
}
template<>
int32 from_string<int32>( const std::string& str, const int32& errorVal );
template<>
uint32 from_string<uint32>(const std::string& str, const uint32& errorVal );
template<>
int64 from_string<int64>( const std::string& str, const int64& errorVal );
template<>
uint64 from_string<uint64>( const std::string& str, const uint64& errorVal );


template <typename value_type>
std::string to_string( const value_type& obj )
{
    std::ostringstream os;
    os << std::fixed;
    os << obj;
    return os.str();
}

std::string from_string( const std::string & str, const std::string &errorVal );
std::string from_string( const char *str, const char *errorVal );
std::string to_string( const char *str );
std::string to_string( const std::string & str );

//#ifdef GCC3_VISIBILITY_ENABLED
//}
//
//using namespace NAMESPACE_MAKE(GCC3_VISIBILITY_NAMESPACE);
//#endif

//#ifdef K3L_HOSTSYSTEM
//}
//
//using namespace K3L_HostSystem;
//#endif

#endif


