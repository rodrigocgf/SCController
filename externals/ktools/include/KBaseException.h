// KBaseException.h: interface for the KBaseException class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_KBASEEXCEPTION_H)
#define AFX_KBASEEXCEPTION_H

#include <KTypeDefs.h>
#include "kstring.h"
#include <stdexcept> 

class KBaseException : public std::exception
{
protected:
    KBaseException() {}
    uint32 Construct( const sbyte* T, va_list List );
    kstring Text;

public:
    KBaseException( const sbyte *s, ... );
    explicit KBaseException( const kstring &s );
    ~KBaseException() throw() {};

    const char *what() const throw() {
        return (const char *) Text.c_str();
    }
private:
    void *operator new(size_t s);
    void *operator new[](size_t s);
};

#define throw_KBaseException(X,...) throw KBaseException("%s(%d): " X, __FILE__, __LINE__, __VA_ARGS__ )

template<class T> class KTemplateException : public KBaseException
{
public:
    KTemplateException( T *obj, const sbyte *fmt, ... ) {
	    va_list args;
	    va_start( args, fmt );
        Text.vFormat( fmt, args );
        Text.AppendFormat( "(%s)", obj->ToString().c_str() );
	    va_end( args );
    }
    KTemplateException( const sbyte *fmt, ... ) 
    {
	    va_list args;
	    va_start( args, fmt );
        Text.vFormat( fmt, args );
	    va_end( args );
    }
    KTemplateException( const kstring& s)
        : KBaseException(s)
    {
    }

};

#endif
