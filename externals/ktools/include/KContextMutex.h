#ifndef KCONTEXTMUTEX_H
#define KCONTEXTMUTEX_H

#include "KHostSystem.h"
#include "KD3/Basics/KMutex.h"
/*
class KContextMutex
{
private:
    bool MacroCounter;
    KHandle MutexHandle;
public:
    KContextMutex( KHandle p ) {
        MutexHandle = p;
        KHostSystem::EnterLocalMutex(MutexHandle);
        MacroCounter = true;
    }

    ~KContextMutex() {
        if( MutexHandle )
            KHostSystem::LeaveLocalMutex( MutexHandle );
    }
    bool MakeContextMutexMacroAvailable()
    {
        bool oldMacro = MacroCounter;
        MacroCounter = false;
        return oldMacro;
    }
};*/
using ktools::KContextMutex;

class KManualContextMutex
{
private:
    KHandle MutexHandle;
    bool Locked;
public:
    KManualContextMutex( KHandle p ) 
    {
        MutexHandle = p;
        Locked = false;
    }

    void Lock()
    {
        Locked = true;
        KHostSystem::EnterLocalMutex(MutexHandle);
    }

    void Unlock()
    {
        Locked = false;
        KHostSystem::LeaveLocalMutex(MutexHandle);
    }

    ~KManualContextMutex() 
    {
        if( MutexHandle && Locked )
            KHostSystem::LeaveLocalMutex( MutexHandle );
    }
};

#endif


