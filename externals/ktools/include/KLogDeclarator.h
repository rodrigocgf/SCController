// Adaptado de "EnumToString.h"

#undef KLOG_OPTION_ELEMENT
#undef KLOG_OPTIONS
#undef KLOG_OPTIONS_END
#undef KLOG_SOURCE_ELEMENT

#ifndef GENERATE_STR_OPTIONS
	#define KLOG_OPTION_ELEMENT( element, value, descr ) element = ( 1<<value ),
    #define KLOG_SOURCE_ELEMENT( element, descr ) element,
    #define KLOG_OPTIONS( ENUM_NAME ) enum ENUM_NAME {
    #define KLOG_OPTIONS_END(ENUM_NAME) }; \
        const char *GetString##ENUM_NAME(enum ENUM_NAME index);
#else
#ifdef GENERATE_STR_LIST
    #define KLOG_OPTIONS( ENUM_NAME ) const char *ENUM_NAME##Str[] = {
    #define KLOG_SOURCE_ELEMENT( element, descr ) descr,
    #define KLOG_OPTION_ELEMENT( element, value, descr ) descr,
    #define KLOG_OPTIONS_END(a) "" };
#endif

#ifdef GENERATE_STR_FUNCTION
    #define KLOG_OPTIONS( ENUM_NAME ) const char * GetString##ENUM_NAME( enum ENUM_NAME index ) {\
	    switch( index ) { 

    #define KLOG_SOURCE_ELEMENT( element, descr ) case element: return descr; break;
    #define KLOG_OPTION_ELEMENT( element, value, descr ) case element: return descr; break;

    #define KLOG_OPTIONS_END( ENUM_NAME ) default: return "UNKOWN"; } } ;
#endif

#endif


