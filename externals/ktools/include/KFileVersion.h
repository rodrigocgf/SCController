//---------------------------------------------------------------------------

#ifndef KFileVersionH
#define KFileVersionH

class KFileVersion
{
    int32 Major, Minor, Release, Build, Size;
    sbyte *Descr;
    public:
    KFileVersion( const sbyte *FileName );
    ~KFileVersion() {
        if( Descr )
            delete[] Descr;
    };

    int32 GetMajor() {
        return Major;
    };
    int32 GetMinor() {
        return Minor;
    };
    int32 GetRelease() {
        return Release;
    };
    int32 GetBuild() {
        return Build;
    };

    bool HasInfo() {
        return Size > 0;
    };

    sbyte *GetDescr( const sbyte *Prefix = NULL );
};


//---------------------------------------------------------------------------
#endif


