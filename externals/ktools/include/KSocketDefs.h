
#ifndef KSocketDefsH
#define KSocketDefsH

#ifdef KWIN32

	typedef SOCKET KSOCKET; //TODO verificar isto em 64bits...

	#ifndef FD_SETSIZE
	#define FD_SETSIZE      2
	#endif /* FD_SETSIZE */

	#define KINVALID_SOCKET	INVALID_SOCKET
#else

    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
	#include <netinet/tcp.h>
    #include <netdb.h>
    #include <errno.h>

    #include <arpa/inet.h>

    #include <sys/types.h>
    #include <sys/uio.h>

    typedef int KSOCKET;

    #define KINVALID_SOCKET	-1
    #define SOCKET_ERROR	-1
    #define closesocket		close
    #define WSAGetLastError()  errno

#endif


#define KMAX_SOCKET_BUFFER   2048

#include "KSocketError.h"

class KSocketsInitializer
{
public:
    KSocketsInitializer();
    ~KSocketsInitializer();
};

#endif

int autoresumeselect( int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds, struct timeval *timeout );

