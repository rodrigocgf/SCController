#pragma once

#ifdef USE_OLD_KTOOLS

#include "KHostSystem.h"
/*#include "../../common/kthread.h"

class KThread :
	public KThread
{
public:
	KThread(bool s);
	~KThread(void);

	bool Loop() {
		return true;
	};
	bool IsSleepNeeded() {
		return false;
	};
};
*/

class KThread 
{
private:	
	static int32 Starter( void * );
	KHandle Handle;

protected:	
	void KickOff()
	{
		Handle = KHostSystem::StartThread( ( void * )Starter, this, 0 );
	}
	
public:
	KThread();
	virtual ~KThread(void);

	bool Terminated;

	virtual void Execute() = 0;
	
	void Terminate() 
	{
		Terminated = true;
	}
};


#endif