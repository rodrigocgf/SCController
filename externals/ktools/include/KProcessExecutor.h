//---------------------------------------------------------------------------

#ifndef KProcessExecutorH
#define KProcessExecutorH


class KProcessExecutor
{
    char *Error;
public:
    KProcessExecutor( char *file, char *path );
    ~KProcessExecutor() {
        if( Error )
            delete[] Error;
    };
    char *GetErrorDescr() {
        return Error;
    };

    bool GetResult() {
        return Error == NULL;
    }
};

class KCommandExecutor
{
    char *Error;
public:
    KCommandExecutor( char *cmd, char *out );
    ~KCommandExecutor() {
        if( Error )
            delete[] Error;
    };
    char *GetErrorDescr() {
        return Error;
    };

    bool GetResult() {
        return Error == NULL;
    }
};

//---------------------------------------------------------------------------
#endif


