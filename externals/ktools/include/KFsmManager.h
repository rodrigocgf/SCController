#ifndef KFSMMANAGER_H
#define KFSMMANAGER_H

//-----------------------------------------------------------------------------
/*
    Each state has the following format:
        Event       (1)
        Condition   (0+)
        Action      (0+)        //include ActionWithParam commands
        NextState   (1)

    Range of valid values for each type of entry:
        Event:              0x00 to 0xff
        NextState:          0x00 to 0x3e  (00xxxxxx)
        Condition:          0x40 to 0x7f  (01xxxxxx)
        Action:             0x80 to 0xf0  (10000000 to 11110000)
        ActionWithParam:    0xf1 to 0xfe  (11110001 to 11111110)
*/
//-----------------------------------------------------------------------------
#define ANY_EVENT                   (static_cast<uint8>( 0xfe ))  //reserved event label - matches any event
#define EOS                         (static_cast<uint8>( 0xff ))  //reserved event label - indicates end of state defintion
#define SAME_STATE                  (static_cast<uint8>( 0x3f ))  //reserved "NEXT_STATE" label
#define STATE_OFFSET                0x00            //STATE opcode start here
#define CONDITION_OFFSET            0x40            //CONDITION opcode start here
#define ACTION_OFFSET               0x80            //ACTION opcode start here
#define ACTION_WITH_PARAM_OFFSET    0xF1            //ACTION_WITH_PARAM opcode start here

//-----------------------------------------------------------------------------
//to define a state table
#define STATE_REF(stateName)                    stateName##TrTable
#define STATE_REF_UNDEFINED                     NULL
//#define STATE(stateName)                        _##stateName
#define STATE(stateName)                        KChildFsm::_##stateName
#define DEFINE_STATE_TABLE		                static const KFsmManager::FsmState sTable[] = {
#define END_STATE_TABLE                         }
#define DECLARE_STATE(stateName, value)         static const uint8 _##stateName = (value+STATE_OFFSET)
//-----------------------------------------------------------------------------
//to define an action table
#define DEFINE_ACTION_TABLE(fsm)				typedef fsm KChildFsm; \
												static void (fsm::* const aTable[])(void) = {
#define END_ACTION_TABLE                        }
#define DECLARE_ACTION(actionName, value )      static const uint8 _##actionName = (value+ACTION_OFFSET); \
												void actionName (void)
#define ACTION_TABLE_REF	                    aTable
//-----------------------------------------------------------------------------
//to define an "action with param" table
#define DEFINE_ACTION_WP_TABLE(fsm)             typedef fsm KChildFsm; \
												static void (fsm::* const awpTable[])(uint8) = {
#define END_ACTION_WP_TABLE                     }
#define DECLARE_ACTION_WP(actionName, value)    static const uint8 __##actionName = (value+ACTION_WITH_PARAM_OFFSET); \
												void actionName (uint8)
#define ACTION_WP_TABLE_REF		                awpTable
//-----------------------------------------------------------------------------
//to define a condition table
#define DEFINE_CONDITION_TABLE(fsm)             typedef fsm KChildFsm; \
												static bool (fsm::* const cTable[])(void) = {
#define END_CONDITION_TABLE                     }
#define DECLARE_CONDITION(conditionName, value) static const uint8 _##conditionName = (value+CONDITION_OFFSET); \
												bool conditionName (void)
#define CONDITION_TABLE_REF		                cTable
//-----------------------------------------------------------------------------
//to fill in transitions into transition tables
#define DEFINE_TRANSITION_TABLE(stateName)      static KFsmManager::FsmTrans stateName##TrTable[] = {
#define END_TRANSITION_TABLE                    EOS }
#define DECLARE_EVENT(eventName, value)			static const uint8 _eventName = value
//#define EVENT(eventName)                        eventName
//#define CONDITION(condName)                     _##condName
//#define ACTION(actionName)                      _##actionName
//#define ACTION_WP(actionName, param)            __##actionName, static_cast< uint8 >( param )
#define EVENT(eventName)                        KChildFsm::eventName
#define CONDITION(condName)                     KChildFsm::_##condName
#define ACTION(actionName)                      KChildFsm::_##actionName
#define ACTION_WP(actionName, param)            KChildFsm::__##actionName, static_cast< uint8 >( param )

//-----------------------------------------------------------------------------
#define KFSM_MANAGER(defaultState) KFsmManager( \
		reinterpret_cast< const FsmState* const			  > ( sTable	 ), \
		reinterpret_cast< const FsmAction* const		  > ( aTable	 ), \
		reinterpret_cast< const FsmActionWithParam* const > ( awpTable   ), \
		reinterpret_cast< const FsmCondition* const		  > ( cTable	 ), \
		reinterpret_cast< const FsmTrans* const			  > ( defaultState##TrTable ))

//-----------------------------------------------------------------------------
class KFsmManager
{
public:
	typedef void (KFsmManager::* FsmAction)(void);
	typedef void (KFsmManager::* FsmActionWithParam)(uint8);
	typedef bool (KFsmManager::* FsmCondition)(void);
	typedef uint8 FsmTrans;
	typedef FsmTrans* FsmState;
	typedef struct {
	    uint16	mNext;
		FsmTrans* mpTbl;
	} FsmIterator;
	typedef enum {
		TRANSITION_NOT_FOUND = 0,
		TRANSITION_EXECUTED,
		CONDITION_FAILED
	} FsmManagerRc;

    KFsmManager( const FsmState* const st,
                 const FsmAction* const at,
                 const FsmActionWithParam* const awpt,
                 const FsmCondition* const ct,
                 const FsmTrans* const dtt) 
        : StateTable(st)
        , ActionTable(at)
        , ActionWithParamTable(awpt)
        , ConditionTable(ct)
        , DefaultTransTable(dtt)
        , State(0) 
    { }

	// DO NOT REMOVE!
	virtual ~KFsmManager() {};

	void state(uint8 st) { State = st; }

	FsmManagerRc Event(uint8 ev);

    virtual void Log( const sbyte *, ... ) = 0;

protected:
	const FsmState* const StateTable;
	const FsmAction* const ActionTable;
	const FsmActionWithParam* const ActionWithParamTable;
	const FsmCondition* const ConditionTable;
	const FsmTrans* const DefaultTransTable;

	uint8 State;

private:
	bool executeTransition (FsmIterator* pIt);
	void skipTransition (FsmIterator* pIt);
	bool isAction (uint8 entry);
	bool isCondition (uint8 entry);
	bool isActionWithParam (uint8 entry);
	bool isNextState (uint8 entry);
	void init (FsmIterator* pIt, const FsmTrans* const pTbl);
	uint8 getFirstOpCode (FsmIterator* pIt);
	uint8 getNextOpCode (FsmIterator* pIt);
};
//-----------------------------------------------------------------------------
#endif


