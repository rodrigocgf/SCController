#pragma once

#ifdef KWIN32
#include "Windows.h"
#endif

/*!
\brief Simple Read/Write Lock mechanism for multiple read, and single write thread access.

This class provides a simple mechanism for providing multiple read and single write
access to shared resources between multiple threads.

*/
class KReadWriteLock
{
public:
    //! Constructor.
    KReadWriteLock(void);
    //! Destructor.
    virtual ~KReadWriteLock(void);
    
    //! Lock for reader access.
    void LockReader();
    //! Unlock reader access.
    void UnlockReader();
    
    //! Lock for writer access.
    void LockWriter();
    //! Unlock writer access.
    void UnlockWriter();

private:
    //! Private copy constructor.
    KReadWriteLock(const KReadWriteLock &cReadWriteLock);
    //! Private assignment operator.
    const KReadWriteLock& operator=(const KReadWriteLock &cReadWriteLock);

#ifdef KWIN32
    //! Increment number of readers.
    void IncrementReaderCount();
    //! Decrement number of readers.
    void DecrementReaderCount();

    //! Writer access event.
    HANDLE m_hWriterEvent;
    //! No readers event.
    HANDLE m_hNoReadersEvent;
    //! Number of readers.
    int m_nReaderCount;
    
    //! Critical section for protecting lock writer method.
    CRITICAL_SECTION m_csLockWriter;
    //! Critical section for protecting reader count.
    CRITICAL_SECTION m_csReaderCount;
#else
    pthread_rwlock_t RWLock;
#endif
};
