#ifndef TIMER_MANAGER_H
#define TIMER_MANAGER_H

#include "KHostSystem.h"
#include <KLogger.h>
#include <list>
#include <set>

#define TIMER_THREAD_SLEEP_TIME     25     //ms

//#ifdef GCC3_VISIBILITY_ENABLED
//namespace NAMESPACE_MAKE(GCC3_VISIBILITY_NAMESPACE)
//{
//#endif

#ifdef KWIN32
# define TIMER_HIDDEN_ATTRIBUTE
#else
# ifdef GCC3_VISIBILITY_ENABLED
#  define TIMER_HIDDEN_ATTRIBUTE __attribute__((visibility("hidden")))
# else
#  define TIMER_HIDDEN_ATTRIBUTE
# endif
#endif

typedef void (USER_TIMER_CALLBACK)(void* userData);
typedef kindex KTimerId;

class TIMER_HIDDEN_ATTRIBUTE TimerCell
{
public:    
    KTimerId timerId;
    uint32 timeInMs;                        //time to expire in ms
    void* userData;
    USER_TIMER_CALLBACK* userCallback;
    mutable bool disabled;
    bool flag;

    void disable() const  {
        disabled = true;
    }

    friend bool operator==(const TimerCell& l, const TimerCell& r)
    {
        return l.timerId == r.timerId;
    }
};

struct TimerCellCompare
{
    bool operator()(const TimerCell& l, const TimerCell& r )
    {
        if( (l.flag == r.flag && l.timeInMs < r.timeInMs ) ||
            (l.flag != r.flag && l.timeInMs > r.timeInMs ) )
            return true;
        else
            return false;
    }
};

class TIMER_HIDDEN_ATTRIBUTE TimerManager 
{
public:
    TimerManager();
    virtual ~TimerManager();
    static TimerManager* instance(void);

    static bool Actived() {
        return m_instance != NULL;
    }

    bool start (void);
    bool stop (void);
    
    KTimerId startTimer (uint32 timeInMs, void* userData,USER_TIMER_CALLBACK* cb);
    void* stopTimer (KTimerId timerId);

private:
    static void Log( KLogLevel level, const char *msg, ... );

    KTimerId m_timerId;

    static void timerThreadStarter(void *Event);         
    static void timerThread(void);         

    typedef std::multiset<TimerCell, TimerCellCompare> TimerList_t;
    //typedef std::list<TimerCell> TimerList_t;//TODO trocar pra set que � ordenado automaticamente? (lembrando que tem que criar um comparador)
    static TimerList_t m_timerList;

    static KHandle m_timerThread, m_timerThread_WaitTermination;
    static KHandle m_shutDownEvent;
    static TimerManager* m_instance;
    static KHandle m_mutex;                     //must be thread safe   
    static bool m_bShuttingDown;
	static bool m_flag;
};

//#ifdef GCC3_VISIBILITY_ENABLED
//}
//
//using namespace NAMESPACE_MAKE(GCC3_VISIBILITY_NAMESPACE);
//#endif

#endif

