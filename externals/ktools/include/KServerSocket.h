#pragma once

#include "KSocketDefs.h"

class KServerSocket;
typedef void ( Kstdcall KClientCallback )( KServerSocket *Server, KSOCKET Socket );

class KServerSocket
{
    KSocketsInitializer Initializer;
private:
    void Prepare();

protected:
	static int32 Kstdcall StartListenerThread( void *p );

	int32 InternalListen();

    bool Listening;

	KSOCKET Handle;
	int32 Port;

    KSocketError Error;
    KClientCallback *ClientCallback;

public:
	KServerSocket(void);
public:
	~KServerSocket(void);

    void Listen( int32 p ) {
        Port = p;
        Prepare();
        KHostSystem::StartThread( ( void * )StartListenerThread, this );
    }

	bool IsListening() {
		return ( Listening );
	}

    void Close() {
        Listening = false;
        closesocket( Handle );
        Handle = KINVALID_SOCKET;
    }

    int32 GetPort() {
        return Port;
    }

    void RegisterClientCallback( KClientCallback *Callback ) {
        ClientCallback = Callback;
    };
};


