#pragma once
#ifndef EXTERNAL_KLOGGER

#include "KD3/kd3.h"

class KLogFilter;
class KFileLogWriter;

class KLogManager
{
    bool InternalLogError;
    ksize _LoggerCount;
    ktools::KThreadMutex _LoggerCountMutex;
    KHandle _ReloadThreadHandle;
    bool _ReloadThreadInitialized;
    ktools::KSemaphore _ReloadThreadJoinSem;
    ktools::KSocketIpcChannel *_RemoteLogChannel;
    ktools::KThreadMutex _RemoteLogChannelMutex;
    bool ReloadThreadInitialized() {
        return _ReloadThreadInitialized;
    }

    static ksize LoggerCount()
    {
        KContextMutex m(GetMe()->_LoggerCountMutex);
        ksize count = GetMe()->_LoggerCount;
        return count;
    }

    std::string ModuleName;
    typedef std::map<std::string, KFileLogWriter *> KNameToFile;
    KNameToFile NameToFile;
    ktools::KThreadMutex Mutex;

    KLogger *_SelfLogger;

    static int32 ReloadConfigThread( void * );
    void ReloadConfigLoop();
    static KLogger *SelfLogger();
    void SendRemoteLogRequest(const kstring& req );

protected:
    KLogFilter *Filter;

public:
    virtual void StartReloadConfigThread();

protected:
    KLogManager();
private:
    //static KLogManager *Instance;
public:
    static KLogManager *GetMe(bool clean = false);

    KSystemTime StartTime;

    void NotifyInternalLogError() {
        InternalLogError = true;
    }

    static void NotifyLoggerCreation();
    static void NotifyLoggerDestruction();

    KLogFilter *GetFilter() {
        return Filter;
    }

    static KLogWriter& ErrorWriter();
    void FlushWriters();

    const char *GetModuleName();

    virtual ~KLogManager();
    virtual KLogWriter *GetWriter( const sbyte *name );

    static void InternalLog( KLogLevel l, const sbyte *fmt, va_list args );

    void LeaveReloadThread();

    void MakeCheckpoint();
    void RequestReload();
    void RequestKConfigReload();
};

class KLogWriter
{
public:
    virtual ~KLogWriter() {}

    virtual KHandle GetMutex() = 0;

    void FmtWrite( const sbyte *fmt, ... ) {
        va_list args; va_start( args, fmt );
        Write( fmt, args );
        va_end( args );
    }

    virtual void Write( const sbyte *fmt, va_list args ) = 0;
    virtual void Flush(bool lowPriority = false) = 0;
    virtual kstring Name() = 0;
};


class KFileLogWriter: public KLogWriter
{
    friend class KLogger;
    friend class KLogManager;
    friend class KLogBuilder;
	FILE *Handle;
    ktools::KThreadMutex Mutex;
    int32 LastDay;

    std::string WriterName;
    std::string FileName;
    std::string Prefix;

    kstring Buffer; //Temporary buffer that will hold the logs while we cant write them to a file (file is busy)

    void OpenLogMonitor();
    void Open( const sbyte *file );
    bool OpenError;
    bool ImACriticalLogFile;

    FILE *_InternalOpen( std::string fn);
//    bool CheckDateAndRotate();
    bool CheckSizeAndRotate();
    virtual void PrintLogOpened();
protected:
    void RenameOlderRotations();
    void Write( const sbyte* str );
public:
    KFileLogWriter( const sbyte *n );
    ~KFileLogWriter();

    KHandle GetMutex() {
        return Mutex.GetHandle();
    }

    void Write( const sbyte *fmt, va_list args );

    void Flush(bool lowPriority = false);

    kstring Name();

    void Rotate();
};

class KMessagesWriter: public KFileLogWriter
{
    kstring _Log;
    //ktools::KProcessMutex _Mutex;

public:
    KMessagesWriter()
        : KFileLogWriter("messages")
    {
    }

    void Write( const sbyte *fmt, va_list args )
    {
		#ifndef KWIN32
		va_list argscp;
		va_copy( argscp, args ); //required in linux when using 64 bits processors
		_Log.vAppendFormat( fmt, argscp );
		va_end( argscp );
		#else
		_Log.vAppendFormat( fmt, args );
		#endif
    }

    void Flush(bool lowPriority = false)
    {
        KFileLogWriter::Write( _Log.c_str() );
        _Log.clear();
        KFileLogWriter::Flush(lowPriority);
    }

    void PrintLogOpened() {};
};

class KLogFilter: public ktools::KSerializable
{
    static const ksize MaxOptionCount = 16;
    friend class KLogManager;
    struct FilterConfigCallback { // prevents to be overwritten on copy
        KLogFilterConfigCallback* Callback;
        FilterConfigCallback& operator=( const FilterConfigCallback& ) { return *this; };
        FilterConfigCallback() : Callback( NULL ) {}
    } FilterConfig;
    KLogOptions OptSources[ klogMaxSources ];
    KLogOptions DisabledSources[ klogMaxSources ];
    bool OptionOverwritten[ klogMaxSources ];
    KLogOptions OriginalOverwritten[ klogMaxSources ];
    void SetOption( KLogSource src, const sbyte *options[] );
    const sbyte* GetConfigFileName();
    bool FullLogging;

    uint64 _MaxLogFileSize;
    uint64 _MaxTotalLogSize;
public:
    KLogFilter();

    bool IsActive( KLogLevel level, KLogSource src, KLogOptions opt );
    void LoadConfig();
    KLogOptions GetOption( KLogSource src ) {
        if( FullLogging )
            return klogAll;
        if( src >= klogSystem && src < klogMaxSources )
            return OptSources[ src ];
        else
            return 0;
    }
    void OverwriteOption( KLogSource src, KLogOptions opt ) {
        if( src <= klogSystem || src > klogMaxSources )
            return;
        OptionOverwritten[ src ] = true;
        OriginalOverwritten[ src ] = OptSources[ src ];
        OptSources[ src ] = opt;
    }
    void ReleaseOverwrittenOption( KLogSource src ) {
        if( !OptionOverwritten[ src ] )
            return;
        OptSources[ src ] = OriginalOverwritten[ src ];
        OptionOverwritten[ src ] = false;
    }
    void SetFilterConfigCallback( KLogFilterConfigCallback *p ) {
        FilterConfig.Callback = p;
    }

    uint64 MaxLogFileSize() { return _MaxLogFileSize; };
    uint64 MaxTotalLogSize() { return _MaxTotalLogSize; };

    void InvokeFilterCallback(bool remoteRequest);

    void Serialize( ktools::KSerializer &S ) {
        ksize Size = klogMaxSources;
        S & Size;
        for( kindex i = 0; ( i < Size ) && ( i < klogMaxSources ); ++i )
        {
            S & OptSources[ i ];
            S & OptionOverwritten[ i ];
            S & DisabledSources[ i ];
        }
        if( S.In() )
        {
            if( Size > klogMaxSources )
            {
                KLogOptions tmp;
                bool tmp2;
                ksize tmpSize = Size;
                while( tmpSize-- > klogMaxSources )
                {
                    S & tmp;
                    S & tmp2;
                }
            }
            else if( Size < klogMaxSources )
            {
                while( Size < klogMaxSources )
                {
                    OptSources[ Size ] = klogAll;       //habilita ou desabilita
                    OptionOverwritten[ Size ] = false;  // o que quer dizer este overwritten?
                    DisabledSources[ Size ] = klogOff;
                    ++Size;
                }
            }
        }
        S & FullLogging;
        S & _MaxLogFileSize;
        S & _MaxTotalLogSize;
    }
};


//Algo tipo um Tagged Union
class KReloadMessage: public ktools::KSerializable
{
public:
    enum KType { INVALID = 0, RELOAD_CONFIGS = 1, RELOAD_LOGS = 2 };
    typedef KTemplateException<KType> KInvalidTypeException;


    KReloadMessage()
        : _Type(INVALID)
    {}

    KReloadMessage( bool ReloadConfig )
        : _Type(RELOAD_CONFIGS)
    {}

    KReloadMessage( const KLogFilter& f )
        : _Type( RELOAD_LOGS )
        , _Filter(f)
    {}

    KType Type() { return _Type; };

    const KLogFilter& GetLogFilter() {
        if( _Type != RELOAD_LOGS )
            throw KInvalidTypeException("%s: Invalid type here.", __FUNCTION__ );
        return _Filter;
    }

    void Serialize( ktools::KSerializer &S ) {
        S & _Type;
        if( _Type == RELOAD_LOGS )
            S & _Filter;
    }
private:
    KType _Type;
    KLogFilter _Filter;
};

#endif
