#pragma once

#include "KBaseException.h"

#ifndef KMAX_ERROR_MESSAGE
#define KMAX_ERROR_MESSAGE KMAX_LOG
#endif

class KSocketError
{
public:
	int32 ErrorCode;
    KSOCKET SocketId;
	char ErrorMsg[ KMAX_ERROR_MESSAGE + 10 ];
	void GetSystemError(); 
public:
	KSocketError(void); 
public:
	~KSocketError(void);

	char *GetErrorMsg() {
		return ErrorMsg;
	}

    void Throw( KSOCKET sock = KINVALID_SOCKET ) { //, sbyte *ExternError = 0 ) {
        SocketId = sock;
		GetSystemError();
		if( sock != KINVALID_SOCKET )
			closesocket( sock );
		throw KBaseException( ErrorMsg ); 
	}
};


