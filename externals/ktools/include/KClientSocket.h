#pragma once

#include "KBaseSocket.h"
//#include "KTools.h"

class KClientSocket : public KBaseSocket
{
	char *Address;
	int32 Port;
public:
	KClientSocket();
	virtual ~KClientSocket();

	void Open();
	void Open( sbyte *, int32 );

	sbyte *GetErrorMsg() {
		return Error.GetErrorMsg();
	}
};

#define KREAD( a, b, c ) KSocketRead( a, b, c, __FUNCTION__, __LINE__ )
void KSocketRead( KClientSocket *socket, void *buf, int size, const char *f, int l );

#define KWRITE( a, b, c ) KSocketWrite( a, b, c, __FUNCTION__, __LINE__ )
void KSocketWrite( KClientSocket *socket, void *buf, int size, const char *f, int l );
