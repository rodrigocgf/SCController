#pragma once

#include "KD3/kd3.h"
#include "KD3/Basics/KCallback.h"

namespace comm
{
    using ktools::KPlainData;
    using ktools::KEmptyData;
    class _Error_COMM_;
    typedef KTemplateException<_Error_COMM_> KCommException;

    namespace KModule
    {
        typedef byte Id;
        enum 
        {
            INVALID = 0,
            COMM    = 1
        };
    };


    struct KMsgId
    {
        enum Id
        {
            INVALID                     = 0,
            HELLO                       = 1,    // KHelloMsg
            LOCAL_ERROR                 = 2,    // kstring
            SYNC_RESPONSE               = 3,    // Anything
            REMOTE_ID                   = 4,    // kstring
            SYNC_FAIL                   = 5,    
            REQ_BROADCASTS              = 6,
            QUERY                       = 7,    // kstring
            CONN_FAIL                   = 8,
            RECONNECTED                 = 9,
            KEEP_ALIVE                  = 10
        };
    };

    typedef uint16 kenvid;
    class KEnvelope : public ktools::KSerializable
    {
    private:
        static const kindex INVALID_CODE = 0;
        static const kindex INVALID_OBJECT = (kindex)-1;
        static kenvid _PacketIdSeed;
        static const kindex SYNC_BIT = 0x80;

        kenvid _PackId;
        KModule::Id _Module;
        uint16 _Code;
        int32 _Group;
        int32 _Item;
        ktools::KBufferHolder _Buffer;
        mutable kstring _Debug;
        bool _Answered;
        bool _Sync;
    public:

        KEnvelope(): 
            _PackId(++_PacketIdSeed),
            _Module(KModule::INVALID),
            _Code(INVALID_CODE), 
            _Group(INVALID_OBJECT),
            _Item(INVALID_OBJECT),
            _Answered(false),
            _Sync(false)
            {}

        KEnvelope( KModule::Id mod, uint16 code, kindex group = 0, kindex item = 0 ) {
            _PackId = ++_PacketIdSeed;
            _Module = mod;
            _Code = code;
            _Group = group;
            _Item = item;
            _Answered = false;
            _Sync = false;
        }

        KEnvelope( KModule::Id mod, uint16 code, kindex group, kindex item, const KSerializable &msg ) {
            _PackId = ++_PacketIdSeed;
            _Module = mod;
            _Code = code;
            _Group = group;
            _Item = item;
            _Answered = false;
            _Sync = false;
            ktools::KBufferSerializer::Serialize( _Buffer, const_cast<KSerializable&>(msg) );
        }
        KEnvelope( KModule::Id mod, uint16 code, const KSerializable &msg ) {
            _PackId = ++_PacketIdSeed;
            _Module = mod;
            _Code = code;
            _Group = INVALID_OBJECT;
            _Item = INVALID_OBJECT;
            _Answered = false;
            _Sync = false;
            ktools::KBufferSerializer::Serialize( _Buffer, const_cast<KSerializable&>(msg) );
        }

        KEnvelope( KModule::Id mod, uint16 code, kindex item, const KSerializable &msg ) {
            _PackId = ++_PacketIdSeed;
            _Module = mod;
            _Code = code;
            _Group = 0;
            _Item = item;
            _Answered = false;
            _Sync = false;
            ktools::KBufferSerializer::Serialize( _Buffer, const_cast<KSerializable&>(msg) );
        }
        
        KEnvelope( const KEnvelope &other ) {
            Copy(other);
        }

        const KEnvelope &operator=(const KEnvelope &other) {
            Copy(other);
            return *this;
        }

        void Copy( const KEnvelope &other ) {
            _PackId = other._PackId;
            _Module = other._Module;
            _Code = other._Code;
            _Group = other._Group;
            _Item = other._Item;
            _Sync = other._Sync;
            _Answered = other._Answered;
            _Buffer.Copy( other._Buffer );
        }

        // otimizacao: serializar menos dados (dah pra reduzir)
        /* First bit of Module attribute is used to transmmit the _Sync flag */
        void Serialize( ktools::KSerializer &S ) {
            byte a = (byte) _Module;
            if( _Sync )
            {
                a |= (SYNC_BIT);
                _Urgent = true;
            }
            S & _PackId & a & _Code & _Group & _Item & _Buffer;
            _Module = (KModule::Id) (a & ~SYNC_BIT);
            if( a & SYNC_BIT )
                _Sync = true;
        }
        void RetrieveMessage( KSerializable &msg ) {
            ktools::KBufferSerializer::Deserialize( msg, _Buffer.GetPointer() );
        }
        kindex Code() const {
            return _Code;
        }
        kindex Item() const {
            return _Item;
        }
        kindex Group() const {
            return _Group;
        }
        KModule::Id Module() const {
            return _Module;
        }
        void Module( KModule::Id id ) {
            _Module = id;
        }
        kenvid PackId() const {
            return _PackId;
        }
        bool Answered() const {
            return _Answered;
        }
        void SetAnswered() {
            _Answered = true;
        }
        bool Sync() const {
            return _Sync;
        }
        void SetSync() {
            _Sync = true;
        }
        const char *String() const {
            _Debug.sprintf( "ENV(p%d,m%d,c%d,g%d,i%d sz=%d)", 
                _PackId,
                _Module,
                _Code,
                _Group,
                _Item,
                _Buffer.GetDataSize() );
            return _Debug.c_str();
        }
    };

    class KResultMsg : public KEnvelope
    {
        bool _ValidResponse;
    public:
        KResultMsg() : _ValidResponse(false) {}
        KResultMsg( const KEnvelope &env ) : KEnvelope(env), _ValidResponse(true) {}
        kindex ReturnCode() const {
            return Group();
        }
        kenvid RequestPackId() const {
            return (kenvid) Item();
        }
        bool IsValid() {
            return _ValidResponse;
        }
    };

    class KHelloMsg: public ktools::KSerializable
    {
    public:
        enum KServiceId
        {
            K3L_ID = 0x4B334C20, //"K3L "
            HMP_ID = 0x4B484D50, //"KHMP"
            K3LS_ID = 0x4B334C53, //"K3LS"
            VOIP_MEDIA_ID = 0x4B564D48, //"KVMH"
        };
        uint32 Magic;
        kindex Major;
        kindex Minor;
        kindex Build;
        
        KHelloMsg(): Magic(0), Major(0), Minor(0), Build(0) {};
        static const char* StringOf( KServiceId id )
        {
            switch( id )
            {
                case K3L_ID:        return "K3L";
                case HMP_ID:        return "KMP Server";
                case K3LS_ID:       return "K3L Server";
                case VOIP_MEDIA_ID: return "VoIP Media Handler";
                default:            return "unknown";
            }
        };

    private:
        void Serialize( ktools::KSerializer &S ) {
            S & Magic & Major & Minor & Build;
        }
    };


    class KCommChannel : public NonCopyable
    {
        ktools::KIpcChannel *RealIpc;
        ktools::KSemaphore TerminationSem;
        bool SignaledTerm;
        KLogger &Logger;
        ktime LastMsgTick;

        ktools::KThreadMutex RecvMutex;
        ktools::KThreadMutex SyncMutex;
        KReadWriteLock RealIpcMutex;

        ktools::KSemaphore ResponseSem;
        KResultMsg Response;
        kenvid RequestId;

        typedef ktools::KCallbackList<void, KCommChannel*> KCallBackList_t;
        KCallBackList_t TerminationCallbacks;

        bool Send( const KEnvelope &env );

        void ClearConnection();

        kstring GetNextId() {
            static kindex seed = 0;
            return kstring("[%d]", seed++);
        }
    public:
        KCommChannel(KLogger &logger);
        void operator=( ktools::KIpcChannel *real ) {
            ClearConnection();
            RealIpc = real;
        }

        bool operator==( const KCommChannel& right ) {
            return RealIpc == right.RealIpc;
        }

        virtual ~KCommChannel();

        bool Receive( KEnvelope &env );
        bool Connected() {
            return RealIpc != NULL;
        }

        void HandShake( KHelloMsg &hello, kstring &remoteid );
        void KeepAlive(ktime timeout);

        void SendAsyncMessage( const KEnvelope &msg ) {
            if( msg.Module() == KModule::COMM && (
                msg.Code() == KMsgId::SYNC_FAIL &&
                msg.Code() == KMsgId::SYNC_RESPONSE ) )
                throw KBaseException( "Cannot send synchronized response through async message" );
            Send( msg );
        }
        
        void SignTermination();
        void Terminate(bool waitBeforeClean);

        stt_code SyncMessage( KEnvelope &env, ktools::KSerializable &data );

        void SyncResponse( KEnvelope &reqMsg, stt_code retcode, ktools::KSerializable &params );
        void SyncFailResponse( KEnvelope &reqMsg, kstring cause, stt_code retcode = ksFail );
        void SyncResponse( KEnvelope &reqMsg, stt_code retcode ) {
            ktools::KEmptyData dummy;
            SyncResponse( reqMsg, retcode, dummy );
        }

        void RegisterTerminationCallback( KCallBackList_t::CallbackFunc_t* func) {
            TerminationCallbacks.RegisterCallback( func );
        }

        void UnregisterTerminationCallback( KCallBackList_t::CallbackFunc_t* func ) {
            TerminationCallbacks.UnregisterCallback( func );
        }

    };


    class KCommClient;
    typedef void (KCommEventHandlerCallback)( KCommClient *caller, KEnvelope &msg, ksize remaining );

    class KCommClientError {};
    typedef KTemplateException<KCommClientError> KCommClientException;

    typedef ktools::KBufferedSocketOptions KCommBufferedOptions;

    class KCommClient
    {
        static std::list<KCommClient *> _InstanceList;
        static ktools::KThreadMutex _InstListMutex;

        ktools::KThreadMutex EventMutex;
        KHandle _RemoteMsgHandlerThreadHandle, _EventDispatcherThread;
        KLogger Logger, CommLogger; 
        comm::KCommChannel Channel;
        bool _Terminated;
        bool _FirstTimeConnection;
        bool _ReceivingEvents;

        struct KConnInfo
        {
            kstring LogId;
            kstring ServerAddr;
            kindex ServerPort;
            kindex ConnCount;
            KHelloMsg::KServiceId ServiceId;
            KConnInfo() : ServerPort(0), ConnCount(0) {}
        } ConnInfo;

        KCommEventHandlerCallback *Callback;
        ktools::KThreadMutex CallbackMutex;

        bool _UsingBufferedTx;
        KCommBufferedOptions _BufferedTxOptions;

        bool ThisClientIsNoMoreItHasCeasedToBeItHasExpiredAndGoneToMeetTheMaker;

        static void DoNothingCallback(KCommClient *caller, comm::KEnvelope &msg, ksize remaining) {
            caller->Log( klogTrace, "Message not handled %s, remaining %d", msg.String(), remaining );            
        }

        void CheckForWindowsThreadTermination(bool &disp, bool &chann);

        kstring GetNextId() {
            return kstring( "%d", ConnInfo.ConnCount++ );
        }
    public:

        KCommClient(KLogSource logSrc, kindex logCliOpt, kindex logCommOpt, const char *logFile, int32 fileopt = 0 ) : 
            _RemoteMsgHandlerThreadHandle(NULL),
            _EventDispatcherThread(NULL),
            Logger( logSrc, logCliOpt, "CONTROL", logFile, fileopt  | klogPrefixOnHeader | klogAppendProcName ), 
            CommLogger( logSrc, logCommOpt, "COMM", logFile, fileopt | klogPrefixOnHeader | klogAppendProcName),
            Channel(CommLogger),
            _Terminated(false),
            _FirstTimeConnection(true),
            _ReceivingEvents(false),
            Callback(NULL),
            _UsingBufferedTx(false),
            ThisClientIsNoMoreItHasCeasedToBeItHasExpiredAndGoneToMeetTheMaker(false)
        {
            KContextMutex m( _InstListMutex );
            _InstanceList.push_back(this);
        }

        ~KCommClient() {
            mutex_context( _InstListMutex )
            {
                _InstanceList.remove(this);
            }
            Shutdown();
        }

        void Connect(kstring logid, kstring serverAddr, kindex serverPort, KHelloMsg::KServiceId, ktime timeout = INFINITE, bool firstConnection = true );
        void RequestEvents( KCommEventHandlerCallback *func, bool forceRequest = false );
        void KeepAlive(ktime timeout = 5000) {
            Channel.KeepAlive(timeout);
        }


        stt_code Query( KModule::Id modid, kstring query, kstring &answer );
        stt_code SendCommand( KModule::Id modid, kindex cmd, kindex dev, kindex ch, const ktools::KSerializable &par );
        stt_code SendCommand( KEnvelope &env, ktools::KSerializable &resp );
        stt_code SendCommand( KEnvelope &env );
        void SendAsyncMessage( comm::KEnvelope &msg ) {
            Channel.SendAsyncMessage(msg);
        }

        void SyncResponse( KEnvelope env, stt_code stt ) {
            Channel.SyncResponse( env, stt );
        }
       
        void SyncFailResponse( KEnvelope env, kstring cause, stt_code stt = ksFail ) {
            Channel.SyncFailResponse( env, cause, stt );
        }

        void SetBufferedTx(const KCommBufferedOptions & options = KCommBufferedOptions())
        {
            _UsingBufferedTx = true;
            _BufferedTxOptions = options;
        }

        void Log( KLogLevel level, const char *fmt, ... );

        bool GetNextAsyncMessage( KEnvelope &env, ksize &remaining, uint32 timeout = INFINITE );
        void EnqueueEvent( KEnvelope &env );
        bool ReceivingEvents() const { return _ReceivingEvents; };
    public: 
        void Shutdown(); 
        bool Connected() {
            return Channel.Connected();
        }


        static void NotifyAppTermination();

    private:
        static int StartHandleRemoteMessagesThread( void *p );
        void HandleRemoteMessages();

        ktools::KSemaphore EventListSem, EventThreadSem;
        std::list<KEnvelope> EventList;
        static int StartProcessEventsThread( void *p );
        void ProcessEvents();

        void Reconnect();
    };

}

