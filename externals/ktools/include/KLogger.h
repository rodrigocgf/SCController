#ifndef EXTERNAL_KLOGGER

#ifndef KLOGGER_H
#define KLOGGER_H

#include <string>
#include <map>
#include "KLogDefs.h"
#include "KD3/Basics/KSystemTime.h"
#include <stdarg.h>
#include "kstring.h"

using ktools::KSystemTime;

namespace ktools
{
	void Initialize();
    void Finalize();
}

// ATENCAO: ao editar este enum, lembre-se de editar tambem os parsers
enum KLogLevel 
{
	klogError = 0,
	klogWarning = 1,
	klogInfo = 2,
    klogNotice = 3,//Notice vai pro arquivo de log, mesmo sem filtro habilitado, mas n�o vai pro messages.log
	klogTrace = 4
};

enum KLogGenericOptions
{
    klogAll = -1,
    klogOff = 0,
    klogOn  = 1
};

typedef ksize KLogOptions;

const sbyte klogDirectionStr[2][5] = { "-> ", "<- " };

enum KLogDirection
{
    klogRight,
    klogLeft,
    klogUndefDirection
};

enum KLogFileNameOptions
{
    klogAppendProcName  = 0x01,
    klogAppendPid       = 0x02,
    klogAppendTimestamp = 0x04,
    //klogNoRotation      = 0x08,
    klogPrefixOnHeader  = 0x10,
};

const uint64 KLOG_DEFAULT_MAX_FILE_SIZE = uint64( 100 ) * 1024 * 1024; //100MB de tamanho m�ximo padr�o
const uint64 KLOG_DEFAULT_MAX_TOTAL_LOG_SIZE = uint64( 10 ) * 1024 * 1024 * 1024; //10GB de tamanho m�ximo de todos os arquivos juntos 


typedef void ( KLogFilterConfigCallback )(bool);

class KLogWriter;

class KLogger
{
private:
    std::string Prefix;
    std::string ObjectId;
    
    KLogWriter *LogWriter;
    friend class KLogWriter;
    bool PrefixOnHeader;
    bool _LogDateOnHeader;
    bool _LowIoPriority;
	
protected:
	KLogSource Source;
    int32 Option;
    
public:
	KLogger(KLogSource Source, int32 Option, const sbyte *Prefix, const sbyte *Name, int32 FileNameOptions = 0, bool LowIoPriority = false );

public:
	~KLogger(void);

    KLogWriter *GetWriter() {
        return LogWriter;
    }

    bool IsActive( KLogLevel l = klogTrace ) const;
    bool DoPrefixOnHeader() {
        return PrefixOnHeader;
    }
    const char *GetPrefix() {
        return Prefix.c_str();
    }
    const std::string &GetObjectId() {
        return ObjectId;
    }
    void SetObjectId( const char *objid ) {
        ObjectId = objid;
    }
    void LowIoPriority(bool low) {
        _LowIoPriority = low;
    }
    bool LowIoPriority() {
        return _LowIoPriority;
    }
    KLogSource GetSource() {
        return Source;
    }

    static void GetLogDirectory( std::string &dir );
    static const std::string &GetLogDirectory();
    static void GetStartTime( KSystemTime &t );
    static void ReloadConfig();
    static KLogOptions GetConfig( KLogSource src );
    static bool IsOptionActive( KLogSource src, KLogOptions opt );
    static void OverwriteOption( KLogSource src, KLogOptions opt );
    static void ReleaseOverwrittenOption( KLogSource src );
    static void InternalLog( KLogLevel, const sbyte *, ... );
    static void SetFilterConfigCallback( KLogFilterConfigCallback *p );
    static void SystemLog( KLogLevel, const char *, ... );
    static void NotifyAppTermination();
    static void NotifyAppInitialization();
    static uint64 MaxLogSize();
    static uint64 MaxTotalLogSize();

    void Log( KLogLevel level, const kstring& str );

    void Log( KLogLevel level, const sbyte *fmt, ... ) {
	    va_list args;
	    va_start( args, fmt );
	    vLog( level, fmt, args ); 
	    va_end( args );
    };

    void Warning( const sbyte *fmt, ... ) {
	    va_list args;
	    va_start( args, fmt );
	    vLog( klogWarning, fmt, args ); 
	    va_end( args );
    };

    void Error( const sbyte *fmt, ... ) {
	    va_list args;
	    va_start( args, fmt );
	    vLog( klogError, fmt, args ); 
	    va_end( args );
    };

    void Info( const sbyte *fmt, ... ) {
	    va_list args;
	    va_start( args, fmt );
	    vLog( klogInfo, fmt, args ); 
	    va_end( args );
    };

    void Notice( const sbyte *fmt, ... ) {
	    va_list args;
	    va_start( args, fmt );
	    vLog( klogNotice, fmt, args ); 
	    va_end( args );
    }

    void Trace( const sbyte *fmt, ... ) {
	    va_list args;
	    va_start( args, fmt );
	    vLog( klogTrace, fmt, args ); 
	    va_end( args );
    };

    void vLog( KLogLevel level, const sbyte *fmt, va_list args );
    void LogChannel( KLogLevel level, int d, int c, const sbyte *fmt, ... );
    void LogDev( KLogLevel level, int d, const sbyte *fmt, ... );
    void LogDsp( KLogLevel level, int dev, int dsp, const sbyte *fmt, ... );
    void LogCall( KLogLevel level, int callid, const sbyte *fmt, ... );
    void LogLink( KLogLevel level, int d, int c, const sbyte *fmt, ... );
    void LogNai( KLogLevel level, int n, const sbyte *fmt, ... );
    void LogNaiAndCall( KLogLevel level, int n, int k, const sbyte *fmt, ... );
    void LogChanAndCall( KLogLevel level, int d, int callid, int cid, const sbyte *fmt, ... );
};

class KLogBuilder
{
    KLogWriter *Writer;

    bool HeaderLogged;
    KLogLevel Level;
    bool FilterApproved;
    KLogger *Logger;
    bool MutexReleased;
    bool DontDestroy;
    void Constructor( KLogWriter *w, KLogger *l = NULL );
public:
    KLogBuilder( KLogger *log ) {
        Constructor( log->GetWriter(), log );
    }
    KLogBuilder( KLogger &log ) {
        Constructor( log.GetWriter(), &log );
    }

    KLogBuilder( KLogger &log, KLogLevel l ) {
        Constructor( log.GetWriter(), &log );
        LogHeader( l );
    }

    KLogBuilder( KLogWriter *srv ) {
        Constructor( srv );
    }

    KLogBuilder( KLogBuilder &b ) {
        Writer = b.Writer;
        HeaderLogged = b.HeaderLogged;
        Level = b.Level;
        FilterApproved = b.FilterApproved;
        Logger = b.Logger;
        MutexReleased = b.MutexReleased;
        b.DontDestroy = true;
        DontDestroy = false;
    }

    ~KLogBuilder();

    void ReleaseMutex();

    void Log( const sbyte *fmt, ... ) {
	    va_list args;
	    va_start( args, fmt );
        vLog( fmt, args );
	    va_end( args );
    };

    void vLog( const sbyte *fmt, va_list args );

    void LogHeader( KLogLevel l );

    void LogChannel( int d, int c ) {
        Log( "|D%d C%03d| ", d, c );
    };
    void LogDev( int d ) {
        Log( "|D%d| ", d );
    };
    void LogDsp( int dev, int dsp ) {
        Log( "|P%c%d| ", dsp + 'A', dev );
    };
    void LogCall( int callid ) {
        Log( "|K%d| ", callid );
    };
    void LogLink( int d, int l ) {
        Log( "|D%d L%d| ", d, l );
    };
    void LogNai( int n ) {
        Log( "|N%d| ", n );
    };
    void LogNaiAndCall( int n, int callid ) {
        Log( "|N%d K%d| ", n, callid );
    }
    void LogChanAndCall( int d, int c, int callid ) {
        Log( "|D%d C03%d K%d| ", d, c, callid );
    };

    void LogSend() {
        LogDirection( klogRight );
    }
    void LogRecv() {
        LogDirection( klogLeft );
    }
    void LogDirection( KLogDirection d ) {
        Log( klogDirectionStr[ d ] );
    }

    void LogSeparator() {
        Log("| " );
    };
    
    KLogBuilder& operator<<( const char * obj )
    {
        Log( obj );
        return *this;
    }

    template<class T>
    KLogBuilder& operator<<( const T& obj )
    {
        Log( "%s", to_string(obj).c_str() );
        return *this;
    }

    void NewLine( bool logHead = true );
};


class KCrashLogger
{
    kstring Message;
public:
    KCrashLogger();
    ~KCrashLogger();
    void Log( const sbyte *fmt, ... ) {
	    va_list args;
	    va_start( args, fmt );
	    vLog( fmt, args ); 
	    va_end( args );
    };
    void vLog( const sbyte *fmt, va_list args );
};

#endif // KLOGGER_H

#endif
