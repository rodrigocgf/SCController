#if !defined(KPRECOMPHEADER_H)
#define KPRECOMPHEADER_H

#if defined( _WINDOWS ) || defined( _Windows ) || defined( _WIN32 ) || defined( WIN32 )
	#ifndef KWIN32
	#define KWIN32 1
	#endif
    #define NOMINMAX
    #include <winsock2.h>
    #include <windows.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <KTypeDefs.h>

#include "KList.h"
#include "KHostSystem.h"
#include "KBaseException.h"


#ifdef KWIN32

//		#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
	#define Kstdcall __stdcall

#else
	#define Kstdcall 
	#define PLX_LINUX	1
	#include <string.h>
#endif	

#ifndef __BORLANDC__
//O KD3 n�o pode ser compilado no Borland, � muito avan�ado pro C++ dele...
#   include "KD3/kd3.h"
#endif
#endif

