#if !defined KTOOLS_LIST_H
#define KTOOLS_LIST_H

#include "KHostSystem.h"

class KList;
struct KListNode
{
	KListNode *Next;
	KListNode *Prev;
	void *Data;
	KList *List;
};

class KList  
{
protected:
	uint32 InternalCount;
	byte SectionActive;
	KListNode *Head;
	KListNode *Tail;

	KListNode *New();

	KHandle ListMutex;
public:
	virtual void Clear();

	void ActivateSection( byte Active );
    KHandle GetMutex() {
        return ListMutex;
    }
	void Lock();
	void Unlock();

	void Set( int32 Index, void *Value );
	KListNode *Get( uint32 Index );
	KListNode *Add( void *Value );
    KListNode *Insert( void *Value, int32 Position = -1 );
	void Remove( int32 Index );
	void Remove( KListNode *Node );
	uint32 Count() { return InternalCount; }

	KList();
	virtual ~KList();
};

#endif // !defined LIST_H


