
#include "SCStdIncludes.h"
#include <TimerManager.h>

#include "SCController.h"
#include "SCConfigReader.h"
#include "SCConferenceService.h"
//#include "SCIoServiceAcceptor.h"
#include "SCAcceptorMonitoring.h"

#include "log4cpp/PropertyConfigurator.hh"

void LoadLog4cppConfig();
void PrintInfo();
void WaitExit();
void PrintExitingInfo();
bool InitializaK3L();
void FinalizeK3L();

#define VERSION "V.1.3 - 4 de Abril de 2019"

int main(int argc, char* argv[])
{
    PrintInfo();    
    _setmaxstdio(2048);
    
    boost::filesystem::path full_path(argv[0]);
    boost::shared_ptr<SCConfigReader> configReaderPtr(new SCConfigReader(full_path));
	
    if ( !InitializaK3L() ) 
    {
        std::cout << "K3L initialization error!\r\n";
        return 1;
    }
    
    LoadLog4cppConfig();
    SCController::GetInstance().RegisterEventHandler();
    SCController::GetInstance().Initialize(configReaderPtr);
    //SCIoServiceAcceptor::GetInstance().Start();
	
	SCAcceptorMonitoring::GetInstance().Start();
    
    WaitExit();
    PrintExitingInfo();

	SCAcceptorMonitoring::GetInstance().Stop();
    //SCIoServiceAcceptor::GetInstance().Stop();
    SCController::GetInstance().Stop();
    
    FinalizeK3L();
  
	return 0;
}

bool InitializaK3L()
{
    char * ret = NULL;
    ret = k3lStart( 3, 2, 0 );
    if( ret && *ret )
    {
        std::cout << "K3LStart error!\r\n";
        return false;
    }

    if ( k3lSetGlobalParam( klpManualChannelAssociation, 1 ) != ksSuccess ) // aloca��o manual dos canais SIP
    {
        std::cout << "k3lSetGlobalParam( klpManualChannelAssociation, 1 ) returned error !!! \r\n";
        return false;
    }
    
    return true;
}

void FinalizeK3L()
{
    k3lSetGlobalParam( klpManualChannelAssociation, 0 );
    k3lRegisterEventHandler( NULL );
    k3lStop();
}

void WaitExit()
{
    std::string input;

    int maxIo = _getmaxstdio();
    do
    {
        //std::cout << "MaxIO = " << maxIo << std:: endl;
        std::cout << std::endl;
        std::cout << "\t\tDigite 'quit' para sair." << std:: endl;
        std:: cin >> input;
    } while ( !boost::iequals(input,"quit") );
    //} while( input.compare( "quit" ) != 0 );
}

void PrintExitingInfo()
{
    std::cout << "Waiting for LUA scripts finalization. \r\n";
    std::cout << "patience... \r\n";
}

void PrintInfo()
{
    std::string sInfo;

    sInfo =  "                                                                  \r\n";
    sInfo += "                                                                  \r\n";
    sInfo += "                                                                  \r\n";
    sInfo += "                                                                  \r\n";
    sInfo += "  ____   ____ ____            _             _ _                   \r\n";
    sInfo += " / ___| / ___/ ___|___  _ __ | |_ _ __ ___ | | | ___ _ __         \r\n";
    sInfo += " \\___ \\| |  | |   / _ \\| '_ \\| __| '__/ _ \\| | |/ _ \\ '__|  \r\n";
    sInfo += "  ___) | |__| |__| (_) | | | | |_| | | (_) | | |  __/ |           \r\n";
    sInfo += " |____/ \\____\\____\\___/|_| |_|\\__|_|  \\___/|_|_|\\___|_|     \r\n";
    sInfo += "                                                                  \r\n";
    sInfo += "                                                                  \r\n";
    sInfo += "                                                                  \r\n";
    sInfo += "                    VERSAO  : ";
    sInfo += VERSION;
    sInfo += "                               \r\n";

    std::cout << sInfo << std::endl;
}

void LoadLog4cppConfig()
{
	const char *file_log4cpp_init = "log4cpp.properties";
	try
	{
		log4cpp::PropertyConfigurator::configure( file_log4cpp_init );
    }
    catch( log4cpp::ConfigureFailure &e )
    {
		std::cout 	<< e.what() 
					<< " [log4cpp::ConfigureFailure catched] while reading " 
					<< file_log4cpp_init 
					<< std::endl;

					exit(1);
    }
}