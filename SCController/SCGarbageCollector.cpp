
#include "SCGarbageCollector.h"
#include "SCScriptRunner.h"


SCGarbageCollector::SCGarbageCollector() : m_stop(false)
{
    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("GarbageCollector") ); 
    Execute();
}

SCGarbageCollector::~SCGarbageCollector()
{
    m_stop = true;
}

/*
SCGarbageCollector& SCGarbageCollector::GetInstance()
{
    static SCGarbageCollector myInstance;
    return myInstance;
}
*/

void SCGarbageCollector::Execute()
{
    m_ExecuteThreadPtr.reset ( new boost::thread ( boost::bind (&SCGarbageCollector::RunThread, this  )));	
}


void SCGarbageCollector::RunThread( )
{
    
    while( !m_stop )
    {
        boost::mutex::scoped_lock lk(  m_GCMutex );
        while ( ObjectsList.empty() && !m_stop )
            m_cond.wait( lk );
                 
        if ( !m_stop)
        {
            GCData * gcData = ObjectsList.back();
            ObjectsList.pop_back();

            if( gcData )
            {
                switch( gcData->Type )
                {
                case ScriptRunner:
                    SC_LOG4CPP_EX(m_pLog4cpp, "Deleting script runner: %p", gcData->Object );
                    
                    delete (SCScriptRunner*) gcData->Object;
                    break;
                default:
                    break;
                }
                delete gcData;
            }
        }

    }
}

template<> void SCGarbageCollector::AddToGarbage<SCScriptRunner> ( SCScriptRunner* Object )
{
    boost::mutex::scoped_lock lk(  m_GCMutex );
    GCData* gcData = new GCData;
    if( gcData )
    {
        gcData->Type = ScriptRunner;
        gcData->Object = Object;

        {
            //SC_LOG4CPP_EX(m_pLog4cpp,"Adding script runner %p", Object );		    
            ObjectsList.push_front( gcData );
        }
        m_cond.notify_one();
    }
}
