
#pragma once

namespace SCConference
{
    typedef enum
    {
        MONITOR,
        DUPLEX,
    } ConfereeStatus;
};

typedef enum SCScriptingMode
{
    UseInitScript,
    UseDNIS,
} SCScriptingMode;