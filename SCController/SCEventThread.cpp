
#include "SCStdIncludes.h"
#include "SCController.h"
#include "SCEventThread.h"

struct EventData
{
    K3L_EVENT* Event;
    int CallId;
	shared_ptr<SCCall> Call;
};

SCEventThread::SCEventThread( ):    Running( false ) 
                                    
{
    EventList.ActivateSection( true );
    KickOff();
    Running.store( true );
    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("EventsThread") ); 

    SC_LOG4CPP( m_pLog4cpp,"<-v-v-v-v-v-v-v-v-v-v-v- EVENTS THREAD -v-v-v-v-v-v-v-v-v-v-v-v->");

}

SCEventThread::~SCEventThread( )
{
    Running.store( false );
    Terminate();
    EventList.ActivateSection( false );

    SC_LOG4CPP( m_pLog4cpp,"<-^-^-^-^-^-^-^-^-^-^-^- EVENTS THREAD -^-^-^-^-^-^-^-^-^-^-^-^-> ");
}

void SCEventThread::AddEvent( K3L_EVENT* Event, int CallId, shared_ptr<SCCall> Call )
{
    if( ! Running.load() )
        return;
	    
    SC_LOG4CPP_EX( m_pLog4cpp, "ADD EVENT TO PROCESS [%p][%d][0x%02x]", this % CallId % Event->Code );
    
    K3L_EVENT* newEvent = new K3L_EVENT;
    memcpy( newEvent, Event, sizeof( K3L_EVENT ) );
    if( Event->ParamSize > 1 )
    {
        try
        {
            newEvent->Params = new byte[Event->ParamSize];
            memcpy( newEvent->Params, Event->Params, Event->ParamSize );
        }
        catch( ... )
        {
            delete newEvent;
            newEvent = NULL;
            throw;
        }
    }
    else
        newEvent->Params = NULL;

    EventData* evData = NULL;
    try
    {
        evData = new EventData;
    }
    catch( ... )
    {
        delete[] newEvent->Params;
        delete newEvent;
        newEvent = NULL;
        throw;
    }
    if( evData )
    {
        evData->Event = newEvent;
        evData->CallId = CallId;
		evData->Call =   Call;

        EventList.Lock();
        EventList.Add( evData );
        EventList.Unlock();
    }
    else
    {
        delete[] newEvent->Params;
        delete newEvent;
        newEvent = NULL;
    }
    
}

void SCEventThread::Execute()
{
    //verificar se tem evento para executar
    //caso houver liberar a mem�ria ap�s execut�-lo
    EventData* eventData = NULL;
    SCController& controller = SCController::GetInstance();
    while( Running.load() )
    {
        EventList.Lock();
        if( EventList.Count() > 0 )
        {
            KListNode* node = EventList.Get( 0 );
            if( !node )
            {
                EventList.Unlock();
                continue;
            }
            eventData = static_cast<EventData*>( node->Data );
            EventList.Remove( node );
            EventList.Unlock();
            if( eventData )
            {   
                switch(eventData->Event->Code)
                {
                case EV_CALL_FAIL:
                    SC_LOG4CPP_EX( m_pLog4cpp,"[%p][%d] EV_CALL_FAIL \r\n", this % eventData->CallId );
                    
                    break;
                case EV_SIP_DTMF_DETECTED:
                    SC_LOG4CPP_EX( m_pLog4cpp,"[%p][%d] EV_SIP_DTMF_DETECTED \r\n", this % eventData->CallId );
                    
                    break;
                case EV_CHANNEL_FREE:
                    SC_LOG4CPP_EX( m_pLog4cpp,"[%p][%d] EV_CHANNEL_FREE \r\n", this % eventData->CallId );
                    
                    break;
                case EV_END_OF_STREAM:
                    SC_LOG4CPP_EX( m_pLog4cpp,"[%p][%d] EV_END_OF_STREAM \r\n", this % eventData->CallId );
                    
                    break;
                case EV_SIP_MEDIA_START:
                    SC_LOG4CPP_EX( m_pLog4cpp,"[%p][%d] EV_SIP_MEDIA_START \r\n", this % eventData->CallId );
                    
                    break;
                case EV_CONNECT:
                    SC_LOG4CPP_EX( m_pLog4cpp,"[%p][%d] EV_CONNECT \r\n", this % eventData->CallId );
                    
                    break;
                case EV_DISCONNECT:
                    SC_LOG4CPP_EX( m_pLog4cpp,"[%p][%d] EV_DISCONNECT \r\n", this % eventData->CallId );
                    
                    break;

                }
                

                //boost::shared_ptr<SCCall> myCall( controller.GetCallShared( eventData->CallId ) ); // OK
				shared_ptr<SCCall> myCall = eventData->Call; // OK
                if( myCall )
                {
                    try
                    {
                        myCall->HandleEvent( eventData->Event );
                    }
                    catch( std::exception& exc )
                    {
                        
                        SC_LOG4CPP_EX( m_pLog4cpp,"Exception while handling an event on EventThread! Channel[%02d][%03d]: %s.", myCall->GetKhompDevice() % myCall->GetKhompChannel() % exc.what() );
                    }
                    catch( ... )
                    {
                        
                        SC_LOG4CPP_EX( m_pLog4cpp,"Unknown exception while handling an event on EventThread! Channel[%02d][%03d].", myCall->GetKhompDevice() % myCall->GetKhompChannel() );
                    }
                }
                
                if( eventData->Event->Code == EV_CHANNEL_FREE )
                {
                    controller.GetChannelAllocator()->ReleaseChannel( SCChannel( myCall->GetKhompDevice(), myCall->GetKhompChannel() ) );
                    //controller.UnMapChannel(SCChannel( myCall->GetKhompDevice(), myCall->GetKhompChannel() ) );
                    controller.DeleteCall( eventData->CallId );
                }
                

                delete[] eventData->Event->Params;
                eventData->Event->Params = NULL;
                delete eventData->Event;
                eventData->Event = NULL;
                delete eventData;
                eventData = NULL;
            }
        }
        else
        {
            EventList.Unlock();
            Sleep( 32 );
        }
    }
}

