
#include <KD3/Basics/KString.h>
#include <Windows.h>

namespace SCOS
{
    int CreateDir( std::string const& path );
    int RemoveDir( std::string const& path );
    int FindFiles( std::string const& path );
    int FindIndexFile( std::string const& path, int index );
    int GetFileSize( std::string const& path );
    bool FileExists( std::string const& path );
    bool DirExists( std::string const& path );
    FILE* OpenFile( std::string const& Filename, bool Append );
    bool WriteToFile( FILE* File, char const* Text, size_t TextLength );
    bool CloseFile( FILE* File );
    bool RenameFile( std::string const& OriginalPath, std::string const& DestinationPath );
};