#include "SCAcceptorMonitoring.h"


SCAcceptorMonitoring::SCAcceptorMonitoring(void) : m_Acceptor( m_IoService, boost::asio::ip::tcp::endpoint( boost::asio::ip::tcp::v4(), 8081 ) )
{
}


SCAcceptorMonitoring::~SCAcceptorMonitoring(void)
{
}

void SCAcceptorMonitoring::Start()
{

    m_ioServiceThrPtr.reset ( new boost::thread ( boost::bind (&SCAcceptorMonitoring::IoServiceThread, this)));
    m_ioServiceThrPtr->detach();
    
}

void SCAcceptorMonitoring::Accept()
{
	
    boost::shared_ptr<SCMonitoring> SCMonitoringPtr;
    SCMonitoringPtr.reset(new SCMonitoring(m_IoService));
    m_MonitoringList.push_back(SCMonitoringPtr);

    m_Acceptor.async_accept( SCMonitoringPtr->GetSocket(), boost::bind( &SCAcceptorMonitoring::HandleAccept, this, SCMonitoringPtr, _1 ) ); // OK
    
}

void SCAcceptorMonitoring::HandleAccept( boost::shared_ptr<SCMonitoring> p_SCMonitoringPtr, boost::system::error_code const& Error )
{
    std::stringstream ss;
    //BOOST_LOG_FN_ENTRY(ss);
    boost::shared_ptr<SCMonitoring> SCMonitoringPtr;

    if ( p_SCMonitoringPtr.get() != NULL ) 
    {
        p_SCMonitoringPtr->Start();

        SCMonitoringPtr.reset(new SCMonitoring(m_IoService));
        m_MonitoringList.push_back(SCMonitoringPtr);
        m_Acceptor.async_accept( SCMonitoringPtr->GetSocket(), boost::bind( &SCAcceptorMonitoring::HandleAccept, this, SCMonitoringPtr, _1 ) );
    }

    //BOOST_LOG_FN_EXIT( ss );
}


void SCAcceptorMonitoring::IoServiceThread()
{
    std::stringstream ss;
    //SC_LOG4CPP_ENTRY( m_pLog4cpp );

    try 
    {
        Accept();
        m_IoService.run();
    }
    catch( boost::system::system_error& err )
    {
        //ss.str(std::string());
        //ss << boost::format("ERROR: Exception on Service.run(): %s") % err.what();
        
        //BOOST_LOG_TRIVIAL(info) << ss.str();        
        //SC_LOG4CPP_EX( m_pLog4cpp,"Exception on Service.run(): %s", err.what() );
    }
    catch( ... )
    {
        //BOOST_LOG_TRIVIAL(info) << "Unknown exception on Conference Service thread!";
        //SC_LOG4CPP_EX( m_pLog4cpp,"Unknown exception on Conference Service thread!" );        
    }

    //SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCAcceptorMonitoring::Stop()
{
    m_IoService.stop();

    std::list<boost::shared_ptr<SCMonitoring> >::const_iterator iterator;
    for (iterator = m_MonitoringList.begin(); iterator != m_MonitoringList.end(); ++iterator) 
	{
        (*iterator)->Stop();
    }
}