#ifndef _ISCCONTROLLER_H_
#define _ISCCONTROLLER_H_

#include "SCChannel.h"

class SCCall;
//class SCChannel;
class SCConferenceManager;

class ISCController
{
public:
    //virtual void RemoveActiveCall(int callId) = 0;
    virtual void AddToGarbageCollector(void * pRunner) = 0;
    virtual boost::shared_ptr<SCCall> GetCallShared( SCChannel const& Channel ) = 0;
    virtual boost::shared_ptr<SCConferenceManager> GetConferenceManager() const = 0;
};

#endif