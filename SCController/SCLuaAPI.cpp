
#include "SCLuaAPI.h" 
#include "SCLuaXML.h"
#include "SCXMLParser.h"
#include "SCController.h"
#include "SCCall.h"
#include "SCConfigReader.h"
#include "SCOSFunctions.h"
#include "SCCDRLogger.h"

#include <lua.hpp>
#include <iostream>
#include <time.h>
#include <direct.h>
#include <fstream>

#include <KHostSystem.h>

struct lua_sclongjmpnode
{
    jmp_buf jmpBuff;
    struct lua_sclongjmpnode* previous;
};

struct lua_scjmpstack
{
    lua_sclongjmpnode* top;
};

SCConfigReader* GetConfigReader( lua_State* L )
{
    SCConfigReader* myConfigReader;
    lua_getglobal( L, "__MyConfigReader" );
    myConfigReader = static_cast<SCConfigReader*>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return myConfigReader;
}

SCCDRLogger* GetCDRLogger( lua_State* L )
{
    SCCDRLogger* myCDR;
    lua_getglobal( L, "__MyCDR" );
    myCDR = static_cast<SCCDRLogger*>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return myCDR;
}

/***
 * Get the call associated to this state, it is saved as a global on the stack creation
 ***/
SCCall* GetCall( lua_State* L )
{
    SCCall* myCall;
    lua_getglobal( L, "__MyCall" );
    myCall = static_cast<SCCall*>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return myCall;
}

struct lua_scjmpstack* GetJmpStack( lua_State* L )
{
    struct lua_scjmpstack* stack;
    lua_getglobal( L, "__JmpStack" );
    if( lua_isuserdata( L, -1 ) )
        stack = static_cast<struct lua_scjmpstack*>( lua_touserdata( L, -1 ) );
    else
        stack = NULL;
    lua_remove( L, -1 );
    return stack;
}

/***
 * number Answer( number DoubleAnswer, number Billing) 
 ***/
static int lua_scAnswer( lua_State* L )
{
    bool doubleAnswer, billing;
    
    doubleAnswer = ( luaL_opt_int( L, 1, 0 ) != 0 );
    billing = ( luaL_opt_int( L, 2, 1 ) != 0 );

    kstring logMessage;
    logMessage.sprintf( "Answer(%d, %d)", doubleAnswer, billing );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    int ret = myCall->Answer( doubleAnswer, billing );
    lua_pushnumber( L, ( ret == ksSuccess ? 0 : 1 ) );
    
    return 1;
}


/***
 * Hangup()
 ***/
static int lua_scHangup( lua_State* L )
{
    kstring logMessage;
    logMessage.sprintf( "Hangup()" );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    myCall->Hangup();

    return 0;
}


/***
 * string GetDnis()
 ***/
static int lua_scGetDnis( lua_State* L )
{
    char const* dnis;

    kstring logMessage;
    logMessage.sprintf( "GetDnis()" );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    dnis = myCall->GetDNIS().c_str();

    lua_pushstring( L, dnis );
    return 1;
}


/***
 * string GetAni()
 ***/
static int lua_scGetAni( lua_State* L )
{
    char const* ani;

    kstring logMessage;
    logMessage.sprintf( "GetAni()" );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    ani = myCall->GetANI().c_str();

    lua_pushstring( L, ani );
    return 1;
}


/***
 * string GetCallId()
 ***/
static int lua_scGetCallId( lua_State* L )
{
    char const* callid;

    kstring logMessage;
    logMessage.sprintf( "GetCallId()" );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    callid = myCall->GetCallId().c_str();

    lua_pushstring( L, callid );
    return 1;
}


/***
 * SetPlayFormat( string PlayFormat )
 ***/
static int lua_scSetPlayFormat( lua_State* L )
{
    char const* format;
    format = luaL_check_string( L, 1 );

    kstring logMessage;
    logMessage.sprintf( "SetPlayFormat(%s)", format );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    myCall->SetPlayFormat( format );

    return 0;
}


/***
 * SetRecordFormat( string RecordFormat )
 ***/
static int lua_scSetRecordFormat( lua_State* L )
{
    char const* format;
    format = luaL_check_string( L, 1 );

    kstring logMessage;
    logMessage.sprintf( "SetRecordFormat(%s)", format );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    myCall->SetRecordFormat( format );
   
    return 0;
}


/***
 * SetPCM( string PCMFormat )
 ***/
static int lua_scSetPCM( lua_State* L )
{
    char const* format;
    format = luaL_check_string( L, 1 );

    //TODO: logica de envio do formato
    return 0;
}


/***
 * number RemotePlayFile( string Filename )
 ***/
static int lua_scRemotePlayFile( lua_State* L )
{
    char const* filename;
    filename = luaL_check_string( L, 1 );

    kstring logMessage;
    SCCall* myCall = GetCall( L );
    
    logMessage.sprintf( "RemotePlayFile(%s)", filename );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    if( SCOS::FileExists( filename ) )
    {
        lua_pushnumber( L, myCall->RemotePlayFile( filename )  );
    }
    else
    {
        logMessage.sprintf( "File not found: %s", filename );
        myCall->Log( logMessage.c_str(), logMessage.length() );
        lua_pushnumber( L, 1 );
    }

    return 1;
}


/***
 * number RemotePlayFileAsync( string Filename )
 ***/
static int lua_scRemotePlayFileAsync( lua_State* L )
{
    char const* filename;
    filename = luaL_check_string( L, 1 );

    kstring logMessage;
    SCCall* myCall = GetCall( L );
    
    logMessage.sprintf( "RemotePlayFileAsync(%s)", filename );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    if( SCOS::FileExists( filename ) )
    {
        lua_pushnumber( L, myCall->RemotePlayFileAsync( filename )  );
    }
    else
    {
        logMessage.sprintf( "File not found: %s", filename );
        myCall->Log( logMessage.c_str(), logMessage.length() );
        lua_pushnumber( L, 1 );
    }

    return 1;
}


/***
 * number RemoteRecordFile( string Filename, number MaxRecordTime, 
 *                          number MaxSilenceTime, number Beep, string DigitMask )
 ***/
static int lua_scRemoteRecordFile( lua_State* L )
{
    char const* filename;
    char const* digitMask;
    int maxRecordTime, maxSilenceTime;
    bool beep;
    int recordReturn;
    int recordedTime = 0;
    kstring logLine;

    filename       = luaL_check_string( L, 1 );
    maxRecordTime  = luaL_check_int( L, 2 );
    maxSilenceTime = luaL_opt_int( L, 3, 0 );
    beep           = ( luaL_opt_int( L, 4, 1 ) != 0 );
    digitMask      = luaL_opt_string( L, 5, "@" );
    
    logLine.sprintf( "RemoteRecordFile(%s, %d, %d, %d, %s)", filename, maxRecordTime, maxSilenceTime, beep, digitMask );
    
    SCCall* myCall = GetCall( L );
    myCall->Log( logLine.c_str(), logLine.size() );
    recordReturn = myCall->RemoteRecordFile( filename, maxRecordTime, maxSilenceTime, beep, digitMask );
    recordedTime = myCall->GetRecordedTime();
    
    bool silenceTimedOut = (recordReturn == 0 && myCall->RecordSilenceTimedOut());

    lua_pushnumber( L, recordReturn );
    lua_pushnumber( L, (recordReturn != 1 ) ? recordedTime : 0 );
    lua_pushnumber( L,  silenceTimedOut ? 1 : 0 );
    
    return 3;
}


/***
 * string | number GetDigits( number NumDigits, number InterDigitTimeout,
 *                            string DigitMask, number Echo )
 ***/
static int lua_scGetDigits( lua_State* L )
{
    int numDigits, interDigitTimeout;
    bool echo;
    char const* digitMask;
    char const* digitsGot;
    kstring logLine;

    numDigits         = luaL_check_int( L, 1 );
    interDigitTimeout = luaL_check_int( L, 2 );
    digitMask         = luaL_opt_string( L, 3, "@" );
    echo              = ( luaL_opt_int( L, 4, 0 ) != 0 );

    logLine.sprintf( "GetDigits(%d, %d, %s, %d)", numDigits, interDigitTimeout, digitMask, echo );
    
    SCCall* myCall = GetCall( L );
    myCall->Log( logLine.c_str(), logLine.length() );
    digitsGot = myCall->GetDigits( numDigits, interDigitTimeout, digitMask, echo );

    if( myCall->WasDisconnected() )
    {
        lua_pushnumber( L, -1 );
    }
    else
    {
        if( strncmp( digitsGot, "", numDigits ) == 0 )
            lua_pushnil( L );
        else
            lua_pushstring( L, digitsGot );
    }

    return 1;
}


/***
 * number ClearDigits()
 ***/
static int lua_scClearDigits( lua_State* L )
{
    kstring logLine;
    logLine.sprintf( "ClearDigits()" );
    
    SCCall* myCall = GetCall( L );
    myCall->Log( logLine.c_str(), logLine.length() );
    lua_pushnumber( L, myCall->ClearDigits() );
    return 1;
}


/***
 * SetDigitMask( string DigitMask )
 ***/
static int lua_scSetDigitMask( lua_State* L )
{
    char const* digitMask;
    kstring logLine;
    
    digitMask = luaL_check_string( L, 1 );

    logLine.sprintf( "SetDigitMask(%s)", digitMask );
    
    SCCall* myCall = GetCall( L );
    myCall->Log( logLine.c_str(), logLine.length() );
    myCall->SetDigitMask( digitMask );
    return 0;
}


/***
 * number GetSwitch()
 ***/
static int lua_scGetSwitch( lua_State* L )
{
    kstring logMessage;
    logMessage.sprintf( "GetSwitch()" );
            
    SCCall* myCall = GetCall( L );
    SCConfigReader * myConfigReader = GetConfigReader( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, myConfigReader->GetIVRSwitch() );
    //lua_pushnumber( L, SCConfigReader::GetIVRSwitch() );
    return 1;
}


/***
 * number GetMachine()
 ***/
static int lua_scGetMachine( lua_State* L )
{
    kstring logMessage;
    logMessage.sprintf( "GetMachine()" );
            
    SCCall* myCall = GetCall( L );
    SCConfigReader * myConfigReader = GetConfigReader( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, myConfigReader->GetMachine() );
    //lua_pushnumber( L, SCConfigReader::GetMachine() );
    return 1;
}


/***
 * number GetChannel()
 ***/
static int lua_scGetChannel( lua_State* L )
{
    kstring logMessage;
    logMessage.sprintf( "GetChannel()" );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, myCall->GetChannel() );
    return 1;
}

static int lua_scLog( lua_State* L )
{
    char const* message = lua_tostring( L, 1 );
    size_t length = lua_strlen( L, 1 );
    GetCall( L )->Log( message, length );
    return 0;
}

/***
 * Function to get the message generated in case of an error 
 * while executing the script
 ***/
static int lua_scErrorHandler( lua_State* L )
{
    int level = 1;  /* skip level 0 (it's this function) */
    int firstpart = 1;  /* still before eventual `...' */
    lua_Debug ar;
    luaL_Buffer b;
    luaL_buffinit(L, &b);
    luaL_addstring(&b, "error: ");
    luaL_addstring(&b, luaL_check_string(L, 1));
    luaL_addstring(&b, "\n");
    while (lua_getstack(L, level++, &ar)) {
        char buff[120];  /* enough to fit following `sprintf's */
        if (level == 2)
            luaL_addstring(&b, "stack traceback:\n");
        else if (level > 12 && firstpart) {
            /* no more than `LEVELS2' more levels? */
            if (!lua_getstack(L, level+10, &ar))
                level--;  /* keep going */
            else {
                luaL_addstring(&b, "       ...\n");  /* too many levels */
                while (lua_getstack(L, level+10, &ar))  /* find last levels */
                    level++;
            }
            firstpart = 0;
            continue;
        }
        sprintf(buff, "%4d:  ", level-1);
        luaL_addstring(&b, buff);
        lua_getinfo(L, "Snl", &ar);
        switch (*ar.namewhat) {
        case 'g':  case 'l':  /* global, local */
            sprintf(buff, "function `%.50s'", ar.name);
            break;
        case 'f':  /* field */
            sprintf(buff, "method `%.50s'", ar.name);
            break;
        case 't':  /* tag method */
            sprintf(buff, "`%.50s' tag method", ar.name);
            break;
        default: {
            if (*ar.what == 'm')  /* main? */
                sprintf(buff, "main of %.70s", ar.short_src);
            else if (*ar.what == 'C')  /* C function? */
                sprintf(buff, "%.70s", ar.short_src);
            else
                sprintf(buff, "function <%d:%.70s>", ar.linedefined, ar.short_src);
            ar.source = NULL;  /* do not print source again */
                 }
        }
        luaL_addstring(&b, buff);
        if (ar.currentline > 0) {
            sprintf(buff, " at line %d", ar.currentline);
            luaL_addstring(&b, buff);
        }
        if (ar.source) {
            sprintf(buff, " [%.70s]", ar.short_src);
            luaL_addstring(&b, buff);
        }
        luaL_addstring(&b, "\n");
    }
    luaL_pushresult(&b);

    GetCall( L )->Log( lua_tostring( L, -1 ), lua_strlen( L, -1 ) );
    return 0;
}


/***
 * number ConferenceAdd( string ConferenceId, ??? ConferenceSize, string MSISDN, ??? ConferenceType, ??? Supervisor, number Beep )
 ***/
static int lua_scConferenceAdd( lua_State* L )
{
    char const * conferenceId;
    char const * msisdn;
    int conferenceSize, conferenceType, conferenceReturn;
    bool supervisor, beep;

    conferenceId   = luaL_check_string( L, 1 );
    conferenceSize = luaL_check_int( L, 2 );
    msisdn         = luaL_check_string( L, 3 );
    conferenceType = luaL_opt_int( L, 4, 0 );
    supervisor     = ( luaL_opt_int( L, 6, 0 ) != 0 );      // default false
    beep           = ( luaL_opt_int( L, 6, 1 ) != 0 );      // default true

    kstring logMessage;
    logMessage.sprintf( "ConferenceAdd(%s, %d, %s, %d, %d, %d)", conferenceId, conferenceSize, msisdn, conferenceType, supervisor, beep );
    
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    conferenceReturn = myCall->ConferenceAdd( conferenceId, conferenceSize, msisdn, conferenceType, supervisor, beep );

    logMessage.sprintf( "ConferenceAdd(%s, %d, %s, %d, %d, %d) -> Return = %d", conferenceId, conferenceSize, msisdn, conferenceType, supervisor, beep, conferenceReturn );
    myCall->Log( logMessage.c_str(), logMessage.length() );

    lua_pushnumber( L, conferenceReturn );

    return 1;
}


/***
 * number ConferenceRemove( string ConferenceId, number ConferenceType )
 ***/
static int lua_scConferenceRemove( lua_State* L )
{
    char const* conferenceId;
    int conferenceType;

    conferenceId  = luaL_check_string( L, 1 );
    conferenceType = luaL_opt_int( L, 2, 0 );

    kstring logMessage;
    logMessage.sprintf( "ConferenceRemove(%s, %d)", conferenceId, conferenceType );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, myCall->ConferenceRemove( conferenceId, conferenceType ) );
    return 1;
}


/***
 * number GetNumOfUsers( string ConferenceId )
 ***/
static int lua_scGetNumOfUsers( lua_State* L )
{
    char const* conferenceId = luaL_check_string( L, 1 );

    kstring logMessage;
    logMessage.sprintf( "GetNumOfUsers(%s)", conferenceId );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, myCall->GetNumOfUsers( conferenceId ) );
    return 1;
}

/***
 * string GetConferenceId()
 ***/
static int lua_scGetConferenceId( lua_State* L )
{
    kstring logMessage;
    logMessage.sprintf( "GetConferenceId()" );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    kstring conferenceId( myCall->GetConferenceId() );

    if( conferenceId.size() > 0 )
        lua_pushstring( L, conferenceId.c_str() );
    else
        lua_pushnumber( L, -1 );

    return 1;
}


/***
 * string ConferenceListen( string ConferenceId )
 ***/
static int lua_scConferenceListen( lua_State* L )
{
    char const* conferenceId = luaL_check_string( L, 1 );

    kstring logMessage;
    logMessage.sprintf( "ConferenceListen(%s)", conferenceId );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, myCall->ConferenceListen( conferenceId ) );
    return 1;
}


/***
 * string ConferenceUnlisten( string ConferenceId )
 ***/
static int lua_scConferenceUnlisten( lua_State* L )
{
    char const* conferenceId = luaL_check_string( L, 1 );

    kstring logMessage;
    logMessage.sprintf( "ConferenceUnlisten(%s)", conferenceId );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, myCall->ConferenceUnlisten( conferenceId ) );
    return 1;
}

static int lua_scConferenceChangeStatus( lua_State* L )
{
    char const* conferenceId;
    int unlisten;
    SCConference::ConfereeStatus confereeType;

    conferenceId = luaL_check_string( L, 1 );
    confereeType = static_cast<SCConference::ConfereeStatus>( luaL_check_int( L, 2 ) );
    unlisten     = luaL_opt_int( L, 3, 0 );

    kstring logMessage;
    logMessage.sprintf( "ConferenceChangeStatus(%s, %d, %d)", conferenceId, confereeType, unlisten );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, myCall->ConferenceChangeStatus( conferenceId, confereeType, unlisten ) ); 
    return 1;

}

static int lua_scPrintStatus( lua_State* L )
{
	return lua_scLog( L );
}

static int lua_scLogCDR( lua_State* L )
{
	const char* portalName = lua_tostring( L, 1 );
	const char* cdrMessage  = lua_tostring( L, 2 );
	size_t cdrMessageLen = lua_strlen( L, 2 );

    GetCDRLogger( L )->Log( portalName, cdrMessage, cdrMessageLen );
    //lua_pushnumber( L, SCCDRLogger::Log( portalName, cdrMessage, cdrMessageLen ) ); 
	return 1;
}

static int lua_scGetTimeslot( lua_State* L )
{
    kstring logMessage;
    logMessage.sprintf( "GetTimeslot()" );
            
    SCCall* myCall = GetCall( L );
    SCConfigReader * myConfigReader = GetConfigReader( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, myConfigReader->GetIVRTimeslot() );
    //lua_pushnumber( L, SCConfigReader::GetIVRTimeslot() );
	return 1;
}

static int lua_scGetGlobalTimeSlot( lua_State* L )
{
    kstring logMessage;
    logMessage.sprintf( "GetGlobalTimeslot()" );
            
    SCCall* myCall = GetCall( L );
    SCConfigReader * myConfigReader = GetConfigReader( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, myConfigReader->GetIVRGlobalTimeslot() );
    //lua_pushnumber( L, SCConfigReader::GetIVRGlobalTimeslot() );
	return 1;
}

static int lua_scGetLocalIP( lua_State* L )
{
	kstring logMessage;
    logMessage.sprintf( "GetLocalIP()" );
            
    SCCall* myCall = GetCall( L );
    SCConfigReader * myConfigReader = GetConfigReader( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushstring( L, myConfigReader->GetLocalIP().c_str() );
    //lua_pushstring( L, SCConfigReader::GetLocalIP().c_str() );
	return 1;
}

static int lua_scGetDateTime( lua_State* L )
{
    char b[32];
	ktools::time::KSystemTime currTime;
    KHostSystem::GetTime( &currTime );
    
   snprintf( b, sizeof( b ), "%04d-%02d-%02d %02d:%02d:%02d.%03d", currTime.Year, currTime.Month, currTime.Day,
                currTime.Hour, currTime.Minute, currTime.Second, currTime.Milli );
    
    lua_pushstring(L, b);
	return 1;
}

static int lua_scFindFiles( lua_State* L )
{
	char const* p1;
	WIN32_FIND_DATA find_data;
	HANDLE h_file = INVALID_HANDLE_VALUE;
	int cont=0, i;
	
	try 
	{
		/* TODO: Logar isso tamb�m
		LUA_USERDATA *Lua_UserData = (LUA_USERDATA *) LuaGetUserData( L );
		Lua_UserData->IVR->Log(LOG_SCRIPT, "FindFiles()");
		*/
		p1 = luaL_check_string(L, 1);

		kstring logMessage;
        logMessage.sprintf( "FindFiles(%s)", p1 );
        
        SCCall* myCall = GetCall( L );
        myCall->Log( logMessage.c_str(), logMessage.length() );

        h_file = FindFirstFile(p1,&find_data);

		// contando quando arquivos existem
		if (h_file!=INVALID_HANDLE_VALUE) 
		do{
			cont++;
		}while(FindNextFile(h_file,&find_data) != 0);
		lua_pushnumber	(L, cont);

		// Fecha o Handle da procura anterior
		FindClose(h_file);

		// listando nome dos arquivos encontrados
		h_file = FindFirstFile(p1,&find_data);
		
		for(i=1;i<=cont;i++){
			lua_pushstring (L,find_data.cFileName);
			FindNextFile(h_file,&find_data);
		}

		FindClose(h_file);
		return cont+1;
	}
	catch(...)
	{
		//Imprimir mensagem de erro no log
		/*LUA_USERDATA *Lua_UserData = (LUA_USERDATA *) LuaGetUserData( L );
		Lua_UserData->IVR->Log(LOG_ERROR, "EXCEPTION: LuaFindFiles()");*/
		return 0;
	}
}

static int lua_scTime( lua_State* L )
{
    kstring logMessage;
    logMessage.sprintf( "Time()" );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, static_cast<double>(::time( NULL )) );
	return 1;
}

static int lua_scTick( lua_State* L )
{
	try
	{
		kstring logMessage;
        logMessage.sprintf( "Tick()" );
            
        SCCall* myCall = GetCall( L );
        myCall->Log( logMessage.c_str(), logMessage.length() );
        lua_pushnumber( L, GetTickCount() );
		return 1;
	}
	catch( ... )
	{
		// TODO: imprimir mensagem de LOG: EXCEPTION: LuaTime
		return 0;
	}
}

static int lua_scCreateDir( lua_State* L )
{
    if( lua_gettop( L ) < 1 ) return 0;
    char const* path = luaL_check_string( L, 1 );
    kstring logMessage;
    logMessage.sprintf( "CreateDir(%s)", path );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, SCOS::CreateDir( path ) );
    return 1;
}

static int lua_scRemoveDir( lua_State* L )
{
    if( lua_gettop( L ) < 1 ) return 0;
    char const* path = luaL_check_string( L, 1 );
    kstring logMessage;
    logMessage.sprintf( "RemoveDir(%s)", path );
            
    SCCall* myCall = GetCall( L );
    myCall->Log( logMessage.c_str(), logMessage.length() );
    lua_pushnumber( L, SCOS::RemoveDir( path ) );
    return 1;
}

static int lua_scFindIndexFile( lua_State* L )
{
	char const* p1;
	WIN32_FIND_DATA find_data;
	HANDLE h_file = INVALID_HANDLE_VALUE;
	int i;
	int index;
	
	try {
		/* TODO: Log this
		LUA_USERDATA *Lua_UserData = (LUA_USERDATA *) LuaGetUserData( L );
		Lua_UserData->IVR->Log(LOG_SCRIPT, "FindIndexFile()");
		*/

		p1 = luaL_check_string(L, 1);
		index = luaL_check_int(L, 2);

		kstring logMessage;
        logMessage.sprintf( "FindIndexFile(%s, %d)", p1, index );
            
        SCCall* myCall = GetCall( L );
        myCall->Log( logMessage.c_str(), logMessage.length() );

        h_file = FindFirstFile(p1,&find_data);

		if (h_file!=INVALID_HANDLE_VALUE)
		{
			for(i=1;i<index;i++)
			{
				if (FindNextFile(h_file,&find_data) == 0 )
				{
					FindClose(h_file);
					lua_pushnumber	(L, 0);
					return 1;
				}
			}
			lua_pushstring (L,find_data.cFileName);	
		}
		else{
			lua_pushnumber	(L, 0);
		}

		FindClose(h_file);
		return 1;
	}
	catch(...){
		/* TODO: Log this
		LUA_USERDATA *Lua_UserData = (LUA_USERDATA *) LuaGetUserData( L );
		Lua_UserData->IVR->Log(LOG_ERROR, "EXCEPTION: LuaFindIndexFile()");
		*/
		return 0;
	}
}

static int lua_scGetFileSize( lua_State* L )
{
    char const* filename;
	try
	{
		//Verifica��o do numero de argumentos
		if( lua_gettop( L ) < 1 )
		{
			//Lua_UserData->IVR->Log( LOG_ERROR, "LuaGetFileSize(): invalid number of arguments." );
			return 0;
		}
		filename = luaL_check_string(L, 1);
		kstring logMessage;
        logMessage.sprintf( "GetFileSize(%s)", filename );
            
        SCCall* myCall = GetCall( L );
        myCall->Log( logMessage.c_str(), logMessage.length() );
        lua_pushnumber( L, SCOS::GetFileSize( filename ) );
		return 1;
	}
	catch(...)
	{
		/*
		LUA_USERDATA *Lua_UserData = (LUA_USERDATA *) LuaGetUserData( L );
		Lua_UserData->IVR->Log( LOG_ERROR, "EXCEPTION: LuaGetFileSize()" );
		*/
		return 0;
	}
}

static int lua_scGetIVRVendorName( lua_State* L )
{
    lua_pushstring( L, "Khomp" );
    return 1;
}

static int lua_scHangupEx( lua_State* L )
{
    lua_pushnumber( L, 0 );
    return 1;
}


static int lua_scGetLogCDREnabled( lua_State* L )
{
    lua_pushnumber( L, 0 );
    return 1;
}


static int lua_scDoFile( lua_State* L )
{
    struct lua_sclongjmpnode node;
    struct lua_scjmpstack* jmpstack = GetJmpStack( L );

    node.previous = jmpstack->top;
    jmpstack->top = &node;
    
	if( setjmp( node.jmpBuff ) == 0 )
        lua_pushnumber( L, lua_dofile( L, luaL_check_string( L, 1 ) ) );
    
    jmpstack->top = node.previous;
	return 1;
}

static int lua_scExit( lua_State* L )
{
    struct lua_scjmpstack* jmpstack = GetJmpStack( L );
    if( jmpstack->top != NULL )
        longjmp( jmpstack->top->jmpBuff, 1 );
    return 0;
}

static const struct luaL_reg lsupportcomm[] =
{
    { "Answer"                , lua_scAnswer                 },
    { "Hangup"                , lua_scHangup                 },
    { "GetDnis"               , lua_scGetDnis                },
    { "GetAni"                , lua_scGetAni                 },
    { "GetCallId"             , lua_scGetCallId              },
    { "SetPlayFormat"         , lua_scSetPlayFormat          },
    { "SetRecordFormat"       , lua_scSetRecordFormat        },
    { "SetPCM"                , lua_scSetPCM                 },
    { "RemotePlayFile"        , lua_scRemotePlayFile         },
    { "RemotePlayFileAsync"   , lua_scRemotePlayFileAsync    },
    { "RemoteRecordFile"      , lua_scRemoteRecordFile       },
    { "GetDigits"             , lua_scGetDigits              },
    { "ClearDigits"           , lua_scClearDigits            },
    { "SetDigMask"            , lua_scSetDigitMask           },
    { "GetMachine"            , lua_scGetMachine             },
    { "GetChannel"            , lua_scGetChannel             },
	{ "Log"                   , lua_scLog                    },
	{ "GetTimeSlot"           , lua_scGetTimeslot            },
	{ "GetSwitch"             , lua_scGetSwitch              },
	{ "GetLocalIP"            , lua_scGetLocalIP             },
	{ "GetDateTime"           , lua_scGetDateTime            },
	{ "PrintStatus"           , lua_scPrintStatus            },
	{ "LogCdr"                , lua_scLogCDR                 },
	{ "GetGlobalTimeSlot"     , lua_scGetGlobalTimeSlot      },
	{ "FindFiles"             , lua_scFindFiles              },
	{ "Time"                  , lua_scTime                   },
	{ "Tick"                  , lua_scTick                   },
	{ "CreateDir"             , lua_scCreateDir              },
	{ "RemoveDir"             , lua_scRemoveDir              },
	{ "FindIndexFile"         , lua_scFindIndexFile          },
	{ "GetFileSize"           , lua_scGetFileSize            },
    { "GetIVRVendorName"      , lua_scGetIVRVendorName       },
    { "HangupEx"              , lua_scHangupEx               },
    { "ConferenceAdd"         , lua_scConferenceAdd          },
    { "ConferenceRemove"      , lua_scConferenceRemove       },
    { "ConferenceListen"      , lua_scConferenceListen       },
    { "ConferenceUnlisten"    , lua_scConferenceUnlisten     },
    { "ConferenceChangeStatus", lua_scConferenceChangeStatus },
    { "GetNumOfUsers"         , lua_scGetNumOfUsers          },
    { "GetConferenceID"       , lua_scGetConferenceId        },
    { "GetLogCDREnabled"      , lua_scGetLogCDREnabled       },
};

int lua_scReadFile( SCCall* MyCall, char const* Filename )
{
    struct lua_scjmpstack jmpStack;
    jmpStack.top = NULL;
    
    //lua_State* newState = lua_open( 65536 );
    lua_State* newState = lua_open( 32768 );
    //lua_State* newState = lua_open( 16384 );

    lua_pushuserdata( newState, MyCall );              // save the class that handles de call related to the scpript
    lua_setglobal( newState, "__MyCall" );

    lua_pushuserdata( newState, &jmpStack );
    lua_setglobal( newState, "__JmpStack" );

    luaL_openl( newState, lsupportcomm );              // load the functions so that they are acessible by the script
    lua_baselibopen( newState );                       // load the base lib
    lua_iolibopen( newState );                         // also load the i/o lib
    lua_strlibopen( newState );                        // and the string lib
	lua_dblibopen( newState );
	lua_mathlibopen( newState );
	lua_socketlibopen( newState );
    lua_sqlodbclibopen ( newState );

    lua_pushcfunction( newState, lua_scExit );          // substituting de exit so that it doesn't finish the whole process,
    lua_setglobal( newState, "exit" );                  // just the script that called it

    lua_pushcfunction( newState, lua_scDoFile );
    lua_setglobal( newState, "dofile" );

    lua_pushcfunction( newState, lua_scErrorHandler ); //
    lua_setglobal( newState, "_ERRORMESSAGE" );        // overload the global function that handles what is done to error messages

    int ret = lua_dofile( newState, Filename );        // read and run the script
    
    try
    {
        lua_close( newState );                             // close and clear the lua_State after finished executing the script
        winsock_close();
    }
    catch( ... )
    {
        printf( "Exception while closing the lua state[%d][%d].\n", MyCall->GetKhompDevice(), MyCall->GetKhompChannel() );
    }

    return ret;
}

int lua_scReadFileCDR( SCCDRLogger* MyCDR, SCCall* MyCall, char const* Filename )
{
    struct lua_scjmpstack jmpStack;
    jmpStack.top = NULL;
    
    //lua_State* newState = lua_open( 65536 );
    lua_State* newState = lua_open( 32768 );
    //lua_State* newState = lua_open( 16384 );

    lua_pushuserdata( newState, MyCall );              // save the class that handles de call related to the scpript
    lua_setglobal( newState, "__MyCall" );

    lua_pushuserdata( newState, MyCDR );              
    lua_setglobal( newState, "__MyCDR");

    lua_pushuserdata( newState, &jmpStack );
    lua_setglobal( newState, "__JmpStack" );

    luaL_openl( newState, lsupportcomm );              // load the functions so that they are acessible by the script
    lua_baselibopen( newState );                       // load the base lib
    lua_iolibopen( newState );                         // also load the i/o lib
    lua_strlibopen( newState );                        // and the string lib
	lua_dblibopen( newState );
	lua_mathlibopen( newState );
	lua_socketlibopen( newState );
    lua_sqlodbclibopen ( newState );

    lua_pushcfunction( newState, lua_scExit );          // substituting de exit so that it doesn't finish the whole process,
    lua_setglobal( newState, "exit" );                  // just the script that called it

    lua_pushcfunction( newState, lua_scDoFile );
    lua_setglobal( newState, "dofile" );

    lua_pushcfunction( newState, lua_scErrorHandler ); //
    lua_setglobal( newState, "_ERRORMESSAGE" );        // overload the global function that handles what is done to error messages

    int ret = lua_dofile( newState, Filename );        // read and run the script
    
    try
    {
        lua_close( newState );                             // close and clear the lua_State after finished executing the script
        winsock_close();
    }
    catch( ... )
    {
        printf( "Exception while closing the lua state[%d][%d].\n", MyCall->GetKhompDevice(), MyCall->GetKhompChannel() );
    }

    return ret;
}


int lua_scReadFileCDR( SCCDRLogger* MyCDR, SCCall* MyCall, SCConfigReader * MyConfigReader, char const* Filename )
{
    struct lua_scjmpstack jmpStack;
    jmpStack.top = NULL;
    XmlParser   xmlParser;
    
    //lua_State* newState = lua_open( 65536 );
    lua_State* newState = lua_open( 32768 );
    //lua_State* newState = lua_open( 16384 );

    lua_pushuserdata( newState, &xmlParser );              
    lua_setglobal( newState, "__MyXmlParser");

    lua_pushuserdata( newState, MyCall );              // save the class that handles de call related to the scpript
    lua_setglobal( newState, "__MyCall" );

    lua_pushuserdata( newState, MyCDR );              
    lua_setglobal( newState, "__MyCDR");

    lua_pushuserdata( newState, MyConfigReader );              
    lua_setglobal( newState, "__MyConfigReader");

    lua_pushuserdata( newState, &jmpStack );
    lua_setglobal( newState, "__JmpStack" );

    luaL_openl( newState, lsupportcomm );              // load the functions so that they are acessible by the script
    Lua_XmlLibOpen( newState);                         // load XmlLibFunctions at SCLuaXML.cpp    

    lua_baselibopen( newState );                       // load the base lib
    lua_iolibopen( newState );                         // also load the i/o lib
    lua_strlibopen( newState );                        // and the string lib
	lua_dblibopen( newState );
	lua_mathlibopen( newState );
	lua_socketlibopen( newState );
    lua_sqlodbclibopen ( newState );

    lua_pushcfunction( newState, lua_scExit );          // substituting de exit so that it doesn't finish the whole process,
    lua_setglobal( newState, "exit" );                  // just the script that called it

    lua_pushcfunction( newState, lua_scDoFile );
    lua_setglobal( newState, "dofile" );

    lua_pushcfunction( newState, lua_scErrorHandler ); //
    lua_setglobal( newState, "_ERRORMESSAGE" );        // overload the global function that handles what is done to error messages

    int ret = lua_dofile( newState, Filename );        // read and run the script
    
    try
    {
        lua_close( newState );                             // close and clear the lua_State after finished executing the script
        winsock_close();
    }
    catch( ... )
    {
        printf( "Exception while closing the lua state[%d][%d].\n", MyCall->GetKhompDevice(), MyCall->GetKhompChannel() );
    }

    return ret;
}
