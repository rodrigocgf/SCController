#include "SCConferenceRoom.h"

ConferenceRoom::ConferenceRoom( const std::string & roomId ,int Size ) : m_RoomId(roomId) , RoomSize( Size )  
{
    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("ConferenceRoom") ); 

    SC_LOG4CPP_EX( m_pLog4cpp,"vvvvvvvvvvvvvvvvvvvvvvvvv CONFERENCE ROOM %s vvvvvvvvvvvvvvvvvvvvvvvvvvvvv", m_RoomId.c_str() );
}

ConferenceRoom::~ConferenceRoom() 
{ 
    //Users.clear(); 
    //Listeners.clear();

    m_Container.clear();
    Supervisors.clear();

    SC_LOG4CPP_EX( m_pLog4cpp,"^^^^^^^^^^^^^^^^^^^^^^^^^ CONFERENCE ROOM %s ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^", m_RoomId.c_str() );
}

void ConferenceRoom::ClearUsers()
{

    m_Container.clear();
    //Users.clear();

    /*
    if ( itRooms->second->GetUsers().size() > 0 ) 
    {   
        std::map< std::string, KChannel >::iterator itUsers;
        for ( itUsers = itRooms->second->GetUsers().begin() ; itUsers != itRooms->second->GetUsers().end() ; itUsers++ )
        {   
            RemoveFromRoom( strRoomId.c_str() , itUsers->first.c_str() );
        }

        std::map< std::string, KChannel >::iterator itListeners;
        for ( itUsers = itRooms->second->GetListeners().begin() ; itUsers != itRooms->second->GetListeners().end() ; itUsers++ )
        {

        }
        
    }
    */
}

bool ConferenceRoom::AddUser( char const* UserId, int UserDevice, int UserChannel, bool Supervisor, bool Beep )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    SCConference::ConfereeStatus    status;
    boost::shared_ptr<RoomUser> roomUserPtr = nullptr;
    std::stack<boost::shared_ptr<RoomUser>> roomUserStack;

    bool userAdded = false;
    if( Users.size() < RoomSize )
    {
        
        boost::upgrade_lock< boost::shared_mutex > lock( m_Mutex );
		boost::upgrade_to_unique_lock<boost::shared_mutex>  unique_lock(lock);

        auto userItr = m_Container.get<ByUserId>().find( UserId );
        if ( userItr == m_Container.get<ByUserId>().end() )
        {
            userAdded = true;
            KChannel myChannel( UserDevice, UserChannel );

            for ( userItr = m_Container.get<ByUserId>().begin(); userItr != m_Container.get<ByUserId>().end() ; userItr++ )
            {
                boost::shared_ptr<RoomUser> roomUser = *userItr;
                status = roomUser->Status;

                if ( status != SCConference::MONITOR )
                {
                    userAdded &= ( roomUser->ChannelUser.MixChannel( myChannel ) == ksSuccess );
                    userAdded &= ( myChannel.MixChannel( roomUser->ChannelUser ) == ksSuccess );

                    if( !userAdded )
                    {
                        SC_LOG4CPP_EX( m_pLog4cpp," [MSISDN=%s] Error while mixing channel[%02d][%03d] audio!", UserId % UserDevice % UserChannel );

                        // if any mix failed undo all the mixes already made.
                        roomUser->ChannelUser.UnmixChannel( myChannel );
                        myChannel.UnmixChannel( roomUser->ChannelUser );

                        while ( !roomUserStack.empty() )
                        {
                            boost::shared_ptr<RoomUser> stacketRoomUser = roomUserStack.top();

                            stacketRoomUser->ChannelUser.UnmixChannel( myChannel );
                            myChannel.UnmixChannel( stacketRoomUser->ChannelUser );

                            roomUserStack.pop();
                        }

                        break;
                    } 
                    else
                    {
                        roomUserStack.push(roomUser);
                    }
                }
            }

            if( userAdded )
            {
                SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] ADDED to Users {ROOM SIZE : %d}",  UserId %  m_Container.size());                

                // OK, all channels were mixed without errors
                if( Beep ) myChannel.Beep();
                if( Supervisor ) Supervisors.push_back( UserId );                
                
                roomUserPtr.reset(new RoomUser( UserId, myChannel , SCConference::DUPLEX ) );
                m_Container.insert(roomUserPtr);

                PrintRoom();

                SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] ADDED to Room [%s]",  UserId % m_RoomId.c_str() );

                // if someone is listening add the channel's audio to them
                for( auto otherItr = m_Container.get<ByUserId>().begin(); otherItr != m_Container.get<ByUserId>().end(); otherItr++ )
                {
                    boost::shared_ptr<RoomUser> roomUserPtr = (*otherItr);
                    if ( roomUserPtr->ChannelListener != KChannel(-1,-1) )
                    {
                        roomUserPtr->ChannelListener.MixChannel( myChannel );
                    }
                }                
            }
            else
                SC_LOG4CPP_EX( m_pLog4cpp,"User Id [%] NOT ADDED to Room [%s]", UserId % m_RoomId.c_str() );                
        }
        else
        {
            SC_LOG4CPP_EX( m_pLog4cpp,"User Id [%] is already on the Room [%s]", UserId % m_RoomId.c_str() );            
            
            // User already inserted, check if the supervisor credential was changed
            std::vector< std::string >::iterator supervisorIt = IsSupervisor( UserId ); 
            
            if( supervisorIt != Supervisors.end() && !Supervisor )
                Supervisors.erase( supervisorIt );
            else if( supervisorIt == Supervisors.end() && Supervisor )
                Supervisors.push_back( UserId );
        }
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"Room [%s] is FULL !", m_RoomId.c_str() );        
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return userAdded;
    

}

bool ConferenceRoom::AddUser_Try1( char const* UserId, int UserDevice, int UserChannel, bool Supervisor, bool Beep )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    SCConference::ConfereeStatus    status;
    boost::shared_ptr<RoomUser> roomUserPtr = nullptr;

    bool userAdded = false;
    if( Users.size() < RoomSize )
    {
        
        boost::upgrade_lock< boost::shared_mutex > lock( m_Mutex );
		boost::upgrade_to_unique_lock<boost::shared_mutex>  unique_lock(lock);

        auto userItr = m_Container.get<ByUserId>().find( UserId );
        if ( userItr == m_Container.get<ByUserId>().end() )
        {
            userAdded = true;

            KChannel myChannel( UserDevice, UserChannel );

            for ( userItr = m_Container.get<ByUserId>().begin(); userItr != m_Container.get<ByUserId>().end() ; userItr++ )
            {
                boost::shared_ptr<RoomUser> roomUser = *userItr;
                status = roomUser->Status;

                if ( status != SCConference::MONITOR )
                {
                    userAdded &= ( roomUser->ChannelUser.MixChannel( myChannel ) == ksSuccess );
                    userAdded &= ( myChannel.MixChannel( roomUser->ChannelUser ) == ksSuccess );

                    if( !userAdded )
                    {
                        SC_LOG4CPP_EX( m_pLog4cpp," [MSISDN=%s] Error while mixing channel[%02d][%03d] audio!", UserId % UserDevice % UserChannel );
                            
                        // if any mix failed undo all the mixes already made.
                        roomUser->ChannelUser.UnmixChannel( myChannel );
                        myChannel.UnmixChannel( roomUser->ChannelUser );

                        for( auto otherItr = m_Container.get<ByUserId>().begin(); otherItr != userItr ; otherItr++ )
                        {
                            boost::shared_ptr<RoomUser> roomOtherPtr = *otherItr;

                            //if ( roomOtherPtr->ChannelUser != roomUser->ChannelUser )
                            //if ( otherItr != userItr )
                            {
                                (*otherItr)->ChannelUser.UnmixChannel( myChannel );
                                myChannel.UnmixChannel( (*otherItr)->ChannelUser );                                
                            }
                        }
                        break;
                    }
                }
            }

            if( userAdded )
            {
                SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] ADDED to Users {ROOM SIZE : %d}",  UserId %  m_Container.size());                

                // OK, all channels were mixed without errors
                if( Beep ) myChannel.Beep();
                if( Supervisor ) Supervisors.push_back( UserId );                
                
                roomUserPtr.reset(new RoomUser( UserId, myChannel , SCConference::DUPLEX ) );
                m_Container.insert(roomUserPtr);

                PrintRoom();

                SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] ADDED to Room [%s]",  UserId % m_RoomId.c_str() );

                // if someone is listening add the channel's audio to them
                for( auto otherItr = m_Container.get<ByUserId>().begin(); otherItr != m_Container.get<ByUserId>().end(); otherItr++ )
                {
                    boost::shared_ptr<RoomUser> roomUserPtr = (*otherItr);
                    if ( roomUserPtr->ChannelListener != KChannel(-1,-1) )
                    {
                        roomUserPtr->ChannelListener.MixChannel( myChannel );
                    }
                }                
            }
            else
                SC_LOG4CPP_EX( m_pLog4cpp,"User Id [%] NOT ADDED to Room [%s]", UserId % m_RoomId.c_str() );                
        }
        else
        {
            SC_LOG4CPP_EX( m_pLog4cpp,"User Id [%] is already on the Room [%s]", UserId % m_RoomId.c_str() );            
            
            // User already inserted, check if the supervisor credential was changed
            std::vector< std::string >::iterator supervisorIt = IsSupervisor( UserId ); 
            
            if( supervisorIt != Supervisors.end() && !Supervisor )
                Supervisors.erase( supervisorIt );
            else if( supervisorIt == Supervisors.end() && Supervisor )
                Supervisors.push_back( UserId );
        }
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"Room [%s] is FULL !", m_RoomId.c_str() );        
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return userAdded;

}

bool ConferenceRoom::AddUser_Former( char const* UserId, int UserDevice, int UserChannel, bool Supervisor, bool Beep )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    bool userAdded = false;
    std::map< std::string, SCConference::ConfereeStatus >::iterator itMuteUsers;

    if( Users.size() < RoomSize )
    {
        userAdded = true;
        auto usersEnd = Users.end();
        // check if the user isn't already in the room
        auto user = Users.find( UserId );

        if( user == usersEnd )
        {
            // if the user was not already added, connect all the mixers and then beep him and add to the room
            KChannel myChannel( UserDevice, UserChannel );
            for( user = Users.begin(); user != usersEnd; ++user )
            {
                itMuteUsers = MuteUsers.find(user->first);
                if ( itMuteUsers != MuteUsers.end() )
                {
                    if ( itMuteUsers->second != SCConference::MONITOR ) 
                    {

                        userAdded &= ( user->second.MixChannel( myChannel ) == ksSuccess );
                        userAdded &= ( myChannel.MixChannel( user->second ) == ksSuccess );
                        if( !userAdded )
                        {
                            SC_LOG4CPP_EX( m_pLog4cpp," [MSISDN=%s] Error while mixing channel[%02d][%03d] audio!", UserId % UserDevice % UserChannel );
                            
                            // if any mix failed undo all the mixes already made.
                            user->second.UnmixChannel( myChannel );
                            myChannel.UnmixChannel( user->second );
                            for( auto other = Users.begin(); other != user; ++other )
                            {
                                other->second.UnmixChannel( myChannel );
                                myChannel.UnmixChannel( other->second );
                            }
                            break;
                        }
                    }
                }
            }
            if( userAdded )
            {
                SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] ADDED to Users {ROOM SIZE : %d}",  UserId %  Users.size());                

                // OK, all channels were mixed without errors
                if( Beep ) myChannel.Beep();
                if( Supervisor ) Supervisors.push_back( UserId );
                Users.insert( std::pair< char const*, KChannel >( UserId, myChannel ) );

                SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] ADDED to Room [%s]",  UserId % m_RoomId.c_str() );                

                MuteUsers.insert( std::pair< char const*, SCConference::ConfereeStatus>( UserId, SCConference::DUPLEX ) );
                // if someone is listening add the channel's audio to them
                if( Listeners.size() > 0 )
                {
                    auto listenersEnd = Listeners.end();
                    for( auto listener = Listeners.begin(); listener != listenersEnd; ++listener )
                    {
                        listener->second.MixChannel( myChannel );
                    }
                }
            }
            else
                SC_LOG4CPP_EX( m_pLog4cpp,"User Id [%] NOT ADDED to Room [%s]", UserId % m_RoomId.c_str() );
                //SC_LOG4CPP_EX( m_pLog4cpp," The MSISDN %s could not be added in the room! channel[%02d][%03d] audio!", UserId % UserDevice % UserChannel );
                
        }
        else
        {
            SC_LOG4CPP_EX( m_pLog4cpp,"User Id [%] is already on the Room [%s]", UserId % m_RoomId.c_str() );
            //SC_LOG4CPP_EX( m_pLog4cpp," The MSISDN %s already on the room! channel[%02d][%03d] audio!", UserId % UserDevice % UserChannel );
            
            // User already inserted, check if the supervisor credential was changed
            std::vector< std::string >::iterator supervisorIt = IsSupervisor( UserId ); 
            
            if( supervisorIt != Supervisors.end() && !Supervisor )
                Supervisors.erase( supervisorIt );
            else if( supervisorIt == Supervisors.end() && Supervisor )
                Supervisors.push_back( UserId );
        }
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"Room [%s] is FULL !", m_RoomId.c_str() );
        //SC_LOG4CPP_EX( m_pLog4cpp," [MSISDN=%s] Room is full! channel[%02d][%03d] audio!", UserId % UserDevice % UserChannel );        
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return userAdded;
}

bool ConferenceRoom::RemoveUser( char const* UserId )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    SCConference::ConfereeStatus    status;
    bool userRemoved = false;

    if ( m_Container.size() > 0 )
    {
        auto userItr = m_Container.get<ByUserId>().find( UserId );
        if ( userItr != m_Container.get<ByUserId>().end() )
        {
            boost::upgrade_lock< boost::shared_mutex > lock( m_Mutex );
		    boost::upgrade_to_unique_lock<boost::shared_mutex>  unique_lock(lock);

            boost::shared_ptr<RoomUser> roomUserPtr = *userItr;

            for ( auto otherItr = m_Container.get<ByUserId>().begin() ; otherItr != m_Container.get<ByUserId>().end() ; otherItr++ )
            {
                boost::shared_ptr<RoomUser> roomOtherPtr = *otherItr;
                status = roomOtherPtr->Status;

                if ( status != SCConference::MONITOR )
                {
                    if ( roomOtherPtr->ChannelUser != roomUserPtr->ChannelUser )                    
                    {
                        roomOtherPtr->ChannelUser.UnmixChannel( roomUserPtr->ChannelUser );
                        roomUserPtr->ChannelUser.UnmixChannel( roomOtherPtr->ChannelUser );

                        SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] REMOVED from Room [%s]", UserId % m_RoomId.c_str()  );                
                    }
                }
            }

            auto isSupervisor = IsSupervisor( UserId );
            if( isSupervisor != Supervisors.end() )
                Supervisors.erase( isSupervisor );

            // if someone is listening add the channel's audio to them
            for( auto anotherItr = m_Container.get<ByUserId>().begin(); anotherItr != m_Container.get<ByUserId>().end(); anotherItr++ )
            {
                boost::shared_ptr<RoomUser> roomAnotherUserPtr = (*anotherItr);
                if ( roomAnotherUserPtr->ChannelListener != KChannel(-1,-1) )
                {
                    roomAnotherUserPtr->ChannelListener.UnmixChannel( roomUserPtr->ChannelUser );
                }
            }

            roomUserPtr->ChannelUser.ClearMixer();            
            m_Container.get<ByUserId>().erase(userItr);
            SC_LOG4CPP_EX( m_pLog4cpp,"UserId [%s] REMOVED from Users {ROOM SIZE : %d}", UserId % m_Container.size());

            PrintRoom();
            
            userRemoved = true;
        }
        else
            SC_LOG4CPP_EX( m_pLog4cpp," User Id [%s] is not in this Room [%s]", UserId % m_RoomId.c_str() );
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"Room [%s] is EMPTY !", m_RoomId.c_str() );        
    }
        

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return userRemoved;

}

bool ConferenceRoom::RemoveUser_Former( char const* UserId )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    bool userRemoved = false;
    std::map< std::string, SCConference::ConfereeStatus >::iterator itMuteUsers;


    if( !Users.empty() )
    {
        auto usersEnd = Users.end();
        // check if the user is in this room
        auto user = Users.find( UserId );
        if( user != usersEnd )
        {
            // if he is unmix all the other channels from him
            for( auto other = Users.begin(); other != usersEnd; ++other )
            {
                itMuteUsers = MuteUsers.find(other->first);
                if ( itMuteUsers != MuteUsers.end() )
                {
                    if ( itMuteUsers->second != SCConference::MONITOR ) 
                    {

                        if( other != user ) // no point in unmixing a channel from itself
                        {
                            other->second.UnmixChannel( user->second );
                            user->second.UnmixChannel( other->second );

                            SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] REMOVED from Room [%s]", UserId % m_RoomId.c_str()  );                
                        }
                    }
                }
            }
            
            auto isSupervisor = IsSupervisor( UserId );
            if( isSupervisor != Supervisors.end() )
                Supervisors.erase( isSupervisor );

            // if someone is listening remove the channel from their audio
            if( Listeners.size() > 0 )
            {
                auto listenersEnd = Listeners.end();
                for( auto listener = Listeners.begin(); listener != listenersEnd; ++listener )
                {
                    listener->second.UnmixChannel( user->second );
                }
            }

            user->second.ClearMixer();
            Users.erase( user );

            SC_LOG4CPP_EX( m_pLog4cpp,"UserId [%s] REMOVED from Users {ROOM SIZE : %d}", UserId % Users.size());                

            MuteUsers.erase( UserId );
            userRemoved = true;
        }
        else
            SC_LOG4CPP_EX( m_pLog4cpp," User Id [%s] is not in this Room [%s]", UserId % m_RoomId.c_str() );
            
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"Room [%s] is EMPTY !", m_RoomId.c_str() );
        //SC_LOG4CPP( m_pLog4cpp,"ConferenceRoom::RemoveUser: Room is empty!" );
    }
        

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return userRemoved;
}

std::vector< std::string >::iterator ConferenceRoom::IsSupervisor( char const* UserId )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    std::vector<std::string >::iterator isSupervisorIt = Supervisors.end();
    if( Supervisors.size() > 0 )
    {
        for( isSupervisorIt = Supervisors.begin(); isSupervisorIt != Supervisors.end(); ++isSupervisorIt )
        {
            if( (*isSupervisorIt).compare( UserId ) == 0 )
            {
                break;
            }
        }
    }
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return isSupervisorIt;
}



bool ConferenceRoom::Listen( char const* UserId )
{
    boost::shared_lock< boost::shared_mutex > lock( m_Mutex );	

    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool listening = true;

    auto userItr = m_Container.get<ByUserId>().find( UserId );
    if ( userItr != m_Container.get<ByUserId>().end() )
    {
        boost::shared_ptr<RoomUser> roomUserPtr = *userItr;
        
        //listening &= UndeafenUser( roomUserPtr->ChannelUser );
        //if( listening )        
            listening &= UnmuteUser( roomUserPtr->ChannelUser );
        

        if ( listening )
            SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] ADDED to Room [%s]",  UserId % m_RoomId.c_str() );                
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," User %s is not in the room!", UserId );
        
        listening = false;
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return listening;
}

bool ConferenceRoom::Listen_Former( char const* UserId )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    bool listening = true;
    auto user = Users.find( UserId );
    if( user != Users.end() )
    {
        listening &= UndeafenUser( user->second );
        if( listening )
        {
            listening &= UnmuteUser( user->second );
        }

        if ( listening )
            SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] ADDED to Room [%s]",  UserId % m_RoomId.c_str() );                
        //TODO: uma forma de tratar caso ocorra alguma falha em uma das operacoes
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," User %s is not in the room!", UserId );
        
        listening = false;
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return listening;
}

bool ConferenceRoom::Unlisten( char const* UserId )
{
    boost::shared_lock< boost::shared_mutex > lock( m_Mutex );	

    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool listening = true;

    auto userItr = m_Container.get<ByUserId>().find( UserId );
    if ( userItr != m_Container.get<ByUserId>().end() )
    {
        boost::shared_ptr<RoomUser> roomUserPtr = *userItr;

        //listening &= DeafenUser( roomUserPtr->ChannelUser );
        //if( listening )
            listening &= MuteUser( roomUserPtr->ChannelUser );
        
        //TODO: uma forma de tratar caso ocorra alguma falha em uma das operacoes
        if ( listening )
            SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] REMOVED from Room [%s]",  UserId % m_RoomId.c_str() );                
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," User %s is not in the room!", UserId );
        
        listening = false;
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return listening;

}

bool ConferenceRoom::Unlisten_Former( char const* UserId )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    bool listening = true;
    auto user = Users.find( UserId );
    if( user != Users.end() )
    {
        listening &= DeafenUser( user->second );
        if( listening )
            listening &= MuteUser( user->second );
        
        //TODO: uma forma de tratar caso ocorra alguma falha em uma das operacoes
        if ( listening )
            SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] REMOVED from Room [%s]",  UserId % m_RoomId.c_str() );                
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," User %s is not in the room!", UserId );
        
        listening = false;
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return listening;
}

bool ConferenceRoom::ChangeStatus( char const* UserId, SCConference::ConfereeStatus Status )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool changed = false;

    auto userItr = m_Container.get<ByUserId>().find( UserId );
    if ( userItr != m_Container.get<ByUserId>().end() )
    {
        boost::shared_ptr<RoomUser> roomUserPtr = *userItr;

        switch( Status )
        {
        case SCConference::MONITOR:    
            changed = MuteUser( roomUserPtr->ChannelUser );
            break;
        case SCConference::DUPLEX:
            changed = UnmuteUser( roomUserPtr->ChannelUser );
            break;
        default: 
            SC_LOG4CPP_EX( m_pLog4cpp," Unknown status %d", Status );
            
            break;
        }
    }
    else
        SC_LOG4CPP_EX( m_pLog4cpp," User %s is not in the room!", UserId );
        
        
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return changed;
}

bool ConferenceRoom::ChangeStatus_Former( char const* UserId, SCConference::ConfereeStatus Status )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool changed = false;

    auto user = Users.find( UserId );
    if( user != Users.end() )
    {
        switch( Status )
        {
        case SCConference::MONITOR:    
            changed = MuteUser( user->second );
            break;
        case SCConference::DUPLEX:
            changed = UnmuteUser( user->second );
            break;
        default: 
            SC_LOG4CPP_EX( m_pLog4cpp," Unknown status %d", Status );
            
            break;
        }
    }
    else
        SC_LOG4CPP_EX( m_pLog4cpp," User %s is not in the room!", UserId );
        
        
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return changed;
}

bool ConferenceRoom::MuteUser( KChannel& UserChannel )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool mute = true;

    
    for ( auto userItr = m_Container.get<ByUserId>().begin(); userItr != m_Container.get<ByUserId>().end(); userItr++ )
    {
        boost::shared_ptr<RoomUser> roomUserPtr = *userItr;
        if ( roomUserPtr->ChannelUser != UserChannel )
        {
            if ( roomUserPtr->Status != SCConference::MONITOR )
            {
                mute &= ( roomUserPtr->ChannelUser.UnmixChannel( UserChannel ) == ksSuccess );
                mute &= ( UserChannel.UnmixChannel( roomUserPtr->ChannelUser ) == ksSuccess ); // +
            }
        } else 
            roomUserPtr->Status = SCConference::MONITOR;
    }

    if( !mute )
    {
        SC_LOG4CPP_EX( m_pLog4cpp," Error while muting user[%02d][%03d]", UserChannel.GetDevice() % UserChannel.GetChannel() );        
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," User at [%02d][%03d] muted ! ", UserChannel.GetDevice() % UserChannel.GetChannel() );
    }
    
    
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return mute;
}

bool ConferenceRoom::MuteUser_Former( KChannel& UserChannel )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    std::map< std::string, SCConference::ConfereeStatus >::iterator itMuteUsers;
    bool mute = true;
    
    for( auto uIter = Users.begin(), uEnd = Users.end(); uIter != uEnd; ++uIter )
    {   
        itMuteUsers = MuteUsers.find( uIter->first );
        if( UserChannel != uIter->second )
        {
            if ( itMuteUsers != MuteUsers.end() )
            {
                if ( itMuteUsers->second != SCConference::MONITOR )
                {
                    mute &= ( uIter->second.UnmixChannel( UserChannel ) == ksSuccess );
                    mute &= ( UserChannel.UnmixChannel( uIter->second ) == ksSuccess ); // +
                }
            }
        } else {
            if ( itMuteUsers != MuteUsers.end() )
                itMuteUsers->second = SCConference::MONITOR;
        }
    }
    
    
    if( !mute )
    {
        SC_LOG4CPP_EX( m_pLog4cpp," Error while muting user[%02d][%03d]", UserChannel.GetDevice() % UserChannel.GetChannel() );        
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," User at [%02d][%03d] muted ! ", UserChannel.GetDevice() % UserChannel.GetChannel() );
    }
    
    
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return mute;
}

bool ConferenceRoom::UnmuteUser( KChannel& UserChannel )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    std::stack<boost::shared_ptr<RoomUser>> roomUserStack;
    bool mute = true;
    for ( auto userItr = m_Container.get<ByUserId>().begin(); userItr != m_Container.get<ByUserId>().end(); userItr++ )
    {
        boost::shared_ptr<RoomUser> roomUserPtr = *userItr;
        if ( roomUserPtr->ChannelUser != UserChannel )
        {
            if ( roomUserPtr->Status == SCConference::DUPLEX )
            {
                mute &= ( roomUserPtr->ChannelUser.MixChannel( UserChannel ) == ksSuccess );
                mute &= ( UserChannel.MixChannel( roomUserPtr->ChannelUser ) == ksSuccess ); // +

                if ( !mute )
                {
                    roomUserPtr->ChannelUser.UnmixChannel( UserChannel );
                    UserChannel.UnmixChannel( roomUserPtr->ChannelUser );

                    while ( !roomUserStack.empty() )
                    {
                        boost::shared_ptr<RoomUser> stacketRoomUser = roomUserStack.top();

                        stacketRoomUser->ChannelUser.UnmixChannel( UserChannel );
                        UserChannel.UnmixChannel( stacketRoomUser->ChannelUser );

                        roomUserStack.pop();
                    }

                    break;
                    
                } else {
                    roomUserStack.push(roomUserPtr);
                }
            }
        } else 
            roomUserPtr->Status = SCConference::DUPLEX;
    }

    if( !mute )
    {
        SC_LOG4CPP_EX( m_pLog4cpp," Error while unmuting user[%02d][%03d]", UserChannel.GetDevice() % UserChannel.GetChannel() );
        
    }
    else 
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"User at [%02d][%03d] unmuted ! ", UserChannel.GetDevice() % UserChannel.GetChannel() );
    }
    
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return mute;
}

bool ConferenceRoom::UnmuteUser_Former( KChannel& UserChannel )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    std::map< std::string, SCConference::ConfereeStatus >::iterator itMuteUsers;
    bool mute = true;
    
    for( auto uIter = Users.begin(), uEnd = Users.end(); uIter != uEnd; ++uIter )
    {
        itMuteUsers = MuteUsers.find( uIter->first );

        if( UserChannel != uIter->second )
        {
            if ( itMuteUsers != MuteUsers.end() )
            {
                if ( itMuteUsers->second == SCConference::MONITOR )
                {

                    mute &= ( uIter->second.MixChannel( UserChannel ) == ksSuccess );
                    mute &= ( UserChannel.MixChannel(uIter->second) == ksSuccess ); // +
                    if (!mute)
                    {
                        for ( auto other = Users.begin(); other != uIter ; ++other )
                        {
                            other->second.UnmixChannel( UserChannel );
                            UserChannel.UnmixChannel( other->second );
                        }
                        break;
                    }
                }
            }
        } else {
            
            if ( itMuteUsers != MuteUsers.end() )
                itMuteUsers->second = SCConference::DUPLEX;
        }
    }
    
    if( !mute )
    {
        SC_LOG4CPP_EX( m_pLog4cpp," Error while unmuting user[%02d][%03d]", UserChannel.GetDevice() % UserChannel.GetChannel() );
        
    }
    else 
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"User at [%02d][%03d] unmuted ! ", UserChannel.GetDevice() % UserChannel.GetChannel() );
    }
    
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return mute;
}


void ConferenceRoom::PrintRoom()
{
	std::stringstream ss;
	std::string strAni;
	std::string strDnis;
    int iDevice;
	int iChannel;
	std::string strNumberOfRooms;	
	std::string strRoomId;
	std::string strUserId;
	
	
	ss.str(std::string());	
	ss << "\r\n";
	ss << "---------------------------------------------------------\r\n";
	ss << boost::format("|          Room %18s                      |\r\n") % m_RoomId;
	
	ss << "---------------------------------------------------------\r\n";
	ss << "|       User Id            Device          Channel      |\r\n";
	ss << "---------------------------------------------------------\r\n";
	
	
	
	
	for ( auto itrDictUser = m_Container.begin(); itrDictUser != m_Container.end(); itrDictUser++ )
	{
		boost::shared_ptr<RoomUser> usersPtr = (*itrDictUser);
		strUserId = usersPtr->UserId;
        iDevice = usersPtr->ChannelUser.GetDevice();
        iChannel = usersPtr->ChannelUser.GetChannel();
		
		ss << boost::format("|      %11s     %7d       %7d          |\r\n") % strUserId.c_str() % iDevice % iChannel;					
	}
	
	ss << "---------------------------------------------------------\r\n";		
	
    	
    SC_LOG4CPP( m_pLog4cpp, ss.str().c_str() );
}


bool ConferenceRoom::DeafenUser( KChannel& User )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    bool isDeaf = true;
    for ( auto userItr = m_Container.get<ByUserId>().begin(); userItr != m_Container.get<ByUserId>().end() ; userItr++ )
    {
        boost::shared_ptr<RoomUser> roomUserPtr = *userItr;

        if ( User != roomUserPtr->ChannelUser )
            isDeaf &= ( User.UnmixChannel( roomUserPtr->ChannelUser ) == ksSuccess );
    }

    if( !isDeaf )
        SC_LOG4CPP_EX( m_pLog4cpp," Error while unmixing user[%02d][%03d]", User.GetDevice() % User.GetChannel() );        
    
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return isDeaf;
}

bool ConferenceRoom::DeafenUser_Former( KChannel& User )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    bool isDeaf = ( User.ClearMixer() == ksSuccess );
    if( !isDeaf )
        SC_LOG4CPP_EX( m_pLog4cpp," Error clearing channel[%02d][%03d] mixer", User.GetDevice() % User.GetChannel() );        
	
    SC_LOG4CPP_EXIT(m_pLog4cpp);

    return isDeaf;
}

bool ConferenceRoom::UndeafenUser( KChannel& User )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    bool undeaf = true;
    for ( auto userItr = m_Container.get<ByUserId>().begin(); userItr != m_Container.get<ByUserId>().end() ; userItr++ )
    {
        boost::shared_ptr<RoomUser> roomUserPtr = *userItr;

        if ( User != roomUserPtr->ChannelUser )
            undeaf &= ( User.MixChannel( roomUserPtr->ChannelUser ) == ksSuccess );
    }

    if( !undeaf )
        SC_LOG4CPP_EX( m_pLog4cpp," Error while unmixing user[%02d][%03d]", User.GetDevice() % User.GetChannel() );        
    
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return undeaf;
}

bool ConferenceRoom::UndeafenUser_Former( KChannel& User )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    bool undeaf = true;
    for( auto other = Users.begin(), usersEnd = Users.end(); other != usersEnd; ++other )
    {
        if( User != other->second )
            undeaf &= ( User.MixChannel( other->second ) == ksSuccess );
    }
    
    if( !undeaf )
        SC_LOG4CPP_EX( m_pLog4cpp," Error while unmixing user[%02d][%03d]", User.GetDevice() % User.GetChannel() );        
    
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return undeaf;
}




/*
bool ConferenceRoom::AddListener( char const* UserId, int KhompDevice, int KhompChannel )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool userListening = true;
    
	if( Listeners.size() == 0 || Listeners.find( UserId ) == Listeners.end() )
    {
        KChannel userChannel( KhompDevice, KhompChannel );
        auto conferenceUser = Users.begin();
        auto usersEnd = Users.end();
        for( ; conferenceUser != usersEnd; ++conferenceUser )
        {
            userListening &= ( userChannel.MixChannel( conferenceUser->second ) == ksSuccess );
            if( !userListening )
            {
                userChannel.ClearMixer();
                break;
            }
        }
        if( userListening )
            Listeners.insert( std::pair< std::string, KChannel >( UserId, userChannel ) );
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return userListening;
}

bool ConferenceRoom::RemoveListener( char const* UserId )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    bool userRemoved = false;
    if( !Listeners.empty() )
    {
        auto listener = Listeners.find( UserId );
        if( listener != Listeners.end() )
        {
            userRemoved = ( listener->second.ClearMixer() == ksSuccess );
            if( userRemoved )
                Listeners.erase( listener );
        }
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return userRemoved;
}
*/