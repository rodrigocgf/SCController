
#pragma once

#include "SCCDRPortal.h"

#include <KD3/Basics/KString.h>
#include <map>


class SCCDRLogger
{
 
private:
    const uint32 TimerInterval;
    std::string CDRBaseDir;
    std::map<std::string, SCCDRPortal*> PortalsRegistry;
    std::mutex Mutex;

    SCCDRLogger();
    SCCDRLogger( SCCDRLogger const& );
    SCCDRLogger& operator=( SCCDRLogger const& );
    ~SCCDRLogger();

    static SCCDRLogger& GetInstance();
    FILE* GetCDRLogFile( SCCDRPortal& Portal );

    bool OpenCDRLogDir( SCCDRPortal& Portal, ktools::time::KSystemTime const& CurrentTime );
    std::string GetCDRLogDir( SCCDRPortal& Portal, ktools::time::KSystemTime const&  CurrentTime );

    int ReadIndexFile( std::string const& BaseDir );
    void WriteIndexFile( std::string const& BaseDir, int Index );

    SCCDRPortal* GetPortal( char const* PortalName );
    bool UpdateFileIndex( SCCDRPortal& Portal, ktools::time::KSystemTime const& CurrentTime );
public:
    
    static int Log( char const* PortalName, char const* CDRMessage, size_t MessageLength );
};
