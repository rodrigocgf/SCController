
#include <iostream>
#include <k3l.h>
#include <string>
#include <boost/thread.hpp>
#include <TimerManager.h>

#include "SCController.h"
#include "SCConfigReader.h"
#include "SCConferenceService.h"

// Depura��o
//#define _CRTDBG_MAP_ALLOC
//#include <stdlib.h>
//#include <crtdbg.h>


boost::asio::io_service Service;
//boost::asio::ip::tcp::acceptor Acceptor( Service, boost::asio::ip::tcp::endpoint( boost::asio::ip::address::from_string( "192.168.3.236" ), 13140 ) );
boost::asio::ip::tcp::acceptor Acceptor( Service, boost::asio::ip::tcp::endpoint( boost::asio::ip::tcp::v4(), 13140 ) );

SCLogger Logger( SCConfigReader::GetLogDir() + "/Main.log" );


int Kstdcall EventHandler( int Obj, K3L_EVENT* Event )
{
    SCController& controller = SCController::GetInstance();
    return controller.EventHandler( Obj, Event );
}

void HandleAccept( SCConferenceService::ptr Client, boost::system::error_code const& Error )
{
    Logger.LOG( SCLogger::SCLOG_TRACE, "Main::HandleAccept (b)\n" );
    static std::string ConferenceClientAddress = SCConfigReader::GetConferenceClientAddress();
    //boost::asio::ip::tcp::endpoint WriterEP( boost::asio::ip::address::from_string( ConferenceClientAddress ), 13150 );
    boost::asio::ip::tcp::endpoint WriterEP( Client->GetSocket().remote_endpoint().address() , Client->GetSocket().remote_endpoint().port() );
    if( Client )
    {
        Client->Start( WriterEP );
        SCConferenceService::ptr newClient = SCConferenceService::New( Service );
        Acceptor.async_accept( newClient->GetSocket(), boost::bind( HandleAccept, newClient, _1 ) );
    }
    Logger.LOG( SCLogger::SCLOG_TRACE, "Main::HandleAccept (e)\n" );
}

void RunService()
{
    Logger.LOG( SCLogger::SCLOG_TRACE, "Main::RunService (b)\n" );
    try
    {
        Service.run();
    }
    catch( boost::system::system_error& err )
    {
        Logger.LOG( SCLogger::SCLOG_ERROR, "Exception on Service.run(): %s\n", err.what() );
    }
    catch( ... )
    {
        Logger.LOG( SCLogger::SCLOG_ERROR, "Unknown exception on Conference Service thread!\n" );
    }
    Logger.LOG( SCLogger::SCLOG_TRACE, "Main::RunService (e)\n" );
}

int main()
{
    //************************************************************
    //
    // Redirecionando a depura��o da ASIO para um arquivo
    //
    //************************************************************
    
    //HANDLE errH = CreateFile( "\\Khomp\\err.txt", GENERIC_WRITE, 0, 0,CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0 );
    //SetStdHandle( STD_ERROR_HANDLE, errH );
    
    //************************************************************
    // FIM DEPURACAO
    //************************************************************

    char* ret = k3lStart( 3, 1, 0 );
    if( ret && *ret )
    {
        Logger.LOG( SCLogger::SCLOG_ERROR, "Erro iniciando a API K3L: %s\n", ret );
        return 1;
    }

    SCController& controller = SCController::GetInstance();

    SCConferenceService::ptr conferenceService = SCConferenceService::New( Service );
    Acceptor.async_accept( conferenceService->GetSocket(), boost::bind( HandleAccept, conferenceService, _1 ) );
      k3lRegisterEventHandler( EventHandler );
    boost::thread serviceThread( RunService );
    //serviceThread.detach();
    
  
    serviceThread.join();
    std::string input;
    do
    {
        std::cout << "Digite 'quit' para sair!" << std:: endl;
        std:: cin >> input;
    }
    while( input.compare( "quit" ) != 0 );

    TimerManager::instance()->stop();
    Service.stop();
    k3lRegisterEventHandler( NULL );
    k3lStop();

    // Depura��o
    //_CrtDumpMemoryLeaks();

    return 0;
}

