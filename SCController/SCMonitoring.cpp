#include "SCMonitoring.h"
#include "SCConfigReader.h"



#define MEM_FN1(x,y) boost::bind(&self_type::x, shared_from_this(), y)
#define MEM_FN2(x,y,z) boost::bind(&self_type::x, shared_from_this(), y, z)
#define MEM_FN3(w,x,y,z) boost::bind(&self_type::w, shared_from_this(), x, y, z)


SCMonitoring::SCMonitoring(io_service& Service) :	ReadSocket( Service ), 
													Started( false ),													
                                                    m_Timeout(5000),
                                                    m_timer(Service),
                                                    m_loggedOn(false)
{	
    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("Monitoring") ); 

    m_MaxChannels = SCController::GetInstance().GetChannelAllocator()->GetTotalChannels();
    m_TelnetState = TSDATA;
    m_optionCmd = 0x00;
    m_option = 0x00;
    m_subNegOption = 0x00;
}


SCMonitoring::~SCMonitoring(void)
{
}

ip::tcp::socket& SCMonitoring::GetSocket() 
{ 
    return ReadSocket; 
}

void SCMonitoring::Start( )
{   
    //SC_LOG4CPP_ENTRY( m_pLog4cpp );
        
    //SC_LOG4CPP( m_pLog4cpp,"----------------------------------------------------------");

    Started = true;
    SendInitialBanner();
    DoRead();
    
    //SC_LOG4CPP_EXIT( m_pLog4cpp );    
}


void SCMonitoring::Stop()
{
    //SC_LOG4CPP_ENTRY( m_pLog4cpp );
	
    if( Started )
    {
        Started = false;
        ReadSocket.close();
    }
    //SC_LOG4CPP_EXIT( m_pLog4cpp );    
}

void SCMonitoring::DoRead()
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );

    memset(m_ReadBuffer,0x00,sizeof(m_ReadBuffer));
    m_ReadIndex = 0;
    
    ReadSocket.async_read_some(   boost::asio::buffer(m_InputBuffer, 1 ) ,
                                  boost::bind(    &SCMonitoring::OnRead, this,
                                                  boost::asio::placeholders::error,
                                                  boost::asio::placeholders::bytes_transferred));
    
    SC_LOG4CPP_EXIT( m_pLog4cpp );    
}

void SCMonitoring::SendInitialBanner()
{
    std::stringstream ss;

    SendInitialOptions();

    ss << "SUPPORTCOMM SCCONTROLLER TELNET PROMPT\r\n";
    ss << "\r\n>PASS ";

    DoWrite(ss.str());
}

void SCMonitoring::TelnetProtocol(char cmdChar)
{
    std::stringstream ss;

    
    char sendBuffer[10];

    switch(m_TelnetState)
    {
        case TSDATA:
            if ( cmdChar == (char)IAC )
            {
                ss.str(std::string());
                ss << " IAC ";
                m_TelnetState = TSIAC;
            }
            else if ( cmdChar == (char)SB )
            {
                ss.str(std::string());
                ss << " SB ";
                m_TelnetState == TSSUBNEG;
            }
            else
            {   
                ProcessInput(cmdChar);
            }

            break;

        case TSIAC:
            m_optionCmd = cmdChar;
            ss.str(std::string());
            switch((char)m_optionCmd)
            {
            case (char)WILL:
                ss << " WILL ";
                m_TelnetState = TSWOPT;
                break;
            case (char)WONT:
                ss << " WONT ";
                m_TelnetState = TSWOPT;
                break;
            case (char)DO:
                ss << " DO ";
                m_TelnetState = TSDOPT;
                break;
            case (char)DONT:
                ss << " DONT ";
                m_TelnetState = TSDOPT;
                break;
            case (char)SB:
                ss << " SB ";
                m_TelnetState = TSSUBNEG;
                break;
            case (char)IAC:
                ss << " IAC ";
                m_TelnetState = TSDATA;
                break;
            default:
                m_TelnetState = TSDATA;
                break;
            }

            break;
        case TSDOPT:
            m_option = cmdChar;
            ss.str(std::string());
            if ( cmdChar == (char)ECHO )
            {
                ss << " ECHO ";
            } 
            else if ( cmdChar == (char) WINDOW_SIZE )
            {
                ss << " WINDOW SIZE ";
            }
            else if ( cmdChar == (char) TERMINAL_TYPE )
            {
                ss << "TERMINAL TYPE";
            } 
            else if ( cmdChar == (char) IAC )
            {
                ss << "IAC";
            } 
            else
                ss << boost::format("{0x%02x}") % static_cast<int>(cmdChar);

            /*
            memset(sendBuffer,0x00,sizeof(sendBuffer));
            sendBuffer[0] = IAC;

            if ( m_optionCmd == (char)DO )
            {   
                sendBuffer[1] = WILL;
            }
            else if ( m_optionCmd == (char)DONT ) 
                sendBuffer[1] = WONT;
            
            
            boost::asio::async_write(   ReadSocket, 
                                boost::asio::buffer(sendBuffer, 2),
                                boost::bind(    &SCMonitoring::OnWrite, this,
                                                boost::asio::placeholders::error,
                                                boost::asio::placeholders::bytes_transferred));
            */

            m_TelnetState = TSDATA;
            break;
        case TSWOPT:
            m_option = cmdChar;
            ss.str(std::string());

            ss.str(std::string());
            if ( cmdChar == (char)ECHO )
            {
                ss << " ECHO ";
            } 
            else if ( cmdChar == (char) WINDOW_SIZE )
            {
                ss << " WINDOW SIZE ";
            }
            else if ( cmdChar == (char) TERMINAL_TYPE )
            {
                ss << "TERMINAL TYPE";
            }
            else if ( cmdChar == (char) IAC )
            {
                ss << "IAC";
            }
            else
                ss << boost::format("{0x%02x}") % static_cast<int>(cmdChar);

            /*
            memset(sendBuffer,0x00,sizeof(sendBuffer));
            sendBuffer[0] = IAC;

            if ( m_optionCmd == (char)WILL )
                sendBuffer[1] = DO;
            else if ( m_optionCmd == (char)WONT ) 
                sendBuffer[1] = DONT;
            
            
            boost::asio::async_write(   ReadSocket, 
                                boost::asio::buffer(sendBuffer, 2),
                                boost::bind(    &SCMonitoring::OnWrite, this,
                                                boost::asio::placeholders::error,
                                                boost::asio::placeholders::bytes_transferred));

            */
            m_TelnetState = TSDATA;
            
            
            break;        
        case TSSUBNEG:
            if ( cmdChar == (char)IAC )
            {
                ss << " IAC ";
                m_TelnetState = TSSUBIAC;
            }
            else
            {
                m_subNegOption = cmdChar;
            }

            break;
        case TSSUBIAC:
            if ( cmdChar == (char)SE )            
            {
                ss << " SE ";
                m_TelnetState =  TSDATA;
            }
            else
                m_TelnetState =  TSSUBNEG;
            break;
    }

    if ( boost::algorithm::trim_copy(ss.str()).length() > 0 )
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"{%s}", ss.str().c_str());        
    }
}

void SCMonitoring::OnRead( error_code const& Error, size_t Bytes )
{
    std::stringstream ss;

    if( Error )
    {
        SC_LOG4CPP_EX( m_pLog4cpp, "ERROR: OnRead => %s", Error.message().c_str() );        
		Stop();		
        SC_LOG4CPP( m_pLog4cpp,"------------------ E N D   O F   M O N I T O R I N G ------------------ ");
    } else {

        if ( (Bytes > 0) && (Bytes == 1) )
            TelnetProtocol(m_InputBuffer[0]);

        m_InputBuffer[0] = 0x00;
        ReadSocket.async_read_some(     boost::asio::buffer(m_InputBuffer, 1 ) ,
                                        boost::bind(    &SCMonitoring::OnRead, this,
                                                        boost::asio::placeholders::error,
                                                        boost::asio::placeholders::bytes_transferred));
    }
}


void SCMonitoring::HandleWait(const boost::system::error_code& error)
{   
    m_timer.cancel();
    {
        boost::mutex::scoped_lock lock(m_BufferMutex);
        m_ReadIndex = 0;
        memset(m_ReadBuffer,0x00,sizeof(m_ReadBuffer));
    }    
    SC_LOG4CPP( m_pLog4cpp,"Cleanning READ BUFFER.");
}



void SCMonitoring::ParseCommand(const char * cmd)
{
	std::stringstream ss;
	std::string::const_iterator start,end;

    boost::mutex::scoped_lock lock(m_BufferMutex);
    std::string command = cmd;
	
	boost::match_results<std::string::const_iterator> what;
	boost::match_flag_type flags = boost::match_any;
	std::string commandItself;
	boost::to_upper(command);

    if ( command.find("QUIT") != std::string::npos )
    {
        Stop();
    }
    else if ( command.find("HELP") != std::string::npos )
    {
        start = command.begin();
	    end = command.end();

        boost::match_results<std::string::const_iterator> results;
		boost::regex expression("^ *HELP *$"); 
        if(boost::regex_search(start, end, what, expression, flags))
		{	
            SendHelpMenu();
        }
    }
	// TIME GET	
	else if ( command.find("TIME") != std::string::npos )
	{	
		boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
        long milliseconds = timeLocal.time_of_day().total_milliseconds();
        boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
        boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);
        ss.str(std::string());
        ss <<  "CURRENT SSController TIME : ";
        ss <<   boost::format("%02i:%02i:%02i.%03i \r\n>") % current_date_milliseconds.time_of_day().hours() 
                                                    % current_date_milliseconds.time_of_day().minutes() 
                                                    % current_date_milliseconds.time_of_day().seconds() 
                                                    % (current_date_milliseconds.time_of_day().fractional_seconds() / 1000); 
                
        SC_LOG4CPP_EX( m_pLog4cpp,"%s",ss.str().c_str());

		DoWrite(ss.str());

	}
	// DATE GET
	else if ( command.find("DATE") != std::string::npos )
	{
		boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
        long milliseconds = timeLocal.time_of_day().total_milliseconds();
        boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
        boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);
        ss.str(std::string());
        ss <<  "CURRENT SSController DATE : ";
        ss <<   boost::format("%04i/%02i/%02i \r\n>") % current_date_milliseconds.date().year() % 
                current_date_milliseconds.date().month().as_number() % current_date_milliseconds.date().day() ;  
                
        SC_LOG4CPP_EX( m_pLog4cpp,"%s",ss.str().c_str());

		DoWrite(ss.str());
	}
	// DISC <Initial Channel> <Final Channel>
	else if ( command.find("DISC") != std::string::npos )
	{	
        int firstChannel = 0;
        int lastChannel = 0;

        start = command.begin();
	    end = command.end();

		boost::regex expression("( *DISC *[0-9]+$)|( *DISC *[0-9]+ *[0-9]+$)"); 
		if(boost::regex_search(start, end, what, expression, flags))
		{	
			commandItself = what[0];

            boost::char_separator<char> sep(" ");
            boost::tokenizer<boost::char_separator<char>> tokens(commandItself, sep);
            int iCount =0 ;
            BOOST_FOREACH(std::string t, tokens)
            {
                if ( iCount == 1 )
                    firstChannel = boost::lexical_cast<int>(boost::algorithm::trim_copy( t ) );
                else if ( iCount == 2 )
                    lastChannel = boost::lexical_cast<int>( boost::algorithm::trim_copy( t ) );
                iCount++;
            }

            ProcessChannelDisconection(firstChannel, lastChannel);
		} 
        else
        {
            ss.str(std::string());
            ss << "\r\nINVALID SYNTAX.\r\n";
            ss << "> ";
            DoWrite(ss.str());
        }
	}
    // FIND - Find Channel by Ani : FIND <Ani>
    else if ( command.find("FIND") != std::string::npos )
    {
        std::string ani;

        start = command.begin();
	    end = command.end();

		boost::regex expression(" *FIND *[0-9]+$"); 
		if(boost::regex_search(start, end, what, expression, flags))
		{	
			commandItself = what[0];

            boost::char_separator<char> sep(" ");
            boost::tokenizer<boost::char_separator<char>> tokens(commandItself, sep);
            int iCount =0 ;
            BOOST_FOREACH(std::string t, tokens)
            {
                if ( iCount == 1 )
                    ani = boost::algorithm::trim_copy( t );
                iCount++;
            }
            ProcessFindChannel(ani);
        }
        else
        {
            ss.str(std::string());
            ss << "\r\nINVALID SYNTAX.\r\n";
            ss << "> ";
            DoWrite(ss.str());
        }
    }
	// BLKC <Initial Channel> <Final Channel>
	else if ( command.find("BLKC") != std::string::npos  )
	{
        int firstChannel = 0;
        int lastChannel = 0;

        start = command.begin();
	    end = command.end();

		boost::regex expression("( *BLKC *[0-9]+$)|( *BLKC *[0-9]+ *[0-9]+$)"); 
		if(boost::regex_search(start, end, what, expression, flags))
		{	
			commandItself = what[0];

            boost::char_separator<char> sep(" ");
            boost::tokenizer<boost::char_separator<char>> tokens(commandItself, sep);
            int iCount =0 ;
            BOOST_FOREACH(std::string t, tokens)
            {
                if ( iCount == 1 )
                    firstChannel = boost::lexical_cast<int>(boost::algorithm::trim_copy( t ) );
                else if ( iCount == 2 )
                    lastChannel = boost::lexical_cast<int>( boost::algorithm::trim_copy( t ) );
                iCount++;
            }

            ProcessChannelBlocking( firstChannel, lastChannel );
		} 
        else
        {
            ss.str(std::string());
            ss << "\r\nINVALID SYNTAX.\r\n";
            ss << "> ";
            DoWrite(ss.str());
        }
	}
	// UNBC <Initial Channel> <Final Channel>
	else if ( command.find("UNBC") != std::string::npos  )
	{
        int firstChannel = 0;
        int lastChannel = 0;

        start = command.begin();
	    end = command.end();

		boost::regex expression("( *UNBC *[0-9]+$)|( *UNBC *[0-9]+ *[0-9]+$)"); 
		if(boost::regex_search(start, end, what, expression, flags))
		{	
			commandItself = what[0];

            boost::char_separator<char> sep(" ");
            boost::tokenizer<boost::char_separator<char>> tokens(commandItself, sep);
            int iCount =0 ;
            BOOST_FOREACH(std::string t, tokens)
            {
                if ( iCount == 1 )
                    firstChannel = boost::lexical_cast<int>(boost::algorithm::trim_copy( t ) );
                else if ( iCount == 2 )
                    lastChannel = boost::lexical_cast<int>( boost::algorithm::trim_copy( t ) );
                iCount++;
            }

			ProcessChannelUnblocking( firstChannel, lastChannel );
		}
        else
        {
            ss.str(std::string());
            ss << "\r\nINVALID SYNTAX.\r\n";
            ss << "> ";
            DoWrite(ss.str());
        }
	}
	// STOC <Initial Channel> <Final Channel>
	else if ( command.find("STOC") != std::string::npos )
	{
        int firstChannel = 0;
        int lastChannel = 0;
        
        start = command.begin();
	    end = command.end();

        boost::regex expression("( *STOC *[0-9]+$)|( *STOC *[0-9]+ *[0-9]+$)"); 
		if(boost::regex_search(start, end, what, expression, flags))
		{	
			commandItself = what[0];

            boost::char_separator<char> sep(" ");
            boost::tokenizer<boost::char_separator<char>> tokens(commandItself, sep);
            int iCount =0 ;
            BOOST_FOREACH(std::string t, tokens)
            {
                if ( iCount == 1 )
                    firstChannel = boost::lexical_cast<int>(boost::algorithm::trim_copy( t ) );
                else if ( iCount == 2 )
                    lastChannel = boost::lexical_cast<int>( boost::algorithm::trim_copy( t ) );
                iCount++;
            }

            ProcessChannelStatus(firstChannel, lastChannel);
        } 
        else 
        {
            ss.str(std::string());
            ss << "\r\nINVALID SYNTAX.\r\n";
            ss << "> ";
            DoWrite(ss.str());
        }
	}
	else if ( command.find("CHNC") != std::string::npos )
	{		
		ProcessChannelCount();
	} 
    else 
    {
        ss.str(std::string());
        ss << "\r\nINVALID COMMAND.\r\n";
        ss << "> ";
        DoWrite(ss.str());
    }
}

void SCMonitoring::DoWrite( std::string const& Message )
{    
    SC_LOG4CPP_ENTRY( m_pLog4cpp );

    std::vector<std::string> messages;
    messages.push_back( Message );
    DoWrite( messages );
    
    SC_LOG4CPP_EXIT( m_pLog4cpp );    
}

void SCMonitoring::DoWrite( std::vector<std::string> const& Messages )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );

    if( Started )
    {
        std::ostream out( &WriteBuffer );
        std::string message;
        for( auto msgIt = Messages.begin(), msgEnd = Messages.end(); msgIt != msgEnd; ++msgIt )
            message += *msgIt;
             
        out << message;
        
         
        boost::asio::async_write(   ReadSocket, 
                                    boost::asio::buffer(message.c_str(), message.size()),
                                    boost::bind(    &SCMonitoring::OnWrite, this,
                                                    boost::asio::placeholders::error,
                                                    boost::asio::placeholders::bytes_transferred));
        
        
    }

    SC_LOG4CPP_EXIT( m_pLog4cpp );    
}

void SCMonitoring::OnWrite( error_code const& Error, size_t bytes )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    DoRead();
    SC_LOG4CPP_EXIT( m_pLog4cpp );    
}

void SCMonitoring::ProcessFindChannel(std::string ani)
{
    std::stringstream ss;
    std::string lAni;

    ss << "\r\n";
	ss << "|-------------------------------------------|----------------------------|\r\n";
    ss << "|               |                           |                            |\r\n";
    ss << "|    CHANNEL    |          STATUS           |            ANI             |\r\n";
    ss << "|-------------------------------------------|----------------------------|\r\n";
    ss << "|               |                           |                            |\r\n";

    for ( int i = 0 ; i < m_MaxChannels ; i++ )
    {
        lAni = SCController::GetInstance().GetChannelAllocator()->GetChannelANI(i);
        if ( lAni == ani )
        {
            if ( SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(i) == ANSWERED )
            {
                ss << boost::format("|       %-3d     |          ANSWERED         |         %-15s    |\r\n") % i % ani.c_str();
            }
        }
    }

    ss << "|               |                           |                            |\r\n";
    ss << "|-------------------------------------------|----------------------------|\r\n> ";
        
    SC_LOG4CPP_EX( m_pLog4cpp,"%s",ss.str().c_str());

	DoWrite(ss.str());

}

void SCMonitoring::ProcessChannelStatus(int firstChannel, int lastChannel)
{
    std::stringstream ss;
    std::string ani;

    if ( lastChannel >= m_MaxChannels )
        lastChannel = m_MaxChannels - 1;

    ss << "\r\n";
	ss << "|-------------------------------------------|----------------------------|\r\n";
    ss << "|               |                           |                            |\r\n";
    ss << "|    CHANNEL    |          STATUS           |            ANI             |\r\n";
    ss << "|-------------------------------------------|----------------------------|\r\n";
    ss << "|               |                           |                            |\r\n";
    if ( lastChannel == 0 )
    {
        ani = SCController::GetInstance().GetChannelAllocator()->GetChannelANI(firstChannel);
        if ( SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(firstChannel) == ANSWERED )
        {
            ss << boost::format("|       %-3d     |          ANSWERED         |         %-15s    |\r\n") % firstChannel % ani.c_str();
        } 
        else if ( SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(firstChannel) == IDLE )
        {
            ss << boost::format("|       %-3d     |            IDLE           |         %-15s    |\r\n") % firstChannel % ani.c_str();
        }
        else if ( SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(firstChannel) == BLOCKED )
        {
            ss << boost::format("|       %-3d     |           BLOCKED         |         %-15s    |\r\n") % firstChannel % ani.c_str();
        }
    } else {
        for ( int i = firstChannel ; i <= lastChannel ; i++ )
        {
            ani = SCController::GetInstance().GetChannelAllocator()->GetChannelANI(i);
            if ( SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(i) == ANSWERED )
            {
                ss << boost::format("|       %-3d     |          ANSWERED         |         %-15s    |\r\n") % i % ani.c_str();
            } 
            else if ( SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(i) == IDLE )
            {
                ss << boost::format("|       %-3d     |            IDLE           |         %-15s    |\r\n") % i % ani.c_str();
            }
            else if ( SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(i) == BLOCKED )
            {
                ss << boost::format("|       %-3d     |          BLOCKED          |         %-15s    |\r\n") % i % ani.c_str();
            }
        }
    }
    ss << "|               |                           |                            |\r\n";
    ss << "|-------------------------------------------|----------------------------|\r\n> ";
        
    SC_LOG4CPP_EX( m_pLog4cpp,"%s",ss.str().c_str());

	DoWrite(ss.str());
}

void SCMonitoring::ProcessChannelDisconection(int firstChannel, int lastChannel)
{
    std::stringstream ss;

    if ( lastChannel >= m_MaxChannels )
        lastChannel = m_MaxChannels - 1;

    if ( lastChannel == 0 )
    {
        SCController::GetInstance().GetChannelAllocator()->DisconnectChannel(firstChannel);
    } 
    else 
    {
        for ( int i = firstChannel ; i <= lastChannel ; i++ )
        {
            SCController::GetInstance().GetChannelAllocator()->DisconnectChannel(i);
        }
    }

    ss << "\r\nOK\r\n> ";
    DoWrite(ss.str());
}

void SCMonitoring::ProcessChannelBlocking(int firstChannel, int lastChannel)
{
    std::stringstream ss;
    CHANNEL_STATUS status;

    if ( lastChannel >= m_MaxChannels )
        lastChannel = m_MaxChannels - 1;

    if ( lastChannel == 0 )
    {
        status = SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(firstChannel);
        if ( status != ANSWERED )
            SCController::GetInstance().GetChannelAllocator()->SetChannelStatus(firstChannel, BLOCKED);
    } 
    else 
    {
        for ( int i = firstChannel ; i <= lastChannel ; i++ )
        {
            status = SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(i);
            if ( status != ANSWERED )
                SCController::GetInstance().GetChannelAllocator()->SetChannelStatus(i, BLOCKED);
        }
    }

    ss << "\r\nOK\r\n> ";
    DoWrite(ss.str());
}

void SCMonitoring::ProcessChannelUnblocking(int firstChannel, int lastChannel)
{
    std::stringstream ss;
    CHANNEL_STATUS status;

    if ( lastChannel >= m_MaxChannels )
        lastChannel = m_MaxChannels - 1;

    if ( lastChannel == 0 )
    {
        status = SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(firstChannel);
        if ( status == BLOCKED )
            SCController::GetInstance().GetChannelAllocator()->SetChannelStatus(firstChannel, IDLE);
    } 
    else 
    {
        for ( int i = firstChannel ; i <= lastChannel ; i++ )
        {
            status = SCController::GetInstance().GetChannelAllocator()->GetChannelStatus(i);
            if ( status == BLOCKED )
                SCController::GetInstance().GetChannelAllocator()->SetChannelStatus(i, IDLE);
        }
    }

    ss << "\r\nOK\r\n> ";
    DoWrite(ss.str());
}

void SCMonitoring::ProcessChannelCount()
{
    std::stringstream ss;

    int totalChannels = SCController::GetInstance().GetChannelAllocator()->GetTotalChannels();
	int busyChannels = SCController::GetInstance().GetChannelAllocator()->GetBusyChannels();
    int blockedChannels = SCController::GetInstance().GetChannelAllocator()->GetBlockedChannels();
	int freeChannels = SCController::GetInstance().GetChannelAllocator()->GetFreeChannels();

    ss << "\r\n";
	ss << "|----------------------------------------------------------------------------|\r\n";
	ss << "|                                                                            |\r\n";
	ss << "|   TOTAL DE CANAIS | CANAIS OCUPADOS | CANAIS BLOQUEADOS | CANAIS LIVRES    |\r\n";
	ss << "|                                                                            |\r\n";
	ss << "|----------------------------------------------------------------------------|\r\n";
	ss << "|                                                                            |\r\n";
    ss << boost::format("|        %-3d        |        %-3d      |         %-3d       |        %-3d       |\r\n") % totalChannels % busyChannels % blockedChannels % freeChannels;
	ss << "|                                                                            |\r\n";
	ss << "|----------------------------------------------------------------------------|\r\n> ";

    
    SC_LOG4CPP_EX( m_pLog4cpp,"%s",ss.str().c_str());

	DoWrite(ss.str());
}

void SCMonitoring::SendHelpMenu()
{
	std::stringstream ss;

	ss << " \r\n";
	ss << " AVAILABLE COMMANDS : \r\n";
	ss << " \r\n";
	ss << " HELP - Help \r\n";
	ss << " QUIT - Exit connection \r\n";
	ss << " TIME - TIME GET \r\n";
	ss << " DATE - DATE GET \r\n";
    ss << " FIND - Find Channel by Ani : FIND <Ani> \r\n";
	ss << " DISC - Disconnect Channel : DISC <Initial Channel> <Final Channel> \r\n";
	ss << " BLKC - Block Channel : BLKC <Initial Channel> <Final Channel> \r\n";
	ss << " UNBC - Unblock Channel : UNBC <Initial Channel> <Final Channel> \r\n";
	ss << " STOC - Channel Status : STOC <Initial Channel> <Final Channel> \r\n";
	ss << " CHNC - Channel count \r\n";
	ss << "> ";

    SC_LOG4CPP_EX( m_pLog4cpp,"%s",ss.str().c_str());

	DoWrite(ss.str());
}

void SCMonitoring::SendPostPassOptions()
{
    char sendBuffer[5];

    memset(sendBuffer,0x00,sizeof(sendBuffer));
    sendBuffer[0] = (char)IAC;   
    sendBuffer[1] = (char)WONT;    
    sendBuffer[2] = (char)ECHO;
    
    sendBuffer[3] = '\r';
    sendBuffer[4] = '\n';
    
    boost::asio::async_write(   ReadSocket, 
                        boost::asio::buffer(sendBuffer, 5),                        
                        boost::bind(    &SCMonitoring::OnWrite, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));

    
    
}

void SCMonitoring::SendInitialOptions()
{
    char sendBuffer[5];

    
    
    memset(sendBuffer,0x00,sizeof(sendBuffer));
    sendBuffer[0] = (char)IAC;    
    sendBuffer[1] = (char)WILL;
    sendBuffer[2] = (char)ECHO;
    sendBuffer[3] = '\n';
    
    boost::asio::async_write(   ReadSocket, 
                        boost::asio::buffer(sendBuffer, 4),                        
                        boost::bind(    &SCMonitoring::OnWrite, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));

    
    memset(sendBuffer,0x00,sizeof(sendBuffer));
    sendBuffer[0] = (char)IAC;    
    sendBuffer[1] = (char)WILL;
    sendBuffer[2] = (char)SUPPRESS_GO_AHEAD;
    sendBuffer[3] = '\n';
    
    boost::asio::async_write(   ReadSocket, 
                        boost::asio::buffer(sendBuffer, 4),                        
                        boost::bind(    &SCMonitoring::OnWrite, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
    
    
    memset(sendBuffer,0x00,sizeof(sendBuffer));
    sendBuffer[0] = (char)IAC;    
    sendBuffer[1] = (char)WONT;
    sendBuffer[2] = (char)LINE_MODE;
    sendBuffer[3] = '\n';
    
    boost::asio::async_write(   ReadSocket, 
                        boost::asio::buffer(sendBuffer, 4),                        
                        boost::bind(    &SCMonitoring::OnWrite, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
    
    
    memset(sendBuffer,0x00,sizeof(sendBuffer));
    sendBuffer[0] = (char)IAC;    
    sendBuffer[1] = (char)DO;
    sendBuffer[2] = (char)WINDOW_SIZE;
    sendBuffer[3] = '\n';
    
    boost::asio::async_write(   ReadSocket, 
                        boost::asio::buffer(sendBuffer, 4),                        
                        boost::bind(    &SCMonitoring::OnWrite, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
    
    
}

void SCMonitoring::ProcessInput(char cmd)
{   
    if ( cmd == '\0' )
    {   
        boost::mutex::scoped_lock lock(m_BufferMutex);
        m_ReadIndex = 0;
        memset(m_ReadBuffer,0x00,sizeof(m_ReadBuffer));
    } else {

        if (  ( cmd != '\n' ) && ( cmd != '\r') ) // FUNCIONA PARA WINDOWS                    
        {
            if ( m_ReadIndex < sizeof(m_ReadBuffer) )
            {
                if ( (cmd != DELETE) && (cmd != BACKSPACE) )
                {
                    m_ReadBuffer[m_ReadIndex] = cmd;
                    m_ReadIndex++;
                } 
                else 
                {
                    if ( m_ReadIndex > 0 )
                    {
                        m_ReadIndex--;
                        m_ReadBuffer[m_ReadIndex] = ' ';                    
                    }
                }
            }
            else 
            {
                m_ReadIndex = 0;
                m_ReadBuffer[m_ReadIndex] = cmd;
            }
        } 
        else
        {
            m_ReadBuffer[m_ReadIndex] = '\0';
                        
            SC_LOG4CPP_EX( m_pLog4cpp,"INPUT COMMAND : %s", m_ReadBuffer);
			        
            if ( !m_loggedOn )
                ParseInput( m_ReadBuffer );
            else
            {   
                ParseCommand( m_ReadBuffer );

                m_timer.expires_from_now(boost::posix_time::milliseconds(m_Timeout));
                m_timer.async_wait(boost::bind(&SCMonitoring::HandleWait, this, boost::asio::placeholders::error));
            }

            //m_timer.expires_from_now(boost::posix_time::milliseconds(m_Timeout));
            //m_timer.async_wait(boost::bind(&SCMonitoring::HandleWait, this, boost::asio::placeholders::error));
        }
    }

}


void SCMonitoring::ParseInput(char * buffer)
{
    std::stringstream ss;
    std::string::const_iterator start,end;
    boost::match_results<std::string::const_iterator> what;
	boost::match_flag_type flags = boost::match_any;
    std::string commandItself;
    std::string command = buffer;
    std::string password;

    ss << boost::format("%s") % command;
    
    SC_LOG4CPP_EX( m_pLog4cpp,"%s",ss.str().c_str());

    password = boost::algorithm::trim_copy(command);
    if ( password.length() == 0 )
        return;

    if ( !password.compare(FIXED_PASSWORD) )
    {
        m_loggedOn = true;
        SendPostPassOptions();
        SendHelpMenu();
    }
    else 
    {
        m_ReadIndex = 0;    
        memset(m_ReadBuffer,0x00,sizeof(m_ReadBuffer));

        ss.str(std::string());
        ss << "\r\nINVALID PASSWORD.\r\n";
        ss << ">PASS ";
        DoWrite(ss.str());
    }
}
