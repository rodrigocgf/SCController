
#include "SCStdIncludes.h"
#include "SCOSFunctions.h"

#include <sys/stat.h>
#include <direct.h>

int SCOS::CreateDir( std::string const& path )
{
    int32 Ret = 0;
    int32 InternalRet;
    std::string::size_type slashoff = 0;
    bool alreadyFoundColon = false;
    
    if( DirExists( path ) )
        return 0; //ja existe e � diretorio

    /*
    while( slashoff != path.npos && Ret == 0 )
    {
        slashoff = path.find_first_of( "/\\" , slashoff+1 );
        std::string sub = path.substr( 0, slashoff );
        if( !alreadyFoundColon && sub.at( 1 ) == ':' )
        {
            slashoff = path.find_first_of( "/\\" , slashoff+1 );
            sub = path.substr( 0, slashoff );
            alreadyFoundColon = true;
        }
        InternalRet = CreateDirectory( sub.c_str(), NULL );
        if( !InternalRet )
        {
            int32 x = GetLastError(); // debug
            if( x == ERROR_ALREADY_EXISTS )
                Ret = 0;
            else
                Ret = 1;
        }
        else
            Ret = 0;
    }
    */

    boost::filesystem::path dir = path;
    try
    {
        if (boost::filesystem::create_directory(dir.string()))
        Ret = 1;
    }
    catch(const boost::filesystem::filesystem_error& e)
    {
        std::cerr << e.what() << '\n';
    }

    return Ret;
}

int SCOS::RemoveDir( std::string const& path )
{
    int32 ret = 1;

    boost::filesystem::path p = path;
    if ( boost::filesystem::remove(p) )
        ret = 0;

    //if( DirExists( path ) && (_rmdir( path.c_str() ) == 0) )
	//	    ret = 0;
	return ret;
}

//TODO: tratar caso de arquivo n�o existir
int SCOS::GetFileSize( std::string const& path )
{
    int size = 0;
    struct stat filestat;

    boost::filesystem::path pName(path);
    size = boost::filesystem::file_size(pName);

    //if( stat( path.c_str(), &filestat ) == 0 )
    //    size = filestat.st_size;
    return size;
}

bool SCOS::FileExists( std::string const& path )
{
    bool ret = false;
    struct stat filestat;
    
    boost::filesystem::path folder(path);
    if( (boost::filesystem::exists(folder)))
        ret = true;

    //if( stat(path.c_str(), &filestat ) == 0 && (filestat.st_mode & S_IFREG) )
    //    ret = true;

    return ret;
}

bool SCOS::DirExists( std::string const& path )
{
    bool ret = false;
    struct stat filestat;

    boost::filesystem::path folder(path);
    if( (boost::filesystem::exists(folder)))
        ret = true;

    //if( (stat( path.c_str(), &filestat ) == 0) && (filestat.st_mode & S_IFDIR) )
    //    ret = true;
    return ret;
}

FILE* SCOS::OpenFile( std::string const& Filename, bool Append )
{
    return fopen( Filename.c_str(), (Append ? "a+" : "w+" ) );
}

bool SCOS::WriteToFile( FILE* File, char const* Text, size_t TextLength )
{
    bool success = (fwrite( Text, sizeof( char ), TextLength, File ) == TextLength );
    fflush( File );
    return success; 
}

bool SCOS::CloseFile( FILE* File )
{
    return (fclose( File ) == 0);
}

static std::string GetLastErrorStdStr(DWORD error)
{
  if (error)
  {
    LPVOID lpMsgBuf;
    DWORD bufLen = FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        error,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );
    if (bufLen)
    {
      LPCSTR lpMsgStr = (LPCSTR)lpMsgBuf;
      std::string result(lpMsgStr, lpMsgStr+bufLen);
      
      LocalFree(lpMsgBuf);

      return result;
    }
  }
  return std::string();
}

bool SCOS::RenameFile( std::string const& OriginalPath, std::string const& DestinationPath )
{
    if ( FileExists(DestinationPath) )
    {
        boost::filesystem::remove(DestinationPath);
    } 
    boost::filesystem::rename(OriginalPath, DestinationPath);
    
    /*
    int moveReturn = MoveFile( OriginalPath.c_str(), DestinationPath.c_str() );
    
    if( moveReturn == 0 )
    {
        std::string errorStr = GetLastErrorStdStr( GetLastError() );

        printf( "Error moving file: %s\n", errorStr.c_str() );
        return false;
    }
    */

    return true;
}
