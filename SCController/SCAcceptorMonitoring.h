#pragma once
#include "SCStdIncludes.h"
#include "SCMonitoring.h"
#include "SCController.h"

class SCAcceptorMonitoring
{
private:
	SCAcceptorMonitoring(void);
	~SCAcceptorMonitoring(void);

	boost::shared_ptr<boost::thread>    m_ioServiceThrPtr;
    boost::asio::io_service             m_IoService;
    boost::asio::ip::tcp::acceptor      m_Acceptor;
	std::list<boost::shared_ptr<SCMonitoring> > m_MonitoringList;

	void Accept();

	void HandleAccept( boost::shared_ptr<SCMonitoring> p_SCMonitoringPtr, boost::system::error_code const& Error );
    void IoServiceThread();
public:
    static SCAcceptorMonitoring & GetInstance()
    {
        static SCAcceptorMonitoring    instance;	// Guaranteed to be destroyed.
                                                    // Instantiated on first use.
        return instance;
    }

    void Start();
    void Stop();
};

