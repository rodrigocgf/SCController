#include "SCChannel.h"

bool operator< ( const SCChannel& left, const SCChannel& right )
{
    if( left.Device < right.Device )
    {
        return true;
    }
    else if( left.Device == right.Device )
    {
        if( left.Channel < right.Channel )
            return true;
        else
            return false;
    }
    else
    {
        return false;
    }
}

bool operator==( const SCChannel& c1, const SCChannel& c2 )
{
    if( c1.Device == c2.Device && c1.Channel == c2.Channel )
        return true;
    else
        return false;
}

bool operator!=( const SCChannel& c1, const SCChannel& c2 )
{
    if( c1.Device == c2.Device && c1.Channel != c2.Channel )
        return true;
    else
        return false;
}
