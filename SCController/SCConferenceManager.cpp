
#include <algorithm>
#include "SCConferenceManager.h"


SCConferenceManager::SCConferenceManager(ISCController * pController) : m_pIController( pController)
{
    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("ConferenceManager") ); 

    SC_LOG4CPP( m_pLog4cpp,"vvvvvvvvvvvvvvvvvvvvvvvvv CONFERENCE MANAGER vvvvvvvvvvvvvvvvvvvvvvvvvvvvv" );
}

SCConferenceManager::~SCConferenceManager()
{
    boost::recursive_mutex::scoped_lock lock(MutexCM);
    RoomsAvailable.clear();
    
    SC_LOG4CPP( m_pLog4cpp,"^^^^^^^^^^^^^^^^^^^^^^^^^ CONFERENCE MANAGER ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" );
}
/*
SCConferenceManager& SCConferenceManager::GetInstance()
{
    static SCConferenceManager instance;
    return instance;
}
*/

std::map< std::string, boost::shared_ptr<ConferenceRoom> > SCConferenceManager::GetRoomsAvailable() 
{ 
    return RoomsAvailable; 
}

void SCConferenceManager::ClearRooms()
{   
    boost::recursive_mutex::scoped_lock lock(MutexCM);

    SC_LOG4CPP( m_pLog4cpp, "SCConferenceManager::ClearRooms() ");
    if ( GetRoomsSize() > 0 ) 
    {   
        std::map< std::string, boost::shared_ptr<ConferenceRoom> >::iterator itRooms;
        for (itRooms = RoomsAvailable.begin(); itRooms != RoomsAvailable.end(); itRooms++)
        {   
            itRooms->second->ClearUsers();
        }

        RoomsAvailable.clear();
    }
}

bool SCConferenceManager::CreateRoom( char const* ConferenceId, int RoomSize )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    
    boost::recursive_mutex::scoped_lock lock(MutexCM);
    bool roomCreated = true;

    SC_LOG4CPP_EX( m_pLog4cpp, " Creating room [%s] (size : %d)\n", ConferenceId % RoomSize );

    auto room = RoomsAvailable.find( ConferenceId );
    if( room == RoomsAvailable.end() )
    {
        boost::shared_ptr<ConferenceRoom> roomPtr(new ConferenceRoom(ConferenceId , RoomSize));
        
        RoomsAvailable.insert( std::pair< std::string, boost::shared_ptr<ConferenceRoom> >( ConferenceId, roomPtr ) );

        SC_LOG4CPP_EX( m_pLog4cpp," CREATED room [%s]", ConferenceId );
        
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," Room [%s] already exists!", ConferenceId );        
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return roomCreated;
}

bool SCConferenceManager::DestroyRoom( char const* RoomID )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    
    boost::recursive_mutex::scoped_lock lock(MutexCM);
    SC_LOG4CPP_EX( m_pLog4cpp, " Destroying room [%s] ", RoomID );

    auto room = RoomsAvailable.find( RoomID );
    if( room != RoomsAvailable.end() )
    {
        RoomsAvailable.erase( RoomID);
        SC_LOG4CPP_EX( m_pLog4cpp," DESTROYED room [%s]", RoomID );
    }

	SC_LOG4CPP_EXIT(m_pLog4cpp);
    return false;
}

bool SCConferenceManager::AddToRoom( char const* ConferenceId, char const* UserId, int KhompDevice, int KhompChannel, 
                                     bool IsSupervisor, bool Beep )
{
    
    boost::recursive_mutex::scoped_lock lock(MutexCM);

    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool userAdded = false;
    
    auto room = RoomsAvailable.find( ConferenceId );
    SC_LOG4CPP_EX( m_pLog4cpp, " Adding UserId [%s] to Room [%s] ", UserId % ConferenceId );

    if( room != RoomsAvailable.end() )
    {   
        userAdded = room->second->AddUser( UserId, KhompDevice, KhompChannel, IsSupervisor, Beep );
        if ( userAdded )
            SC_LOG4CPP_EX( m_pLog4cpp, " ADDED UserId [%s] to Room [%s] ", UserId % ConferenceId );
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] Room [%s] does not exist!", UserId % ConferenceId );        
    }

    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return userAdded;
}

bool SCConferenceManager::RemoveFromRoom( char const* ConferenceId, char const* UserId )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool userRemoved = false;
    
    boost::recursive_mutex::scoped_lock lock(MutexCM);
    auto room = RoomsAvailable.find( ConferenceId );

    SC_LOG4CPP_EX( m_pLog4cpp, " Removing UserId [%s] from Room [%s]", UserId % ConferenceId );
    if( room != RoomsAvailable.end() )
    {   
        userRemoved = room->second->RemoveUser( UserId );
        if ( userRemoved ) 
            SC_LOG4CPP_EX( m_pLog4cpp, " REMOVED UserId [%s] from room [%s] ", UserId % ConferenceId );
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] Room [%s] does not exist!", UserId, ConferenceId );        
    }
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return userRemoved;
}

int SCConferenceManager::GetNumberOfUsers( char const* ConferenceId  )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    int userCount = 0;
    
    boost::recursive_mutex::scoped_lock lock(MutexCM);
    auto room = RoomsAvailable.find( ConferenceId );
    if( room != RoomsAvailable.end() )
    {   
        userCount = room->second->GetNumberOfUsers();

        SC_LOG4CPP_EX( m_pLog4cpp," Room [%s] has %d users.", ConferenceId % userCount);        
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," Room [%s] does not exist!", ConferenceId );        
    }
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return userCount;
}

/*bool SCConferenceManager::ConferenceListen( char const* ConferenceId, char const* UserId, int KhompDevice, int KhompChannel )
{
    bool listening = false;
    Mutex.Lock();
    auto room = RoomsAvailable.find( ConferenceId );
    if( room != RoomsAvailable.end() )
    {
        try
        {
            listening = room->second.AddListener( UserId, KhompDevice, KhompChannel );
        }
        catch( ... )
        {
            Mutex.Unlock();
            throw;
        }
    }
    Mutex.Unlock();
    return listening;
}

bool SCConferenceManager::ConferenceUnlisten( char const* ConferenceId, char const* UserId )
{
    bool userRemoved = false;
    Mutex.Lock();
    auto room = RoomsAvailable.find( ConferenceId );
    if( room != RoomsAvailable.end() )
    {
        try
        {
            userRemoved = room->second.RemoveListener( UserId );
        }
        catch( ... )
        {
            Mutex.Unlock();
            throw;
        }
    }
    Mutex.Unlock();
    return userRemoved;
}*/

bool SCConferenceManager::ConferenceListen( char const* ConferenceId, char const* UserId )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool listening = false;
    
    boost::recursive_mutex::scoped_lock lock(MutexCM);

    SC_LOG4CPP_EX( m_pLog4cpp, " Adding UserId [%s] to Room [%s]", UserId % ConferenceId );
    auto room = RoomsAvailable.find( ConferenceId );
    if( room !=  RoomsAvailable.end() )
    {   
        listening = room->second->Listen( UserId );
        SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"ADDED UserId [%s] to Room [%s] ", UserId % ConferenceId );            
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"Room [%s] does not exist!", ConferenceId );
        //SC_LOG4CPP_EX( m_pLog4cpp," [MSISDN=%s] Room '%s' does not exist!", UserId, ConferenceId );        
    }
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return listening;
}

bool SCConferenceManager::ConferenceUnlisten( char const* ConferenceId, char const* UserId )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool listening = false;
    
    boost::recursive_mutex::scoped_lock lock(MutexCM);

    SC_LOG4CPP_EX( m_pLog4cpp, "Removing UserId [%s] from Room [%s]", UserId % ConferenceId );
    auto room = RoomsAvailable.find( ConferenceId );
    if( room !=  RoomsAvailable.end() )
    {   
        listening = room->second->Unlisten( UserId );
        SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"REMOVED UserId [%s] from Room [%s] ", UserId % ConferenceId );            
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"Room [%s] does not exist!", ConferenceId );
        //SC_LOG4CPP_EX( m_pLog4cpp," [MSISDN=%s] Room '%s' does not exist!", UserId, ConferenceId );        
    }
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return listening;
}

bool SCConferenceManager::ConferenceChangeStatus( char const* ConferenceId, char const* UserId, SCConference::ConfereeStatus Status )
{
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    bool statusChanged = false;
    
    boost::recursive_mutex::scoped_lock lock(MutexCM);

    auto room = RoomsAvailable.find( ConferenceId );
    if( room != RoomsAvailable.end() )
    {   
        statusChanged = room->second->ChangeStatus( UserId, Status );
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp," UserId [%s] Room [%s] does not exist!", UserId % ConferenceId );        
    }
    SC_LOG4CPP_EXIT(m_pLog4cpp);
    return statusChanged;
}

