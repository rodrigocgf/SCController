#pragma once


#include <stdio.h>
#include <tchar.h>
#include <k3l.h>
#include <iostream>
#include <deque>
#include <map>
#include <string>
#include <iterator>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <fstream>
#include <ctime>
#include <atomic>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/regex.hpp>
#include <boost/thread/xtime.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
//#include <boost/threadpool.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/locks.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/format.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/attributes/attribute.hpp>
#include <boost/log/attributes/attribute_cast.hpp>
#include <boost/log/attributes/attribute_value.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/channel_feature.hpp>
#include <boost/log/sources/channel_logger.hpp>
#include <boost/move/utility.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/attributes/attribute_name.hpp>
#include <boost/log/attributes/scoped_attribute.hpp>
#include <boost/log/expressions/attr_fwd.hpp>
#include <boost/log/expressions/attr.hpp>
#include <boost/log/expressions/predicates/has_attr.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/log/expressions/formatters/named_scope.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>

#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.

#include "log4cpp/Category.hh"
#include "log4cpp/RollingFileAppender.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/Layout.hh"
#include "log4cpp/PatternLayout.hh"
#include "log4cpp/Priority.hh"

#define SC_LOG4CPP(Logger, Message)			\
		Logger->debug( Message );

#define SC_LOG4CPP_ENTRY(Logger)			            \
        { std::stringstream ss;                         \
        ss << "[" << __FUNCTION__ << "] (b)";           \
        Logger->debug( ss.str().c_str() ); }

#define SC_LOG4CPP_EXIT(Logger)			                \
        { std::stringstream ss;                         \
        ss << "[" << __FUNCTION__ << "] (e)";           \
        Logger->debug( ss.str().c_str() ); }

#define SC_LOG4CPP_ENTRY_EX(Logger,Message, ...)			\
        { std::stringstream ss;                             \
        ss << "[" << __FUNCTION__ << "] (b)";               \
        ss << boost::format(Message) % __VA_ARGS__;         \
        Logger->debug( ss.str().c_str() ); }

#define SC_LOG4CPP_EX(Logger,Message, ...)			        \
        { std::stringstream ss;                             \
        ss << "[" << __FUNCTION__ << "] ";                  \
        ss << boost::format(Message) % __VA_ARGS__;         \
        Logger->debug( ss.str().c_str() ); }

#define SC_LOG4CPP_EXIT_EX(Logger,Message, ...)			    \
        { std::stringstream ss;                             \
        ss << "[" << __FUNCTION__ << "] (e)";               \
        ss << boost::format(Message) % __VA_ARGS__;         \
        Logger->debug( ss.str().c_str() ); }

typedef enum
{
    IDLE = 0,
    BLOCKED,
    ANSWERED
} CHANNEL_STATUS;

#define FIXED_PASSWORD  "multimidia"

typedef enum
{
    SE = 240,                   // End of subnegotiation parameters.
    NOP = 241,                  // No operation.
    Data_Mark = 242,            // The data stream portion of a Synch.
                                // This should always be accompanied
                                // by a TCP Urgent notification.
    Break = 243,                // NVT character BRK.
    Interrupt_Process = 244,    // The function IP.
    Abort_output = 245,         // The function AO.
    Are_You_There = 246,        // The function AYT.
    Erase_character = 247,      // The function EC.
    Erase_Line = 248,           // The function EL.
    Go_ahead = 249,             // The GA signal.
    SB = 250,                   // Indicates that what follows is
                                // subnegotiation of the indicated
                                // option.
    WILL = 251,                 // WILL (option code)
                                // Indicates the desire to begin
                                // performing, or confirmation that
                                // you are now performing, the
                                // indicated option.
    WONT = 252,                 // WON'T (option code)
                                // Indicates the refusal to perform,
                                // or continue performing, the
                                // indicated option.
    DO = 253,                   // DO (option code)
                                // Indicates the request that the
                                // other party perform, or
                                // confirmation that you are expecting
                                // the other party to perform, the
                                // indicated option.
    DONT = 254,                 // DON'T (option code) 
                                // Indicates the demand that the
                                // other party stop performing,
                                // or confirmation that you are no
                                // longer expecting the other party
                                // to perform, the indicated option.
    IAC = 255                   // INTERPRET AS COMMAND
} TELNET;

typedef enum
{
    SUPPRESS_GO_AHEAD = 3,
    STATUS = 5,
    ECHO = 1,
    TIMING_MARK = 6,
    TERMINAL_TYPE = 24,
    WINDOW_SIZE = 31,
    TERMAINAL_SPEED = 32,
    REMOTE_FLOW_CONTROL = 33,
    LINE_MODE = 34,
    ENV_VARIABLES = 36
} TELNET_OPTIONS;

typedef enum
{
    TSDATA = 0,                   
    TSIAC = 1,                
    TSDOPT = 2,
    TSWOPT = 3,
    TSSUBIAC = 4,
    TSSUBNEG = 5
} TSTATE;

#define BACKSPACE   (char)(0x08)
#define DELETE      (char)(0x7f)