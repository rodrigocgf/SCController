
#pragma once

#include "SCConfigReader.h"
#include "SCOSFunctions.h"

#include <mutex>
#include <cstdarg>
#include <atomic>
#include <iostream>

#include <KHostSystem.h>


class SCLogger
{
public:
    	static const int SCLOG_CONSOLE = 0x01;
        static const int SCLOG_TRACE   = 0x02;
        static const int SCLOG_DEBUG   = 0x04;
        static const int SCLOG_ERROR   = 0x08;

//private:
    FILE* LogFile;
    std::atomic_bool  FileOpen;
    boost::recursive_mutex LogMutex;
    std::string FileName;

    boost::filesystem::ofstream fOut;
    //fOut.open(Filename.c_str(),std::ios::out);

	static const int MAX_MSG_SIZE = 2048;

    int CurrentLevel;

    SCLogger( SCLogger& );
    SCLogger( );

	bool LOGToFile( std::string const& Message )
	{   
        try
        {
            if( !IsOpen() ) 
                Open();

            fOut << Message;
            fOut.flush();
        
		    return true;
        }
        catch(...)
        {
            return false;
        }
	}

public:
    SCLogger( std::string const& FileName , int logLevel)
        : LogFile( NULL )
        , CurrentLevel( SCLOG_ERROR )
        , FileName( FileName )
    {
        FileOpen = false;
        CurrentLevel = logLevel;
        //CurrentLevel = SCConfigReader::GetLogLevel();
    }

	inline int GetActiveLevel() const { return CurrentLevel; }

    ~SCLogger()
    {
       Close();
    }

    bool Open( )
    {   
        boost::recursive_mutex::scoped_lock lk(  LogMutex );
        if( !IsOpen() )
        {   
            fOut.open(FileName.c_str(),std::ios::out | std::ios::app);
            FileOpen = true;
            
        }
        return IsOpen();
    }

    void Close( )
    {   
        boost::recursive_mutex::scoped_lock lk(  LogMutex );
        FileOpen = false;
        
        if ( fOut )
            fOut.close();
        
    }

    bool IsOpen() const { return FileOpen; }

    bool LOG( const int Level, char const* fmt, ... )
    {
        boost::recursive_mutex::scoped_lock lk(  LogMutex );

        std::stringstream ss;
        bool msgWritten = false;
        if( Level >= CurrentLevel || Level & SCLOG_CONSOLE )
        {
            char message[MAX_MSG_SIZE];
            va_list vl;
            va_start( vl, fmt );
            vsnprintf( message, sizeof( message ) / sizeof( char ), fmt, vl );
            va_end( vl );

            std::string logMessage;
            
            boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
            long milliseconds = timeLocal.time_of_day().total_milliseconds();
            boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
            boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);
            
            ss.str(std::string());
            ss <<   boost::format("[%04i-%02i-%02i %02i:%02i:%02i.%03i] %s") % current_date_milliseconds.date().year() % 
                    current_date_milliseconds.date().month().as_number() % current_date_milliseconds.date().day() %
                    current_date_milliseconds.time_of_day().hours() % current_date_milliseconds.time_of_day().minutes() %
                    current_date_milliseconds.time_of_day().seconds() %
                    (current_date_milliseconds.time_of_day().fractional_seconds() / 1000) %  message;
            logMessage = ss.str();
			
			if( Level & SCLOG_CONSOLE )
				printf( logMessage.c_str() );
			if( Level >= CurrentLevel )
				msgWritten = LOGToFile( logMessage );
        }
        return msgWritten;
    }
};




#define SC_LOG(Logger, Level, Message, ...)			\
	if( Level >= Logger.GetActiveLevel() )			\
		Logger.LOG( Level, Message, __VA_ARGS__ );

#define SC_LOG_FN_ENTRY( Logger )							\
	SC_LOG( Logger, SCLogger::SCLOG_TRACE, __FUNCTION__ " (b)\n" );

#define SC_LOG_FN_ENTRY_EX( Logger, Params, ... )			\
	SC_LOG( Logger, SCLogger::SCLOG_TRACE, __FUNCTION__ Params " (b)\n", __VA_ARGS__ );

#define SC_LOG_FN_EXIT( Logger )							\
	SC_LOG( Logger, SCLogger::SCLOG_TRACE, __FUNCTION__ " (e)\n" );

#define SC_LOG_FN_EXIT_EX( Logger, Params, ... )			\
	SC_LOG( Logger, SCLogger::SCLOG_TRACE, __FUNCTION__ Params " (e)\n", __VA_ARGS__ );



#define SC_LOG_PTR(Logger, Level, Message, ...)			\
	if( Level >= Logger->GetActiveLevel() )			\
		Logger->LOG( Level, Message, __VA_ARGS__ );

#define SC_LOG_FN_ENTRY_PTR( Logger )							\
	SC_LOG_PTR( Logger, SCLogger::SCLOG_TRACE, __FUNCTION__ " (b)\n" );

#define SC_LOG_FN_ENTRY_EX_PTR( Logger, Params, ... )			\
	SC_LOG_PTR( Logger, SCLogger::SCLOG_TRACE, __FUNCTION__ Params " (b)\n", __VA_ARGS__ );

#define SC_LOG_FN_EXIT_PTR( Logger )							\
	SC_LOG_PTR( Logger, SCLogger::SCLOG_TRACE, __FUNCTION__ " (e)\n" );

#define SC_LOG_FN_EXIT_EX_PTR( Logger, Params, ... )			\
	SC_LOG_PTR( Logger, SCLogger::SCLOG_TRACE, __FUNCTION__ Params " (e)\n", __VA_ARGS__ );
