
#ifndef __SCGARBAGECOLLECTOR_H__
#define __SCGARBAGECOLLECTOR_H__

#include "SCStdIncludes.h"
//#include "SCLogger.h"

typedef enum CollectableObjects
{
    ScriptRunner,
} ObjectType;

struct GCData
{
    ObjectType Type;
    void* Object;
};


class SCGarbageCollector 
{
private:
    
    boost::mutex                  m_GCMutex;
    std::deque<GCData *>        ObjectsList;
    boost::condition            m_cond;
    volatile bool               m_stop;
    //SCLogger                    Logger;
    log4cpp::Category * 					m_pLog4cpp;

    boost::shared_ptr<boost::thread> m_ExecuteThreadPtr;
	
    //SCGarbageCollector();
    //~SCGarbageCollector();
    //SCGarbageCollector( SCGarbageCollector& );
    //SCGarbageCollector& operator=( SCGarbageCollector const& );
    
    void Execute();
    void RunThread();
public:

    SCGarbageCollector();
    ~SCGarbageCollector();
    SCGarbageCollector( SCGarbageCollector& );
    SCGarbageCollector& operator=( SCGarbageCollector const& );
    //static SCGarbageCollector& GetInstance();
    template<typename T> void AddToGarbage( T* Object );
};

#endif 