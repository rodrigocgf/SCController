

#ifndef _INCLUDED_KHOMP_SCLUAAPI_H_
#define _INCLUDED_KHOMP_SCLUAAPI_H_


struct lua_State;
class SCCall;
class SCCDRLogger;
class SCConfigReader;

int lua_scReadFile( SCCall* MyCall, char const* Filename );
int lua_scReadFileCDR( SCCDRLogger* MyCDR, SCCall* MyCall, char const* Filename );
int lua_scReadFileCDR( SCCDRLogger* MyCDR, SCCall* MyCall, SCConfigReader * MyConfigReader , char const* Filename );
char const* lua_scGetErrorMessage( );

#endif