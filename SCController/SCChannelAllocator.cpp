
#include "SCCall.h"
#include "SCChannelAllocator.h"


SCChannelAllocator::SCChannelAllocator(ISCController * controller) : m_pController(controller)
{
    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("ChannelAllocator") ); 
}

SCChannelAllocator::SCChannelAllocator() 
{
    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("ChannelAllocator") ); 
}

SCChannelAllocator::~SCChannelAllocator()
{
}

void SCChannelAllocator::ReleaseChannel( SCChannel Channel )
{   
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp,"[%02d][%03d]", Channel.Device % Channel.Channel );
	
    {
        boost::mutex::scoped_lock lock(  QueueMutex );
		//std::lock_guard<std::mutex> lock( QueueMutex );
		
		//reverse search, because the probability of the channel being on the back of the list is larger
		auto queueIt = std::find( FreeChannels.rbegin(), FreeChannels.rend(), Channel );
		if( queueIt == FreeChannels.rend() )
		{	
			FreeChannels.push_back( Channel );
            
            SC_LOG4CPP_EX(m_pLog4cpp,"Channel Releasing [%02d][%03d] (freeCount=%d)\n", Channel.Device % Channel.Channel % FreeChannels.size() );

            auto itrChannelsStatus = m_ChannelsStatusMap.find(Channel);
            if ( itrChannelsStatus != m_ChannelsStatusMap.end() )
            {
                m_ChannelsStatusMap[Channel] = IDLE;
            } else {
                m_ChannelsStatusMap.insert( std::pair< SCChannel, CHANNEL_STATUS >( Channel, IDLE ) );
            }
		}
	}
	
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp,"[%02d][%03d]", Channel.Device % Channel.Channel );    
}


SCChannel SCChannelAllocator::AllocateChannel()
{
    //std::lock_guard<std::mutex>lock( QueueMutex );
    boost::mutex::scoped_lock lock(  QueueMutex );
    
    SCChannel frontChannel;
    frontChannel.Device = -1;
    frontChannel.Channel = -1;

    if ( FreeChannels.size() > 0 )
    {
        frontChannel = FreeChannels.front();

        auto itrChannelsStatus = m_ChannelsStatusMap.find(frontChannel);
        bool mapSearchEnd = false;
        bool idleFound = false;
        int iLoopCount = 0;
        do {
            if (itrChannelsStatus->second == BLOCKED )
            {
                itrChannelsStatus++;
                if ( itrChannelsStatus == m_ChannelsStatusMap.end() )
                    itrChannelsStatus = m_ChannelsStatusMap.begin();
            } 
            else if (itrChannelsStatus->second == IDLE )
                idleFound = true;

            iLoopCount++; 
            if ( iLoopCount == m_ChannelsStatusMap.size() )
                mapSearchEnd = true;
        } while (!mapSearchEnd && !idleFound);

        if ( iLoopCount <= m_ChannelsStatusMap.size() && idleFound ) 
        {
            if ( frontChannel == (SCChannel)(itrChannelsStatus->first) )
            {   
                FreeChannels.pop_front();
                itrChannelsStatus->second = ANSWERED;
                
                SC_LOG4CPP_EX(m_pLog4cpp,"Channel Allocation [%02d][%03d] (freeCount=%d)", frontChannel.Device % frontChannel.Channel % FreeChannels.size() );
            } 
            else 
            {
                SCChannel channelAux;
                do
                {   
                    frontChannel = FreeChannels.front();	
                    channelAux = frontChannel;
                    FreeChannels.pop_front();
                    if ( frontChannel != itrChannelsStatus->first )
                    {   
                        FreeChannels.push_back(channelAux);
                    }
                } while ( frontChannel != itrChannelsStatus->first );
        
                itrChannelsStatus->second = ANSWERED;
                
                SC_LOG4CPP_EX(m_pLog4cpp,"Channel Allocation [%02d][%03d] (freeCount=%d)", frontChannel.Device % frontChannel.Channel % FreeChannels.size() );
            }
        } else {
            frontChannel.Device = -1;
            frontChannel.Channel = -1;
            
            SC_LOG4CPP(m_pLog4cpp,"There are no Free Channels. All are busy or blocked. ");
        }    
    }

    return frontChannel;
}

void SCChannelAllocator::InvalidateChannel( SCChannel const& Channel )
{	
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp,"[%02d][%03d]", Channel.Device % Channel.Channel );
	{
        boost::mutex::scoped_lock lock(  QueueMutex );
		//std::lock_guard<std::mutex> lock( QueueMutex );
		auto chan = std::find( FreeChannels.begin(), FreeChannels.end(), Channel );
		if( chan != FreeChannels.end() )
			FreeChannels.erase( chan );
	}
	
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp,"[%02d][%03d]", Channel.Device % Channel.Channel );
    
}

int SCChannelAllocator::GetTotalChannels()
{
    boost::mutex::scoped_lock lock(  QueueMutex );
	//std::lock_guard<std::mutex> lock( QueueMutex );

	return m_MaxChannels;
}

int SCChannelAllocator::GetFreeChannels()
{
    boost::mutex::scoped_lock lock(  QueueMutex );
	//std::lock_guard<std::mutex> lock( QueueMutex );

    int iCount = 0;
    auto itrChannelsStatus = m_ChannelsStatusMap.begin();    
    while ( itrChannelsStatus != m_ChannelsStatusMap.end() )
    {
        if (itrChannelsStatus->second == IDLE )
            iCount++;

        itrChannelsStatus++;
    }

	return iCount;
}


/*
int SCChannelAllocator::GetFreeChannels()
{
	std::lock_guard<std::mutex> lock( QueueMutex );

	return FreeChannels.size();
}
*/

int SCChannelAllocator::GetBusyChannels()
{
	//std::lock_guard<std::mutex> lock( QueueMutex );
    boost::mutex::scoped_lock lock(  QueueMutex );

    int iCount = 0;
    auto itrChannelsStatus = m_ChannelsStatusMap.begin();    
    while ( itrChannelsStatus != m_ChannelsStatusMap.end() )
    {
        if (itrChannelsStatus->second == ANSWERED )
            iCount++;

        itrChannelsStatus++;
    }

	return iCount;
}

/*
int SCChannelAllocator::GetBusyChannels()
{
	std::lock_guard<std::mutex> lock( QueueMutex );

	return m_MaxChannels - FreeChannels.size();
}
*/

int SCChannelAllocator::GetBlockedChannels()
{
	//std::lock_guard<std::mutex> lock( QueueMutex );
    boost::mutex::scoped_lock lock(  QueueMutex );

    int iCount = 0;
    auto itrChannelsStatus = m_ChannelsStatusMap.begin();    
    while ( itrChannelsStatus != m_ChannelsStatusMap.end() )
    {
        if (itrChannelsStatus->second == BLOCKED )
            iCount++;

        itrChannelsStatus++;
    }

	return iCount;
}

void SCChannelAllocator::SetMaxChannels(int value)
{
	m_MaxChannels = value;
}

void SCChannelAllocator::SetChannelStatus(int channel , CHANNEL_STATUS status)
{
    //std::lock_guard<std::mutex>lock( QueueMutex );
    boost::mutex::scoped_lock lock(  QueueMutex );

    SCChannel Channel;
    Channel.Device = 0;
    Channel.Channel = channel;
    auto itrChannelsStatus = m_ChannelsStatusMap.find(Channel);

    if ( itrChannelsStatus != m_ChannelsStatusMap.end() )
    {
        m_ChannelsStatusMap[Channel] = status;
    }
}

CHANNEL_STATUS SCChannelAllocator::GetChannelStatus(int channel)
{
    //std::lock_guard<std::mutex>lock( QueueMutex );
    boost::mutex::scoped_lock lock(  QueueMutex );

    if ( (channel < 0) || (channel >= m_MaxChannels) )
        return IDLE;

    SCChannel Channel;
    Channel.Device = 0;
    Channel.Channel = channel;
    auto itChannelsMap = m_ChannelsStatusMap.find( Channel );	
    if ( itChannelsMap != m_ChannelsStatusMap.end() )
	{
        return m_ChannelsStatusMap[Channel];
    }
    return IDLE;
}

std::string SCChannelAllocator::GetChannelANI(int channel)
{
    std::string ani;
    if ( m_pController != NULL )
    {
        if ( m_pController->GetCallShared(SCChannel(0,channel)).get() != NULL )
            ani = m_pController->GetCallShared(SCChannel(0,channel))->GetANI();
    }
    
    return ani;
}

void SCChannelAllocator::DisconnectChannel(int channel)
{
    if ( m_pController != NULL )
    {
        if ( m_pController->GetCallShared(SCChannel(0,channel)).get() != NULL )
            m_pController->GetCallShared(SCChannel(0,channel))->DisconnectChannel();
    }
}