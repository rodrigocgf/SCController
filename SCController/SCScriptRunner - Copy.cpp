
#include "SCScriptRunner.h"

#include "SCLuaAPI.h"
#include "SCConfigReader.h"
//#include "SCGarbageCollector.h"
#include "SCChannelAllocator.h"


SCLogger SCScriptRunner::Logger( SCConfigReader::GetLogDir()  + "/ScriptRunner.log" );

void SCScriptRunner::Execute()
{
	SC_LOG_FN_ENTRY( Logger );
    int ret;
    ktools::time::KSystemTime currTime;
    KHostSystem::GetTime( &currTime );
    ktools::kstring scriptPath;

    SCCall* Call = MyCall.get();

    try
    {
        scriptPath = GetScriptPath();
		
		Logger.LOG( SCLogger::SCLOG_DEBUG, "Going to read script[%s]: %s\n", Call->GetCallId().c_str(), scriptPath.c_str() );
        ret = lua_scReadFile( Call, scriptPath.c_str() );      // reads the script and runs it
		Logger.LOG( SCLogger::SCLOG_DEBUG, "Finished reading script[%s][%d]: %s\n", Call->GetCallId().c_str(), ret, scriptPath.c_str() );
        
        if( ret != 0 )
        {
            (MyCall.get())->Hangup();
            
            printf( "[%04d-%02d-%02d %02d:%02d:%02d.%03d] %d - ", currTime.Year, currTime.Month, currTime.Day, currTime.Hour, currTime.Minute, currTime.Second, 
                        currTime.Milli, ret );
            printf( "Error while running the script (%s). Channel[%02d][%03d].\n", scriptPath.c_str(), Call->GetKhompDevice(), Call->GetKhompChannel() );
        }
    }
    catch( std::exception& exc )
    {
        (MyCall.get())->Hangup();
        printf( "[%04d-%02d-%02d %02d:%02d:%02d.%03d] ", currTime.Year, currTime.Month, currTime.Day, currTime.Hour, currTime.Minute, currTime.Second, 
            currTime.Milli );

        printf( "Exception while executing the call script (%s)! Channel[%02d][%03d]: %s\n", scriptPath.c_str(), Call->GetKhompDevice(), Call->GetKhompChannel(), exc.what() );
    }
    catch( ... )
    {
        (MyCall.get())->Hangup();
        printf( "[%04d-%02d-%02d %02d:%02d:%02d.%03d] ", currTime.Year, currTime.Month, currTime.Day, currTime.Hour, currTime.Minute, currTime.Second, 
                    currTime.Milli );

        printf( "Unknown exception while executing the call script (%s)! Channel[%02d][%03d].\n", scriptPath.c_str(), Call->GetKhompDevice(), Call->GetKhompChannel() );
    }
    Terminate();
    //SCChannelAllocator::GetInstance().ReleaseChannel( SCChannel( Call->GetKhompDevice(), Call->GetKhompChannel() ) );
    
    //SCGarbageCollector::GetInstance().AddToGarbage<SCScriptRunner>( this ); // OK
	SC_LOG_FN_EXIT( Logger );
}

ktools::kstring SCScriptRunner::GetScriptPath()
{
    ktools::kstring scriptPath;
    if( SCConfigReader::GetScriptingMode() != UseDNIS )
    {
        scriptPath.sprintf( "%s\\%s", SCConfigReader::GetScriptDir().c_str(), SCConfigReader::GetScriptInit().c_str() );
    }
    else
    {
        ktools::kstring const& DNIS = MyCall.get()->GetDNIS();
        scriptPath.sprintf( "%s\\%s\\%s.lua", SCConfigReader::GetScriptDir().c_str(), DNIS.c_str(), DNIS.c_str() );
    }
    return scriptPath;
}