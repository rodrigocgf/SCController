
#pragma once

#ifdef _DEBUG
#define BOOST_ASIO_ENABLE_HANDLER_TRACKING
#endif

#include "SCStdIncludes.h"

#include "SCConferenceManager.h"
#include "ISCController.h"
#include "SCLogger.h"


using namespace boost::asio;
using namespace boost::posix_time;



class SCConferenceService : public boost::enable_shared_from_this< SCConferenceService >, boost::noncopyable
{
public:
    typedef SCConferenceService self_type;


    ip::tcp::socket ReadSocket;
    boost::asio::streambuf ReadBuffer;
    boost::asio::streambuf WriteBuffer;
    char write_buffer_[1024];
    bool Started;
    deadline_timer Timer;
    //SCLogger Logger;
    log4cpp::Category * 					m_pLog4cpp;    
        
    
    SCConferenceService( io_service& Service , ISCController * p_controller);
    ~SCConferenceService(void);
    
    void Start( );
    void Stop();
    bool HasStarted() const { return Started; }
    ip::tcp::socket& GetSocket();

    typedef boost::shared_ptr< SCConferenceService > ptr;
    typedef boost::system::error_code error_code;
    

private:
    ISCController *					m_pController;

    void OnRead( error_code const& Error, size_t Bytes );
    void DoRead();
    void OnWrite( error_code const& Error, size_t Bytes );
    void DoWrite( std::vector<std::string> const& Messages );
    void DoWrite( std::string const& Message );
    
    void ParseInput( std::vector<std::string>& ParseResult, std::string& ParamsStr, char separator );
    
    void OnCriaSala( std::vector<std::string> const& Params );
    void OnDestroiSala( std::vector<std::string> const& Params );
    void OnBloqueiaFala( std::vector<std::string>& Params );
    void OnDesbloqueiaFala( std::vector<std::string>& Params );
    void OnExpulsaUsuario( std::vector<std::string>& Params );
    void OnMonitoraUsuario( std::vector<std::string>& Params );
    void OnTerminaMonitoracaoUsuario( std::vector<std::string>& Params );
    void OnMonitorConversaUsuario( std::vector<std::string>& Params );
    void OnMonitorTerminaConversa( std::vector<std::string>& Params );
    void OnUsuarioConversaParticipante( std::vector<std::string>& Params );
    void OnTerminaConversa( std::vector<std::string>& Params );
    void OnMonitoraSala( std::vector<std::string>& Params );
    void OnTerminaMonitoracaoSala( std::vector<std::string>& Params );
    void OnRemoveParticipanteSala( std::vector<std::string>& Params );
    void OnHabilitaConversaPersonalidade( std::vector<std::string>& Params );
    void OnDesabilitaConversaPersonalidade( std::vector<std::string>& Params );
    void OnEntraAtendimento( std::vector<std::string>& Params );
    void OnSaiAtendimento( std::vector<std::string>& Params );
    void OnPing();

    void ToUpper( std::string& Text );
    void PrintParams( const std::string & functionName , const std::vector<std::string>& Params);
};
 