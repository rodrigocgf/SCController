
#include "SCController.h"
#include "SCCall.h"
#include "SCScriptRunner.h"
#include <iostream>
#include <fstream>
#include <thread>
#include "SCGarbageCollector.h"


SCController::SCController():   InternalId( 0 ) ,                                 
                                m_Stopping(false),
                                m_ChanAllocatorPtr( new SCChannelAllocator(this) )
{
    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("Controller") ); 

    m_GarbageCollector.reset(new SCGarbageCollector());    
        
    m_ConferenceManagerPtr.reset(new SCConferenceManager(this) );
}

SCController::~SCController()
{
}

void SCController::Initialize(boost::shared_ptr<SCConfigReader> configReaderPtr)
{       
    SC_LOG4CPP_ENTRY(m_pLog4cpp);
    
    m_configReaderPtr = configReaderPtr;
    m_CDRLogger.reset(new SCCDRLogger(m_configReaderPtr->GetCDRDir() , m_configReaderPtr->GetCDRInterval()) );    

    m_IoServiceAcceptor.reset(new SCIoServiceAcceptor( this ) );
	m_IoServiceAcceptor->Start();

	int voipDevice = k3lGetDeviceCount() - 1;
    K3L_DEVICE_CONFIG devConf;
    k3lGetDeviceConfig( voipDevice, ksoDevice, &devConf, sizeof( devConf ) );
	m_ChanAllocatorPtr->SetMaxChannels(devConf.ChannelCount);
	for( int chan = 0; chan < devConf.ChannelCount; ++chan )
    {
        K3L_CHANNEL_STATUS chanStatus;
        k3lGetDeviceStatus( voipDevice, ksoChannel + chan, &chanStatus, sizeof( chanStatus ) );
        if( chanStatus.CallStatus == kcsFree )
        {   
            m_ChanAllocatorPtr->ReleaseChannel( SCChannel( voipDevice, chan ) );
        }
    }
        
    
    SC_LOG4CPP_EXIT(m_pLog4cpp);
}

void SCController::Stop()
{    
    SC_LOG4CPP_ENTRY(m_pLog4cpp);

    m_Stopping = true;
    
    if ( m_DictChannels.size() != 0 )
    {   
        RemoveActiveCalls();   
    }

    m_CDRLogger->Finalize();

    m_IoServiceAcceptor->Stop();

    SC_LOG4CPP_EXIT(m_pLog4cpp);    
}

void SCController::RegisterEventHandler()
{
     k3lRegisterEventHandler( &SCController::K3LEventHandler);
}

int Kstdcall SCController::K3LEventHandler( int Obj, K3L_EVENT* Event )
{
    return SCController::GetInstance().EventHandler(Obj, Event);
}


int SCController::EventHandler( int Obj, K3L_EVENT* Event )
{    
    //SC_LOG4CPP_ENTRY(m_pLog4cpp);

    SCChannel channel( Event->DeviceId, Obj );
    switch( Event->Code )
    {
	case EV_SIP_INVITE:   
        OnSipInvite( Event->AddInfo ); 
        break;
    case EV_NEW_CALL:     
        OnNewCall( channel, Event ); 
        break;

    default:              
        OnDefault( channel, Event ); 
        break;
    }
    
    //SC_LOG4CPP_EXIT(m_pLog4cpp);
    
    return ksSuccess;
}

void SCController::OnSipInvite( const int CallId )
{   
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp,"CallId [%d]", CallId);

    SCChannel channel;
    char callid[64];

    if ( !m_Stopping ) 
    {
        memset(callid,0x00,sizeof(callid) );
        sprintf( callid, "call_id=\"%d\"", CallId );
    
        try
        {
            channel = m_ChanAllocatorPtr->AllocateChannel();
        }
        catch( std::exception& exc )
        {   
            SC_LOG4CPP_EX( m_pLog4cpp, "ERROR allocating channel for %s, %s", callid % exc.what() );
            
            SendCommand( -1, -1, CM_CANCEL_SIP_CALL, reinterpret_cast<byte*>(callid) );
            return;

        }
        catch( ... )
        {
            SC_LOG4CPP_EX( m_pLog4cpp, "ERROR allocating channel for %s", callid );
            
            SendCommand( -1, -1, CM_CANCEL_SIP_CALL, reinterpret_cast<byte*>(callid) );
            return;
        }

        if ( channel.Channel != -1 )
        {
            stt_code ret = SendCommand( channel.Device, channel.Channel, CM_ASSOCIATE_SIP_CHANNEL, reinterpret_cast<byte*>(callid) );
            if( ret != ksSuccess )
            {
                m_ChanAllocatorPtr->ReleaseChannel( channel );
                SC_LOG4CPP_EX( m_pLog4cpp,"ERROR associating SIP channel[%02d](%d), %s", channel.Channel % ret % callid );
                
            } else {
                SC_LOG4CPP_EX( m_pLog4cpp,"OnSipInvite : CHANNEL ASSOCIATED ( CM_ASSOCIATE_SIP_CHANNEL ) : %s ", callid );                
            }
        } else {
            SC_LOG4CPP_EX( m_pLog4cpp,"OnSipInvite : CHANNEL BLOCKED  : %s ", callid );            
            SendCommand( -1, -1, CM_CANCEL_SIP_CALL, reinterpret_cast<byte*>(callid) );
        }
    }

    SC_LOG4CPP_EXIT_EX(m_pLog4cpp,"CallId [%d]", CallId);    
}

void SCController::OnNewCall( SCChannel const& Channel, K3L_EVENT* Event )
{   
    std::stringstream ss;
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp, "[%02d][%03d]", Channel.Device % Channel.Channel );

    char ani[KMAX_DIAL_NUMBER + 1];
    char dnis[KMAX_DIAL_NUMBER + 1];

    shared_ptr<SCCall> callPtr = nullptr;
    boost::shared_ptr<CChannels> channelsPtr = nullptr;

    try
    {   
        
        boost::upgrade_lock<boost::shared_mutex> lock( DictChannelsMutex );
	    boost::upgrade_to_unique_lock<boost::shared_mutex>  unique_lock(lock);
                

        if ( !m_Stopping ) 
        {   
            if ( m_ChanAllocatorPtr->GetChannelStatus(Channel.Channel) != BLOCKED )
            {   
                auto CChannelsItr = m_DictChannels.get<ByAll>().find(boost::make_tuple( Channel.Device , Channel.Channel , InternalId ) );
			    if ( CChannelsItr == m_DictChannels.get<ByAll>().end() )
			    {   
                    memset(ani,0x00,sizeof(ani));                    
                    memset(dnis,0x00,sizeof(dnis));

                    k3lGetEventParam( Event, "orig_addr", ani, KMAX_DIAL_NUMBER );
                    k3lGetEventParam( Event, "dest_addr", dnis, KMAX_DIAL_NUMBER ); 

                    InternalId++;

                    callPtr.reset( new SCCall( this , m_configReaderPtr, Channel.Device, Channel.Channel, ani, dnis, InternalId ) );
                    channelsPtr.reset(new CChannels( Channel.Device, Channel.Channel , callPtr , InternalId ) );
                    //channelsPtr.reset(new CChannels( callPtr->GetDevice(), callPtr->GetChannel() , callPtr , InternalId ) );

                    m_DictChannels.insert(channelsPtr);                    

                    SC_LOG4CPP( m_pLog4cpp,"------------------------------------------------------------------------------------");
				    SC_LOG4CPP( m_pLog4cpp,"                                      NEW Call                                      ");
				    SC_LOG4CPP( m_pLog4cpp,"                                      ========                                      ");
				    SC_LOG4CPP( m_pLog4cpp,"[     Device     -    Channel   -          ANI         -     InternalId    ] ");
				    ss << boost::format("      %-5d     -       %-5d   -    %-10s     -        %-7d    ") % callPtr->GetDevice() % callPtr->GetChannel() % ani % InternalId;
				    SC_LOG4CPP( m_pLog4cpp, ss.str().c_str() );
				    SC_LOG4CPP( m_pLog4cpp,"------------------------------------------------------------------------------------");				
				    
                    //InternalId++;
                
                    SCScriptRunner * runnerPtr = new SCScriptRunner(m_configReaderPtr,m_CDRLogger , callPtr , this);
                    runnerPtr->Execute();
                    
                }
            }
        }
    }
    catch( std::exception& exc )
    {	
        SC_LOG4CPP_EX( m_pLog4cpp, "Exception while creating a new call! Channel[%02d][%03d]: %s", Channel.Device % Channel.Channel % exc.what() );        
    }
    catch( ... )
    {
        SC_LOG4CPP_EX( m_pLog4cpp, "Unknown exception while creating a new call! Channel[%02d][%03d]", Channel.Device % Channel.Channel );        
    }
	
    PrintDictChannels();

    SC_LOG4CPP_EXIT_EX(m_pLog4cpp, "[%02d][%03d]", Channel.Device % Channel.Channel );    
}

void SCController::AddToGarbageCollector(void * pRunner)
{
    SCScriptRunner * pSCScriptRunner =  ( SCScriptRunner * ) pRunner;
    m_GarbageCollector->AddToGarbage<SCScriptRunner>( pSCScriptRunner );
}

void SCController::RemoveActiveCalls()
{   

    boost::upgrade_lock<boost::shared_mutex> lock( DictChannelsMutex );
	boost::upgrade_to_unique_lock<boost::shared_mutex>  unique_lock(lock);
	
	BOOST_FOREACH( boost::shared_ptr<CChannels> dictChannelPtr , m_DictChannels )
	{
		if ( dictChannelPtr.get() != nullptr )
		{
			if ( dictChannelPtr->CallPtr != nullptr )
			{
				if ( !dictChannelPtr->CallPtr->WasDisconnected() )
				{   
					dictChannelPtr->CallPtr->Stop();
				}	
			}
		}		
	}
	
	m_DictChannels.clear();
}


void SCController::DeleteCall( int InternalId )
{   

    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp, "Internal Id [%d]", InternalId );

    {
        boost::upgrade_lock<boost::shared_mutex> lock( DictChannelsMutex );
        boost::upgrade_to_unique_lock<boost::shared_mutex>  unique_lock(lock);

        auto ChannelPtr = m_DictChannels.get<ByInternalId>().find( InternalId );
        if ( ChannelPtr != m_DictChannels.get<ByInternalId>().end() )
        {
            boost::shared_ptr<CChannels> channelsPtr = *ChannelPtr;

            SC_LOG4CPP_EX( m_pLog4cpp, "Delete Call : ANI [%s] InternalId [%d]", channelsPtr->CallPtr->GetANI().c_str() % InternalId );
            m_DictChannels.get<ByInternalId>().erase(ChannelPtr);	

        
        }
    }

    PrintDictChannels();
	
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp,"Internal Id [%d]", InternalId );	
}

void SCController::DeleteCall( SCChannel Channel )
{   
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp,"[%02d][%03d]", Channel.Device % Channel.Channel );

    {
        boost::upgrade_lock<boost::shared_mutex> lock( DictChannelsMutex );
        boost::upgrade_to_unique_lock<boost::shared_mutex>  unique_lock(lock);
        
        auto ChannelPtr = m_DictChannels.get<ByChannelDevice>().find( boost::make_tuple( Channel.Device, Channel.Channel) );        
        if ( ChannelPtr != m_DictChannels.get<ByChannelDevice>().end() )
        {
            SC_LOG4CPP_EX( m_pLog4cpp, " Device : [%02d] Channel : [%03d]" , Channel.Device % Channel.Channel );
            m_DictChannels.get<ByChannelDevice>().erase(ChannelPtr);		

            PrintDictChannels();
        }
    }

    PrintDictChannels();

    SC_LOG4CPP_EXIT_EX(m_pLog4cpp,"[%02d][%03d]", Channel.Device % Channel.Channel );
}

void SCController::OnDefault( SCChannel const& Channel, K3L_EVENT* Event )
{       
    boost::shared_lock<boost::shared_mutex> lk( DictChannelsMutex );

    if ( ! ( (Channel.Device >= 0) && ( Channel.Channel >= 0) ) )
        return;
    
    switch(Event->Code)
    {
    case EV_SIP_DTMF_DETECTED:
        SC_LOG4CPP_EX( m_pLog4cpp, "[%02d][%03d] EV_SIP_DTMF_DETECTED \r\n", Channel.Device % Channel.Channel );        
        break;
    case EV_CHANNEL_FREE:
        SC_LOG4CPP_EX( m_pLog4cpp, "[%02d][%03d] EV_CHANNEL_FREE \r\n", Channel.Device % Channel.Channel );
        
        break;
    case EV_END_OF_STREAM:
        SC_LOG4CPP_EX( m_pLog4cpp, "[%02d][%03d] EV_END_OF_STREAM \r\n", Channel.Device % Channel.Channel );
        
        break;
    case EV_SIP_MEDIA_START:
        SC_LOG4CPP_EX( m_pLog4cpp, "[%02d][%03d] EV_SIP_MEDIA_START \r\n", Channel.Device % Channel.Channel );
        
        break;
    case EV_CONNECT:
        SC_LOG4CPP_EX( m_pLog4cpp, "[%02d][%03d] EV_CONNECT \r\n", Channel.Device % Channel.Channel );
        
        break;
    case EV_DISCONNECT:
        SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d] EV_DISCONNECT \r\n", Channel.Device % Channel.Channel );
        
        break;
    default:
        SC_LOG4CPP_EX( m_pLog4cpp, "[%02d][%03d] Event: [0x%02x]", Channel.Device % Channel.Channel % Event->Code );
        break;

    }

    int iEventThread  = 0;
    auto CChannelsItr = m_DictChannels.get<ByChannelDevice>().find( boost::make_tuple(  Channel.Device, Channel.Channel ) );
    if ( CChannelsItr != m_DictChannels.get<ByChannelDevice>().end() )
    {
        boost::shared_ptr<CChannels> channelsPtr = (*CChannelsItr);        
        boost::shared_ptr<SCCall> callPtr = channelsPtr->CallPtr;
           
        //SC_LOG4CPP_EX( m_pLog4cpp, "Dev [%02d] Chan [%03d] ANI [%s] \r\n", channelsPtr->Device % channelsPtr->Channel % callPtr->GetANI().c_str() );
        iEventThread = channelsPtr->InternalId % ThreadCount;
        SC_LOG4CPP_EX( m_pLog4cpp, "ANI [%s] Add Event [0x%02x] to EventsThread [%d]", callPtr->GetANI().c_str() % Event->Code % iEventThread );
		try
        {
            EventThreads[(channelsPtr->InternalId % ThreadCount)].AddEvent( Event, channelsPtr->InternalId, callPtr );
        }
        catch( std::exception& exc )
        {	
            SC_LOG4CPP_EX( m_pLog4cpp, "Exception while adding an event!! Channel[%02d][%03d] Event(0x%02x): %s\n", channelsPtr->Device % channelsPtr->Channel % Event->Code % exc.what() );                
        }
        catch( ... )
        {
            SC_LOG4CPP_EX( m_pLog4cpp,"Unknown exception while adding an event!! Channel[%02d][%03d] Event(0x%02x).\n", channelsPtr->Device % channelsPtr->Channel % Event->Code );				
        }
	    
    } else {
        SC_LOG4CPP_EX( m_pLog4cpp,"Device [%d] Channel [%d] NOT FOUND at DictChannels.", Channel.Device % Channel.Channel );
    }

    
}

stt_code SCController::SendCommand( int Device, int Channel, int Command, byte* Params )
{	
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp,"[%02d][%03d][0x%02x]", Device % Channel % Command );
    
    K3L_COMMAND cmd;
    cmd.Cmd = Command;
    cmd.Object = Channel;
    cmd.Params = Params;
	
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp, "[%02d][%03d][0x%02x]", Device % Channel % Command );
    
    return k3lSendCommand( Device, &cmd );
}

shared_ptr<SCCall> SCController::GetCallShared( int InternalId )
{
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp,  "Internal Id [%d]", InternalId );

    shared_ptr<SCCall> myCall;
    {   
        boost::shared_lock<boost::shared_mutex> lk( DictChannelsMutex );
        auto CChannelsItr = m_DictChannels.get<ByInternalId>().find(InternalId);
	if ( CChannelsItr != m_DictChannels.get<ByInternalId>().end() )
	    {
		    myCall = (*CChannelsItr)->CallPtr;
	    }
	}
	
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp,  "Internal Id [%d]", InternalId );
    
    return myCall;
}

shared_ptr<SCCall> SCController::GetCallShared( SCChannel const& Channel )
{   
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp, "[%02d][%03d]", Channel.Device % Channel.Channel );

    shared_ptr<SCCall> myCall;
    {   
        boost::shared_lock<boost::shared_mutex> lk( DictChannelsMutex );
        auto CChannelsItr = m_DictChannels.get<ByChannelDevice>().find( boost::make_tuple(  Channel.Device, Channel.Channel ) );
        if ( CChannelsItr != m_DictChannels.get<ByChannelDevice>().end() )
	    {
		    myCall = (*CChannelsItr)->CallPtr;
	    }
	}
	
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp, "[%02d][%03d]", Channel.Device % Channel.Channel );
    
    return myCall;
}


void SCController::PrintDictChannels()
{
	std::string strAni;
	std::string strDnis;
	int         iDevice;
    int         iChannel;
	std::string strCallId;
	std::string strNumberOfCalls;
	std::stringstream ss;
	
	
	
	boost::shared_lock<boost::shared_mutex> lock( DictChannelsMutex );			
	
	ss << boost::format("%d") % m_DictChannels.size();
	strNumberOfCalls = ss.str();
	
	ss.str(std::string());
	ss << "\r\n";
	ss << "--------------------------------------------------------------------------------------------\r\n";
	ss << "                                    KHOMP  Calls                                            \r\n";
	ss << "--------------------------------------------------------------------------------------------\r\n";
    ss << "                                " << strNumberOfCalls << "  Active Calls   \r\n";
	ss << "--------------------------------------------------------------------------------------------\r\n";
	ss << "         ANI            DNIS           Device        Channel             Call Id            \r\n";
	ss << "--------------------------------------------------------------------------------------------\r\n";
	
	
	BOOST_FOREACH( boost::shared_ptr<CChannels> dictChannelPtr , m_DictChannels )
	{
		if ( dictChannelPtr.get() != nullptr )
		{
			if ( dictChannelPtr->CallPtr != nullptr )
			{
				strAni = dictChannelPtr->CallPtr->GetANI();
				strDnis = dictChannelPtr->CallPtr->GetDNIS();
                iDevice = dictChannelPtr->CallPtr->GetDevice();
				iChannel = dictChannelPtr->CallPtr->GetChannel();								
				strCallId = dictChannelPtr->CallPtr->GetCallId();
				
				ss << boost::format("     %10s     |  %5s  |   %5d    |   %5d    |      %15s       \r\n") % 
					strAni.c_str() % strDnis.c_str() % iDevice % iChannel % strCallId.c_str();			
			}
		}
	}
	
	ss << "--------------------------------------------------------------------------------------------\r\n";
	
	SC_LOG4CPP( m_pLog4cpp, ss.str().c_str());
}



/*
SCCall* SCController::GetCall( int InternalId )
{	    
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp, "Internal Id [%d]", InternalId );
    
	SCCall* myCall = nullptr;
	{   
        boost::shared_lock<boost::shared_mutex> lk( DictChannelsMutex );
		myCall = UnsafeGetCall( InternalId ).get();
	}
	
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp, "Internal Id [%d]", InternalId );    
    return myCall;
}

SCCall* SCController::GetCall( SCChannel const&  Channel )
{	    
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp,"[%02d][%03d]", Channel.Device % Channel.Channel );

    boost::shared_lock<boost::shared_mutex> lk( DictChannelsMutex );

    SCCall * pCall = nullptr;	
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp,"Device [%02d] - Channel [%03d]", Channel.Device % Channel.Channel );		
	
	auto ChannelPtr = m_DictChannels.get<ByChannelDevice>().find( boost::make_tuple( Channel.Device, Channel.Channel ) );
    if ( ChannelPtr != m_DictChannels.get<ByChannelDevice>().end() )
	{        
		pCall = (*ChannelPtr)->CallPtr.get();
	}
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp,"Device [%02d] - Channel [%03d]", Channel.Device % Channel.Channel );		
    
    return pCall;
}
*/

/*
shared_ptr<SCCall> SCController::GetCallShared( int InternalId )
{
	shared_ptr<SCCall> myCall;	
    
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp, "Internal Id [%d]", InternalId );
	{   
        boost::shared_lock<boost::shared_mutex> lk( DictChannelsMutex );
		myCall = UnsafeGetCall( InternalId );
	}
	
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp, "Internal Id [%d]", InternalId );
    
    return myCall;
}
*/

/*
void SCController::UnMapChannel( SCChannel const& Channel )
{	  
    boost::upgrade_lock<boost::shared_mutex> lock( DictChannelsMutex );
    boost::upgrade_to_unique_lock<boost::shared_mutex>  unique_lock(lock);

    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp,"[%02d][%03d]", Channel.Device % Channel.Channel );
    
    auto ChannelPtr = m_DictChannels.get<ByChannelDevice>().find( boost::make_tuple(    Channel.Device, Channel.Channel) );
    if ( ChannelPtr != m_DictChannels.get<ByChannelDevice>().end() )
    {
        SC_LOG4CPP_EX( m_pLog4cpp, " Device : [%02d] / Channel : [%03d] <=== UNMAP \n", Channel.Device % Channel.Channel );
        m_DictChannels.get<ByChannelDevice>().erase(ChannelPtr);		
    }

    SC_LOG4CPP_EXIT_EX(m_pLog4cpp,"[%02d][%03d]", Channel.Device % Channel.Channel );
}
*/

/*
shared_ptr<SCCall> SCController::UnsafeGetCall( int InternalId )
{   
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp,  "Internal Id [%d]", InternalId );

    shared_ptr<SCCall> callPtr;

    auto CChannelsItr = m_DictChannels.get<ByInternalId>().find(InternalId);
	if ( CChannelsItr != m_DictChannels.get<ByInternalId>().end() )
	{
        callPtr = (*CChannelsItr)->CallPtr;
    }   
    
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp,  "Internal Id [%d]", InternalId );
    
    return callPtr;    
}
*/
