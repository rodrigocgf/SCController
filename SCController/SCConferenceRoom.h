#pragma once

#include "SCStdIncludes.h"
#include "SCLogger.h"
#include "KChannel.h"
#include "SCTypes.h"
#include "ConferenceCommands.h"


class ConferenceRoom
{
private:

    std::map< std::string, KChannel >                           Users;
    std::map< std::string, SCConference::ConfereeStatus >       MuteUsers;
    std::map< std::string, KChannel >                           Listeners;

    boost::shared_mutex					                        m_Mutex;	
    RoomContainer                                               m_Container;

    std::vector< std::string >                                  Supervisors;
    int RoomSize;
    std::string                             m_RoomId;
    
    log4cpp::Category * 					m_pLog4cpp;

    bool MuteUser_Former( KChannel& UserChannel );
    bool MuteUser( KChannel& UserChannel );

    bool UnmuteUser_Former( KChannel& UserChannel );
    bool UnmuteUser( KChannel& UserChannel );

    bool DeafenUser_Former( KChannel& User );
    bool DeafenUser( KChannel& User );

    bool UndeafenUser_Former( KChannel& User );
    bool UndeafenUser( KChannel& User );

public:
    ConferenceRoom( const std::string & roomId , int Size );
    ~ConferenceRoom();

    void ClearUsers();
    std::map< std::string, KChannel > GetUsers() { return Users; }
    std::map< std::string, KChannel > GetListeners() { return Listeners; }

    bool AddUser_Former( char const* UserId, int UserDevice, int UserChannel, bool IsSupervisor, bool Beep );
    bool AddUser_Try1( char const* UserId, int UserDevice, int UserChannel, bool IsSupervisor, bool Beep );
    bool AddUser( char const* UserId, int UserDevice, int UserChannel, bool IsSupervisor, bool Beep );

    bool RemoveUser_Former( char const* UserId );
    bool RemoveUser( char const* UserId );
    std::vector< std::string >::iterator IsSupervisor( char const* UserId );
    int  GetNumberOfUsers() const { return (int) Users.size(); }

    bool Listen( char const* UserId );
    bool Listen_Former( char const* UserId );

    bool Unlisten( char const* UserId );
    bool Unlisten_Former( char const* UserId );

    //bool AddListener( char const* UserId, int KhompDevice, int KhompChannel );
    //bool RemoveListener( char const* UserId );

    bool ChangeStatus( char const* UserId, SCConference::ConfereeStatus Status );
    bool ChangeStatus_Former( char const* UserId, SCConference::ConfereeStatus Status );

    void PrintRoom();
};

