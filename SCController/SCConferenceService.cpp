
#include "SCCall.h"
#include "SCConferenceService.h"
#include "SCConfigReader.h"



#define MEM_FN1(x,y) boost::bind(&self_type::x, shared_from_this(), y)
#define MEM_FN2(x,y,z) boost::bind(&self_type::x, shared_from_this(), y, z)
#define MEM_FN3(w,x,y,z) boost::bind(&self_type::w, shared_from_this(), x, y, z)


SCConferenceService::SCConferenceService( io_service& Service , ISCController * p_controller ) :   
    ReadSocket( Service ), 
    Started( false ), 
    Timer( Service ) ,
    m_pController(p_controller)
                                                                    
{
    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("ConferenceService") ); 
        
    SC_LOG4CPP( m_pLog4cpp,"<-v-v-v-v-v-v-v-v-v-v-v- CONFERENCE SERVICE -v-v-v-v-v-v-v-v-v-v-v-v->");
}

SCConferenceService::~SCConferenceService(void)
{   
    SC_LOG4CPP( m_pLog4cpp,"<-^-^-^-^-^-^-^-^-^-^-^- CONFERENCE SERVICE -^-^-^-^-^-^-^-^-^-^-^-^-> ");
    
}

ip::tcp::socket& SCConferenceService::GetSocket() 
{ 
    return ReadSocket; 
}

void SCConferenceService::Start( )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    
    Started = true;
    DoRead();
    
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}


void SCConferenceService::Stop()
{    
    SC_LOG4CPP_ENTRY( m_pLog4cpp );

    m_pController->GetConferenceManager()->ClearRooms();

    if( Started )
    {
        Started = false;
        ReadSocket.close();
    }
        
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::DoRead()
{    
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    async_read_until( ReadSocket, ReadBuffer, "\n", MEM_FN2( OnRead, _1, _2 ) );
    SC_LOG4CPP_EXIT( m_pLog4cpp );    
}

void SCConferenceService::DoWrite( std::string const& Message )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    std::vector<std::string> messages;
    messages.push_back( Message );
    DoWrite( messages );
    SC_LOG4CPP_EXIT( m_pLog4cpp );    
}

void SCConferenceService::DoWrite( std::vector<std::string> const& Messages )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    if( Started )
    {
        std::ostream out( &WriteBuffer );
        std::string message;
        for( auto msgIt = Messages.begin(), msgEnd = Messages.end(); msgIt != msgEnd; ++msgIt )
             message += *msgIt + "\n";
       
        out << message.length() << "\n"; 
        out << Messages.size() << "\n";
        out << message << "\n";
       
        async_write( ReadSocket, WriteBuffer, MEM_FN2( OnWrite, _1, _2 ) );
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );    
}

void SCConferenceService::OnWrite( error_code const& Error, size_t bytes )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    WriteBuffer.consume( WriteBuffer.size() );
    DoRead();
    SC_LOG4CPP_EXIT( m_pLog4cpp );    
}

void SCConferenceService::ParseInput( std::vector< std::string >& ParseResult, std::string& ParamsStr, char Separator )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    size_t position;
    while( (position = ParamsStr.find( Separator )) != std::string::npos )
    {
        if( position > 0 )
        {
            ParseResult.push_back( ParamsStr.substr( 0, position ) );
            ParamsStr.erase( 0, position + 1 );
        }
        else
            ParamsStr.erase( 0 );
    }
    if( ParamsStr.length() > 0 )
        ParseResult.push_back( ParamsStr );
    SC_LOG4CPP_EXIT( m_pLog4cpp );    
}

void SCConferenceService::OnRead( error_code const& Error, size_t Bytes )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    if( Error )
    {        
        //SC_LOG4CPP_EX( m_pLog4cpp,"ERROR: OnRead => %s", Error.message().c_str() );
    }

    if( Started )
    {
        // Read the data from the read buffer
        streambuf::const_buffers_type bufs = ReadBuffer.data();
        std::string input( buffers_begin( bufs ), buffers_begin( bufs ) + ReadBuffer.size() );

        ReadBuffer.consume( ReadBuffer.size() );

        //break the input using \n as a separator
        std::vector< std::string > parsedInput;
        try
        {
            ParseInput( parsedInput, input, '\n' );

            if( parsedInput.size() == 0 )
                return;

            parsedInput.erase( parsedInput.begin() );

            std::string action( parsedInput[0] );
            ToUpper( action );
        
            SC_LOG4CPP( m_pLog4cpp,"---------------------------------------------------------------------");
			SC_LOG4CPP_EX( m_pLog4cpp,"ACTION => %s", action.c_str() );			
			SC_LOG4CPP( m_pLog4cpp,"---------------------------------------------------------------------");            
            

            if( action == "CRIA_SALA" )
            {
                OnCriaSala( parsedInput );
            }
            else if( action == "DESTROI_SALA" )
            {
                OnDestroiSala( parsedInput );
            }
            else if( action == "BLOQUEIA_FALA" )
            {
                OnBloqueiaFala( parsedInput );
            }
            else if( action == "DESBLOQUEIA_FALA" )
            {
                OnDesbloqueiaFala( parsedInput );
            }
            else if( action == "EXPULSA_USUARIO" )
            {
                OnExpulsaUsuario( parsedInput );
            }
            else if( action == "MONITORA_USUARIO" )
            {
                OnMonitoraUsuario( parsedInput );
            }
            else if( action == "TERMINA_MONITORACAO_USUARIO" )
            {
                OnTerminaMonitoracaoUsuario( parsedInput );
            }
            else if( action == "MONITOR_CONVERSA_USUARIO" )
            {
                OnMonitorConversaUsuario( parsedInput );
            }
            else if( action == "MONITOR_TERMINA_CONVERSA" )
            {
                OnMonitorTerminaConversa( parsedInput );
            }
            else if( action == "USUARIO_CONVERSA_PARTICIPANTE" )
            {
                OnUsuarioConversaParticipante( parsedInput );
            }
            else if( action == "TERMINA_CONVERSA" )
            {
                OnTerminaConversa( parsedInput );
            }
            else if( action == "MONITORA_SALA" )
            {
                OnMonitoraSala( parsedInput );
            }
            else if( action == "TERMINA_MONITORACAO_SALA" )
            {
                OnTerminaMonitoracaoSala( parsedInput );
            }
            else if( action == "REMOVE_PARTICIPANTE_SALA" )
            {
                OnRemoveParticipanteSala( parsedInput );
            }
            else if( action == "HABILITA_CONVERSA_PERSONALIDADE" )
            {
                OnHabilitaConversaPersonalidade( parsedInput );
            }
            else if( action == "DESABILITA_CONVERSA_PERSONALIDADE" )
            {
                OnDesabilitaConversaPersonalidade( parsedInput );
            }
            else if( action == "ENTRA_ATENDIMENTO" )
            {
                OnEntraAtendimento( parsedInput );
            }
            else if( action == "SAI_ATENDIMENTO" )
            {
                OnSaiAtendimento( parsedInput );
            }
            else if( action == "PING" )
            {
                OnPing();
            }
            else
            {                
                SC_LOG4CPP_EX( m_pLog4cpp,"INVALID ACTION: %s", action );
                DoWrite( "ERRO" );
            }
        }
        catch( ... )
        {            
            SC_LOG4CPP( m_pLog4cpp,"EXCEPTION WHILE HANDLING MESSAGE!!" );
            DoWrite( "ERRO" );
        }
    }

    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnCriaSala( std::vector<std::string> const& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;
   
    PrintParams("OnCriaSala", Params);

    if( Params.size() == 4 )
    {
        std::string roomId = Params[1];
        int roomSize = atoi( Params[2].c_str() );
        int beep = atoi( Params[3].c_str() );
                
        //if ( m_pController->GetConferenceManager()->CreateRoom( roomId.c_str() ,roomId.c_str(), roomSize , false ) )
        if ( m_pController->GetConferenceManager()->CreateRoom( roomId.c_str(), roomSize ) )
            cmdReturn = 1;

        SC_LOG4CPP_EX( m_pLog4cpp,"CRIA_SALA: CreateRoom( %s, %d )[%d]", roomId.c_str() % roomSize % cmdReturn );        
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );

        SC_LOG4CPP_EX( m_pLog4cpp,"CRIA_SALA: INVALID PARAM COUNT: received %d, expected 4 (%s)", Params.size() % paramsStr.c_str() );
        
    }
    
    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }

    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnDestroiSala( std::vector<std::string> const& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;

    PrintParams("OnDestroiSala", Params);

    if( Params.size() == 2 )
    {
        std::string roomId = Params[1];        
        m_pController->GetConferenceManager()->DestroyRoom( roomId.c_str() );
        
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );

        SC_LOG4CPP_EX( m_pLog4cpp,"DESTROI_SALA: INVALID PARAM COUNT: received %d, expected 2 (%s)", Params.size() % paramsStr.c_str() );        
    }

    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnBloqueiaFala( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;

    PrintParams("OnBloqueiaFala", Params);

    if( Params.size() == 3 )
    {
        std::vector<std::string> subParams;
        ParseInput( subParams, Params[1], ':' );
        if( subParams.size() == 4 )
        {
            std::string RoomId = Params[2];
            int userChannel = atoi( subParams[1].c_str() );
            //shared_ptr<SCCall> userCall =  SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
            
            if( userCall != NULL )
            {
                cmdReturn = userCall->ConferenceChangeStatus( RoomId.c_str(), SCConference::MONITOR );                
                SC_LOG4CPP_EX( m_pLog4cpp,"BLOQUEIA_FALA: ConferenceChangeStatus( %s, %d )[%d]", RoomId.c_str() % SCConference::MONITOR % cmdReturn );
            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"BLOQUEIA_FALA: INVALID CHANNEL PASSED: %d", userChannel );                
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"BLOQUEIA_FALA: ERROR PARSING CHANNEL INFO" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
        
        SC_LOG4CPP_EX( m_pLog4cpp,"BLOQUEIA_FALA: INVALID PARAM COUNT: received %d, expected 3 (%s)", Params.size() % paramsStr.c_str() );
    }
    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnDesbloqueiaFala( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;
    if( Params.size() == 3 )
    {
        std::vector<std::string> subParams;
        ParseInput( subParams, Params[1], ':' );
        if( subParams.size() == 4 )
        {
            std::string RoomId = Params[2];
            int userChannel = atoi( subParams[1].c_str() );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
            
            if( userCall != NULL )
            {
                cmdReturn = userCall->ConferenceChangeStatus( RoomId.c_str(), SCConference::DUPLEX );                
                SC_LOG4CPP_EX( m_pLog4cpp,"DESBLOQUEIA_FALA: ConferenceChangeStatus( %s, %d )[%d]", RoomId.c_str() % SCConference::DUPLEX % cmdReturn );
            }
            else
            {                
                SC_LOG4CPP_EX( m_pLog4cpp,"DESBLOQUEIA_FALA: INVALID CHANNEL PASSED: %d", userChannel );
            }
        }
        else
        {            
            SC_LOG4CPP( m_pLog4cpp,"DESBLOQUEIA_FALA: ERROR PARSING CHANNEL INFO" );
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
        
        SC_LOG4CPP_EX( m_pLog4cpp, "DESBLOQUEIA_FALA: INVALID PARAM COUNT: received %d, expected 3 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

/*
Params 

    [0] : expulsa_usuario
    [1] : 1:2:0:99   => Params[1] => subParams
         timeslot : channel : timeslotglobalconferencista : idmaquina

    [2] : 0 => roomId

*/
void SCConferenceService::OnExpulsaUsuario( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;

    PrintParams("OnExpulsaUsuario", Params);

    if( Params.size() == 3 )
    {
        std::vector<std::string> subParams;
        ParseInput( subParams, Params[1], ':' );
        if( subParams.size() == 4 )
        {
            int userChannel = atoi( subParams[1].c_str() );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1 ) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
            
            if( userCall != NULL )
            {
                std::string idSala = Params[2] + ":" + subParams[3];
                cmdReturn = userCall->ConferenceRemove( idSala.c_str(), 0 );
                
                if( cmdReturn == 0 )
                    userCall->Hangup();
            }
            else
            {                
                SC_LOG4CPP_EX( m_pLog4cpp,"EXPULSA_USUARIO: INVALID CHANNEL PASSED: %d", userChannel );
            }
        }
        else
        {            
            SC_LOG4CPP( m_pLog4cpp,"EXPULSA_USUARIO: ERROR PARSING CHANNEL INFO" );
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );

        
        SC_LOG4CPP_EX( m_pLog4cpp,"EXPULSA_USUARIO: INVALID PARAM COUNT: received %d, expected 3 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnMonitoraUsuario( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;

    PrintParams("OnMonitoraUsuario", Params);

    if( Params.size() == 5 )
    {
        std::vector<std::string> monitorParams, userParams;
        ParseInput( monitorParams, Params[1], ':' );
        ParseInput( userParams, Params[2], ':' );
        if( monitorParams.size() == 4 && userParams.size() == 4 )
        {
            int monitorChannel = atoi( monitorParams[1].c_str() );
            //shared_ptr<SCCall> monitorCall = SCController::GetInstance().GetCallShared( SCChannel( 0, monitorChannel - 1) );
            boost::shared_ptr<SCCall> monitorCall = m_pController->GetCallShared( SCChannel( 0, monitorChannel - 1) );

            int userChannel = atoi( userParams[1].c_str() );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
            
            

            if( monitorCall != NULL && userCall != NULL )
            {
                bool error = false;
                int removeMonitorFromRoom = atoi( Params[4].c_str() );
                if( removeMonitorFromRoom )
                {
                    cmdReturn = monitorCall->ConferenceRemove( Params[3].c_str(), 0 );                    
                    SC_LOG4CPP_EX( m_pLog4cpp,"MONITORA_USUARIO: ConferenceRemove( %s )[%d]", Params[3].c_str() % cmdReturn );
                    error = ( cmdReturn != 1 );
                }

                if( !error )
                {
                    cmdReturn = monitorCall->ListenToChannel( userCall->GetKhompDevice(), userCall->GetKhompChannel() );                    
                    SC_LOG4CPP_EX( m_pLog4cpp,"MONITORA_USUARIO: ListenToChannel( %d, %d )[%d]", userCall->GetKhompDevice() % userCall->GetKhompChannel() % cmdReturn );
                }
            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"MONITORA_USUARIO: INVALID CHANNEL PASSED: monitor(%d) user(%d)", monitorChannel % userChannel );                
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp, "MONITORA_USUARIO: ERROR PARSING CHANNEL INFO" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
                
        SC_LOG4CPP_EX( m_pLog4cpp,"MONITORA_USUARIO: INVALID PARAM COUNT: received %d, expected 5 (%s)", Params.size() % paramsStr.c_str() );
    }
    
    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnTerminaMonitoracaoUsuario( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;

    PrintParams("OnTerminaMonitoracaoUsuario", Params);

    if( Params.size() == 6 )
    {
        std::vector<std::string> monitorParams, userParams;

        ParseInput( monitorParams, Params[1], ':' );
        ParseInput( userParams, Params[4], ':' );

        if( monitorParams.size() == 4 && userParams.size() == 4 )
        {
            int monitorChannel = atoi( monitorParams[1].c_str() );
            //shared_ptr<SCCall> monitorCall = SCController::GetInstance().GetCallShared( SCChannel( 0, monitorChannel - 1) );
            boost::shared_ptr<SCCall> monitorCall = m_pController->GetCallShared( SCChannel( 0, monitorChannel - 1) );
            

            int userChannel = atoi( userParams[1].c_str() );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1 ) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
            
            

            if( monitorCall != NULL && userCall != NULL )
            {
                cmdReturn = monitorCall->UnlistenChannel( userCall->GetKhompDevice(), userCall->GetKhompChannel() );                
                SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_MONITORACAO_USUARIO: UnlistenChannel( %d, %d )[%d]", userCall->GetKhompDevice() % userCall->GetKhompChannel() % cmdReturn );

                if( cmdReturn == 1 )
                {
                    int conferenceAdd = atoi( Params[5].c_str() );
                    if( conferenceAdd )
                    {
                        std::string& roomId = Params[2];
                        int roomSize = atoi( Params[3].c_str() );

                        cmdReturn = monitorCall->ConferenceAdd( roomId.c_str(), roomSize, monitorCall->GetANI().c_str(), 0, true, false );
                        
                        SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_MONITORACAO_USUARIO: ConferenceAdd( %s, %d, %s, 0, true, false )[%d]", 
                            roomId.c_str() % roomSize % monitorCall->GetANI().c_str() % cmdReturn );
                    }
                }
            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_MONITORACAO_USUARIO: INVALID CHANNEL: monitor( %d ) user( %d )", monitorChannel % userChannel );                
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"TERMINA_MONITORACAO_USUARIO: ERROR PARSING CHANNEL INFO" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
        
        SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_MONITORACAO_USUARIO: INVALID PARAM COUNT: received %d, expected 6 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

/*
ACTION : OnMonitorConversaUsuario

[0] : monitor_conversa_usuario

[1] : 1:1:0:99 => monitorParams => monitorChannel = 1

[2] : 3_27:99 => roomID

[3] : CP05051709545615:99 => roomIDPrivate

[4] : 0

*/

void SCConferenceService::OnMonitorConversaUsuario( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;
    
    PrintParams("OnMonitorConversaUsuario", Params);

    if( Params.size() == 5 )
    {
        std::vector<std::string> monitorParams;
        ParseInput( monitorParams, Params[1], ':' );
        if( monitorParams.size() == 4 )
        {
            int monitorChannel = atoi( monitorParams[1].c_str() );
            std::string& roomID = Params[2];
            std::string& roomIDPrivate = Params[3];
            int confereeType = atoi( Params[4].c_str() );
            int conferenceSize = 2;

            //shared_ptr<SCCall> monitorCall = SCController::GetInstance().GetCallShared( SCChannel( 0, monitorChannel - 1) );
            boost::shared_ptr<SCCall> monitorCall = m_pController->GetCallShared( SCChannel( 0, monitorChannel - 1) );
            
            if( monitorCall )
            {
                cmdReturn = monitorCall->ConferenceRemove( roomID.c_str(), confereeType );
                
                SC_LOG4CPP_EX( m_pLog4cpp,"MONITORA_CONVERSA_USUARIO: ConferenceRemove( %s, %d )[%d]", roomID.c_str() % confereeType % cmdReturn );
                if( cmdReturn == 1 )
                {
                    
                    cmdReturn = monitorCall->ConferenceAdd( roomIDPrivate.c_str(), conferenceSize, monitorCall->GetANI().c_str(), SCConference::DUPLEX, true, false ); 
                    
                    SC_LOG4CPP_EX( m_pLog4cpp,"MONITORA_CONVERSA_USUARIO: ConferenceAdd( %s, %d, %s, DUPLEX, true, false )[%d]", 
                        roomIDPrivate.c_str() % conferenceSize % monitorCall->GetANI().c_str() % cmdReturn );
                    
                }
            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"MONITORA_CONVERSA_USUARIO: INVALID CHANNEL ( %d )", monitorChannel );                
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"MONITORA_CONVERSA_USUARIO: ERROR PARSING CHANNEL INFO");            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );        
        SC_LOG4CPP_EX( m_pLog4cpp,"MONITOR_CONVERSA_USUARIO: INVALID PARAM COUNT: received %d, expected 5 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnMonitorTerminaConversa( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;

    PrintParams("OnMonitorTerminaConversa", Params);

    if( Params.size() == 6 )
    {
        std::vector<std::string> monitorParams;
        ParseInput( monitorParams, Params[1], ':' );

        if( monitorParams.size() == 4 )
        {
            int monitorChannel = atoi( monitorParams[1].c_str() );
            
            //shared_ptr<SCCall> monitorCall = SCController::GetInstance().GetCallShared( SCChannel( 0, monitorChannel - 1) );
            boost::shared_ptr<SCCall> monitorCall = m_pController->GetCallShared( SCChannel( 0, monitorChannel - 1) );
            
            if( monitorCall != NULL )
            {
                std::string& roomID = Params[2];
                int conferenceSize = atoi( Params[3].c_str() );
                std::string& roomIDPrivate = Params[4];
                int confereeType = atoi( Params[5].c_str() );
                
                cmdReturn = monitorCall->ConferenceRemove( roomIDPrivate.c_str(), SCConference::DUPLEX );
                
                SC_LOG4CPP_EX( m_pLog4cpp,"MONITOR_TERMINA_CONVERSA: ConferenceRemove( %s, DUPLEX )[%d]", roomIDPrivate.c_str() % cmdReturn );

                if( cmdReturn == 1 )
                {
                    cmdReturn = monitorCall->ConferenceAdd( roomID.c_str(), conferenceSize, monitorCall->GetANI().c_str(), confereeType, true, false );                    
                    SC_LOG4CPP_EX( m_pLog4cpp,"MONITOR_TERMINA_CONVERSA: ConferenceAdd( %s, %d, %s, %d, true, false )[%d]", 
                        roomID.c_str() % conferenceSize % monitorCall->GetANI().c_str() % confereeType % cmdReturn );
                }
            }
            else
            {                
                SC_LOG4CPP_EX( m_pLog4cpp,"MONITOR_TERMINA_CONVERSA: INVALID CHANNEL ( %d )", monitorChannel );
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"MONITOR_TERMINA_CONVERSA: ERROR PARSING CHANNEL INFO" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );        

        SC_LOG4CPP_EX( m_pLog4cpp,"MONITOR_TERMINA_CONVERSA: INVALID PARAM COUNT: received %d, expected 6 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

/*

ACTION : OnUsuarioConversaParticipante

[0] : usuario_conversa_participante

[1] : 1:2:0:99 => Params[1] => userParams ; userChannel = userParams[1] = 2

[2] : 3_29:99

[3] : CP05051712232235:99

*/
void SCConferenceService::OnUsuarioConversaParticipante( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;
    std::vector<std::string> replyMessages;
    int roomSize = 0;
    std::string roomID;
    std::string roomIDPrivate;

    PrintParams("OnUsuarioConversaParticipante", Params);

    if( Params.size() == 4 )
    {
        std::vector<std::string> userParams;
        ParseInput( userParams, Params[1], ':' );
        
        if( userParams.size() == 4 )
        {
            int userChannel = atoi( userParams[1].c_str() );

            SC_LOG4CPP_EX( m_pLog4cpp,"Device [0] / Channel [%d] .", (userChannel - 1) );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1 ) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, (userChannel - 1) ) );
            
            if( userCall != NULL )
            {
                roomID = Params[2];
                cmdReturn = userCall->ConferenceRemove( roomID.c_str(), SCConference::DUPLEX );
                
                SC_LOG4CPP_EX( m_pLog4cpp,"USUARIO_CONVERSA_PARTICIPANTE: ConferenceRemove( %s, DUPLEX )[%d]", roomID.c_str() % cmdReturn );

                if( cmdReturn == 1 )
                {
                    roomIDPrivate = Params[3];
                    roomSize = 2;

                    replyMessages.push_back( "OK" );
                    cmdReturn = userCall->ConferenceAdd( roomIDPrivate.c_str(), roomSize, userCall->GetANI().c_str(), SCConference::DUPLEX, false, false );

                    SC_LOG4CPP_EX( m_pLog4cpp,"USUARIO_CONVERSA_PARTICIPANTE: ConferenceAdd( %s, %d, %s, DUPLEX, false, false )[%d]", 
                        roomIDPrivate.c_str() % roomSize % userCall->GetANI().c_str() % cmdReturn );
                }
            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"USUARIO_CONVERSA_PARTICIPANTE: INVALID CHANNEL ( %d )", userChannel );                
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"USUARIO_CONVERSA_PARTICIPANTE: ERROR PARSING CHANNEL INFO" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
        
        SC_LOG4CPP_EX( m_pLog4cpp,"USUARIO_CONVERSA_PARTICIPANTE: INVALID PARAM COUNT: received %d, expected 4 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: replyMessages.push_back( "TIMEOUT" ); break;
    case 0:  replyMessages.push_back( "ERRO"    ); break;
    case 1:  replyMessages.push_back( "OK"      ); break;
    default: break;
    }
    DoWrite( replyMessages );
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnTerminaConversa( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;
    std::vector<std::string> replyMessages;

    PrintParams("OnTerminaConversa", Params);

    if( Params.size() == 5 )
    {
        std::vector<std::string> userParams;
        ParseInput( userParams, Params[1], ':' );

        if( userParams.size() == 4 )
        {
            int userChannel = atoi( userParams[1].c_str() );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
            
            if( userCall != NULL )
            {
                std::string& roomID = Params[2];
                int roomSize = atoi( Params[3].c_str() );
                std::string& roomIDPrivate = Params[4];
                
                cmdReturn = userCall->ConferenceRemove( roomIDPrivate.c_str(), SCConference::DUPLEX );                
                SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_CONVERSA: ConferenceRemove( %s, DUPLEX )[%d]", roomIDPrivate.c_str() % cmdReturn );

                if( cmdReturn == 1 )
                {
                    replyMessages.push_back( "OK" );
                    cmdReturn = userCall->ConferenceAdd( roomID.c_str(), roomSize, userCall->GetANI().c_str(), SCConference::DUPLEX, false, false );
                    
                    SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_CONVERSA: ConferenceAdd( %s, %d, %s, DUPLEX, false, false )[%s]", 
                        roomID.c_str() % roomSize % userCall->GetANI().c_str() % cmdReturn );
                }
            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_CONVERSA: INVALID CHANNEL ( %d )", userChannel );                
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"TERMINA_CONVERSA: ERROR_PARSING CHANNEL INFO" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );        
        
        SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_CONVERSA: INVALID PARAM COUNT: received %d, expected 5 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: replyMessages.push_back( "TIMEOUT" ); break;
    case 0:  replyMessages.push_back( "ERRO"    ); break;
    case 1:  replyMessages.push_back( "OK"      ); break;
    default: break;
    }
    DoWrite( replyMessages );
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnMonitoraSala( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;

    PrintParams("OnMonitoraSala", Params);

    if( Params.size() == 5 )
    {
        std::vector<std::string> monitorParams;
        ParseInput( monitorParams, Params[1], ':' );

        if( monitorParams.size() == 4 )
        {
            int monitorChannel = atoi( monitorParams[1].c_str() );
            SC_LOG4CPP_EX( m_pLog4cpp,"Device [0] / Channel [%d] to be monitored.", (monitorChannel - 1) );

            //shared_ptr<SCCall> monitorCall = SCController::GetInstance().GetCallShared( SCChannel( 0, monitorChannel - 1 ) );
            boost::shared_ptr<SCCall> monitorCall = m_pController->GetCallShared( SCChannel( 0, (monitorChannel - 1) ) );
            
            if( monitorCall != NULL )
            {
                SCConference::ConfereeStatus confereeT = static_cast<SCConference::ConfereeStatus>(atoi( Params[2].c_str() ));
                int roomSize = atoi( Params[3].c_str() );
                std::string& roomID = Params[4];

                cmdReturn = monitorCall->ConferenceAdd( roomID.c_str(), roomSize, monitorCall->GetANI().c_str(), confereeT, true, false );
                
                SC_LOG4CPP_EX( m_pLog4cpp,"MONITORA_SALA: ConferenceAdd( %s, %d, %s, %d, true, false )[%d]", 
                    roomID.c_str() % roomSize % monitorCall->GetANI().c_str() % confereeT %  cmdReturn );
            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"MONITORA_SALA: INVALID CHANNEL( %d )", monitorChannel );                
            }
        }
        else
        {            
            SC_LOG4CPP( m_pLog4cpp,"MONITORA_SALA: ERROR_PARSING CHANNEL INFO" );
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );        
        SC_LOG4CPP_EX( m_pLog4cpp,"MONITORA_SALA: INVALID PARAM COUNT: received %d, expected 5 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }

    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnTerminaMonitoracaoSala( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;

    PrintParams("OnTerminaMonitoracaoSala", Params);

    if( Params.size() == 4 )
    {
        std::vector<std::string> monitorParams;
        ParseInput( monitorParams, Params[1], ':' );

        if( monitorParams.size() == 4 )
        {
            int monitorChannel = atoi( monitorParams[1].c_str() );
            //shared_ptr<SCCall> monitorCall = SCController::GetInstance().GetCallShared( SCChannel( 0, monitorChannel - 1) );
            boost::shared_ptr<SCCall> monitorCall = m_pController->GetCallShared( SCChannel( 0, monitorChannel - 1) );
                        
            if( monitorCall != NULL )
            {
                std::string& roomID = Params[2];

                cmdReturn = monitorCall->ConferenceRemove( roomID.c_str(), SCConference::DUPLEX );
                
                SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_MONITORACAO_SALA: ConferenceRemove( %s, DUPLEX )[%d]", roomID.c_str() % cmdReturn );

            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_MONITORACAO_SALA: INVALID CHANNEL ( %d )", monitorChannel );                
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"TERMINA_MONITORACAO_SALA: ERROR PARSING CHANNEL INFO" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
        
        SC_LOG4CPP_EX( m_pLog4cpp,"TERMINA_MONITORACAO_SALA: INVALID PARAM COUNT: received %d, expected 4 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnRemoveParticipanteSala( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;

    PrintParams("OnRemoveParticipanteSala", Params);

    if( Params.size() == 4 )
    {
        std::vector<std::string> userParams;
        ParseInput( userParams, Params[1], ':' );

        if( userParams.size() == 4 )
        {
            int userChannel = atoi( userParams[1].c_str() );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1 ) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
                        
            if( userCall != NULL )
            {
                std::string& roomId = Params[2];

                cmdReturn = userCall->ConferenceRemove( roomId.c_str(), SCConference::DUPLEX );
                
                SC_LOG4CPP_EX( m_pLog4cpp,"REMOVE_PARTICIPANTE_SALA: ConferenceRemove( %s, DUPLEX )[%d]" ,roomId.c_str() % cmdReturn );
            }
            else
            {
                
                SC_LOG4CPP_EX( m_pLog4cpp,"REMOVE_PARTICIPANTE_SALA: INVALID CHANNEL( %d )", userChannel );
            }
        }
        else
        {     
            SC_LOG4CPP( m_pLog4cpp,"REMOVE_PARTICIPANTE_SALA: ERROR PARSING CHANNEL INFO" );
            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
                
        SC_LOG4CPP_EX( m_pLog4cpp,"REMOVE_PARTICIPANTE_SALA: INVALID PARAM COUNT: received %d, expected 4 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: DoWrite( "TIMEOUT" ); break;
    case 0:  DoWrite( "ERRO"    ); break;
    case 1:  DoWrite( "OK"      ); break;
    default: break;
    }
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnHabilitaConversaPersonalidade( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;
    std::vector<std::string> replyMessages;

    PrintParams("OnHabilitaConversaPersonalidade", Params);

    if( Params.size() == 4 )
    {
        std::vector<std::string> subParams;
        ParseInput( subParams, Params[1], ':' );
        
        if( subParams.size() != 4 )
        {
            int userChannel = atoi( subParams[1].c_str() );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1 ) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
            
            if( userCall != NULL )
            {
                std::string& roomId = Params[2];
                std::string& roomIdPrivate = Params[3];

                cmdReturn = userCall->ConferenceRemove( roomId.c_str(), SCConference::DUPLEX );
                
                SC_LOG4CPP_EX( m_pLog4cpp,"HABILITA_CONVERSA_PERSONALIDADE: %04d->ConferenceRemove( %s, DUPLEX )[%d]", userChannel % roomId.c_str() % cmdReturn );
                if( cmdReturn == 1 )
                {
                    replyMessages.push_back( "OK" );
                    cmdReturn = userCall->ConferenceAdd( roomIdPrivate.c_str(), 2, userCall->GetANI().c_str(), SCConference::DUPLEX, false, false );
                                        
                    SC_LOG4CPP_EX( m_pLog4cpp,"HABILITA_CONVERSA_PERSONALIDADE: %04d->ConferenceAdd( %s, 2, %s, DUPLEX, false, false )[%d]", userChannel % 
                        roomIdPrivate.c_str() % userCall->GetANI().c_str() % cmdReturn );
                }
            }
            else
            {                
                SC_LOG4CPP_EX( m_pLog4cpp,"HABILITA_CONVERSA_PERSONALIDADE: INVALID CHANNEL ( %d )", userChannel );
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"HABILITA_CONVERSA_PERSONALIDADE: ERRO PARSING CHANNEL INFO\n" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
                
        SC_LOG4CPP_EX( m_pLog4cpp,"HABILITA_CONVERSA_PERSONALIDADE: INVALID PARAM COUNT: received %d, expected 4 (%s)", Params.size() % paramsStr.c_str() );
    }
        

    switch( cmdReturn )
    {
    case -1: replyMessages.push_back( "TIMEOUT" ); break;
    case 0:  replyMessages.push_back( "ERRO"    ); break;
    case 1:  replyMessages.push_back( "OK"      ); break;
    default: break;
    }
    DoWrite( replyMessages );
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnDesabilitaConversaPersonalidade( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;
    std::vector<std::string> replyMessages;

    PrintParams("OnDesabilitaConversaPersonalidade", Params);

    if( Params.size() == 3 )
    {
        std::vector<std::string> subParams;
        ParseInput( subParams, Params[1], ':' );
        if( subParams.size() == 4 )
        {
            int userChannel = atoi( subParams[1].c_str() );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1 ) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
            
            if( userCall != NULL )
            {
                std::string& roomId = Params[2];

                cmdReturn = userCall->ConferenceRemove( roomId.c_str(), SCConference::DUPLEX );                
                SC_LOG4CPP_EX( m_pLog4cpp,"DESABILITA_CONVERSA_PERSONALIDADE: %04d->ConferenceRemove( %s, DUPLEX )[%d]", userChannel %
                    roomId.c_str() % cmdReturn );

                if( cmdReturn == 1 )
                {
                    replyMessages.push_back( "OK" );
                    cmdReturn = userCall->ConferenceListen( roomId.c_str() );
                    
                    SC_LOG4CPP_EX( m_pLog4cpp, "DESABILITA_CONVERSA_PERSONALIDADE: %04d->ConferenceListen( %s )[%d]", roomId.c_str() % cmdReturn );
                }
            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"DESABILITA_CONVERSA_PERSONALIDADE: INVALID CHANNEL ( %d )", userChannel );                
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"DESABILITA_CONVERSA_PERSONALIDADE: ERROR PARSING CHANNEL INFO" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
                
        SC_LOG4CPP_EX( m_pLog4cpp,"DESABILITA_CONVERSA_PERSONALIDADE: INVALID PARAM COUNT: received %d, expected 3 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: replyMessages.push_back( "TIMEOUT" ); break;
    case 0:  replyMessages.push_back( "ERRO"    ); break;
    case 1:  replyMessages.push_back( "OK"      ); break;
    default: break;
    }
    DoWrite( replyMessages );
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnEntraAtendimento( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;
    std::vector<std::string> replyMessages;

    PrintParams("OnEntraAtendimento", Params);

    if( Params.size() == 4 )
    {
        std::vector<std::string> subParams;
        ParseInput( subParams, Params[1], ':' );
        if( subParams.size() == 4 )
        {
            int userChannel = atoi( subParams[1].c_str() );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
            
            if( userCall != NULL )
            {
                std::string& roomID = Params[2];
                std::string& roomIDPrivate = Params[3];

                cmdReturn = userCall->ConferenceRemove( roomID.c_str(), SCConference::DUPLEX );
                
                SC_LOG4CPP_EX( m_pLog4cpp,"ENTRA_ATENDIMENTO: %04d->ConferenceRemove( %s, DUPLEX )[%d]", userChannel % roomID.c_str() % cmdReturn );
                
                if( cmdReturn == 1 )
                {
                    replyMessages.push_back( "OK" );
                    cmdReturn = userCall->ConferenceAdd( roomIDPrivate.c_str(), 2, userCall->GetANI().c_str(), SCConference::DUPLEX, false, false );
                                        
                    SC_LOG4CPP_EX( m_pLog4cpp,"ENTRA_ATENDIMENTO: %04d->ConferenceAdd( %s, 2, %s, DUPLEX, false, false )[%d]", userChannel %
                        roomIDPrivate.c_str() % userCall->GetANI().c_str() % cmdReturn );
                }
            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"ENTRA_ATENDIMENTO: INVALID CHANNEL ( %d )", userChannel );                
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"ENTRA_ATENDIMENTO: ERROR PARSING CHANNEL INFO" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
                
        SC_LOG4CPP_EX( m_pLog4cpp,"ENTRA_ATENDIMENTO: INVALID PARAM COUNT: received %d, expected 4 (%s)", Params.size() % paramsStr.c_str() );
    }

    switch( cmdReturn )
    {
    case -1: replyMessages.push_back( "TIMEOUT" ); break;
    case 0:  replyMessages.push_back( "ERRO"    ); break;
    case 1:  replyMessages.push_back( "OK"      ); break;
    default: break;
    }
    DoWrite( replyMessages );
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnSaiAtendimento( std::vector<std::string>& Params )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    int cmdReturn = 0;
    std::vector<std::string> replyMessages;

    PrintParams("OnSaiAtendimento", Params);

    if( Params.size() == 5 )
    {
        std::vector<std::string> subParams;
        ParseInput( subParams, Params[1], ':' );
        if( subParams.size() == 4 )
        {
            int userChannel = atoi( subParams[1].c_str() );
            //shared_ptr<SCCall> userCall = SCController::GetInstance().GetCallShared( SCChannel( 0, userChannel - 1 ) );
            boost::shared_ptr<SCCall> userCall =  m_pController->GetCallShared( SCChannel( 0, userChannel - 1) );
            
            if( userCall != NULL )
            {
                std::string& roomID = Params[2];
                int roomSize = atoi( Params[3].c_str() );
                std::string& roomIDPrivate = Params[4];

                cmdReturn = userCall->ConferenceRemove( roomIDPrivate.c_str(), SCConference::DUPLEX );
                                
                SC_LOG4CPP_EX( m_pLog4cpp,"SAI_ATENDIMENTO: %04d->ConferenenceRemove( %s, DUPLEX )[%d]", userChannel % roomIDPrivate.c_str() % cmdReturn );

                if( cmdReturn == 1 )
                {
                    replyMessages.push_back( "OK" );
                    cmdReturn = userCall->ConferenceAdd( roomID.c_str(), roomSize, userCall->GetANI().c_str(), SCConference::DUPLEX, false, false );
                                        
                    SC_LOG4CPP_EX( m_pLog4cpp,"SAI_ATENDIMENTO: %04d->ConferenceAdd( %s, %d, %s, DUPLEX, false, false )[%d]", userChannel %
                        roomID.c_str() % roomSize % userCall->GetANI().c_str() % cmdReturn );
                }
            }
            else
            {
                SC_LOG4CPP_EX( m_pLog4cpp,"SAI_ATENDIMENTO: INVALID CHANNEL ( %d )", userChannel );                
            }
        }
        else
        {
            SC_LOG4CPP( m_pLog4cpp,"SAI_ATENDIMENTO: ERROR PARSING CHANNEL INFO" );            
        }
    }
    else
    {
        std::string paramsStr;
        for( auto vecBeg = Params.begin(), vecEnd = Params.end(); vecBeg != vecEnd; ++vecBeg )
            paramsStr.append( *vecBeg + "," );
                
        SC_LOG4CPP_EX( m_pLog4cpp,"SAI_ATENDIMENTO: INVALID PARAM COUNT: received %d, expected 5 (%s)", Params.size() % paramsStr.c_str() );
    }
        

    switch( cmdReturn )
    {
    case -1: replyMessages.push_back( "TIMEOUT" ); break;
    case 0:  replyMessages.push_back( "ERRO"    ); break;
    case 1:  replyMessages.push_back( "OK"      ); break;
    default: break;
    }
    DoWrite( replyMessages );
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}

void SCConferenceService::OnPing( )
{
    SC_LOG4CPP_ENTRY( m_pLog4cpp );
    if( Started )
        DoWrite( "OK" );
    else
        DoWrite( "ERRO" );
    SC_LOG4CPP_EXIT( m_pLog4cpp );
}


void SCConferenceService::ToUpper( std::string& Text )
{
    for_each(Text.begin(), Text.end(), [](char& in){ in = ::toupper(in); });
}

void SCConferenceService::PrintParams( const std::string & functionName , const std::vector<std::string>& Params )
{
    std::stringstream ss;
    std::string strMsg;
    
    ss << "ACTION : " << functionName << "\r\n";
    for(int i=0; i < Params.size(); i++) {
        ss << "[" << boost::lexical_cast<std::string>(i) << "] : " << Params[i] << "\r\n";        
    }
    strMsg = ss.str();
        
    SC_LOG4CPP_EX( m_pLog4cpp,"%s", strMsg.c_str() );    
}