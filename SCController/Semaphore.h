
#ifndef _MY_SEMAPHORE_H_
#define _MY_SEMAPHORE_H_

#include <mutex>
#include <condition_variable>
#include <atomic>
#include <KD3/Basics/KSemaphore.h>

class Semaphore : public ktools::KSemaphore
{
private:
	Semaphore( Semaphore const& );
	Semaphore& operator=( Semaphore const& );

public:
	Semaphore() : KSemaphore() {}
	~Semaphore() {}
};

#endif