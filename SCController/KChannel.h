
#pragma once

#include "SCStdIncludes.h"

class KChannel
{
private:

    int Device;
    int Channel;

    K3L_CHANNEL_STATUS ChanStt;

public:
    KChannel( int Dev, int Chan )
        : Device( Dev )
        , Channel( Chan )
    {}
    KChannel(const KChannel &other) : Device(other.Device), Channel(other.Channel)
    {}
    ~KChannel() {}

    inline int GetDevice() const { return Device; }
    inline int GetChannel() const { return Channel; }

    stt_code SendCommand( int Command, void* Params = 0 );

    stt_code Connect();
    stt_code Disconnect();
    stt_code Reset();

    stt_code Beep();

    stt_code Play( char const* Filename );
    stt_code StopPlay();

    stt_code Record( char const* Filename, int Codec );
    stt_code StopRecord();

    stt_code EnableAudioEvents();
    stt_code DisableAudioEvents();

    stt_code ClearMixer( );
    stt_code MixChannel( KChannel const& Other );
    stt_code UnmixChannel( KChannel const& Other );

    K3L_CHANNEL_STATUS const& GetStatus();    

    bool operator==( KChannel const& Other );
    bool operator!=( KChannel const& Other );    
};

bool operator<( const KChannel& c1, const KChannel& c2 );
