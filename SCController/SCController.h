
#ifndef _INCLUDED_KHOMP_SCCONTROLLER_H_
#define _INCLUDED_KHOMP_SCCONTROLLER_H_

#include "SCStdIncludes.h"
#include "ISCController.h"
#include "SCEventThread.h"
#include "SCChannelAllocator.h"
#include "SCCDRLogger.h"
#include "SCLogger.h"
#include "SCGarbageCollector.h"
#include "SCIoServiceAcceptor.h"

#include "ConferenceCommands.h"

class SCCall;

using boost::shared_ptr;


class SCController : public ISCController
{
    friend class SCCall;
private:
    unsigned int InternalId;
    
	//std::map< SCChannel, int > ActiveChannels;
    //std::map< int, boost::shared_ptr<SCCall> > ActiveCalls;

    //boost::mutex ActiveCallsMutex;
    //boost::recursive_mutex CallsMutex;
    boost::condition	m_condWaitCalls;


    DictChannels							m_DictChannels;
	boost::shared_mutex						DictChannelsMutex;


    boost::shared_ptr<SCGarbageCollector>   m_GarbageCollector;
	boost::shared_ptr<SCCDRLogger>          m_CDRLogger;    
    boost::shared_ptr<SCIoServiceAcceptor>  m_IoServiceAcceptor;
    boost::shared_ptr<SCConferenceManager> 	m_ConferenceManagerPtr;

    boost::shared_ptr<SCConfigReader>       m_configReaderPtr;
    // LOG4CPP
	log4cpp::Category * 					m_pLog4cpp;	    

    boost::shared_ptr<SCChannelAllocator> m_ChanAllocatorPtr;
    
    void PrintDictChannels();
    static const int ThreadCount = 50;
    SCEventThread EventThreads[ThreadCount];

    static int Kstdcall K3LEventHandler( int Obj, K3L_EVENT* Event );

    void OnSipInvite( const int CallId );
    void OnNewCall( SCChannel const& Channel, K3L_EVENT* Event );
    void OnDefault( SCChannel const& Channel, K3L_EVENT* Event );

    stt_code SendCommand( int Device, int Channel, int Command, byte* Params = NULL );

    //SCCall* GetCall( int InternalId );
    //SCCall* GetCall( SCChannel const& Channel );

    //boost::shared_ptr<SCCall> UnsafeGetCall( int InternalId );

    SCController();
    SCController( SCController& );
    SCController& operator=( SCController const& );
    ~SCController();
    
    

public:

    static SCController& GetInstance() 
    {
        static SCController Instance;
        return Instance; 
    }

    void RemoveActiveCalls();
    SCChannelAllocator * GetChannelAllocator() { return m_ChanAllocatorPtr.get(); }
    boost::shared_ptr<SCConferenceManager> GetConferenceManager() const { return m_ConferenceManagerPtr; }
    void AddToGarbageCollector(void * pRunner);
    //void RemoveActiveCall(int callId);
    //std::map< int, boost::shared_ptr<SCCall> > GetActiveCalls() const { return ActiveCalls; }
    void Initialize(boost::shared_ptr<SCConfigReader> configReaderPtr);
    void Stop();
    void RegisterEventHandler();
    int EventHandler( int Obj, K3L_EVENT* Evt );
    boost::shared_ptr<SCCall> GetCallShared( int InternalId );
    boost::shared_ptr<SCCall> GetCallShared( SCChannel const& Channel );
    //std::map< SCChannel, int > & GetActiveChannels() { return ActiveChannels; }
    void DeleteCall( int InternalId );
    void DeleteCall( SCChannel Channel );
    //void UnMapChannel(SCChannel const& Channel );
    
    volatile bool m_Stopping;
};

#endif