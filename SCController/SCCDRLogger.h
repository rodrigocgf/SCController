
#pragma once

#include "SCCDRPortal.h"

#include <KD3/Basics/KString.h>
#include <map>


class SCCDRLogger
{
 
private:
    const uint32 TimerInterval;
    ktools::kstring CDRBaseDir;
    std::map<ktools::kstring, SCCDRPortal*> PortalsRegistry;
    std::mutex Mutex;

    
    
    FILE* GetCDRLogFile( SCCDRPortal& Portal );

    bool OpenCDRLogDir( SCCDRPortal& Portal, ktools::time::KSystemTime const& CurrentTime );
    ktools::kstring GetCDRLogDir( SCCDRPortal& Portal, ktools::time::KSystemTime const&  CurrentTime );

    int ReadIndexFile( ktools::kstring const& BaseDir );
    void WriteIndexFile( ktools::kstring const& BaseDir, int Index );

    SCCDRPortal* GetPortal( char const* PortalName );
    bool UpdateFileIndex( SCCDRPortal& Portal, ktools::time::KSystemTime const& CurrentTime );
public:
    SCCDRLogger(const std::string & cdrDir , int cdrInterval);
    //SCCDRLogger();
    SCCDRLogger( SCCDRLogger const& );
    SCCDRLogger& operator=( SCCDRLogger const& );
    ~SCCDRLogger();

    void CloseCDR(std::string PortalName, std::string CallId, std::string ANI , std::string DNIS);
    void Finalize();
    int Log( char const* PortalName, char const* CDRMessage, size_t MessageLength );
};
