
#include <k3l.h>
#include "SCCall.h"
#include "SCEventContainer.h"

struct EventData
{
    K3L_EVENT* Event;
    SCCall* EventCall;
};

SCEventContainer::SCEventContainer( )
{
    EventList.ActivateSection( true );
    KickOff();
}

SCEventContainer::~SCEventContainer( )
{
    EventList.ActivateSection( false );
}

void SCEventContainer::AddEvent( K3L_EVENT* Event, SCCall* EventCall )
{
    K3L_EVENT* newEvent = new K3L_EVENT;
    newEvent->AddInfo = Event->AddInfo;
    newEvent->Code = Event->Code;
    newEvent->DeviceId = Event->DeviceId;
    newEvent->ObjectId = Event->ObjectId;
    newEvent->ObjectInfo = Event->ObjectInfo;
    newEvent->ParamSize = Event->ParamSize;

    if( Event->ParamSize > 1 )
    {
        newEvent->Params = new byte[Event->ParamSize];
        memcpy( newEvent->Params, Event->Params, Event->ParamSize );
    }
    else
        newEvent->Params = NULL;

    EventData* evData = new EventData;
    evData->Event = newEvent;
    evData->EventCall = EventCall;

    EventList.Lock();
    EventList.Add( evData );
    EventList.Unlock();
}

void SCEventContainer::Execute()
{
     //verificar se tem evento para executar
    //caso houver liberar a mem�ria ap�s execut�-lo
    EventData* eventData = NULL;

    while( true )
    {
        EventList.Lock();
        if( EventList.Count() > 0 )
        {
            KListNode* node = EventList.Get( 0 );
            if( !node )
            {
                EventList.Unlock();
                return;
            }
            eventData = static_cast<EventData*>( node->Data );
            EventList.Remove( node );
            EventList.Unlock();
        }
        else
        {
            EventList.Unlock();
            Sleep( 32 );
        }
    
        if( eventData )
        {
            //TODO: tratar a validade do ponteiro para chamada
            eventData->EventCall->HandleEvent( eventData->Event );
            delete[] eventData->Event->Params;
            delete eventData->Event;
        }
        delete eventData;
        eventData = NULL;
    }
}