#include "SCCall.h"
#include "SCController.h"
#include "SCConfigReader.h"
#include "SCConferenceManager.h"
#include "SCOSFunctions.h"
#include "SCChannelAllocator.h"
#include "SCLuaAPI.h"

#include <lua.hpp>
#include <time.h>
#include <cerrno>
#include <fstream>

TimerManager* SCCall::Timers = TimerManager::instance();

static void DigitTimeoutCallback( void* UserData )
{
    SCController& controller = SCController::GetInstance();
    int callId = (int)UserData;
    shared_ptr<SCCall> myCall = controller.GetCallShared( callId );
    if( myCall )
        myCall->OnDigitTimeout();
}

static void MaxRecordTimeoutCallback( void* UserData )
{
    SCController& controller = SCController::GetInstance();
    int callId = (int)UserData;
    shared_ptr<SCCall> myCall = controller.GetCallShared( callId );
    if( myCall )
        myCall->OnMaxRecordTimeout();
}

static void MaxRecordSilenceTimeouCallback( void* UserData )
{
    SCController& controller = SCController::GetInstance();
    int callId = (int)UserData;
    shared_ptr<SCCall> myCall = controller.GetCallShared( callId );
    if( myCall )
        myCall->OnMaxRecordSilenceTimeout();
}


SCCall::SCCall( ISCController * pController, boost::shared_ptr<SCConfigReader> configReaderPtr, int device, 
                int channel, char const* _ANI, char const* DNIS, unsigned int InternalId )
    : m_pController(pController) 
    , ReceivedDisconnect( false )
    , HungUp( false )
    , InternalId( InternalId )
    , KhompChannel( device, channel )
    , Device( device )
    , Channel( channel )
    , MaxRecordSilenceTime( 0 )
    , MaxRecordTimer( 0 )
    , MaxRecordSilenceTimer( 0 )
    , BeepRecord( true )
    , NumDigitsExpected( -1 )
    , InterDigitTimeout( -1 )
    , DigitMask( "@" )
    , EchoDigit( false )
    , ANI( _ANI )
    , DNIS( DNIS )
    , PlayingFile( false )
    , SyncPlaying( false )
    , RecordingFile( false )
    , GettingDigits( false )
    , RecordStoppedBySilence( false )
    , InConference( false )
    , ListeningToConference( false )
    , ConferenceRoom( "" )
    , ReceivedDigits( "" )
    , Connected( false )    
    //, ConferenceManager( SCConferenceManager::GetInstance() )
    , m_configReaderPtr(configReaderPtr)
{
    this->Channel += 1;                         // a pedido do Marcos Fernandes, para manter funcionamento das ferramentas de CDR j� existentes.
                                                // canal n�o pode come�ar a partir do 0.
    

    ktools::time::KSystemTime currTime;
    KHostSystem::GetTime( &currTime );

    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("Call") ); 

    SC_LOG4CPP_EX( m_pLog4cpp,"[%s][%d] vvvvvvvvvvvvvvvvvvvvvvvvv C A L L vvvvvvvvvvvvvvvvvvvvvvvvvvvvv", ANI.c_str() % Channel );

	char callid[32];
    sprintf( callid, "%02d%02d%04d%04d%02d%02d%02d%02d%02d%03d%02d\0", m_configReaderPtr->GetIVRSwitch(), m_configReaderPtr->GetMachine(), KhompChannel.GetChannel(), currTime.Year, currTime.Month,
                currTime.Day, currTime.Hour, currTime.Minute, currTime.Second, currTime.Milli, m_configReaderPtr->GetIVRType() );
	this->CallId = callid;
    
	char logBaseDir[2048];
	sprintf( logBaseDir, "%s/%04d/%02d/%02d", m_configReaderPtr->GetLogLuaDir().c_str(), currTime.Year, currTime.Month,currTime.Day );
    
    SCOS::CreateDir( logBaseDir );
	
	char logfilename[16];
	sprintf( logfilename, "\\%02d%03d.txt", Device, Channel );
	
	this->LogFilename = logBaseDir;
	this->LogFilename.append( logfilename );
}

void SCCall::ClearTimers()
{	
    SC_LOG4CPP_ENTRY_EX(m_pLog4cpp, "[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    
    if( DigitTimer ) Timers->stopTimer( DigitTimer );
    if( MaxRecordTimer ) Timers->stopTimer( MaxRecordTimer );
    if( MaxRecordSilenceTimer ) Timers->stopTimer( MaxRecordSilenceTimer );
    DigitTimer = 0;
    MaxRecordTimer = 0;
    MaxRecordSilenceTimer = 0;
    RecordedTime.stop();
	
    SC_LOG4CPP_EXIT_EX(m_pLog4cpp, "[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    
}

void SCCall::ReleaseSemaphores()
{    
    SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d] [%s].\r\n", KhompChannel.GetDevice() % KhompChannel.GetChannel() % GetCallId().c_str());

    PlaySemaphore.Release();
    DigitSemaphore.Release();
    RecordSemaphore.Release();
    ConnectSemaphore.Release();    
}

SCCall::~SCCall()
{

    SC_LOG4CPP_EX( m_pLog4cpp,"[%s][%d] ^^^^^^^^^^^^^^^^^^^^^^^^^ C A L L ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^", ANI.c_str() % Channel );
    
    if( LogFile.is_open() ) LogFile.close();


    if( InConference )
    {
        InConference = false;
        RemoveFromConferenceRoom();
    }
}

void SCCall::RemoveFromConferenceRoom()
{	
    SC_LOG4CPP_EX( m_pLog4cpp, " Room Id : %s - ANI : %s \r\n" , ConferenceRoom.c_str() % GetANI().c_str() );
        
    m_pController->GetConferenceManager()->RemoveFromRoom( ConferenceRoom.c_str(), GetANI().c_str() );
	ConferenceRoom = "";
}

void SCCall::HandleEvent( K3L_EVENT* Event )
{   
    boost::mutex::scoped_lock lock(  CallMutex );    
    
    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d][0x%02x]", KhompChannel.GetDevice() % KhompChannel.GetChannel() % Event->Code );
        
	
    SCChannel channel(KhompChannel.GetDevice(), KhompChannel.GetChannel());

    switch( Event->Code )
    {
    case EV_CONNECT:
        SC_LOG4CPP_EX( m_pLog4cpp, "===> EV_CONNECT [%02d][%03d] [%s]", KhompChannel.GetDevice() % KhompChannel.GetChannel() % ANI.c_str() );
        
        Connected = true;
		ReleaseSemaphore( ConnectSemaphore );

        SC_LOG4CPP_EX( m_pLog4cpp, "[%02d][%03d] [%s] ConnectSemaphore released .", KhompChannel.GetDevice() % KhompChannel.GetChannel() % ANI.c_str() );
        
		KhompChannel.ClearMixer();
    break;

    case EV_SIP_DTMF_DETECTED:
    case EV_DTMF_DETECTED:
        OnDigitDetected( static_cast<char>( Event->AddInfo ) );
    break;

    case EV_DISCONNECT:
        SC_LOG4CPP_EX( m_pLog4cpp, "===> EV_DISCONNECT [%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel());
        
        OnDigitDetected( (char)0xff );
        
        if( !HungUp ) ReceivedDisconnect = true;
        if( InConference )
        {
            SC_LOG4CPP_EX( m_pLog4cpp,"SCCall::HandleEvent{EV_DISCONNECT}[%02d][%03d][0x%02x] <Conference> -> Room='%s', MSISDN=%s(l)\n", 
                KhompChannel.GetDevice() % KhompChannel.GetChannel() % Event->Code % ConferenceRoom.c_str() % GetANI().c_str() );
            
        }

    break;
        
    case EV_CHANNEL_FAIL:
    case EV_CHANNEL_FREE:
        SC_LOG4CPP_EX( m_pLog4cpp,"===> EV_SIP_CANCEL | EV_CALL_FAIL | EV_CHANNEL_FAIL | EV_CHANNEL_FREE [%02d][%03d]\r\n",KhompChannel.GetDevice() % KhompChannel.GetChannel() );
        
        OnDigitDetected( (char)0xff );
        
        if( InConference )
        {
            SC_LOG4CPP_EX( m_pLog4cpp, "{EV_CHANNEL_FREE}[%02d][%03d][0x%02x] <Conference> -> Room='%s', MSISDN=%s(l)\n", 
                KhompChannel.GetDevice() % KhompChannel.GetChannel() % Event->Code % ConferenceRoom.c_str() % GetANI().c_str() );
            
        }

        if( !ReceivedDisconnect && !HungUp ) ReceivedDisconnect = true;
        
        ClearTimers();

        StopPlaying();
        StopRecording();
        if( GettingDigits ) GettingDigits = false;
        ReleaseSemaphores();

        if( ListeningToConference ) 
        {
            m_pController->GetConferenceManager()->ConferenceUnlisten( ConferenceRoom.c_str(), GetANI().c_str() );

            SC_LOG4CPP_EX( m_pLog4cpp,"{EV_CHANNEL_FREE}[%02d][%03d][0x%02x] <Calling ConferenceUnlisten> -> Room='%s', MSISDN=%s(l)\n", 
                KhompChannel.GetDevice() % KhompChannel.GetChannel() % Event->Code % ConferenceRoom.c_str() % GetANI().c_str() );

            
        }

        if( InConference )
        {
            m_pController->GetConferenceManager()->RemoveFromRoom( ConferenceRoom.c_str(), GetANI().c_str() );
            
            SC_LOG4CPP_EX( m_pLog4cpp,"{EV_CHANNEL_FREE}[%02d][%03d][0x%02x] <Calling RemoveFromRoom> -> Room='%s', MSISDN=%s(l)\n", 
                KhompChannel.GetDevice() % KhompChannel.GetChannel() % Event->Code % ConferenceRoom.c_str() % GetANI().c_str() ); 
        }

        KhompChannel.ClearMixer();
    break;
        
    case EV_END_OF_STREAM:
		if( PlayingFile )
		{
			PlayingFile = false;
			if( SyncPlaying )
				ReleaseSemaphore( PlaySemaphore );
		}
        break;

    case EV_TONE_BEGIN:
    case EV_HMP_TONE_BEGIN:
        if( Event->AddInfo == ktoneSilence && !MaxRecordSilenceTimer )
            MaxRecordSilenceTimer = Timers->startTimer( MaxRecordSilenceTime, (void*)InternalId, MaxRecordSilenceTimeouCallback );
    break;

    case EV_TONE_END:
    case EV_HMP_TONE_END:
        if( Event->AddInfo == ktoneSilence )
        {
            if( MaxRecordSilenceTimer ) Timers->stopTimer( MaxRecordSilenceTimer );
            MaxRecordSilenceTimer = 0;
        }
    break;

    default:
    break;
    }

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d][0x%02x]", KhompChannel.GetDevice() % KhompChannel.GetChannel() % Event->Code );
	
}

int SCCall::Answer( bool DoubleAnswer, bool Billing )
{   
    boost::unique_lock<boost::mutex> lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"Dev: [%02d] Chan: [%03d] ANI: [%s]", KhompChannel.GetDevice() % KhompChannel.GetChannel() % ANI.c_str() );

    stt_code ret = KhompChannel.Connect();
    if( ret == ksSuccess && !Connected )
    {
        lock.unlock();
        SC_LOG4CPP_EX( m_pLog4cpp,"Dev: [%02d] Chan: [%03d] ANI: [%s] Waiting for ConnectSemaphore.", 
            KhompChannel.GetDevice() % KhompChannel.GetChannel() % ANI.c_str());
        
        ConnectSemaphore.Wait();        
    }

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp, "Dev: [%02d] Chan: [%03d] ANI: [%s]", KhompChannel.GetDevice() % KhompChannel.GetChannel() % ANI.c_str() );
    
    return ( ret == ksSuccess ? 0 : 1 );
}

void SCCall::Stop()
{
    if( !WasDisconnected() )
    {
        HungUp = true;
        KhompChannel.Disconnect();
    }
    ReleaseSemaphores();

}

void SCCall::Hangup( )
{   
    boost::mutex::scoped_lock lock(  CallMutex );
        
    SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d]-[%s][%s] - Call Hanged up(l)\n", KhompChannel.GetDevice() % KhompChannel.GetChannel() % GetANI().c_str() % GetCallId().c_str() );
    
    if( !WasDisconnected() )
    {
        HungUp = true;
        KhompChannel.Disconnect();
    }    

    if ( InConference )
	{
		InConference = false;
		SC_LOG4CPP( m_pLog4cpp,"Going to Call RemoveFromConferenceRoom()");
		RemoveFromConferenceRoom();
	}
}

char const* SCCall::GetDigits( int NumDigits, int InterDigitTimeout, char const* DigitMask, bool Echo )
{   
    boost::unique_lock<boost::mutex> lock(  CallMutex );
     
    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp, "[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    
    
    if( !WasDisconnected() )
    {
        this->NumDigitsExpected = NumDigits;
        this->InterDigitTimeout = InterDigitTimeout * 1000;
        this->EchoDigit = Echo;
        this->DigitMask = DigitMask;

        GettingDigits = true;

        if( ReceivedDigits.size() < NumDigits && !WasDisconnected() )
        {
            if( InterDigitTimeout )
            {
                DigitTimer = Timers->startTimer( this->InterDigitTimeout, (void*)InternalId, DigitTimeoutCallback );
            }
            lock.unlock();
            DigitSemaphore.Wait();
            
            lock.lock();
        }

        DigitsRequested = ReceivedDigits.substr( 0, NumDigits );
        ReceivedDigits.erase( 0, NumDigits );
        GettingDigits = false;
    }

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp, "[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    
    
    return DigitsRequested.c_str();
}

void SCCall::OnDigitDetected( char Digit )
{    
    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );

    Timers->stopTimer( DigitTimer );
    DigitTimer = 0;
    ReceivedDigits.push_back( Digit );

    SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d] Receivedigits : %s", KhompChannel.GetDevice() % KhompChannel.GetChannel() % ReceivedDigits.c_str() );

    if( WasDisconnected() )
    {
		ReleaseSemaphore( DigitSemaphore );
    }
    else if( GettingDigits )
    {
        if( EchoDigit )
            KhompChannel.Beep();
        if( ReceivedDigits.size() == NumDigitsExpected )
        {
            GettingDigits = false;
            ReleaseSemaphore( DigitSemaphore );
        }
        else
        {
            DigitTimer = Timers->startTimer( InterDigitTimeout, (void*)InternalId, DigitTimeoutCallback );
        }
    }
    else
    {
        if( ( DigitMask.find( '@' ) != std::string::npos ) ||
            ( DigitMask.size() > 0 && DigitMask.find( Digit ) != std::string::npos ) )
        {
            if( PlayingFile )
            {
               StopPlaying();
            }
            if( RecordingFile )
            {
               StopRecording();
            }
        }            
    }          

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    
}

void SCCall::OnDigitTimeout( )
{   
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    

    DigitTimer = 0;
    GettingDigits = false;
    ReleaseSemaphore( DigitSemaphore );

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    
}

int SCCall::ClearDigits( )
{   
    boost::mutex::scoped_lock lock(  CallMutex );

    int ret;

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    

    ReceivedDigits.clear();
    ret = ReceivedDigits.compare( "" ) == 0 ? 0 : -1;
        
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    return ret;
}

void SCCall::SetDigitMask( char const* Mask )
{   
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    

    DigitMask.clear();
    DigitMask.append( Mask );

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    
}

bool FormatEquals( char const* Format1, char const* Format2 )
{
	if( !Format1 || !Format2 ) return false;

	const size_t format1len = strlen( Format1 );
	const size_t format2len = strlen( Format2 );
	
	if( format1len != format2len ) return false;
	
	return ( strncmp( Format1, Format1, format1len ) == 0 );
}

void SCCall::SetPlayFormat( char const* Format )
{   
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    

    if( FormatEquals( "WAV_G711_ALAW", Format ) ||
		FormatEquals( "PCM64",         Format ) ||
        FormatEquals( "PCM_G711_ALAW", Format ) ||
        FormatEquals( "WAV_PCM"      , Format )   )
    {
		PlayFormat.clear();	
        PlayFormat = Format;
    }
    //TODO: tratar formatos passados inv�lidos

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    
}

void SCCall::SetRecordFormat( char const* Format )
{   
    boost::mutex::scoped_lock lock(  CallMutex );
        
    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );

    if( FormatEquals( "WAV_G711_ALAW", Format ) ||
        FormatEquals( "WAV_PCM"      , Format )   )
    {
		RecordFormat.clear();
		RecordFormat = Format;
    }
    //TODO: tratar formatos passados inv�lidos

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    
}

int SCCall::RemotePlayFile( char const* Filename )
{   
    boost::unique_lock<boost::mutex> lock(  CallMutex );

    stt_code ret = 0;

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    
    
    if( !WasDisconnected() )
    {
        if( ReceivedDigits.size() == 0 ){
            try
            {
                ret = KhompChannel.Play( Filename );
            }
            catch( ... )
            {
                PlayingFile = false;
                KhompChannel.StopPlay();
                KhompChannel.Disconnect();
                lock.unlock();
                return -1;
            }
            PlayingFile = ( ret == ksSuccess );
            ret = PlayingFile ? 0 : 1;
            if( PlayingFile )
            {
                SyncPlaying = true;
                lock.unlock();
                PlaySemaphore.Wait();
                //PlaySemaphore.Wait(PLAY_SEM_TIMEOUT);
                lock.lock();
                if( WasDisconnected() )
                    ret = -1;
            }
        }
    }
    else
    {
        ret = -1;
    }

    if( ret == 0 )
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d] : [%s] PLAYED SUCCESSFULLY.\n", KhompChannel.GetDevice() % KhompChannel.GetChannel() % Filename );
    }
    else
    {
       SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d] : [%s] PLAYED WITH ERROR.\n", KhompChannel.GetDevice() % KhompChannel.GetChannel() % Filename );
    }

    PlayingFile = false;
    
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    
    return ret;
}

int SCCall::RemotePlayFileAsync( char const* Filename )
{
    boost::unique_lock<boost::mutex> lock(  CallMutex );

    stt_code ret;

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    

    if( !WasDisconnected() )
    {
        try
        {
            ret = KhompChannel.Play( Filename );
        }
        catch( ... )
        {
            PlayingFile = false;
            KhompChannel.Disconnect();
            KhompChannel.Reset();
            lock.unlock();
            return -1;
        }
        PlayingFile = ( ret == ksSuccess );
        if( PlayingFile )
        {
            SyncPlaying = false ;          
        }
        ret = PlayingFile ? 0 : 1;
    }
    else
    {
        ret = -1;
        PlayingFile = false;
    }

    
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    
    return ret;
}

void SCCall::StopPlaying( )
{    
    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );

    KhompChannel.StopPlay();
    PlayingFile = false;
    ReleaseSemaphore( PlaySemaphore );
    
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
}

int SCCall::RemoteRecordFile( char const* Filename, int MaxRecordTime, int MaxSilenceTime, bool Beep, char const* DigitMask )
{   
    boost::unique_lock<boost::mutex> lock(  CallMutex );

    KCodecIndex codec = kci8kHzALAW;
    stt_code ret;

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    

    if( !WasDisconnected() )
    {
        try
        {
            this->DigitMask = DigitMask;
            if( RecordFormat.compare( "WAV_PCM" ) == 0 )
                codec = kci8kHzPCM;
        
            ret = KhompChannel.Record( Filename, codec );
        }
        catch( ... )
        {
            RecordingFile = false;
            KhompChannel.Reset();
            lock.unlock();
            return -1;
        }
        if( ret == ksBusy )
            KhompChannel.StopRecord();
        RecordingFile = ( ret == ksSuccess );
        ret = RecordingFile ? 0 : 1;

        if( RecordingFile )
        {
            if( Beep )
                KhompChannel.Beep();
            RecordedTime.start();
            if( MaxRecordTime > 0 )
                MaxRecordTimer = Timers->startTimer( MaxRecordTime * 1000, (void*)InternalId, MaxRecordTimeoutCallback );

            if( MaxSilenceTime > 0 )
            {
                MaxRecordSilenceTime = MaxSilenceTime * 1000;
                KhompChannel.EnableAudioEvents();
                if( KhompChannel.GetStatus().AudioStatus == ktoneSilence )
                    MaxRecordSilenceTimer = Timers->startTimer( MaxRecordSilenceTime, (void*)InternalId, MaxRecordSilenceTimeouCallback );
            }
            lock.unlock();
            RecordSemaphore.Wait();
            
            lock.lock();
            if( WasDisconnected() )
                ret = -1;
        }
        if( MaxSilenceTime > 0 )
            KhompChannel.DisableAudioEvents();
    }
    else
        ret = -1;
    RecordingFile = false;
      
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    
    return ret;
}

void SCCall::StopRecording( )
{    
    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );

    KhompChannel.StopRecord();
    RecordedTime.stop();
    if( MaxRecordTimer ) Timers->stopTimer( MaxRecordTimer );
    if( MaxRecordSilenceTimer ) Timers->stopTimer( MaxRecordSilenceTimer );
    MaxRecordTimer = 0;
    MaxRecordSilenceTimer = 0;
    RecordingFile = false;
    ReleaseSemaphore( RecordSemaphore );
        
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
}

void SCCall::OnMaxRecordTimeout( )
{   
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   

    MaxRecordTimer = 0;
    StopRecording();

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
}

int SCCall::GetRecordedTime( )
{   
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   
    return static_cast<int>(RecordedTime.getElapsedTimeInSec());
}

void SCCall::OnMaxRecordSilenceTimeout()
{   
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   

    RecordStoppedBySilence = true;
    MaxRecordSilenceTimer = 0;
    StopRecording();

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
}

int SCCall::ConferenceAdd( char const* ConferenceId, int ConferenceSize, char const* MSISDN, int ConferenceType, bool Supervisor, bool Beep )
{
    boost::mutex::scoped_lock lock(  CallMutex );
    int addReturn = -1;

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   
    
    SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d]-[%s][%s] - numOfUsers = %d (l)\n", KhompChannel.GetDevice() % KhompChannel.GetChannel() % 
        GetANI().c_str() % GetCallId().c_str() % m_pController->GetConferenceManager()->GetNumberOfUsers(ConferenceId) );

    if( !WasDisconnected() )
    {
        if( ConferenceRoom.empty() )
        {
            ConferenceRoom = ConferenceId;            
            
            SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d]-[%s][%s] - ConferenceId = '%s' ['%s'] (l)\n", KhompChannel.GetDevice() % 
                KhompChannel.GetChannel() % GetANI().c_str() % GetCallId().c_str() % ConferenceRoom.c_str() % ConferenceId );

            if( m_pController->GetConferenceManager()->CreateRoom( ConferenceRoom.c_str(), ConferenceSize ) )
            {
                InConference = m_pController->GetConferenceManager()->AddToRoom( ConferenceId, GetANI().c_str(), KhompChannel.GetDevice(), KhompChannel.GetChannel(), Supervisor, Beep );
            }
        }
        addReturn = ( InConference ? 1 : 0 );
    }
    else
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d]-[%s][%s] - Call Hanged up (l)\n", KhompChannel.GetDevice() % KhompChannel.GetChannel() % 
            GetANI().c_str() % GetCallId().c_str() );
        
    }
    
    SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d]-[%s][%s] - Return = %d (l)\n", KhompChannel.GetDevice() % KhompChannel.GetChannel() % 
        GetANI().c_str() % GetCallId().c_str() % addReturn );
    

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    return addReturn;
}

int SCCall::ConferenceRemove( char const* ConferenceId, int ActionType )
{
    boost::mutex::scoped_lock lock(  CallMutex );
    int removeReturn = -1;

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   
        
    SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d]-[%s][%s] - ConferenceId = '%s' - numOfUsers = %d (l)\n", KhompChannel.GetDevice() % 
        KhompChannel.GetChannel() % GetANI().c_str() % GetCallId().c_str() % ConferenceId % m_pController->GetConferenceManager()->GetNumberOfUsers(ConferenceId) );

    //if( !WasDisconnected() )
    {
        InConference = !( m_pController->GetConferenceManager()->RemoveFromRoom( ConferenceId, GetANI().c_str() ) );
        if( ! InConference ) 
            ConferenceRoom = "";
        
        removeReturn = ( InConference ? 0 : 1 ); // Conversa privada OK
        //removeReturn = ( InConference ? 1 : 0 ); // Desconectar Usuario OK
    }
    
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );

    return removeReturn;
}

int SCCall::ConferenceListen( char const* ConferenceId )
{
    boost::mutex::scoped_lock lock(  CallMutex );
    
    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   
    int listenReturn = -1;
        
    
    if( !WasDisconnected() )
    {
        //ListeningToConference = ConferenceManager.ConferenceListen( ConferenceId, GetANI().c_str(), GetKhompDevice(), GetKhompChannel() );
        ListeningToConference = m_pController->GetConferenceManager()->ConferenceListen( ConferenceId, GetANI().c_str() );
        if( ListeningToConference )
        {
            listenReturn = 1;
            ConferenceRoom = ConferenceId;
            SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"User Id [%s] ADDED to Room [%s] ", GetANI().c_str() % ConferenceId );            
        }
        else
        {
            listenReturn = 0;
            SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"User Id [%s] NOT ADDED to Room [%s] ", GetANI().c_str() % ConferenceId );            
        }
    }
    
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    return listenReturn;
}

int SCCall::ConferenceUnlisten( char const* ConferenceId )
{
    boost::mutex::scoped_lock lock(  CallMutex );
        
    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   
    int unlistenReturn = -1;
     
   
    if( !WasDisconnected() )
    {
        ListeningToConference = (! m_pController->GetConferenceManager()->ConferenceUnlisten( ConferenceId, GetANI().c_str() ) );
        if( !ListeningToConference )
        {
            unlistenReturn = 1;
            SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"User Id [%s] REMOVED to Room [%s] ", GetANI().c_str() % ConferenceId );            
        }
        else
        {
            unlistenReturn = 0;
            SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"User Id [%s] NOT REMOVED to Room [%s] ", GetANI().c_str() % ConferenceId );            
        }
    }
    
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    
    return unlistenReturn;
}

int SCCall::ConferenceChangeStatus( char const* ConferenceId, SCConference::ConfereeStatus Status, int Unlisten )
{
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   
    int changeReturn = -1;
     
    
    if( !WasDisconnected() )
    {
        bool changed = m_pController->GetConferenceManager()->ConferenceChangeStatus( ConferenceId, GetANI().c_str(), Status );
        changeReturn = changed ? 1 : 0;
    }
    
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    return changeReturn;
}

int SCCall::GetNumOfUsers( char const* ConferenceId )
{
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   
    int userCount;
 
        
    userCount = m_pController->GetConferenceManager()->GetNumberOfUsers( ConferenceId );
        
    SC_LOG4CPP_EX( m_pLog4cpp,"[%02d][%03d]-[%s][%s] - ConferenceId = '%s', numOfUsers = %d (l)", KhompChannel.GetDevice() % KhompChannel.GetChannel() % 
        GetANI().c_str() % GetCallId().c_str() % ConferenceId % userCount );

    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    
    return userCount;
}

char const* SCCall::GetConferenceId()
{
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   
    
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    return ConferenceRoom.c_str();
}

bool SCCall::ListenToChannel( int Device, int Channel )
{
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );    
    
    
    bool listening = (KhompChannel.MixChannel( KChannel( Device, Channel ) ) == ksSuccess);
    
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    return listening;
}

bool SCCall::UnlistenChannel( int Device, int Channel )
{
    boost::mutex::scoped_lock lock(  CallMutex );

    SC_LOG4CPP_ENTRY_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );   
    bool unlisten = false;
    
    unlisten = (KhompChannel.UnmixChannel( KChannel( Device, Channel ) ) == ksSuccess);
    
    SC_LOG4CPP_EXIT_EX( m_pLog4cpp,"[%02d][%03d]", KhompChannel.GetDevice() % KhompChannel.GetChannel() );
    return unlisten;
}

void SCCall::Log( char const* message, size_t length )
{   
    boost::mutex::scoped_lock lock(  CallMutex );

    std::stringstream ss;
    try
    {
        if( !LogFile.is_open() )
            LogFile.open( LogFilename, std::ios::app | std::ios::ate );
    
        if( LogFile.is_open() )
        {
            std::string logMessage;
            
            boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
            long milliseconds = timeLocal.time_of_day().total_milliseconds();
            boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
            boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);
            
            ss.str(std::string());
            ss <<   boost::format("[%02i:%02i:%02i.%03i] %s \r\n")  % current_date_milliseconds.time_of_day().hours() 
                                                                    % current_date_milliseconds.time_of_day().minutes() 
                                                                    % current_date_milliseconds.time_of_day().seconds() 
                                                                    % (current_date_milliseconds.time_of_day().fractional_seconds() / 1000) 
                                                                    %  message;
            logMessage = ss.str();
            LogFile << logMessage;
            LogFile.flush();

        }
    }
    catch( ... )
    {
        SC_LOG4CPP_EX( m_pLog4cpp,"Exception while writing log message!! Channel[%02d][%03d]", GetKhompDevice() % GetKhompChannel() );        
    }
    
    
}

void SCCall::ReleaseSemaphore( Semaphore& Sem )
{
	try
	{
		Sem.Release();
	}
	catch( ... )
	{
		printf( "Exception on ReleaseSemaphore\n" );
	}
}


//boost::shared_ptr<SCCall> SCCall::New( int Device, int Channel, char const* ANI, char const* DNIS, unsigned int InternalID )
//{
//    shared_ptr<SCCall> myptr( new SCCall( Device, Channel, ANI, DNIS, InternalID ) );
//    return myptr;
//}

void SCCall::SetCDRLogger(SCCDRLogger * pCDRLogger)
{
    m_pCDRLogger = pCDRLogger;
}

void SCCall::DisconnectChannel()
{
    Hangup();
}