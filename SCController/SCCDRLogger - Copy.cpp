
#include <KHostSystem.h>
#include <TimerManager.h>
#include <fstream>

#include "SCCDRLogger.h"
#include "SCConfigReader.h"
#include "SCOSFunctions.h"

#define CDR_DIR_PATTERN "%s/%s/%04d-%02d-%02d"
#define CDR_TMP_FILE_PATTERN "%s/CDR_%04d.tmp"
#define CDR_FILE_PATTERN "%s/CDR_%04d.txt"
#define CDR_INDEX_FILE_PATTERN "%s/fileIndex.txt"

SCCDRLogger::SCCDRLogger()
    : CDRBaseDir( SCConfigReader::GetCDRDir() )
    , TimerInterval( SCConfigReader::GetCDRInterval() )
{
}

SCCDRLogger::~SCCDRLogger()
{
    auto portalsEnd = PortalsRegistry.end();
    for( auto portal = PortalsRegistry.begin(); portal != portalsEnd; ++portal )
    {
        if( portal->second->IsFileOpen() )
        {
            std::string OriginalFilename = portal->second->GetPortalFilename();
            std::string DestinationFilename = OriginalFilename;
            boost::replace_all(DestinationFilename, ".tmp", ".txt");
            //DestinationFilename.replace( ".tmp", ".txt" );
            portal->second->SetPortalFile( NULL );
            SCOS::RenameFile( OriginalFilename, DestinationFilename );
        }
        delete portal->second;
    }
}

SCCDRLogger& SCCDRLogger::GetInstance()
{
    static SCCDRLogger instance;
    return instance;
}

SCCDRPortal* SCCDRLogger::GetPortal( char const* PortalName )
{
    SCCDRPortal* portal;
    std::lock_guard<std::mutex> lock( Mutex );
    auto portalRegistry = PortalsRegistry.find( PortalName );
    if( portalRegistry == PortalsRegistry.end() )
    {
        portal = new SCCDRPortal( PortalName );
        PortalsRegistry.insert( std::pair<std::string, SCCDRPortal*>( PortalName, portal ) );
    }
    else
        portal = portalRegistry->second;
    return portal;
}

int SCCDRLogger::Log( char const* PortalName, char const* CDRMessage, size_t MessageLength )
{
    int ret = 0;
    FILE* logFile = NULL;
    SCCDRLogger& logger = GetInstance();
    SCCDRPortal* portal = logger.GetPortal( PortalName );
        
    try
    {
        portal->Lock();
        logFile = logger.GetCDRLogFile( *portal );
        if( logFile != NULL )
        {
            if( !SCOS::WriteToFile( logFile, CDRMessage, MessageLength ) )
                printf( "Erro escrevendo mensagem no arquivo\n" );
            SCOS::WriteToFile( logFile, "\n", sizeof( char ) );
            ret = 1;
        }
        portal->Unlock();
    }
    catch( ... )
    {
        portal->Unlock();
        ret = 0;
    }
    return ret;
}

int SCCDRLogger::ReadIndexFile( std::string const& BaseDir )
{
    std::stringstream ss;
    int ret = 0;
    std::string name;
    
    ss << boost::format(CDR_INDEX_FILE_PATTERN) % BaseDir.c_str();
    name = ss.str();
    //name.sprintf( CDR_INDEX_FILE_PATTERN, BaseDir.c_str() );
    std::fstream indexFile( name );
    if( indexFile.is_open() )
    {
        std::string indexStr;
        indexFile >> indexStr;
        indexFile.close();
        ret = atoi( indexStr.c_str() );
    }
    return ret;
}

void SCCDRLogger::WriteIndexFile( std::string const& BaseDir, int Index )
{
    std::stringstream ss;
    std::string name;

    ss << boost::format(CDR_INDEX_FILE_PATTERN) % BaseDir.c_str();
    name = ss.str();
    //name.sprintf( CDR_INDEX_FILE_PATTERN, BaseDir.c_str() );
    std::ofstream indexFile( name, std::ios::trunc );
    if( indexFile.is_open() )
    {
        indexFile << Index;
        indexFile.close();
    }
}

bool SCCDRLogger::OpenCDRLogDir( SCCDRPortal& Portal, ktools::time::KSystemTime const& CurrentTime )
{
    std::string cdrDir( GetCDRLogDir( Portal, CurrentTime ) );

    if( SCOS::CreateDir( cdrDir ) != 0 )
        return false;
    
    Portal.SetPortalDir( cdrDir );

    return true;
}

std::string SCCDRLogger::GetCDRLogDir( SCCDRPortal& Portal, ktools::time::KSystemTime const& CurrentTime )
{
    std::stringstream ss;
    std::string cdrDir;
    
    ss << boost::format(CDR_DIR_PATTERN) % CDRBaseDir.c_str() % Portal.GetName() % CurrentTime.Year %CurrentTime.Month %CurrentTime.Day;
    cdrDir = ss.str();
    //cdrDir.sprintf( CDR_DIR_PATTERN, CDRBaseDir.c_str(), Portal.GetName(), CurrentTime.Year, CurrentTime.Month, CurrentTime.Day );
    return cdrDir;
}

bool SCCDRLogger::UpdateFileIndex( SCCDRPortal& Portal, ktools::time::KSystemTime const& CurrentTime )
{
    bool indexUpdated = false;
    int fileIndex = 1;
    uint32 currSecondTimestamp = (CurrentTime.Hour * 3600) + (CurrentTime.Minute * 60) + CurrentTime.Second;
    
    bool lastLogDayChanged = false;
    if( !Portal.IsInitialized() )
    {
        fileIndex = ReadIndexFile( Portal.GetPortalDir() );
        ++fileIndex;
        indexUpdated = true;
    }
    else
    {
        lastLogDayChanged = (Portal.GetLastDayLogged() != CurrentTime.Day);
        if( !lastLogDayChanged )
        {
            kstring tmpFilename;
            tmpFilename.sprintf( CDR_TMP_FILE_PATTERN, Portal.GetPortalDir().c_str(), Portal.GetCurrentFileIndex() );
            if( (currSecondTimestamp - Portal.GetLastSecondTimestamp()) >= TimerInterval     // check if the time limit for a CDR file was reached
                || !SCOS::FileExists( tmpFilename ) )                                   // or the file was already processed
            {
                fileIndex = Portal.GetCurrentFileIndex() + 1;
                indexUpdated = true;
            }
        }
        else
        {
            indexUpdated = true;
        }
    }

    if( indexUpdated )
    {
        WriteIndexFile( Portal.GetPortalDir(), fileIndex );
        Portal.UpdatePortal( CurrentTime.Day, fileIndex, currSecondTimestamp );
    }

    return indexUpdated;
}

FILE* SCCDRLogger::GetCDRLogFile( SCCDRPortal& Portal )
{
    std::stringstream ss;
    bool indexUpdated = false;
    std::string cdrDir;
    ktools::time::KSystemTime currTime;
    
    KHostSystem::GetTime( &currTime );
    
    if( !OpenCDRLogDir( Portal, currTime ) )
        return NULL;

    std::string OriginalFilename = Portal.GetPortalFilename();
    
    indexUpdated = UpdateFileIndex( Portal, currTime );
   
    if( indexUpdated )
    {
        if( Portal.IsFileOpen() )
        {
            std::string DestinationFilename = OriginalFilename;
            boost::replace_all(DestinationFilename, ".tmp", ".txt");
            //DestinationFilename.replace( ".tmp", ".txt" );
            Portal.SetPortalFile( NULL );
            SCOS::RenameFile( OriginalFilename, DestinationFilename );
        }
    }

    if( !Portal.IsFileOpen() )
    {
        std::string cdrFile;
        ss << boost::format(CDR_TMP_FILE_PATTERN) % Portal.GetPortalDir().c_str() % Portal.GetCurrentFileIndex();
        cdrFile = ss.str();
        //cdrFile.sprintf( CDR_TMP_FILE_PATTERN, Portal.GetPortalDir().c_str(), Portal.GetCurrentFileIndex() );
        Portal.SetPortalFile( cdrFile.c_str());
    }
    
    return Portal.GetPortalFile();
}

