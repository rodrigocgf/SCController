
#ifndef __SCSCRIPTRUNNER_H__
#define __SCSCRIPTRUNNER_H__

#include <KD3/Basics/KThread.h>

#include "SCCall.h"
#include "SCLogger.h"

class SCScriptRunner : public ktools::KThread
{
private:
    boost::shared_ptr<SCCall> MyCall;

    SCScriptRunner();
    SCScriptRunner( SCScriptRunner const& );

    void Execute();
    
    ktools::kstring GetScriptPath();

	static SCLogger Logger;

public:

    SCScriptRunner( boost::shared_ptr<SCCall> const& Call )
        : MyCall( Call )
    {}

    ~SCScriptRunner( )
    {}

    void Activate() { KickOff(); }
};

#endif
