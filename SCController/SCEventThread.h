
#ifndef _INCLUDED_KHOMP_SCEVENTTHREAD_H_
#define _INCLUDED_KHOMP_SCEVENTTHREAD_H_

#include <KD3/Basics/KThread.h>
#include <KList.h>
#include <atomic>
#include "SCLogger.h"
#include "SCCall.h"
#include "SCStdIncludes.h"

struct K3L_EVENT;

class SCEventThread : public ktools::KThread
{
    KList EventList;
    std::atomic<bool> Running;
    
    //boost::shared_ptr<SCLogger> LoggerPtr;
    log4cpp::Category * 					m_pLog4cpp;
    
    void Execute();
    
public:

    SCEventThread( );
    virtual ~SCEventThread( );

    void AddEvent( K3L_EVENT* Event, int EventCall, boost::shared_ptr<SCCall> Call );
};

#endif