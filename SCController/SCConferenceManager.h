
#pragma once

#include <map>
#include <string>
#include <vector>

#include "SCStdIncludes.h"
#include "SCLogger.h"
#include "KChannel.h"
#include "SCTypes.h"
#include "SCConferenceRoom.h"

#include "ISCController.h"


class SCConferenceManager
{
private:
    //std::mutex Mutex;
    boost::recursive_mutex MutexCM;
    
    
    //std::map< std::string, ConferenceRoom > RoomsAvailable;
    std::map< std::string, boost::shared_ptr<ConferenceRoom> > RoomsAvailable;
        
    log4cpp::Category * 					m_pLog4cpp;
    ISCController *							m_pIController;    

public:
    SCConferenceManager(ISCController * pController );
    SCConferenceManager( const SCConferenceManager& );
    ~SCConferenceManager();
    SCConferenceManager& operator=( const SCConferenceManager& );

    //static SCConferenceManager& GetInstance();

    std::map< std::string, boost::shared_ptr<ConferenceRoom> > GetRoomsAvailable();
    int GetRoomsSize() { return RoomsAvailable.size(); }

    void ClearRooms();
    
    //TODO: como tratar o supervisor??
    bool CreateRoom( char const* ConferenceId, int RoomSize );
    bool DestroyRoom( char const* RoomID );
    bool AddToRoom( char const* ConferenceId, char const * UserId, int KhompDevice, int KhompChannel, bool IsSupervisor, bool Beep );
    bool RemoveFromRoom( char const* ConferenceId, char const* UserId );
    int GetNumberOfUsers( char const* ConferenceId );
    //bool ConferenceListen( char const* ConferenceId, char const* UserId, int KhompDevice, int KhompChannel );
    //bool ConferenceUnlisten( char const* ConferenceId, char const* UserId );
    bool ConferenceListen( char const* ConferenceId, char const* UserId );
    bool ConferenceUnlisten( char const* ConferenceId, char const* UserId );
    bool ConferenceChangeStatus( char const* ConferenceId, char const* UserId, SCConference::ConfereeStatus Status ); 
};
