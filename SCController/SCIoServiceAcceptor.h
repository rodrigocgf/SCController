#pragma once

#include "SCStdIncludes.h"
#include "SCConferenceService.h"
#include "ISCController.h"


class SCIoServiceAcceptor
{
private:
    //SCIoServiceAcceptor(void);
    //~SCIoServiceAcceptor(void);

    ISCController *						m_Controller;
    boost::shared_ptr<boost::thread>    m_ioServiceThrPtr;
    boost::asio::io_service             m_IoService;
    boost::asio::ip::tcp::acceptor      m_Acceptor;
    std::list<boost::shared_ptr<SCConferenceService> > m_ConferenceServiceList;
    
    void Accept();
    
    //void HandleAccept( boost::system::error_code const& Error ); // OK
    //void HandleAccept( boost::shared_ptr<SCConferenceService> m_SCConferencePtr, boost::system::error_code const& Error );
    void HandleAccept( boost::shared_ptr<SCConferenceService> paramConferencePtr, ISCController * p_Controller, boost::system::error_code const& Error );
    void IoServiceThread();
public:
    /*
    static SCIoServiceAcceptor& GetInstance()
    {
        static SCIoServiceAcceptor    instance;   // Guaranteed to be destroyed.
                                                     // Instantiated on first use.
        return instance;
    }
    */
    SCIoServiceAcceptor(ISCController * pParent);	
    ~SCIoServiceAcceptor(void);

    void Start();
    void Stop();
};

