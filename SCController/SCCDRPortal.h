
#pragma once

#include "SCOSFunctions.h"

#include <mutex>

class SCCDRPortal
{
private:
    std::string Name;
    uint32 LastDayLogged;
    uint32 CurrentFileIndex;
    uint32 LastSecondTimestamp;

    std::mutex Mutex;

    bool FileOpen;
    FILE* PortalFile;
    std::string PortalDir;
    std::string Filename;

    
    SCCDRPortal( SCCDRPortal const& );
    SCCDRPortal& operator=( SCCDRPortal const& );
    
public:

    SCCDRPortal( )
        : LastDayLogged( 0 )
        , CurrentFileIndex( 0 )
        , LastSecondTimestamp( 0 )
        , FileOpen( false ) 
        , PortalFile( NULL )
        , PortalDir( "" ){}

    SCCDRPortal( char const* Name )
        : Name( Name )
        , LastDayLogged( 0 )
        , CurrentFileIndex( 0 )
        , LastSecondTimestamp( 0 )
        , FileOpen( false ) 
        , PortalFile( NULL )
        , PortalDir( "" ){}
    ~SCCDRPortal() { }

    void   SetName(std::string name ) { Name = name; }
    bool   IsFileOpen()             const { return FileOpen; }
    bool   IsInitialized()          const { return LastDayLogged != 0; }
  
    char const* GetName()           const { return Name.c_str(); }
    uint32 GetLastDayLogged()       const { return LastDayLogged; }
    uint32 GetCurrentFileIndex()    const { return CurrentFileIndex; }
    uint32 GetLastSecondTimestamp() const { return LastSecondTimestamp; }
    FILE* GetPortalFile()           const { return PortalFile; }

    std::string const& GetPortalFilename() const { return Filename; }
    std::string const& GetPortalDir()      const { return PortalDir; } 

    void UpdatePortal( uint32 LastDay, uint32 FileIndex, uint32 Timestamp )
    {
        LastDayLogged = LastDay;
        CurrentFileIndex = FileIndex;
        LastSecondTimestamp = Timestamp;
    }

    void SetLastDayLogged( uint32 LastDay )         { LastDayLogged = LastDay; }
    void SetCurretFileIndex( uint32 Index )         { CurrentFileIndex = Index; }
    void SetLastSecondTimestamp( uint32 Timestamp ) { LastSecondTimestamp = Timestamp; }
    void SetPortalFile( char const* Name ) 
    { 
        if( ! Name  )
        {
            SCOS::CloseFile( PortalFile );
            PortalFile = NULL;
            Filename = "";
            FileOpen = false;
        }
        else
        {
            Filename = Name; 
            PortalFile = SCOS::OpenFile( Filename, true ); 
            FileOpen = ( PortalFile != NULL);
        }
    }
    void SetPortalDir( std::string Dir ) { PortalDir = Dir; }

    void Lock()   { Mutex.lock(); }
    void Unlock() { Mutex.unlock(); }
};  