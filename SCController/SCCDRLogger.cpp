
#include <KHostSystem.h>
#include <TimerManager.h>
#include <fstream>

#include "SCCDRLogger.h"
#include "SCConfigReader.h"
#include "SCOSFunctions.h"

#define CDR_DIR_PATTERN "%s/%s/%04d-%02d-%02d"
#define CDR_TMP_FILE_PATTERN "%s/CDR_%04d.tmp"
#define CDR_FILE_PATTERN "%s/CDR_%04d.txt"
#define CDR_INDEX_FILE_PATTERN "%s/fileIndex.txt"

SCCDRLogger::SCCDRLogger(const std::string & cdrDir , int cdrInterval)
    : CDRBaseDir( cdrDir )
    , TimerInterval( cdrInterval )
{
}

/*
SCCDRLogger::SCCDRLogger()
    : CDRBaseDir( SCConfigReader::GetCDRDir() )
    , TimerInterval( SCConfigReader::GetCDRInterval() )
{
}
*/

SCCDRLogger::~SCCDRLogger()
{
    
}

void SCCDRLogger::Finalize()
{
    auto portalsEnd = PortalsRegistry.end();
    for( auto portal = PortalsRegistry.begin(); portal != portalsEnd; ++portal )
    {
        if( portal->second->IsFileOpen() )
        {
            ktools::kstring OriginalFilename = portal->second->GetPortalFilename();
            ktools::kstring DestinationFilename = OriginalFilename;
            DestinationFilename.replace( ".tmp", ".txt" );
            portal->second->SetPortalFile( NULL );
            SCOS::RenameFile( OriginalFilename, DestinationFilename );
        }
        delete portal->second;
    }
}

SCCDRPortal* SCCDRLogger::GetPortal( char const* PortalName )
{
    SCCDRPortal* portal;
    std::lock_guard<std::mutex> lock( Mutex );
    auto portalRegistry = PortalsRegistry.find( PortalName );
    if( portalRegistry == PortalsRegistry.end() )
    {
        portal = new SCCDRPortal( PortalName );
        PortalsRegistry.insert( std::pair<ktools::kstring, SCCDRPortal*>( PortalName, portal ) );
    }
    else
        portal = portalRegistry->second;
    return portal;
}

int SCCDRLogger::Log( char const* PortalName, char const* CDRMessage, size_t MessageLength )
{
    int ret = 0;
    FILE* logFile = NULL;
    
    SCCDRPortal* portal = GetPortal( PortalName );
        
    try
    {
        portal->Lock();
        logFile = GetCDRLogFile( *portal );
        if( logFile != NULL )
        {
            if( !SCOS::WriteToFile( logFile, CDRMessage, MessageLength ) )
                printf( "Erro escrevendo mensagem no arquivo\n" );
            SCOS::WriteToFile( logFile, "\n", sizeof( char ) );
            ret = 1;
        }
        portal->Unlock();
    }
    catch( ... )
    {
        portal->Unlock();
        ret = 0;
    }
    return ret;
}

int SCCDRLogger::ReadIndexFile( ktools::kstring const& BaseDir )
{
    int ret = 0;
    ktools::kstring name;
    name.sprintf( CDR_INDEX_FILE_PATTERN, BaseDir.c_str() );
    std::fstream indexFile( name );
    if( indexFile.is_open() )
    {
        ktools::kstring indexStr;
        indexFile >> indexStr;
        indexFile.close();
        ret = atoi( indexStr.c_str() );
    }
    return ret;
}

void SCCDRLogger::WriteIndexFile( ktools::kstring const& BaseDir, int Index )
{
    
    ktools::kstring name;
    name.sprintf( CDR_INDEX_FILE_PATTERN, BaseDir.c_str() );
    std::ofstream indexFile( name, std::ios::trunc );
    if( indexFile.is_open() )
    {
        indexFile << Index;
        indexFile.close();
    }
}

bool SCCDRLogger::OpenCDRLogDir( SCCDRPortal& Portal, ktools::time::KSystemTime const& CurrentTime )
{
    ktools::kstring cdrDir( GetCDRLogDir( Portal, CurrentTime ) );

    if( SCOS::CreateDir( cdrDir ) != 0 )
        return false;
    
    Portal.SetPortalDir( cdrDir );

    return true;
}

ktools::kstring SCCDRLogger::GetCDRLogDir( SCCDRPortal& Portal, ktools::time::KSystemTime const& CurrentTime )
{
    ktools::kstring cdrDir;
    cdrDir.sprintf( CDR_DIR_PATTERN, CDRBaseDir.c_str(), Portal.GetName(), CurrentTime.Year, CurrentTime.Month, CurrentTime.Day );
    return cdrDir;
}

bool SCCDRLogger::UpdateFileIndex( SCCDRPortal& Portal, ktools::time::KSystemTime const& CurrentTime )
{
    bool indexUpdated = false;
    int fileIndex = 1;
    uint32 currSecondTimestamp = (CurrentTime.Hour * 3600) + (CurrentTime.Minute * 60) + CurrentTime.Second;
    
    bool lastLogDayChanged = false;
    if( !Portal.IsInitialized() )
    {
        fileIndex = ReadIndexFile( Portal.GetPortalDir() );
        ++fileIndex;
        indexUpdated = true;
    }
    else
    {
        lastLogDayChanged = (Portal.GetLastDayLogged() != CurrentTime.Day);
        if( !lastLogDayChanged )
        {
            kstring tmpFilename;
            tmpFilename.sprintf( CDR_TMP_FILE_PATTERN, Portal.GetPortalDir().c_str(), Portal.GetCurrentFileIndex() );
            if( (currSecondTimestamp - Portal.GetLastSecondTimestamp()) >= TimerInterval     // check if the time limit for a CDR file was reached
                || !SCOS::FileExists( tmpFilename ) )                                   // or the file was already processed
            {
                fileIndex = Portal.GetCurrentFileIndex() + 1;
                indexUpdated = true;
            }
        }
        else
        {
            indexUpdated = true;
        }
    }

    if( indexUpdated )
    {
        WriteIndexFile( Portal.GetPortalDir(), fileIndex );
        Portal.UpdatePortal( CurrentTime.Day, fileIndex, currSecondTimestamp );
    }

    return indexUpdated;
}

FILE* SCCDRLogger::GetCDRLogFile( SCCDRPortal& Portal )
{
    bool indexUpdated = false;
    ktools::kstring cdrDir;
    ktools::time::KSystemTime currTime;
    
    KHostSystem::GetTime( &currTime );
    
    if( !OpenCDRLogDir( Portal, currTime ) )
        return NULL;

    ktools::kstring OriginalFilename = Portal.GetPortalFilename();
    
    indexUpdated = UpdateFileIndex( Portal, currTime );
   
    if( indexUpdated )
    {
        if( Portal.IsFileOpen() )
        {
            ktools::kstring DestinationFilename = OriginalFilename;
            DestinationFilename.replace( ".tmp", ".txt" );
            Portal.SetPortalFile( NULL );
            SCOS::RenameFile( OriginalFilename, DestinationFilename );
        }
    }

    if( !Portal.IsFileOpen() )
    {
        ktools::kstring cdrFile;
        cdrFile.sprintf( CDR_TMP_FILE_PATTERN, Portal.GetPortalDir().c_str(), Portal.GetCurrentFileIndex() );
        Portal.SetPortalFile( cdrFile );
    }
    
    return Portal.GetPortalFile();
}

void SCCDRLogger::CloseCDR(std::string PortalName, std::string CallId, std::string ANI , std::string DNIS)
{
    
    std::stringstream ss;
    std::stringstream ssdt;

    boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
    long milliseconds = timeLocal.time_of_day().total_milliseconds();
    boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
    boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);

    
    ssdt << boost::format("%04i-%02i-%02i %02i:%02i:%02i.%03i") % current_date_milliseconds.date().year() % 
            current_date_milliseconds.date().month().as_number() % current_date_milliseconds.date().day() %
            current_date_milliseconds.time_of_day().hours() % current_date_milliseconds.time_of_day().minutes() %
            current_date_milliseconds.time_of_day().seconds() %
            (current_date_milliseconds.time_of_day().fractional_seconds() / 1000);

    ss << boost::format("CDR_CLOSE, CALL_ID='%s', FINAL_DATETIME='%s', NUMBER_A='%s', NUMBER_B='%s'") % CallId.c_str() % ssdt.str().c_str() % ANI % DNIS;
    
    Log( PortalName.c_str(), ss.str().c_str() , ss.str().size() );
}