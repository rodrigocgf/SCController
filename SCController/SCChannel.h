#ifndef _SCCHANNEL_H_
#define _SCCHANNEL_H_

typedef struct SCChannel
{
    SCChannel() : Device( -1 ), Channel( -1 ) {}
    SCChannel( int Dev, int Chan ) : Device( Dev ), Channel( Chan ){}
    int Device;
    int Channel;
} SCChannel;
bool operator<( const SCChannel& c1, const SCChannel& c2 );
bool operator==( const SCChannel& c1, const SCChannel& c2 );
bool operator!=( const SCChannel& c1, const SCChannel& c2 );

#endif
