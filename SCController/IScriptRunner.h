#ifndef _ISCSCRIPTRUNNER_H_
#define _ISCSCRIPTRUNNER_H_

#include "SCStdIncludes.h"
class SCCDRPortal;

class ISCScriptRunner
{
public:
    virtual int ReadCDRIndexFile( std::string  BaseDir ) = 0;
    virtual void WriteCDRIndexFile( std::string  BaseDir, int Index ) = 0;
    virtual bool UpdateCDRFileIndex( SCCDRPortal& Portal, boost::posix_time::ptime  CurrentTime ) = 0;
};

#endif