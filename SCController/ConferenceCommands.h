#ifndef _CM_COMMANDS_H_
#define _CM_COMMANDS_H_

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/member.hpp>

#include "KChannel.h"
#include "SCChannelAllocator.h"

using boost::multi_index_container;
using namespace boost::multi_index;

/*
class Call;

class CPortais 
{
public:
	CPortais() {}
	CPortais( std::string _portalName , int _numUsers , int _numRooms ): PortalName(_portalName), NumUsers(_numUsers), NumRooms(_numRooms)
	{}
	
	void IncNumRooms() 
	{
		NumRooms++;
	}
	
	void IncNumUsers()
	{
		NumUsers++;
	}
	
	std::string PortalName;
	int			NumUsers;
	int			NumRooms;
};	
*/	



/*
class CChannels
{
public:
	CChannels(){}
	CChannels( int device, int channel ,  boost::shared_ptr<Call> pCallPtr , int internalId ) : 
		Device(device), Channel(channel), CallPtr(pCallPtr), InternalId(internalId) {}
		
    int             Device;
    int             Channel;
	boost::shared_ptr<Call>			CallPtr;
	int				InternalId;
};
*/
/*
struct ByPortalName{};

typedef multi_index_container
<
	boost::shared_ptr<CPortais>,
	indexed_by<		
		ordered_unique< 
			tag<ByPortalName>,
			member< CPortais, std::string, &CPortais::PortalName> 
		>		
	>
> ContainerPortais;
*/

struct ByUserId{};
struct ByChannelUser{};

class RoomUser
{
public:
    RoomUser( std::string _userId, KChannel _channelUser ) : UserId(_userId) , ChannelUser(_channelUser) , ChannelListener(-1,-1)
    {}
    RoomUser( std::string _userId, KChannel _channelUser ,SCConference::ConfereeStatus   _status ) : 
        UserId(_userId) , ChannelUser(_channelUser) , Status(_status), ChannelListener(-1,-1) 
    {}
    RoomUser() : ChannelUser(-1,-1) , ChannelListener(-1,-1)
    {}
    ~RoomUser()
    {}

    std::string                     UserId;
    SCConference::ConfereeStatus    Status;
    KChannel                        ChannelUser;
    KChannel                        ChannelListener;
        
};

typedef multi_index_container
<
	boost::shared_ptr<RoomUser>,
	indexed_by<    
		ordered_unique<
            tag<ByUserId>,
			member< RoomUser, std::string, &RoomUser::UserId> 
		>,
        ordered_unique<
            tag<ByChannelUser>,
            member< RoomUser, KChannel, &RoomUser::ChannelUser>
        >
    >
> RoomContainer;

class CChannels
{
public:
	CChannels(){}
	
    CChannels( int device, int  channel ,  boost::shared_ptr<SCCall> pCallPtr , int  internalId ) : 
		Device(device), Channel(channel), CallPtr(pCallPtr), InternalId(internalId)
    {
        
    }
		
    int                             Device;
    int                             Channel;
	boost::shared_ptr<SCCall>		CallPtr;
	int		                        InternalId;

    
    inline bool operator < ( const CChannels & other)
    {
        if ( Device == other.Device )
        {
            if ( Channel < other.Channel )
            {
                return true;
            } else {
                return false;
            }
        } else if ( Device > other.Device ) {
            return false;
        } else if ( Device < other.Device ) {
            return true;
        }
    }

    inline bool operator == ( const CChannels & other)
    {
        if ( (Device == other.Device) && ( Channel == other.Channel ) )
            return true;
        else
            return false;
    }

    inline bool operator != ( const CChannels & other )
    {
        if ( (Device == other.Device) && ( Channel == other.Channel ) )
            return false;
        else
            return true;
    }
    

};	

struct ByChannelDevice{};
struct ByChannel{};
struct ByInternalId{};
struct ByAll{};


typedef multi_index_container
<
	boost::shared_ptr<CChannels>,
	indexed_by<    
		ordered_unique<
			tag<ByAll>,
			composite_key<
                boost::shared_ptr<CChannels>,
                member< CChannels, int, &CChannels::Device>,
				member< CChannels, int, &CChannels::Channel>,
				member< CChannels, int, &CChannels::InternalId>
			>
		>,
        ordered_unique<
            tag<ByChannelDevice>,
			composite_key<
                boost::shared_ptr<CChannels>,
                member< CChannels, int, &CChannels::Device>,
				member< CChannels, int, &CChannels::Channel>
            >
        >,         
		ordered_unique< 
			tag<ByChannel>,
			member< CChannels, int, &CChannels::Channel> 
		> ,		
		ordered_unique< 
			tag<ByInternalId>,
			member< CChannels, int, &CChannels::InternalId> 
		>		
	>	
> DictChannels;



#endif