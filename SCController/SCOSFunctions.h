
#include <KD3/Basics/KString.h>
#include <Windows.h>

namespace SCOS
{
    int CreateDir( ktools::kstring const& path );
    int RemoveDir( ktools::kstring const& path );
    int FindFiles( ktools::kstring const& path );
    int FindIndexFile( ktools::kstring const& path, int index );
    int GetFileSize( ktools::kstring const& path );
    bool FileExists( ktools::kstring const& path );
    bool DirExists( ktools::kstring const& path );
    FILE* OpenFile( ktools::kstring const& Filename, bool Append );
    bool WriteToFile( FILE* File, char const* Text, size_t TextLength );
    bool CloseFile( FILE* File );
    bool RenameFile( ktools::kstring const& OriginalPath, ktools::kstring const& DestinationPath );
};