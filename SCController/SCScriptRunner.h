#pragma once
#include "SCStdIncludes.h"
#include "SCCall.h"
#include "SCCDRLogger.h"
#include "ISCController.h"


class SCScriptRunner 
{
public:
    SCScriptRunner(void);
    SCScriptRunner( boost::shared_ptr<SCConfigReader> configReaderPtr, boost::shared_ptr<SCCall> const& Call , ISCController * pController);
    SCScriptRunner( boost::shared_ptr<SCConfigReader> configReaderPtr, boost::shared_ptr<SCCDRLogger> const& CDR , boost::shared_ptr<SCCall> const& Call , ISCController * pController);
    
    ~SCScriptRunner(void);

    ISCController * m_pController;
    boost::shared_ptr<SCConfigReader> m_configReaderPtr;
    std::string GetScriptPath(SCCall * CallPtr);
    boost::mutex m_CDRMutex;
    int m_CDRTimerInterval;

    void Execute();
    void RunThread(boost::shared_ptr<SCCDRLogger> p_CDRPtr, boost::shared_ptr<SCCall> p_CallPtr, boost::shared_ptr<SCConfigReader> p_configReaderPtr);
    
    boost::shared_ptr<boost::thread> m_ExecuteThreadPtr;
	boost::shared_ptr<SCCall> m_CallPtr;
    boost::shared_ptr<SCCDRLogger> m_CDRLoggerPtr;
    
};

