#include "SCScriptRunner.h"
#include "SCGarbageCollector.h"
#include "SCLuaAPI.h"
#include "SCConfigReader.h"

typedef std::map< int, boost::shared_ptr<SCCall> > CallIdToCallMap;

SCScriptRunner::SCScriptRunner(void)
{
}

SCScriptRunner::SCScriptRunner( boost::shared_ptr<SCConfigReader> configReaderPtr, boost::shared_ptr<SCCall> const& Call , ISCController * pController): 
    m_CallPtr( Call ), m_pController(pController), m_configReaderPtr(configReaderPtr)
{

}

SCScriptRunner::SCScriptRunner( boost::shared_ptr<SCConfigReader> configReaderPtr, boost::shared_ptr<SCCDRLogger> const& CDR , boost::shared_ptr<SCCall> const& Call , ISCController * pController) :  
    m_CDRLoggerPtr(CDR) , 
    m_CallPtr( Call ), 
    m_pController(pController),    
    m_configReaderPtr(configReaderPtr)
{
    m_CDRTimerInterval = m_configReaderPtr->GetCDRInterval();
}

void SCScriptRunner::Execute()
{   
    m_ExecuteThreadPtr.reset ( new boost::thread ( boost::bind (&SCScriptRunner::RunThread, this , m_CDRLoggerPtr, m_CallPtr , m_configReaderPtr )));	
}

SCScriptRunner::~SCScriptRunner(void)
{
}

void SCScriptRunner::RunThread(boost::shared_ptr<SCCDRLogger> p_CDRPtr, boost::shared_ptr<SCCall> p_CallPtr, boost::shared_ptr<SCConfigReader> p_configReaderPtr)
{
    std::stringstream ss;
    
    int ret = 0;
    std::string scriptPath;
    boost::posix_time::ptime timeLocal = boost::posix_time::second_clock::local_time();
    tm tm_local_time = to_tm(timeLocal);

    SCCall* CallPtr = p_CallPtr.get();
    SCCDRLogger* CDRPtr = p_CDRPtr.get();
    SCConfigReader * ConfigReaderPtr = p_configReaderPtr.get();
    
    try
    {
        CallPtr->SetCDRLogger(CDRPtr);
        scriptPath = GetScriptPath(CallPtr);
        
        //ret = lua_scReadFileCDR( CDRPtr, CallPtr, scriptPath.c_str() );
        ret = lua_scReadFileCDR( CDRPtr, CallPtr, ConfigReaderPtr, scriptPath.c_str() );
        
        if( ret != 0 )
        {
            CallPtr->Hangup();
            
            ss.str(std::string());
            ss <<   boost::format("Error while running the script (%s). Channel[%02i][%03i].") % 
                    scriptPath.c_str(), CallPtr->GetKhompDevice(), CallPtr->GetKhompChannel();
                    
            //BOOST_LOG_TRIVIAL(info) << ss.str();
            printf( "Error while running the script (%s). Channel[%02d][%03d].\n", scriptPath.c_str(), 
                CallPtr->GetKhompDevice(), CallPtr->GetKhompChannel() );
                

            boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
            long milliseconds = timeLocal.time_of_day().total_milliseconds();
            boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
            boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);
            ss.str(std::string());
            ss <<   boost::format("%04i%02i%02i%02i%02i%02i%03i") % current_date_milliseconds.date().year() % 
                    current_date_milliseconds.date().month().as_number() % current_date_milliseconds.date().day() %
                    current_date_milliseconds.time_of_day().hours() % current_date_milliseconds.time_of_day().minutes() %
                    current_date_milliseconds.time_of_day().seconds() %
                    (current_date_milliseconds.time_of_day().fractional_seconds() / 1000);

            
            //BOOST_LOG_TRIVIAL(info) << ss.str();
            //printf( "[%04d-%02d-%02d %02d:%02d:%02d.%03d] %d - ", currTime.Year, currTime.Month, currTime.Day, currTime.Hour, currTime.Minute, currTime.Second, 
            //currTime.Milli, ret );
        }
    }
    catch( std::exception& exc )
    {
        //CallPtr->Hangup();
        
        //printf( "[%04d-%02d-%02d %02d:%02d:%02d.%03d] ", currTime.Year, currTime.Month, currTime.Day, currTime.Hour, currTime.Minute, currTime.Second, 
        //    currTime.Milli );

        //printf( "Exception while executing the call script (%s)! Channel[%02d][%03d]: %s\n", scriptPath.c_str(), 
        //    m_CallPtr->GetKhompDevice(), m_CallPtr->GetKhompChannel(), exc.what() );
    }
    catch( ... )
    {
        //CallPtr->Hangup();
        
        //printf( "[%04d-%02d-%02d %02d:%02d:%02d.%03d] ", currTime.Year, currTime.Month, currTime.Day, currTime.Hour, currTime.Minute, currTime.Second, 
        //            currTime.Milli );

        //printf( "Unknown exception while executing the call script (%s)! Channel[%02d][%03d].\n", scriptPath.c_str(), 
        //    m_CallPtr->GetKhompDevice(), m_CallPtr->GetKhompChannel() );
    }
    
    m_pController->AddToGarbageCollector(this);
    
}


std::string SCScriptRunner::GetScriptPath(SCCall * CallPtr)
{
    ktools::kstring scriptPath;
    if( m_configReaderPtr->GetScriptingMode() != UseDNIS )
    {
        scriptPath.sprintf( "%s\\%s", m_configReaderPtr->GetScriptDir().c_str(), m_configReaderPtr->GetScriptInit().c_str() );
    }
    else
    {
        ktools::kstring const& DNIS = CallPtr->GetDNIS();
        scriptPath.sprintf( "%s\\%s\\%s.lua", m_configReaderPtr->GetScriptDir().c_str(), DNIS.c_str(), DNIS.c_str() );
    }
    return scriptPath;
}