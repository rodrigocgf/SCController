
#pragma once

#include "SCStdIncludes.h"
//#include <KD3\Basics\KString.h>

#include "SCTypes.h"

class SCConfigReader
{
private:
    std::string CurrentDirectory;

    int Machine;
    int IVRType;
    int IVRSwitch;
    int IVRTimeslot;
    int IVRGlobalTimeslot;
    SCScriptingMode ScriptingMode;
    int LogLevel;
    uint32 CDRInterval;
    std::string ScriptDir;
    std::string ScriptInitName;
    std::string LogLuaDir;
    std::string CDRDir;
    std::string LocalIP;
    std::string ConferenceClientAddress;

    //SCConfigReader();
    //SCConfigReader( SCConfigReader const& );
    //SCConfigReader& operator=( SCConfigReader const& );
    //~SCConfigReader() {}~SCConfigReader() {}
    //static SCConfigReader& GetInstance();

public:    
    SCConfigReader();
    SCConfigReader(boost::filesystem::path execPath);
    ~SCConfigReader() {}
    SCConfigReader( SCConfigReader const& );
    SCConfigReader& operator=( SCConfigReader const& );

    int GetMachine() const;
    int GetIVRType();
    int GetIVRSwitch();
    int GetIVRTimeslot();
    int GetIVRGlobalTimeslot();
    SCScriptingMode GetScriptingMode();
    int GetLogLevel();
    uint32 GetCDRInterval();
        
    
    std::string const& GetScriptDir();
    std::string const& GetScriptInit();
    std::string const& GetLogLuaDir();
    std::string const& GetCDRDir();
    std::string const& GetLocalIP();
    std::string const& GetConferenceClientAddress();
};
