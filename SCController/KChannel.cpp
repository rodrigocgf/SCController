
#include <KD3/Basics/KString.h>
#include "KChannel.h"

stt_code KChannel::SendCommand( int Command, void* Params )
{
    K3L_COMMAND cmd;
    cmd.Cmd = Command;
    cmd.Object = Channel;
    cmd.Params = static_cast<byte*>( Params );
    return k3lSendCommand( Device, &cmd );
}

stt_code KChannel::Connect()
{
    return SendCommand( CM_CONNECT );
}

stt_code KChannel::Disconnect()
{
    return SendCommand( CM_DISCONNECT );
}

stt_code KChannel::Reset()
{
    return SendCommand(CM_RESET_CHANNEL);
}

stt_code KChannel::Beep()
{
    return SendCommand( CM_SEND_BEEP );
}

stt_code KChannel::Play( char const* Filename )
{
    std::stringstream ss;
    std::string params;
    ss << boost::format("file_name=\"%s\"") % Filename;
    params = ss.str();
    //params.sprintf( "file_name=\"%s\"", Filename );
    return SendCommand( CM_PLAY, const_cast<char*>( params.c_str() ) );
}

stt_code KChannel::StopPlay()
{
    return SendCommand( CM_STOP_PLAY );
}

stt_code KChannel::Record( char const* Filename, int Codec )
{
    std::stringstream ss;
    std::string params;
    ss << boost::format("file_name=\"%s\" codec=\"%d\"") % Filename % Codec;
    params = ss.str();
    //params.sprintf( "file_name=\"%s\" codec=\"%d\"", Filename, Codec );
    return SendCommand( CM_RECORD_TO_FILE_EX, const_cast<char*>( params.c_str() ) );
}

stt_code KChannel::StopRecord()
{
    return SendCommand( CM_STOP_RECORD );
}

stt_code KChannel::EnableAudioEvents()
{
    stt_code ret;
    K3L_CHANNEL_CONFIG chanConf;
    k3lGetDeviceConfig( Device, ksoChannel + Channel, &chanConf, sizeof( chanConf ) );
    if( chanConf.Signaling == ksigSIP )
        ret = SendCommand( CM_HMP_ENABLE_DETECTION );
    else
        ret = SendCommand( CM_ENABLE_AUDIO_EVENTS );
    return ret;
}

stt_code KChannel::DisableAudioEvents()
{
    stt_code ret;
    K3L_CHANNEL_CONFIG chanConf;
    k3lGetDeviceConfig( Device, ksoChannel + Channel, &chanConf, sizeof( chanConf ) );
    if( chanConf.Signaling == ksigSIP )
        ret = SendCommand( CM_HMP_DISABLE_DETECTION );
    else
        ret = SendCommand( CM_DISABLE_AUDIO_EVENTS );
    return ret;
}

stt_code KChannel::ClearMixer()
{
    return SendCommand( CM_CLEAR_SWITCH );
}

stt_code KChannel::MixChannel( KChannel const& Other )
{
    std::stringstream ss;
    std::string params;
    ss << boost::format("device=\"%d\" channel=\"%d\" enable=\"true\"") % Other.GetDevice() % Other.GetChannel();
    params = ss.str();
    //params.sprintf( "device=\"%d\" channel=\"%d\" enable=\"true\"", Other.GetDevice(), Other.GetChannel() );

    return SendCommand( CM_SWITCH, const_cast<char*>( params.c_str() ) );
}

stt_code KChannel::UnmixChannel( KChannel const& Other )
{
    std::stringstream ss;
    std::string params;
    
    ss << boost::format("device=\"%d\" channel=\"%d\" enable=\"false\"") % Other.GetDevice() % Other.GetChannel();
    params = ss.str();
    //params.sprintf( "device=\"%d\" channel=\"%d\" enable=\"false\"", Other.GetDevice(), Other.GetChannel() );

    return SendCommand( CM_SWITCH, const_cast<char*>( params.c_str() ) );
}

K3L_CHANNEL_STATUS const& KChannel::GetStatus()
{
    k3lGetDeviceStatus( Device, ksoChannel + Channel, &ChanStt, sizeof( ChanStt ) );
    return ChanStt;
}

bool KChannel::operator==( KChannel const& Other )
{
    if ( this->Device == Other.Device && this->Channel == Other.Channel )
        return true;
    else
        return false;
    //return this->Device == Other.Device && this->Channel == Other.Channel;
}

bool KChannel::operator!=( KChannel const& Other )
{
    if ( this->Device == Other.Device && this->Channel == Other.Channel )
        return false;
    else
        return true;
    //return !(*this == Other);
}


bool operator< ( const KChannel& left, const KChannel& right )
{
    if( left.GetDevice() < right.GetDevice() )
    {
        return true;
    }
    else if( left.GetDevice() == right.GetDevice() )
    {
        if( left.GetChannel() < right.GetChannel() )
            return true;
        else
            return false;
    }
    else
    {
        return false;
    }
}