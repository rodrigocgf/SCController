#include "SCIoServiceAcceptor.h"


SCIoServiceAcceptor::SCIoServiceAcceptor(ISCController * pParent) : 
    m_Acceptor( m_IoService, boost::asio::ip::tcp::endpoint( boost::asio::ip::tcp::v4(), 13140 ) ) ,
    m_Controller(pParent)
{
    
}


SCIoServiceAcceptor::~SCIoServiceAcceptor(void)
{
    m_Acceptor.close();
}

void SCIoServiceAcceptor::Start()
{

    m_ioServiceThrPtr.reset ( new boost::thread ( boost::bind (&SCIoServiceAcceptor::IoServiceThread, this)));
    m_ioServiceThrPtr->detach();
    
}

void SCIoServiceAcceptor::Stop()
{
    m_IoService.stop();

    std::list<boost::shared_ptr<SCConferenceService> >::const_iterator iterator;
    for (iterator = m_ConferenceServiceList.begin(); iterator != m_ConferenceServiceList.end(); ++iterator) {
        (*iterator)->Stop();
    }
}

void SCIoServiceAcceptor::Accept()
{

    boost::shared_ptr<SCConferenceService> m_SCConference;
    m_SCConference.reset(new SCConferenceService(m_IoService , m_Controller));
    m_ConferenceServiceList.push_back(m_SCConference);

    m_Acceptor.async_accept( m_SCConference->GetSocket(), boost::bind( &SCIoServiceAcceptor::HandleAccept, this, m_SCConference, m_Controller , _1 ) ); // OK
    
}

void SCIoServiceAcceptor::HandleAccept( boost::shared_ptr<SCConferenceService> paramConferencePtr, ISCController * p_Controller, boost::system::error_code const& Error )
{
    std::stringstream ss;
    //SC_LOG4CPP_ENTRY( m_pLog4cpp );
    boost::shared_ptr<SCConferenceService> conferencePtr;

    if ( paramConferencePtr.get() != NULL ) 
    {
        paramConferencePtr->Start();

        conferencePtr.reset(new SCConferenceService(m_IoService, p_Controller));
        m_ConferenceServiceList.push_back(conferencePtr);
        m_Acceptor.async_accept( conferencePtr->GetSocket(), boost::bind( &SCIoServiceAcceptor::HandleAccept, this, conferencePtr, p_Controller , _1 ) );
    }

    //SC_LOG4CPP_EXIT( m_pLog4cpp );
}


void SCIoServiceAcceptor::IoServiceThread()
{
    std::stringstream ss;    
    //SC_LOG4CPP_ENTRY( m_pLog4cpp );

    try 
    {
        Accept();
        m_IoService.run();
    }
    catch( boost::system::system_error& err )
    {
        //ss.str(std::string());
        //ss << boost::format("ERROR: Exception on Service.run(): %s") % err.what();
        
        //BOOST_LOG_TRIVIAL(info) << ss.str();
        
        //SC_LOG4CPP_EX( m_pLog4cpp,"Exception on Service.run(): %s", err.what() );
    }
    catch( ... )
    {
        //BOOST_LOG_TRIVIAL(info) << "Unknown exception on Conference Service thread!";
        //SC_LOG4CPP( m_pLog4cpp,"Unknown exception on Conference Service thread!" );        
    }

    //SC_LOG4CPP_EXIT( m_pLog4cpp );
}
