
#pragma once

#include <fstream>

#include "SCStdIncludes.h"
#include <KD3/Basics/KTimer.h>
#include <TimerManager.h>

#include "SCStdIncludes.h"
#include "KChannel.h"
#include "SCTypes.h"
#include "Semaphore.h"
#include "SCLogger.h"
#include "SCCDRLogger.h"
#include "ISCController.h"

using namespace ktools;

class SCConferenceManager;

class SCCall : public boost::enable_shared_from_this<SCCall>
{
    typedef SCCall self_type;
private:

    bool Connected;
    ISCController *				            m_pController;	

    //boost::recursive_mutex CallMutex;
    boost::mutex CallMutex;
    boost::shared_ptr<SCConfigReader>       m_configReaderPtr;
    log4cpp::Category * 					m_pLog4cpp;
    
    SCCDRLogger * m_pCDRLogger;
    int IVRType;
    int Channel;                            // channel number where the call is active
    int Device;
    KChannel KhompChannel;

    std::string ANI;                            // origin number of the call
    std::string DNIS;                           // destination number of the call
    std::string CallId;                         // call unique id
    unsigned int InternalId;                // internal identification for the call
    volatile bool ReceivedDisconnect;                // flag to indicate if the call received a disconnection, before sending it
    volatile bool HungUp;                            // flag to indicate if the call started the disconnection
    bool IsFree;

    bool PlayingFile;                       // indicate whether a play is beeing executed or not
    bool SyncPlaying;                       // indicate whether the play is synchronous or asynchronous
    std::string PCMFormat;                      // format to be used on raw playing
    std::string PlayFormat;                     // format to be used by the Play function
    
    std::string RecordFormat;                   // format to be used by the Record function
    int MaxRecordSilenceTime;               // maximum amount of silence that should be expected on a recording
    bool BeepRecord;                        // wether a beep should be put at the begining of a call
    bool RecordingFile;                     // flag indicating wether the call is being recorded
    bool RecordStoppedBySilence;            // flag indicating wether the record was disconnected due to silence timeout
    ktools::KTimer RecordedTime;            // counter for the duration of the recording
    
    std::string ReceivedDigits;                 // digits detected during the call
    std::string DigitsRequested;                // string to return the amount of digits requested by the user
    int NumDigitsExpected;                  // amount of digits expeted before returning to the call
    int InterDigitTimeout;                  // max time expected between digits
    std::string DigitMask;                      // mask of digits that terminate a call
    bool EchoDigit;                         // flag indictating whether a beep should be sent after each digit received
    bool GettingDigits;

    //static SCConferenceManager& ConferenceManager;
    //SCConferenceManager& ConferenceManager;

    bool InConference;
    std::string ConferenceRoom;
    bool ListeningToConference;

    // Timers
    static TimerManager* Timers;            // class that manages the system timers
    KTimerId DigitTimer;                    // inter digit timeout timer
    KTimerId MaxRecordTimer;                // max record time
    KTimerId MaxRecordSilenceTimer;         // 
    
    // Semaphores are needed for the synchronous functions
    Semaphore PlaySemaphore;
    Semaphore DigitSemaphore;
    Semaphore RecordSemaphore;
    Semaphore ConnectSemaphore;

    std::string LogFilename;
    std::ofstream LogFile;

    void OnDigitDetected( char Digit );

    void StopPlaying( );
    void StopRecording( );

    void ClearTimers();
    void ReleaseSemaphores();
    void RemoveFromConferenceRoom();

	void ReleaseSemaphore( Semaphore& Sem );


public:
    SCCall( ISCController * pController , boost::shared_ptr<SCConfigReader> configReaderPtr, int device, int channel, char const* ANI, char const* DNIS, unsigned int InternalID );
    ~SCCall();

    
    //static boost::shared_ptr<SCCall> New( int Device, int Channel, char const* ANI, char const* DNIS, unsigned int InternalID );
    int GetDevice() const { return Device; }
    int GetChannel() const { return Channel; }
    int GetKhompDevice() const { return KhompChannel.GetDevice(); }
    int GetKhompChannel() const { return KhompChannel.GetChannel(); }
    
    std::string const& GetANI() const { return ANI; }
    std::string const& GetDNIS() const { return DNIS; }
    std::string const& GetCallId() const { return CallId; }
    unsigned int GetInternalId() const { return InternalId; }


    int Answer( bool DoubleAnswer, bool Billing );
    void Hangup();
    void Stop();
    bool WasDisconnected() { return ReceivedDisconnect || HungUp; }
    void DisconnectChannel();

    void SetPlayFormat( char const* Format );
    int  RemotePlayFile( char const* Filename );
    int  RemotePlayFileAsync( char const* Filename );
    
    void SetPCMFormat( char const* Format );
    
    void SetRecordFormat( char const* Format );
    int  RemoteRecordFile( char const* Filename, int MaxRecordTime, int MaxSilenceTime, bool Beep, char const* DigitMask );
    int  GetRecordedTime();
    bool RecordSilenceTimedOut() { return RecordStoppedBySilence; }
    
    char const* GetDigits( int NumDigits, int InterDigitTimeout, char const* DigitMask, bool Echo );
    int ClearDigits();
    void SetDigitMask( char const* Mask );

    int ConferenceAdd( char const* ConferenceId, int ConferenceSize, char const* MSISDN, int ConferenceType, bool Supervisor, bool Beep );
    int ConferenceRemove( char const* ConferenceId, int ConferenceType );
    int ConferenceListen( char const* ConferenceId );
    int ConferenceUnlisten( char const* ConferenceId );
    int ConferenceChangeStatus( char const* ConferenceId, SCConference::ConfereeStatus Status, int Unlisten = false );
    int GetNumOfUsers( char const* ConferenceId );

    bool ListenToChannel( int Device, int Channel );
    bool UnlistenChannel( int Device, int Channel );

    char const* GetConferenceId();

    void HandleEvent( K3L_EVENT* Event );

    // functions called when a timer expires
    void OnDigitTimeout();
    void OnMaxRecordTimeout();
    void OnMaxRecordSilenceTimeout();

    void SetCDRLogger(SCCDRLogger * pCDRLogger);
    void Log( char const* message, size_t length );
};


