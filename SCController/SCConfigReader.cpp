
#include "SCConfigReader.h"
#include "SCLogger.h"


//SCConfigReader& SCConfigReader::GetInstance()
//{
//    static SCConfigReader instance;
//    return instance;
//}

SCConfigReader::SCConfigReader(boost::filesystem::path execPath) 
    : Machine( 1 )
    , IVRType( 3 )
    , IVRSwitch( 0 )
    , IVRTimeslot( 0 )
    , IVRGlobalTimeslot( 0 )
    , ScriptingMode( UseInitScript )
    , CDRInterval( 120 )
    , ScriptDir( "D:\\VoiceGateway\\Scripts\\IVR" )
    , ScriptInitName( "init.lua" )
    , LogLuaDir( "D:\\VoiceGateway\\Logs" )
    , CDRDir( "D:\\VoiceGateway\\CDR" )
    , LocalIP( "" )
    , ConferenceClientAddress( "127.0.0.1" )
    , LogLevel( SCLogger::SCLOG_ERROR )
{
    std::string paramValue;
        
    std::string strCurDir;
    char sep = '\\';
    size_t i = execPath.string().rfind(sep, execPath.string().length());
    if (i != std::string::npos) 
        strCurDir = execPath.string().substr(0, i);
    
    boost::filesystem::path pDir(strCurDir);
    boost::filesystem::path pFileName("SCController.conf");
    boost::filesystem::path fullPath = pDir / pFileName;

    //std::ifstream configFile( "D:\\VoiceGateway\\hmp.conf" );
    std::ifstream configFile( fullPath.string() );
    if( configFile.is_open() )
    {
        while( !configFile.eof() )
        {
            std::string line;
            std::getline( configFile, line );

            std::vector<std::string> strs;
            boost::split(strs,line,boost::is_any_of("="));
            if ( (line.size() > 0 ) &&  (strs.size() > 0) )
            {
                std::vector<std::string>::iterator it = strs.begin();
                std::string paramName = boost::algorithm::trim_copy(*it);
                it++;
                
                //if( paramName.compare( "machine" ) == 0 )
                if ( boost::iequals(paramName,"machine") )
                {   
                    paramValue = boost::algorithm::trim_copy(*it);
                    Machine = atoi( paramValue.c_str());
                }
                //else if( paramName.compare( "ivrtype" ) == 0 )
                else if ( boost::iequals(paramName,"ivrtype") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    IVRType = atoi( paramValue.c_str() );
                }
                //else if( paramName.compare( "scriptdir" ) == 0 )
                else if ( boost::iequals(paramName,"scriptdir") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    ScriptDir = paramValue;
                }
                //else if( paramName.compare( "scriptinitname" ) == 0 )
                else if ( boost::iequals(paramName,"scriptinitname") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    ScriptInitName = paramValue;
                }
                //else if( paramName.compare( "LogLuaDir" ) == 0 )
                else if ( boost::iequals(paramName,"LogLuaDir") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    LogLuaDir = paramValue;
                }
                //else if( paramName.compare( "cdrdir" ) == 0 )
                else if ( boost::iequals(paramName,"cdrdir") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    CDRDir = paramValue;
                }
                //else if ( paramName.compare( "ivrswitch" ) == 0 )
                else if ( boost::iequals(paramName,"ivrswitch") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    IVRSwitch = atoi( paramValue.c_str() );
                }
                //else if( paramName.compare( "ivrtimeslot" ) == 0 )
                else if ( boost::iequals(paramName,"ivrtimeslot") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    IVRTimeslot = atoi( paramValue.c_str() );
                }
                //else if( paramName.compare( "ivrglobaltimeslot" ) == 0 )
                else if ( boost::iequals(paramName,"ivrglobaltimeslot") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    IVRGlobalTimeslot = atoi( paramValue.c_str() );
                }
                //else if( paramName.compare( "scriptingmode" ) == 0 )
                else if ( boost::iequals(paramName,"scriptingmode") )
                {
                    ScriptingMode = static_cast<SCScriptingMode>( atoi( (*it).c_str() ) );
                }
                //else if( paramName.compare( "localip" ) == 0 )
                else if ( boost::iequals(paramName,"localip") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    LocalIP = paramValue;
                }
                //else if( paramName.compare( "cdrinterval" ) == 0 )
                else if ( boost::iequals(paramName,"cdrinterval") )
                {
                    CDRInterval = static_cast<uint32>( atoi( (*it).c_str() ) ); 
                }
                //else if( paramName.compare( "conferenceclientaddress" ) == 0 )
                else if ( boost::iequals(paramName,"conferenceclientaddress") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    ConferenceClientAddress = paramValue;
                }
                //else if( paramName.compare( "loglevel" ) == 0 )
                else if ( boost::iequals(paramName,"loglevel") )
                {
                    paramValue = boost::algorithm::trim_copy(*it);
                    LogLevel = atoi( paramValue.c_str() );
                }
            }

        }
    }
}

SCConfigReader::SCConfigReader()
    : Machine( 1 )
    , IVRType( 3 )
    , IVRSwitch( 0 )
    , IVRTimeslot( 0 )
    , IVRGlobalTimeslot( 0 )
    , ScriptingMode( UseInitScript )
    , CDRInterval( 120 )
    , ScriptDir( "D:\\VoiceGateway\\Scripts\\IVR" )
    , ScriptInitName( "init.lua" )
    , LogLuaDir( "D:\\VoiceGateway\\Logs" )
    , CDRDir( "D:\\VoiceGateway\\CDR" )
    , LocalIP( "" )
    , ConferenceClientAddress( "127.0.0.1" )
    , LogLevel( SCLogger::SCLOG_ERROR )
{
    
}

int SCConfigReader::GetMachine() const
{
    
    return Machine;
}

int SCConfigReader::GetIVRType()
{
    
    return IVRType;
}

int SCConfigReader::GetIVRSwitch()
{
    
    return IVRSwitch;
}

int SCConfigReader::GetIVRTimeslot()
{
    
    return IVRTimeslot;
}

int SCConfigReader::GetIVRGlobalTimeslot()
{
    return IVRGlobalTimeslot;
}

SCScriptingMode SCConfigReader::GetScriptingMode()
{
    return ScriptingMode;
}

std::string const& SCConfigReader::GetScriptDir()
{
    return ScriptDir;
}

std::string const& SCConfigReader::GetScriptInit()
{
    return ScriptInitName;
}

std::string const& SCConfigReader::GetLogLuaDir()
{
    return LogLuaDir;
}

std::string const& SCConfigReader::GetCDRDir()
{
    return CDRDir;
}

std::string const& SCConfigReader::GetLocalIP()
{
    return LocalIP;
}

uint32 SCConfigReader::GetCDRInterval()
{
    return CDRInterval;
}

std::string const& SCConfigReader::GetConferenceClientAddress()
{
    return ConferenceClientAddress;
}

int SCConfigReader::GetLogLevel()
{
    return LogLevel;
}