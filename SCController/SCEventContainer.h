
#ifndef _INCLUDED_KHOMP_SCEVENTTHREAD_H_
#define _INCLUDED_KHOMP_SCEVENTTHREAD_H_

#include <KD3/Basics/KThread.h>
#include <KList.h>

struct K3L_EVENT;
class SCCall;

class SCEventThread : public ktools::KThread
{
    KList EventList;

    void Execute();
    
public:

    SCEventThread( );
    ~SCEventThread( );

    void AddEvent( K3L_EVENT* Event, SCCall* EventCall );
};

#endif