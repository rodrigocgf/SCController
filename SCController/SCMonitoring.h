#pragma once

#include "SCStdIncludes.h"
#include "SCLogger.h"
#include "SCController.h"

using namespace boost::asio;
using namespace boost::posix_time;

class SCMonitoring : public boost::enable_shared_from_this< SCMonitoring > , boost::noncopyable
{
public:
	typedef SCMonitoring self_type;

	SCMonitoring(io_service& Service);
	~SCMonitoring(void);

    boost::mutex    m_BufferMutex;
    char m_InputBuffer[2];
    char m_ReadBuffer[15];
    int  m_ReadIndex;
	ip::tcp::socket ReadSocket;

    streambuf ReadBuffer;
    streambuf WriteBuffer;
	bool Started;
	//SCLogger Logger;
    log4cpp::Category * 					m_pLog4cpp;

	ip::tcp::socket& GetSocket();
	void Start( );
	void Stop();

	typedef boost::system::error_code error_code;

private:
    long				            m_Timeout;
	boost::asio::deadline_timer     m_timer;
    bool                            m_loggedOn;
    int                             m_MaxChannels;
    TSTATE                          m_TelnetState;
    char                            m_optionCmd;
    char                            m_option;
    char                            m_subNegOption;
    //int                             m_NumOptionBytes;
    
    void TelnetProtocol(char cmdChar);
    void SendInitialBanner();
    void HandleWait(const boost::system::error_code& error);
	void OnRead( error_code const& Error, size_t Bytes );
	void DoRead();
    void ProcessInput(char cmd);
    void ParseInput(char * buffer);
    void ParseCommand(const char * cmd);

	void DoWrite( std::string const& Message );
	void DoWrite( std::vector<std::string> const& Messages );
	void OnWrite( error_code const& Error, size_t bytes );

    void SendHelpMenu();
    void SendInitialOptions();
    void SendPostPassOptions();
    void ProcessChannelCount();
    void ProcessChannelStatus(int firtChannel, int lastChannel);
    void ProcessChannelDisconection(int firtChannel, int lastChannel);
    void ProcessChannelBlocking(int firtChannel, int lastChannel);
    void ProcessChannelUnblocking(int firtChannel, int lastChannel);
    void ProcessFindChannel(std::string ani);
};

