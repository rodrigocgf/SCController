
#pragma once

#include "SCStdIncludes.h"
#include "SCLogger.h"
#include "ISCController.h"
#include "SCChannel.h"


class SCChannelAllocator
{
private:    
    boost::mutex QueueMutex;

	std::deque<SCChannel> FreeChannels;
    std::map< SCChannel, CHANNEL_STATUS > m_ChannelsStatusMap;

	ISCController * m_pController;
    //boost::shared_ptr<SCLogger> LoggerPtr;
    log4cpp::Category * 					m_pLog4cpp;
	int m_MaxChannels;

public:
    
    SCChannelAllocator(ISCController * controller);
    SCChannelAllocator();
	~SCChannelAllocator();
    SCChannelAllocator( SCChannelAllocator const& );
    SCChannelAllocator& operator=( SCChannelAllocator const& );

    static SCChannelAllocator& GetInstance()
    {
        static SCChannelAllocator myAllocator;
        return myAllocator;
    }

    void ReleaseChannel( SCChannel Channel );
    SCChannel AllocateChannel( );
    void InvalidateChannel( SCChannel const& Channel );

	int GetTotalChannels();
	int GetFreeChannels();
	int GetBusyChannels();
    int GetBlockedChannels();

    CHANNEL_STATUS GetChannelStatus(int channel);
    void SetChannelStatus(int channel , CHANNEL_STATUS status);
	void SetMaxChannels(int value);

    std::string GetChannelANI(int channel);
    void DisconnectChannel(int channel);
};


class AllocatorQueueEmptyException : public std::exception
{
    virtual char const* what() const throw()
    {
        return "There is no channel available for allocation!";
    }
};